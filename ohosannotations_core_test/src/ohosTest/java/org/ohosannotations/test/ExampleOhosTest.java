package org.ohosannotations.test;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Ohos示例测试
 *
 * @since 2021-07-19
 */
public class ExampleOhosTest {
    /**
     * 开始测试
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("org.ohosannotations.test", actualBundleName);
    }
}