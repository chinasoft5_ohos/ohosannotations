package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.Fullscreen;

/**
 * 满屏显示
 *
 * @since 2021-07-19
 */
@EAbility
@Fullscreen
public class FullscreenAbility extends Ability {
}
