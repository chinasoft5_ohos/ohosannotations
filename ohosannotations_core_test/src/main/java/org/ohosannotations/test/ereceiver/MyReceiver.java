/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.ereceiver;

import ohos.event.commonevent.CommonEventData;
import ohos.event.commonevent.CommonEventSubscribeInfo;
import ohos.event.commonevent.CommonEventSubscriber;

import com.chinasoft_ohos.commontools.toast.Toast;

import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EReceiver;
import org.ohosannotations.annotations.Trace;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.test.ebean.EnhancedClass;
import org.ohosannotations.test.eservice.MyService;

import timber.log.Timber;

/**
 * 广播接受者
 *
 * @since 2021-06-09
 */
@EReceiver
public class MyReceiver extends CommonEventSubscriber {
    @Bean
    EnhancedClass dependency;

    /**
     * 构造参数
     *
     * @param subscribeInfo 订阅信息
     */
    public MyReceiver(CommonEventSubscribeInfo subscribeInfo) {
        super(subscribeInfo);
    }

    @Override
    public void onReceiveEvent(CommonEventData commonEventData) {
        showToast();
        workInBackground();
    }

    @Trace
    @UiThread
    void showToast() {
        Toast.show("Hello World!");
    }

    @Trace
    @Background
    void workInBackground() {
        Timber.d(MyService.class.getSimpleName() + "Doing some background work.");
    }
}
