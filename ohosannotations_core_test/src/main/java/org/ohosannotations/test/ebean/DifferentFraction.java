/**
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.ebean;

import ohos.aafwk.ability.fraction.Fraction;

import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EFraction;

/**
 * 不同的Fraction
 *
 * @since 2021-07-20
 */
@EFraction
public class DifferentFraction extends Fraction {
    @Bean
    AbilityScopedBean abilityScopedBean;

    @Bean
    FractionScopedBean fractionScopedBean;
}
