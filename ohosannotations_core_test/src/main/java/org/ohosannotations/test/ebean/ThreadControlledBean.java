/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.ebean;

import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.EBean;
import org.ohosannotations.annotations.SupposeBackground;
import org.ohosannotations.annotations.SupposeUiThread;
import org.ohosannotations.annotations.UiThread;

/**
 * ThreadControlledBean
 *
 * @since 2021-07-20
 */
@EBean
public class ThreadControlledBean {
    /**
     * serial1
     */
    public static final String SERIAL1 = "serial1";
    /**
     * serial2
     */
    public static final String SERIAL2 = "serial2";

    /**
     * uiSupposed
     */
    @SupposeUiThread
    public void uiSupposed() {
    }

    /**
     * backgroundSupposed
     */
    @SupposeBackground
    public void backgroundSupposed() {
    }

    /**
     * serialBackgroundSupposed
     */
    @SupposeBackground(serial = {SERIAL1, SERIAL2})
    public void serialBackgroundSupposed() {
    }

    /**
     * uiSupposedAndUi
     *
     * @param delegate 可运行
     */
    @SupposeUiThread
    @UiThread
    public void uiSupposedAndUi(Runnable delegate) {
        delegate.run();
    }

    /**
     * backgroundSupposeAndBackground
     *
     * @param delegate 可运行调试
     */
    @SupposeBackground(serial = SERIAL1)
    @Background(serial = SERIAL2)
    public void backgroundSupposeAndBackground(Runnable delegate) {
        delegate.run();
    }
}
