/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.ebean;

import ohos.aafwk.ability.Ability;

import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EAbility;

/**
 * BeanInjectedAbility
 *
 * @since 2021-07-20
 */
@EAbility
public class BeanInjectedAbility extends Ability {
    /**
     * EmptyDependency
     */
    @Bean
    public EmptyDependency dependency;
    /**
     * SomeInterface
     */
    @Bean(SomeImplementation.class)
    public SomeInterface interfaceDependency;
    /**
     * SomeSingleton
     */
    @Bean
    public SomeSingleton singletonDependency;
    /**
     * AbilityScopedBean
     */
    @Bean
    public AbilityScopedBean abilityScopedBean;
    /**
     * FractionScopedBean
     */
    @Bean
    public FractionScopedBean fractionScopedDependency;
    /**
     * EmptyDependency
     */
    public EmptyDependency methodInjectedDependency;
    /**
     * SomeInterface
     */
    public SomeInterface methodInjectedInterface;
    /**
     * SomeSingleton
     */
    public SomeSingleton methodInjectedSingleton;
    /**
     * AbilityScopedBean
     */
    public AbilityScopedBean methodInjectedAbilityScoped;
    /**
     * FractionScopedBean
     */
    public FractionScopedBean methodInjectedFractionScoped;
    /**
     * EmptyDependency
     */
    public EmptyDependency annotatedParamDependency;
    /**
     * SomeInterface
     */
    public SomeInterface annotatedParamInterface;
    /**
     * SomeSingleton
     */
    public SomeSingleton annotatedParamSingleton;
    /**
     * AbilityScopedBean
     */
    public AbilityScopedBean annotatedParamAbilityScoped;
    /**
     * FractionScopedBean
     */
    public FractionScopedBean annotatedParamFractionScoped;
    /**
     * EmptyDependency
     */
    public EmptyDependency multiDependency;
    /**
     * SomeInterface
     */
    public SomeInterface multiDependencyInterface;
    /**
     * SomeSingleton
     */
    public SomeSingleton multiDependencySingleton;
    /**
     * AbilityScopedBean
     */
    public AbilityScopedBean multiDependencyAbilityScopedBean;
    /**
     * FractionScopedBean
     */
    public FractionScopedBean multiDependencyFractionScopedBean;

    /**
     * injectDependency
     *
     * @param methodInjectedDependency 空依赖
     */
    @Bean
    protected void injectDependency(EmptyDependency methodInjectedDependency) {
        this.methodInjectedDependency = methodInjectedDependency;
    }

    /**
     * injectInterface
     *
     * @param methodInjectedInterface 注解接口
     */
    @Bean(SomeImplementation.class)
    protected void injectInterface(SomeInterface methodInjectedInterface) {
        this.methodInjectedInterface = methodInjectedInterface;
    }

    /**
     * injectSingleton
     *
     * @param methodInjectedSingleton methodInjectedSingleton
     */
    @Bean
    protected void injectSingleton(SomeSingleton methodInjectedSingleton) {
        this.methodInjectedSingleton = methodInjectedSingleton;
    }

    /**
     * injectAbilityScope
     *
     * @param methodInjectedAbilityScoped methodInjectedAbilityScoped
     */
    @Bean
    protected void injectAbilityScope(AbilityScopedBean methodInjectedAbilityScoped) {
        this.methodInjectedAbilityScoped = methodInjectedAbilityScoped;
    }

    /**
     * injectFractionScope
     *
     * @param methodInjectedFractionScoped methodInjectedFractionScoped
     */
    @Bean
    protected void injectFractionScope(FractionScopedBean methodInjectedFractionScoped) {
        this.methodInjectedFractionScoped = methodInjectedFractionScoped;
    }

    /**
     * injectDependencyAnnotatedParam
     *
     * @param annotatedParamDependency annotatedParamDependency
     */
    protected void injectDependencyAnnotatedParam(@Bean EmptyDependency annotatedParamDependency) {
        this.annotatedParamDependency = annotatedParamDependency;
    }

    /**
     * injectInterfaceAnnotatedParam
     *
     * @param annotatedParamInterface annotatedParamInterface
     */
    protected void injectInterfaceAnnotatedParam(@Bean(SomeImplementation
        .class) SomeInterface annotatedParamInterface) {
        this.annotatedParamInterface = annotatedParamInterface;
    }

    /**
     * injectSingletonAnnotatedParam
     *
     * @param annotatedParamSingleton annotatedParamSingleton
     */
    protected void injectSingletonAnnotatedParam(@Bean SomeSingleton annotatedParamSingleton) {
        this.annotatedParamSingleton = annotatedParamSingleton;
    }

    /**
     * injectAbilityScopedAnnotatedParam
     *
     * @param annotatedParamAbilityScoped annotatedParamAbilityScoped
     */
    protected void injectAbilityScopedAnnotatedParam(@Bean AbilityScopedBean annotatedParamAbilityScoped) {
        this.annotatedParamAbilityScoped = annotatedParamAbilityScoped;
    }

    /**
     * injectFractionScopedAnnotatedParam
     *
     * @param annotatedParamFractionScoped annotatedParamFractionScoped
     */
    protected void injectFractionScopedAnnotatedParam(@Bean FractionScopedBean annotatedParamFractionScoped) {
        this.annotatedParamFractionScoped = annotatedParamFractionScoped;
    }

    /**
     * injectMultipleDependencies
     *
     * @param multiDependency multiDependency
     * @param multiDependencyInterface multiDependencyInterface
     * @param multiDependencySingleton multiDependencySingleton
     * @param multiDependencyAbilityScopedBean multiDependencyAbilityScopedBean
     * @param multiDependecyFractionScopedBean multiDependecyFractionScopedBean
     */
    protected void injectMultipleDependencies(
        @Bean EmptyDependency multiDependency,
        @Bean(SomeImplementation.class) SomeInterface multiDependencyInterface,
        @Bean SomeSingleton multiDependencySingleton,
        @Bean AbilityScopedBean multiDependencyAbilityScopedBean,
        @Bean FractionScopedBean multiDependecyFractionScopedBean) {
        this.multiDependency = multiDependency;
        this.multiDependencyInterface = multiDependencyInterface;
        this.multiDependencySingleton = multiDependencySingleton;
        this.multiDependencyAbilityScopedBean = multiDependencyAbilityScopedBean;
        this.multiDependencyFractionScopedBean = multiDependecyFractionScopedBean;
    }
}
