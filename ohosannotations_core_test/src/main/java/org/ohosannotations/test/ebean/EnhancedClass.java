/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.ebean;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.app.IAbilityManager;
import ohos.data.rdb.RdbStore;

import org.ohosannotations.annotations.AfterInject;
import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.Click;
import org.ohosannotations.annotations.EBean;
import org.ohosannotations.annotations.RootContext;
import org.ohosannotations.annotations.SystemService;
import org.ohosannotations.annotations.Trace;
import org.ohosannotations.annotations.Transactional;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.res.StringRes;
import org.ohosannotations.test.ThreadAbility;

/**
 * 增强的类
 *
 * @author dev
 * @since 2021-06-07
 */
@EBean
public class EnhancedClass {
    @Bean
    SecondDependency secondDependency;

    @RootContext
    Ability ability;

    @RootContext
    Context context;

    @RootContext
    Ability service;

    @RootContext
    ThreadAbility threadAbility;

    @ComponentById
    Text myTextView;
    @StringRes
    String hello;
    @SystemService
    IAbilityManager abilityManager;

    @Click
    void myButton() {
    }

    @UiThread
    void uiThread() {
    }

    @UiThread(delay = 2000)
    @Trace
    void uiThreadDelayed() {
    }

    @Background
    @Trace
    void background() {
    }

    @Transactional
    void successfulTransaction(RdbStore db) {
        db.executeSql("Some SQL");
    }

    @Transactional
    void rollbackedTransaction(RdbStore db) {
        throw new IllegalArgumentException();
    }

    @AfterInject
    void calledAfterInjection() {
    }
}
