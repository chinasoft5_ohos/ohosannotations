/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.ebean;

import ohos.app.Context;

import org.ohosannotations.annotations.EBean;
import org.ohosannotations.annotations.RootContext;

/**
 * SomeBean
 *
 * @since 2021-06-07
 */
@EBean
public class SomeBean {
    /**
     * Context
     */
    @RootContext
    public Context context;

    Context methodInjectedContext;
    Context multiInjectedContext;

    @RootContext
    void methodInjectedContext(Context methodInjectedContext) {
        this.methodInjectedContext = methodInjectedContext;
    }

    void multiInjectedContext(@RootContext Context multiInjectedContext, @RootContext Context multiInjectedContext2) {
        this.multiInjectedContext = multiInjectedContext;
    }
}
