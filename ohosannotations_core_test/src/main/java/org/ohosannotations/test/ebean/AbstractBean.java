/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.ebean;

import ohos.app.Context;

import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.EBean;
import org.ohosannotations.annotations.RootContext;

/**
 * AbstractBean
 *
 * @since 2021-07-20
 */
@EBean
public abstract class AbstractBean {
    @RootContext
    Context context;

    /**
     * 构造参数
     *
     * @param param 字符串参数
     */
    public AbstractBean(String param) {
    }

    @Background
    void backgroundMethod() {
    }
}
