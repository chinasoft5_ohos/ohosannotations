/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.PageSlider;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.PageScrollStateChanged;
import org.ohosannotations.annotations.PageScrolled;
import org.ohosannotations.annotations.PageSelected;

/**
 * PageChangeListenerAbility
 *
 * @since 2021-06-16
 */
@EAbility(ResourceTable.Layout_viewpagers)
public class PageChangeListenerAbility extends Ability {
    /**
     * 视图寻呼机
     */
    public PageSlider viewPager = null;
    /**
     * 位置
     */
    public int position = 0;
    /**
     * 位置偏移量
     */
    public float positionOffset = 0f;
    /**
     * 像素位置偏移
     */
    public int positionOffsetPixels = 0;

    /**
     * 状态
     */
    public int state = 0;

    /**
     * 页面滚动
     *
     * @param slider slider
     * @param p1 p1
     * @param offset 抵消
     * @param offsetPixels 抵消像素
     */
    @PageScrolled(ResourceTable.Id_viewPager1)
    public void pageScrolled(PageSlider slider, int p1, float offset, int offsetPixels) {
        viewPager = slider;
        position = p1;
        positionOffset = offset;
        positionOffsetPixels = offsetPixels;
    }

    /**
     * 页面滚动状态改变
     *
     * @param slider slider
     * @param s1 年代
     */
    @PageScrollStateChanged(ResourceTable.Id_viewPager1)
    public void pageScrollStateChanged(PageSlider slider, int s1) {
        viewPager = slider;
        state = s1;
    }

    /**
     * 页面选择
     *
     * @param slider slider
     * @param p1 p1
     */
    @PageSelected(ResourceTable.Id_viewPager1)
    public void pageSelected(PageSlider slider, int p1) {
        viewPager = slider;
        position = p1;
    }

    /**
     * 页面selected2
     */
    @PageSelected(ResourceTable.Id_viewPager2)
    public void pageSelected2() {
    }

    /**
     * 页面scrolled2
     */
    @PageScrolled(ResourceTable.Id_viewPager2)
    public void pageScrolled2() {
    }

    /**
     * 页面滚动状态changed2
     */
    @PageScrollStateChanged(ResourceTable.Id_viewPager2)
    public void pageScrollStateChanged2() {
    }

    /**
     * pageScrollStateChanged3
     *
     * @param s1 s
     * @param v1 v
     */
    @PageScrollStateChanged(ResourceTable.Id_viewPager3)
    public void pageScrollStateChanged3(int s1, PageSlider v1) {
        viewPager = v1;
        state = s1;
    }

    /**
     * pageScrollStateChanged3
     *
     * @param p1 p
     * @param v1 v
     */
    @PageSelected(ResourceTable.Id_viewPager3)
    public void pageSelected3(int p1, PageSlider v1) {
        viewPager = v1;
        position = p1;
    }

    /**
     * pageScrollStateChanged3
     *
     * @param p1 p
     */
    @PageScrolled(ResourceTable.Id_viewPager3)
    public void pageScrolled3(int p1) {
        position = p1;
    }

    /**
     * pageScrollStateChanged3
     *
     * @param p1 p
     * @param f1 f
     */
    @PageScrolled(ResourceTable.Id_viewPager4)
    public void pageScrolled4(float f1, int p1) {
        positionOffset = f1;
        positionOffsetPixels = p1;
    }
}

