/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.test;

import ohos.agp.components.Button;
import ohos.agp.components.Component;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.LongClick;

/**
 * LongClicksHandledAbility
 *
 * @since 2021-06-16
 */
@EAbility(ResourceTable.Layout_clickable_widgets)
public class LongClicksHandledAbility extends EventsHandledAbstractAbility {
    /**
     * onLongClick
     *
     * @param com 视图
     */
    @LongClick(ResourceTable.Id_stackOverflowProofButton)
    public void onLongClick(Component com) {
        avoidStackOverflowEventHandled = true;
    }

    /**
     * conventionButton
     */
    @LongClick
    public void conventionButton() {
        conventionButtonEventHandled = true;
    }

    /**
     * buttonWithButtonArgument
     *
     * @param button 按钮
     */
    @LongClick
    public void buttonWithButtonArgument(Button button) {
        viewArgument = button;
    }

    /**
     * snakeCaseButton
     */
    @LongClick
    public void snakeCaseButton() {
        snakeCaseButtonEventHandled = true;
    }

    /**
     * extendedConventionButtonLongClicked
     */
    @LongClick
    public void extendedConventionButtonLongClicked() {
        extendedConventionButtonEventHandled = true;
    }

    /**
     * overridenConventionButton
     */
    @LongClick(ResourceTable.Id_configurationOverConventionButton)
    public void overridenConventionButton() {
        overridenConventionButtonEventHandled = true;
    }

    /**
     * unboundButton
     */
    public void unboundButton() {
        unboundButtonEventHandled = true;
    }

    /**
     * buttonWithViewArgument
     *
     * @param viewArgument 视图
     */
    @LongClick
    public void buttonWithViewArgument(Component viewArgument) {
        this.viewArgument = viewArgument;
    }

    /**
     * multipleButtonWithViewArgument
     *
     * @param viewArgument 视图
     */
    @LongClick({ResourceTable.Id_button1, ResourceTable.Id_button2})
    public void multipleButtonWithViewArgument(Component viewArgument) {
        this.viewArgument = viewArgument;
        multipleButtonsEventHandled = true;
    }
}
