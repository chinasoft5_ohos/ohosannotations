/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.agp.components.AbsButton;
import ohos.agp.components.Checkbox;

import org.ohosannotations.annotations.CheckedChange;
import org.ohosannotations.annotations.EAbility;

/**
 * 变更处理的Ability
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_checkable_widgets)
public class CheckedChangeHandledAbility extends EventsHandledAbstractAbility {
    /**
     * 公共按钮
     *
     * @param evt 按钮
     * @param checked 是否选中
     */
    @CheckedChange
    public void conventionButton(AbsButton evt, boolean checked) {
        conventionButtonEventHandled = true;
    }

    /**
     * 复选框
     *
     * @param evt 复选框
     * @param checked 是否被选中
     */
    @CheckedChange
    public void checkBox(Checkbox evt, boolean checked) {
    }

    /**
     * 驼峰按钮
     *
     * @param checked 是否被选中
     * @param evt 按钮
     */
    @CheckedChange
    public void snakeCaseButton(boolean checked, AbsButton evt) {
        snakeCaseButtonEventHandled = true;
    }

    /**
     * 扩展公共按钮
     *
     * @param evt 按钮
     */
    @CheckedChange
    public void extendedConventionButton(AbsButton evt) {
        extendedConventionButtonEventHandled = true;
    }

    /**
     * 重写公共按钮
     */
    @CheckedChange(ResourceTable.Id_configurationOverConventionButton)
    public void overridenConventionButton() {
        overridenConventionButtonEventHandled = true;
    }

    /**
     * 带视图参数的按钮
     *
     * @param checked 是否被选中
     */
    @CheckedChange
    public void buttonWithViewArgument(boolean checked) {
    }

    /**
     * 多个按钮与视图参数
     *
     * @param button 按钮
     * @param checked 是否被选中
     */
    @CheckedChange({ResourceTable.Id_button1, ResourceTable.Id_button2})
    public void multipleButtonWithViewArgument(AbsButton button, boolean checked) {
    }
}
