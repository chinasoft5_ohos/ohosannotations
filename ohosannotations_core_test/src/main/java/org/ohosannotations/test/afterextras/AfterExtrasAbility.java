/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.afterextras;

import ohos.aafwk.ability.Ability;

import org.ohosannotations.annotations.AfterExtras;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.Extra;
import org.ohosannotations.test.ResourceTable;

/**
 * AfterExtrasAbility
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_ability_main)
public class AfterExtrasAbility extends Ability {
    /**
     * EXTRA_DATA_KEY
     */
    public static final String EXTRA_DATA_KEY = "EXTRA_DATA";
    /**
     * extraDataSet
     */
    @Extra(EXTRA_DATA_KEY)
    public boolean extraDataSet = false;
    /**
     * afterExtrasCalled
     */
    public boolean afterExtrasCalled = false;

    @AfterExtras
    void afterExtras() {
        afterExtrasCalled = true;
    }
}
