/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.agp.components.Button;
import ohos.agp.components.Component;

import org.ohosannotations.annotations.Click;
import org.ohosannotations.annotations.EAbility;

/**
 * 点击处理Ability
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_clickable_widgets)
public class ClicksHandledAbility extends EventsHandledAbstractAbility {
    /**
     * 点击事件
     *
     * @param component 视图
     */
    @Click(ResourceTable.Id_stackOverflowProofButton)
    public void onClick(Component component) {
        avoidStackOverflowEventHandled = true;
    }

    /**
     * 资源按钮
     */
    @Click(resName = {"libResButton1", "libResButton2"})
    public void libResButton() {
        libResButtonEventHandled = true;
    }

    /**
     * 公共按钮
     */
    @Click
    public void conventionButton() {
        conventionButtonEventHandled = true;
    }

    /**
     * 驼峰按钮
     */
    @Click
    public void snakeCaseButton() {
        snakeCaseButtonEventHandled = true;
    }

    /**
     * 扩展公共按钮点击事件
     */
    @Click
    public void extendedConventionButtonClicked() {
        extendedConventionButtonEventHandled = true;
    }

    /**
     * 重写公共按钮
     */
    @Click(ResourceTable.Id_configurationOverConventionButton)
    public void overridenConventionButton() {
        overridenConventionButtonEventHandled = true;
    }

    /**
     * 解绑按钮
     */
    public void unboundButton() {
        unboundButtonEventHandled = true;
    }

    /**
     * 带视图参数按钮点击事件
     *
     * @param viewArgument 视图
     */
    @Click
    public void buttonWithViewArgument(Component viewArgument) {
        this.viewArgument = viewArgument;
    }

    /**
     * 带参数按钮点击事件
     *
     * @param viewArgument 按钮
     */
    @Click
    public void buttonWithButtonArgument(Button viewArgument) {
        this.viewArgument = viewArgument;
    }

    /**
     * 多个按钮与视图参数
     *
     * @param viewArgument 视图
     */
    @Click({ResourceTable.Id_button1, ResourceTable.Id_button2})
    public void multipleButtonWithViewArgument(Component viewArgument) {
        this.viewArgument = viewArgument;
        multipleButtonsEventHandled = true;
    }
}
