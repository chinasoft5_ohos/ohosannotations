/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;


import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.ComponentsById;
import org.ohosannotations.annotations.EAbility;

import java.util.List;


/**
 * 组件注入能力
 *
 * @author dev
 * @since 2021-07-22
 */
@EAbility(ResourceTable.Layout_component_injected)
public class ComponentsInjectedAbility extends Ability {
    int counter;

    @ComponentById
    Button myButton;

    @ComponentById(ResourceTable.Id_my_text_view)
    Text someView;

    @ComponentById
    Text myTextView;

    @ComponentsById({ResourceTable.Id_my_text_view, ResourceTable.Id_myButton})
    List<Component> views;

    @ComponentsById({ResourceTable.Id_my_text_view, ResourceTable.Id_someView})
    List<Text> textViews;

    Text methodInjectedView;
    Text multiInjectedView;
    List<Text> methodInjectedViews;
    List<Component> multiInjectedViews;

    @AfterViews
    void incrementCounter() {
        counter++;
    }

    @ComponentById(ResourceTable.Id_my_text_view)
    void methodInjectedView(Text someView) {
        methodInjectedView = someView;
    }

    void multiInjectedView(@ComponentById Text someView,
        @ComponentById(ResourceTable.Id_my_text_view) Text abilityPrefs) {
        multiInjectedView = someView;
    }

    @ComponentsById({ResourceTable.Id_my_text_view, ResourceTable.Id_someView})
    void methodInjectedViews(List<Text> someView) {
        methodInjectedViews = someView;
    }

    void multiInjectedViews(@ComponentsById({ResourceTable.Id_someView,
        ResourceTable.Id_myButton}) List<Component> someView,
        @ComponentsById(ResourceTable.Id_my_text_view) List<Text> abilityPrefs) {
        multiInjectedViews = someView;
    }
}
