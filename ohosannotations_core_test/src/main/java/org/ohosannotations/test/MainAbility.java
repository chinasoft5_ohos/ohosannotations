package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import org.ohosannotations.test.slice.MainAbilitySlice;

/**
 * MainAbility
 *
 * @since 2021-07-19
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
