package org.ohosannotations.test;

import ohos.aafwk.ability.AbilityPackage;

import com.chinasoft_ohos.commontools.toast.Toast;
import com.chinasoft_ohos.commontools.util.LogTree;

import timber.log.Timber;

/**
 * 测试程序入口
 *
 * @since 2021-07-19
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        Toast.init(getContext()); // 吐司初始化
        Timber.plant(new LogTree.SystemOut());
    }
}
