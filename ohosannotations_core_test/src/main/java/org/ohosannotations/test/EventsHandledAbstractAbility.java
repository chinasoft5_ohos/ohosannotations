/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;


import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;

/**
 * 事件处理抽象Ability
 *
 * @since 2021-07-20
 */
public abstract class EventsHandledAbstractAbility extends Ability {
    /**
     * 视图
     */
    protected Component viewArgument;
    /**
     * 避免处理堆栈溢出事件
     */
    protected boolean avoidStackOverflowEventHandled;
    /**
     * 按钮事件处理
     */
    protected boolean conventionButtonEventHandled;
    /**
     * 迂回按钮事件处理
     */
    protected boolean snakeCaseButtonEventHandled;
    /**
     * 扩展约定按钮事件处理
     */
    protected boolean extendedConventionButtonEventHandled;
    /**
     * 覆盖按钮事件处理
     */
    protected boolean overridenConventionButtonEventHandled;
    /**
     * 释放按钮时间处理
     */
    protected boolean unboundButtonEventHandled;
    /**
     * 多种按钮事件释放
     */
    protected boolean multipleButtonsEventHandled;
    /**
     * 资源按钮事件处理
     */
    protected boolean libResButtonEventHandled;
}
