/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.bluetooth.BluetoothHost;
import ohos.net.NetManager;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SimInfoManager;
import ohos.usb.USBCore;


import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.SystemService;

/**
 * 与服务能力
 *
 * @author dev
 * @since 2021-07-22
 */
@EAbility
public class AbilityWithServices extends Ability {
    @SystemService
    RadioInfoManager radioInfoManager;

    @SystemService
    SimInfoManager simInfoManager;

    @SystemService
    NetManager netManager;

    @SystemService
    BluetoothHost bluetoothHost;

    @SystemService
    USBCore usbCore;


    RadioInfoManager injectRadioInfoManager;

    @SystemService
    void injectAppWidgetManager(RadioInfoManager radioInfoManager) {
        this.injectRadioInfoManager = radioInfoManager;
    }
}
