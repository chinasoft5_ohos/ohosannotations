/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TextField;
import ohos.multimodalinput.event.KeyEvent;

import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.EditorAction;

/**
 * EditorActionsHandledAbility
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_textviews_injected)
public class EditorActionsHandledAbility extends Ability {
    @ComponentById
    TextField editText1;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @EditorAction(ResourceTable.Id_editText1)
    void editorActionWithFullParameters(int actionId) {
    }

    @EditorAction(ResourceTable.Id_editText2)
    void editorActionInversedParameters(int actionId) {
    }

    @EditorAction(ResourceTable.Id_textView1)
    void editorActionNoParameters() {
    }

    @EditorAction(ResourceTable.Id_textView2)
    boolean editorActionWithBooleanReturned(int actionId) {
        return false;
    }
}
