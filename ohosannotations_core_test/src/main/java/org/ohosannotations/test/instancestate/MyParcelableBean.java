/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.instancestate;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * 包装Bean类
 *
 * @since 2021-07-20
 */
public class MyParcelableBean implements Sequenceable {
    private int xx;

    /**
     * 构造参数
     *
     * @param in 包装类
     */
    protected MyParcelableBean(Parcel in) {
        xx = in.readInt();
    }

    /**
     * 构造参数
     *
     * @param x1 整数类型
     */
    public MyParcelableBean(int x1) {
        this.xx = x1;
    }

    /**
     * 获取X
     *
     * @return int整数
     */
    public int getXx() {
        return xx;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + xx;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MyParcelableBean other = (MyParcelableBean) obj;
        if (xx != other.xx) {
            return false;
        }
        return true;
    }

    /**
     * 列集
     *
     * @param out 包装类
     * @return boolean布尔值
     */
    public boolean marshalling(Parcel out) {
        out.writeInt(xx);
        return true;
    }

    /**
     * unmarshalling
     *
     * @param in in
     * @return true
     */
    public boolean unmarshalling(Parcel in) {
        this.xx = in.readInt();
        return true;
    }

    /**
     * Sequenceable
     */
    public static final Sequenceable.Producer PRODUCER = new Sequenceable.Producer() {
        /**
         * createFromParcel
         *
         * @param in in
         * @return instance
         */
        public MyParcelableBean createFromParcel(Parcel in) {
            MyParcelableBean instance = new MyParcelableBean(in);
            instance.unmarshalling(in);
            return instance;
        }  // 必须实现Producer
    };
}
