/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.instancestate;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * 通用Parcelable Bean类
 *
 * @param <T> 泛型
 * @since 2021-07-20
 */
public class MyGenericParcelableBean<T> implements Sequenceable {
    /**
     * 可排序的生产者
     *
     * @since 2021-07-20
     */
    private Sequenceable.Producer producer = new Sequenceable.Producer() {
        /**
         * 创建从包裹
         *
         * @param in 在
         * @return {@link MyParcelableBean}
         */
        public MyParcelableBean createFromParcel(Parcel in) {
            MyParcelableBean instance = new MyParcelableBean(in);
            instance.unmarshalling(in);
            return instance; // 必须实现Producer
        }
    };
    private T tt;

    /**
     * 构造参数
     *
     * @param tt 泛型
     */
    public MyGenericParcelableBean(T tt) {
        this.tt = tt;
    }

    /**
     * 构造参数
     *
     * @param in 包装类
     */
    protected MyGenericParcelableBean(Parcel in) {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (tt == null ? 0 : tt.hashCode());
        return result;
    }

    /**
     * 编组
     *
     * @param out 输出包装类
     * @return boolean布尔值
     */
    public boolean marshalling(Parcel out) {
        return true;
    }

    /**
     * 解组
     *
     * @param in 输入包装类
     * @return boolean布尔值
     */
    public boolean unmarshalling(Parcel in) {
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MyGenericParcelableBean other = (MyGenericParcelableBean) obj;
        if (tt == null) {
            if (other.tt != null) {
                return false;
            }
        } else if (!tt.equals(other.tt)) {
            return false;
        }
        return true;
    }
}
