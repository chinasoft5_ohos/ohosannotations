/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.nonconfiguration;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.AbilityInfo;
import ohos.global.configuration.Configuration;
import ohos.utils.PacMap;

import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.NonConfigurationInstance;
import org.ohosannotations.test.ResourceTable;
import org.ohosannotations.test.ebean.EmptyDependency;
import org.ohosannotations.test.ebean.SomeImplementation;
import org.ohosannotations.test.ebean.SomeInterface;

import java.util.logging.Logger;

import timber.log.Timber;

/**
 * 非配置能力
 *
 * @author dev
 * @since 2021-07-26
 */
@EAbility(ResourceTable.Layout_ability_main)
public class NonConfigurationAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Logger.getGlobal().warning(maintainedDependency.toString());
        Logger.getGlobal().warning(recreatedDependency.toString());
        Logger.getGlobal().warning(maintainedAbstracted.toString());
        Logger.getGlobal().warning(someObject.toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        maintainedDependency = null;
        recreatedDependency = null;
        maintainedAbstracted = null;
        someObject = null;
    }

    @Bean
    @NonConfigurationInstance
    EmptyDependency maintainedDependency;

    @Bean
    EmptyDependency recreatedDependency;

    @Bean(SomeImplementation.class)
    @NonConfigurationInstance
    SomeInterface maintainedAbstracted;

    @NonConfigurationInstance
    Object someObject;

    @Override
    public void onSaveAbilityState(PacMap outState) {
        Timber.e("onSaveAbilityState");
        super.onSaveAbilityState(outState);
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        Timber.e("onRestoreAbilityState");
        super.onRestoreAbilityState(inState);
    }

    @Override
    public Object onStoreDataWhenConfigChange() {
        Timber.e("onStoreDataWhenConfigChange");
        return super.onStoreDataWhenConfigChange();
    }

    @Override
    public Object getLastStoredDataWhenConfigChanged() {
        Timber.e("getLastStoredDataWhenConfigChanged");
        return super.getLastStoredDataWhenConfigChanged();
    }

    @Override
    public void onConfigurationUpdated(Configuration configuration) {
        Timber.e("onConfigurationUpdated");
        super.onConfigurationUpdated(configuration);
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        Timber.e("onOrientationChanged");
        super.onOrientationChanged(displayOrientation);
    }
}
