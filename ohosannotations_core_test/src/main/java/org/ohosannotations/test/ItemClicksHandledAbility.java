/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.ItemClick;
import org.ohosannotations.annotations.ItemLongClick;
import org.ohosannotations.annotations.ItemSelect;

import java.util.ArrayList;
import java.util.List;

/**
 * 项点击处理能力
 *
 * @author dev
 * @since 2021-07-22
 */
@EAbility(ResourceTable.Layout_item_clicks_handled)
public class ItemClicksHandledAbility extends Ability {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");
    @ComponentById
    ListContainer listContainer;

    @ComponentById
    ListContainer listContainerWithArgument;

    @ComponentById
    ListContainer listContainerWithArgumentWithParameterType;

    @ComponentById
    ListContainer listViewWithPosition;

    @ComponentById
    ListContainer listViewWithOneParam;

    boolean isSpinnerItemClicked = false;
    boolean isListViewItemClicked = false;
    boolean isListViewParametrizedItemClicked = false;
    boolean isListViewWithPositionItemSelected;
    boolean isListViewWithOneParamItemSelected;

    int listViewWithPositionClickedPosition;
    int listViewWithPositionItemSelectedPosition;
    String listViewWithArgumentSelectedItem = null;

    private BaseItemProvider adapter;
    private BaseItemProvider parametrizedAdapter;

    /**
     * 初始化视图
     */
    @AfterViews
    void initView() {
        adapter = new ArrayProvider(getStringArray(ResourceTable.Strarray_planets_array),
            ResourceTable.Layout_simple_spinner_item, this);
        parametrizedAdapter = new ArrayProvider(stringLists(),
            ResourceTable.Layout_simple_spinner_item, this);

        listContainer.setItemProvider(adapter);
        listContainerWithArgument.setItemProvider(adapter);
        listContainerWithArgumentWithParameterType.setItemProvider(parametrizedAdapter);
        listViewWithPosition.setItemProvider(adapter);
        listViewWithOneParam.setItemProvider(adapter);
        isSpinnerItemClicked = false;
        isListViewItemClicked = false;
        isListViewWithPositionItemSelected = false;
        listViewWithPositionClickedPosition = 0;
        isListViewWithOneParamItemSelected = false;
        listViewWithPositionItemSelectedPosition = 0;
        HiLog.info(LABEL, ""
            + isSpinnerItemClicked + isListViewItemClicked + isListViewWithPositionItemSelected
            + listViewWithPositionClickedPosition
            + isListViewWithOneParamItemSelected + listViewWithPositionItemSelectedPosition);
    }

    /**
     * 列表容器
     */
    @ItemClick
    public void listContainer() {
        isListViewItemClicked = true;
    }

    /**
     * 与参数列表视图
     *
     * @param selectedItem 选择项
     */
    @ItemClick(ResourceTable.Id_listContainerWithArgument)
    public void listViewWithArgument(String selectedItem) {
        listViewWithArgumentSelectedItem = selectedItem;
        HiLog.info(LABEL, "" + listViewWithArgumentSelectedItem);
    }

    /**
     * 与参数类型列表视图与争论
     *
     * @param item 项
     */
    @ItemClick(ResourceTable.Id_listContainerWithArgumentWithParameterType)
    protected void listViewWithArgumentWithParameterType(ArrayList<String> item) {
        isListViewParametrizedItemClicked = true;
        HiLog.info(LABEL, "" + isListViewParametrizedItemClicked);
    }

    /**
     * 与一般的通配符类型列表视图与争论
     *
     * @param item 项
     */
    @ItemClick(ResourceTable.Id_listContainerWithArgumentWithGenericWildcard)
    protected void listViewWithArgumentWithGenericWildcardType(ArrayList<?> item) {
        isListViewParametrizedItemClicked = true;
        HiLog.info(LABEL, "" + isListViewParametrizedItemClicked);
    }

    /**
     * 列表视图的位置
     *
     * @param position 位置
     */
    @ItemClick
    void listViewWithPosition(int position) {
        listViewWithPositionClickedPosition = position;
    }

    /**
     * 列表视图与位置项选中
     *
     * @param selected 选择
     * @param position 位置
     */
    @ItemSelect
    void listViewWithPositionItemSelected(boolean selected, int position) {
        isListViewWithPositionItemSelected = selected;
        listViewWithPositionItemSelectedPosition = position;
    }

    /**
     * 与一个参数列表视图项选中
     *
     * @param selected 选择
     */
    @ItemSelect
    void listViewWithOneParamItemSelected(boolean selected) {
        isListViewWithOneParamItemSelected = selected;
    }

    /**
     * 在项目选择
     *
     * @param selected 选择
     * @param position 位置
     */
    @ItemSelect(ResourceTable.Id_listContainer)
    void onItemSelected(boolean selected, int position) {
    }

    /**
     * 项目长点击列表视图与地位
     *
     * @param position 位置
     */
    @ItemLongClick
    void listViewWithPositionItemLongClicked(int position) {
    }

    /**
     * 与参数类型长点击列表视图与争论
     *
     * @param item 项
     */
    @ItemLongClick(ResourceTable.Id_listContainerWithArgumentWithParameterType)
    protected void listViewWithArgumentWithParameterTypeLongClick(ArrayList<String> item) {
    }

    /**
     * 列表视图与一般的通配符类型长点击进行论证
     *
     * @param item 项
     */
    @ItemLongClick(ResourceTable.Id_listContainerWithArgumentWithGenericWildcard)
    protected void listViewWithArgumentWithGenericWildcardTypeLongClick(ArrayList<?> item) {
    }

    /**
     * 字符串列表
     *
     * @return {@link List<ArrayList<String>>}
     */
    private List<ArrayList<String>> stringLists() {
        List<ArrayList<String>> stringLists = new ArrayList<ArrayList<String>>();
        for (int i = 0; i < 10; i++) {
            ArrayList<String> stringList = new ArrayList<String>();
            for (int j = 0; j < 4; j++) {
                stringList.add(i + " : " + j);
            }
            stringLists.add(stringList);
        }
        return stringLists;
    }

    /**
     * ArrayProvider
     *
     * @since 2021-07-22
     */
    static class ArrayProvider extends BaseItemProvider {
        private String[] textArray;
        private List<ArrayList<String>> objects;
        private Ability ability;
        private Componentholder viewholder;
        private int resId;

        /**
         * 构造函数
         *
         * @param textArray textArray
         * @param ability ability
         */
        ArrayProvider(String[] textArray, int resId, Ability ability) {
            this.textArray = textArray;
            this.resId = resId;
            this.ability = ability;
        }

        /**
         * 构造函数
         *
         * @param objects objects
         * @param ability ability
         */
        ArrayProvider(List<ArrayList<String>> objects, int resId, Ability ability) {
            this.objects = objects;
            this.resId = resId;
            this.ability = ability;
        }

        @Override
        public int getCount() {
            int size = 0;
            if (textArray != null && textArray.length > 0) {
                size = textArray.length;
            } else if (objects != null && !objects.isEmpty()) {
                size = objects.size();
            }
            return size;
        }

        @Override
        public Object getItem(int position) {
            if (textArray != null) {
                return textArray[position];
            } else if (objects != null && !objects.isEmpty()) {
                return objects.get(position);
            } else {
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component convertComponent,
            ComponentContainer componentContainer) {
            if (null == convertComponent) {
                viewholder = new Componentholder();
                convertComponent = LayoutScatter.getInstance(ability)
                    .parse(resId, null, false);
                viewholder.mText = (Text) convertComponent.findComponentById(ResourceTable.Id_text1);
                convertComponent.setTag(viewholder);
            } else {
                viewholder = (Componentholder) convertComponent.getTag();
            }
            if (textArray != null && textArray.length > 0) {
                String text = textArray[position];
                viewholder.mText.setText(text);
            } else if (objects != null && !objects.isEmpty()) {
            } else {
            }

            return convertComponent;
        }

        /**
         * holder
         *
         * @since 2021-04-29
         */
        static class Componentholder {
            Text mText;
        }
    }
}
