/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.Extra;

import java.util.ArrayList;

/**
 * ExtraInjectedAbility
 *
 * @since 2021-07-20
 */
@EAbility
public class ExtraInjectedAbility extends Ability {
    @Extra("aStringExtra")
    String stringExtra;

    @Extra("arrayExtra")
    CustomData[] arrayExtra;

    @Extra("listExtra")
    ArrayList<String> listExtra;

    @Extra("intExtra")
    int intExtra;

    @Extra("byteArrayExtra")
    byte[] byteArrayExtra;

    @Extra
    String extraWithoutValue;

    @Extra
    void methodInjectedExtra(String methodInjectedExtra) {
    }

    void multiInjectedExtra(@Extra String multiInjectedExtra, @Extra String multiInjectedExtra2) {
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    void intentWithExtras() {
        ExtraInjectedAbility_.intent(this).arrayExtra(null).start();
        ExtraInjectedAbility_.intent(this).intExtra(42).get();
        ExtraInjectedAbility_.intent(this).stringExtra("hello").startForResult(42);
    }
}
