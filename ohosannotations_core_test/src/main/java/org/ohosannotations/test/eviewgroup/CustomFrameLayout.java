/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.eviewgroup;

import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.Click;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.EViewGroup;
import org.ohosannotations.annotations.LongClick;
import org.ohosannotations.annotations.Touch;
import org.ohosannotations.annotations.Trace;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.annotations.res.StringRes;
import org.ohosannotations.test.ResourceTable;

/**
 * 自定义布局
 *
 * @since 2021-06-04
 */
@EViewGroup(ResourceTable.Layout_component)
public class CustomFrameLayout extends StackLayout {
    /**
     * tv
     */
    @ComponentById(ResourceTable.Id_title)
    protected Text tv;

    /**
     * 副标题
     */
    @ComponentById
    protected Text subtitle;

    /**
     * res
     */
    @StringRes(ResourceTable.String_app_name)
    protected String res;

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param ii ii
     */
    public CustomFrameLayout(Context context, int ii) {
        super(context);
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param attrs attrs
     */
    public CustomFrameLayout(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * 之后的View
     */
    @Trace
    @AfterViews
    protected void afterViews() {
    }

    /**
     * 点击事件
     */
    @Click
    protected void title() {
    }

    /**
     * 长按事件
     */
    @LongClick(ResourceTable.Id_title)
    protected void titleLongClick() {
    }

    /**
     * 触摸事件
     *
     * @param event event
     */
    @Touch(ResourceTable.Id_title)
    protected void titleTouched(TouchEvent event) {
    }

    /**
     * 后台子线程
     */
    @Background
    protected void someBackgroundTask() {
    }

    /**
     * 主线程
     */
    @UiThread
    protected void someUiThreadTask() {
    }
}
