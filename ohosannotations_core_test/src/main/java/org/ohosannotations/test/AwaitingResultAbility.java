/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.utils.net.Uri;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.OnAbilityResult;

import java.util.ArrayList;

/**
 * @since 2021-06-04
 */
@EAbility(ResourceTable.Layout_component_injected)
public class AwaitingResultAbility extends EventsHandledAbstractAbility {
    static final int FIRST_REQUEST = 11;
    static final int SECOND_REQUEST = 22;
    static final int THIRD_REQUEST = 33;
    static final int FORTH_REQUEST = 44;
    static final int FIFTH_REQUEST = 55;
    static final int SIXTH_REQUEST = 66;
    boolean onResultCalled = false;
    boolean onResultWithDataCalled = false;
    boolean onAbilityResultWithResultCodeAndDataCalled = false;
    boolean onAbilityResultWithDataAndResultCodeCalled = false;
    boolean onResultWithIntResultCodeCalled = false;
    boolean onResultWithIntegerResultCodeCalled = false;
    boolean onResultWithResultExtraCodeCalled = false;
    Intent originalIntent;
    Intent extraIntent;

    @OnAbilityResult(FIRST_REQUEST)
    void onResult() {
        onResultCalled = true;
    }

    @OnAbilityResult(SECOND_REQUEST)
    void onResultWithData(Intent intentData) {
        onResultWithDataCalled = true;
    }

    @OnAbilityResult(SECOND_REQUEST)
    void onAbilityResultWithResultCodeAndData(int result, Intent intentData) {
        onAbilityResultWithResultCodeAndDataCalled = true;
    }

    @OnAbilityResult(SECOND_REQUEST)
    void onAbilityResultWithDataAndResultCode(Intent intentData, int result) {
        onAbilityResultWithDataAndResultCodeCalled = true;
    }

    @OnAbilityResult(THIRD_REQUEST)
    void onResultWithIntResultCode(int resultCode) {
        onResultWithIntResultCodeCalled = true;
    }

    @OnAbilityResult(THIRD_REQUEST)
    void onResultWithIntegerResultCode(Integer resultCodeInteger) {
        onResultWithIntegerResultCodeCalled = true;
    }

    // CHECKSTYLE:OFF

    @OnAbilityResult(FORTH_REQUEST)
    void onResultWithResultExtra(int resultCode, @OnAbilityResult.Extra("value") int i1,
        @OnAbilityResult.Extra String str, @OnAbilityResult.Extra Uri uri, @OnAbilityResult.Extra ArrayList<Uri> uris,
        @OnAbilityResult.Extra String[] strings) {
        onResultWithResultExtraCodeCalled = true;
    }

    // CHECKSTYLE:ON
    @OnAbilityResult(FIFTH_REQUEST)
    void onResultWithIntentExtras(Intent originalIntent, @OnAbilityResult.Extra Intent extraIntent) {
        this.originalIntent = originalIntent;
        this.extraIntent = extraIntent;
    }

    @OnAbilityResult(SIXTH_REQUEST)
    void onResultWithIntentExtras(@OnAbilityResult.Extra byte bytee, @OnAbilityResult.Extra byte[] bytes) {
    }
}
