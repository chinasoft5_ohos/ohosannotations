/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.res;

import ohos.aafwk.ability.Ability;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.res.PixelMapRes;
import org.ohosannotations.annotations.res.StringRes;
import org.ohosannotations.test.ResourceTable;

/**
 * 资源Ability
 *
 * @since 2021-06-04
 */
@EAbility(ResourceTable.Layout_ability_main)
public class ResAbility extends Ability {
    @PixelMapRes(ResourceTable.Media_icon)
    int icon;

    @StringRes(ResourceTable.String_hello)
    String injectedString1;

    @StringRes(ResourceTable.String_hello)
    String injectedString;
    String methodInjectedString;
    String multiInjectedString;
    int methodInjectedElement;
    int multiInjectedElement;

    @StringRes(ResourceTable.String_hello)
    void injectedString(String anythingWeWant) {
        methodInjectedString = anythingWeWant;
    }

    void stringResources(@StringRes(ResourceTable.String_hello) String injectedStr,
        @StringRes(ResourceTable.String_hello) String string) {
        multiInjectedString = injectedStr;
    }

    @PixelMapRes(ResourceTable.Media_icon)
    void icon(int anythingWeWant) {
        methodInjectedElement = anythingWeWant;
    }

    void drawableResources(@PixelMapRes(ResourceTable.Media_icon) int icon1,
        @PixelMapRes(ResourceTable.Media_icon) int resNameIcon) {
        multiInjectedElement = icon1;
    }
}
