/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.eintentservice;

import ohos.aafwk.ability.IntentAbility;
import ohos.aafwk.content.Intent;

import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EIntentService;
import org.ohosannotations.annotations.ServiceAction;
import org.ohosannotations.test.ebean.EnhancedClass;

/**
 * IntentService
 *
 * @since 2021-06-09
 */
@EIntentService
public class IntentServiceHandledAction extends IntentAbility {
    @Bean
    EnhancedClass dependency;
    /**
     * 为Tes处理的行动
     */
    private Object actionForTestHandled = null;

    /**
     * 构造参数
     */
    public IntentServiceHandledAction() {
        super(IntentServiceHandledAction.class.getSimpleName());
    }

    @ServiceAction
    void myAction() {
        actionForTestHandled = new Object();
    }

    @ServiceAction
    void myActionOneParam(String valueString) {
        actionForTestHandled = valueString;
    }

    @ServiceAction("myAction")
    void myActionTwoParams(String valueString, long valueLong) {
        actionForTestHandled = valueString;
    }

    @Override
    protected void onProcessIntent(Intent intent) {
        // Do nothing here
    }
}
