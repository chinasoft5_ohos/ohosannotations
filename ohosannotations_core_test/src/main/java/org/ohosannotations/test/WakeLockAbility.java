/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.RunningLock;

/**
 * 唤醒锁Ability
 *
 * @since 2021-07-20
 */
@EAbility
public class WakeLockAbility extends Ability {
    /**
     * 使用唤醒所默认值
     *
     * @param callback 回调
     */
    @RunningLock
    public void useWakeLockDefaultValues(Callback callback) {
        if (callback != null) {
            callback.onCall();
        }
    }

    /**
     * 使用唤醒锁自定义级别
     */
    @RunningLock(lockType = RunningLock.RunningLockType.PROXIMITY_SCREEN_CONTROL)
    public void useWakeLockCustomLevel() {
    }

    /**
     * 使用唤醒锁自定义标签
     */
    @RunningLock(tag = "HelloWakeLock")
    public void useWakeLockCustomTag() {
    }

    /**
     * 接口回调
     */
    public interface Callback {
        /**
         * 回调方法
         */
        void onCall();
    }
}
