/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.eprovider;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;

import com.chinasoft_ohos.commontools.toast.Toast;

import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EProvider;
import org.ohosannotations.annotations.Trace;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.annotations.sharedpreferences.Pref;
import org.ohosannotations.test.ebean.EnhancedClass;
import org.ohosannotations.test.eservice.MyService;
import org.ohosannotations.test.prefs.SomePrefs_;

import timber.log.Timber;

/**
 * 数据库Provider
 *
 * @since 2021-06-09
 */
@EProvider
public class MyProvider extends Ability {
    @Bean
    EnhancedClass dependency;

    @Pref
    SomePrefs_ somePrefs;

    @Override
    public int delete(Uri uri, DataAbilityPredicates predicates) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        return "";
    }

    @Override
    public int insert(Uri uri, ValuesBucket value) {
        return 0;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        return null; // 返回空
    }

    @Override
    public int update(Uri uri, ValuesBucket value, DataAbilityPredicates predicates) {
        return 0;
    }

    @UiThread
    @Trace
    void showToast() {
        Toast.show("Hello World!");
    }

    @Trace
    @Background
    void workInBackground() {
        Timber.d(MyService.class.getSimpleName() + "Doing some background work.");
    }
}
