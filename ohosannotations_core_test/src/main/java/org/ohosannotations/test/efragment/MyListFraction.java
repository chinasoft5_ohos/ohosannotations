/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.efragment;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.app.Context;

import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.EFraction;
import org.ohosannotations.annotations.IgnoreWhen;
import org.ohosannotations.annotations.ItemClick;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.test.ResourceTable;

/**
 * 我列出分数
 *
 * @author dev
 * @since 2021-07-22
 */
@EFraction(ResourceTable.Layout_list_fraction)
public class MyListFraction extends Fraction {
    @ComponentById(value = ResourceTable.Id_list)
    ListContainer list;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Context context = getFractionAbility().getContext();
        String[] strings = context.getStringArray(ResourceTable.Strarray_planets_array);
        ItemProvider sampleItemProvider = new ItemProvider(strings, context);
        list.setItemProvider(sampleItemProvider);
    }


    @UiThread
    void uiThread() {
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    @IgnoreWhen(IgnoreWhen.State.DETACHED)
    void uiThreadIgnored() {
    }

    @UiThread(id = 1)
    void uiThreadWithId() {
    }

    @Background
    void backgroundThread() {
    }

    @Background
    @IgnoreWhen(IgnoreWhen.State.DETACHED)
    void backgroundThreadIgnored() {
    }

    @IgnoreWhen(IgnoreWhen.State.DETACHED)
    void ignored() {
    }

    @IgnoreWhen(IgnoreWhen.State.VIEW_DESTROYED)
    void ignoreWhenViewDestroyed() {
    }

    void notIgnored() {
    }

    @ItemClick
    void listItemClicked(String string) {
    }

    /**
     * ItemProvider
     *
     * @since 2021-07-22
     */
    public static final class ItemProvider extends BaseItemProvider {
        private String[] list;
        private Context context;

        /**
         * ItemProvider
         *
         * @param list list
         * @param context context
         */
        public ItemProvider(String[] list, Context context) {
            this.list = list.clone();
            this.context = context;
        }

        /**
         * getCount
         *
         * @return list
         */
        @Override
        public int getCount() {
            return list == null ? 0 : list.length;
        }

        /**
         * getItem
         *
         * @param position position
         * @return null
         */
        @Override
        public Object getItem(int position) {
            if (list != null && position >= 0 && position < list.length) {
                return list[position];
            }
            return null;
        }

        /**
         * getItemId
         *
         * @param position position
         * @return position
         */
        @Override
        public long getItemId(int position) {
            return position;
        }

        /**
         * getComponent
         *
         * @param position position
         * @param convertComponent convertComponent
         * @param componentContainer componentContainer
         * @return cpt
         */
        @Override
        public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
            final Component cpt;
            if (convertComponent == null) {
                cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_simple_spinner_item, null, false);
            } else {
                cpt = convertComponent;
            }
            Text text = (Text) cpt.findComponentById(ResourceTable.Id_text1);
            text.setText(list[position]);
            return cpt;
        }
    }
}
