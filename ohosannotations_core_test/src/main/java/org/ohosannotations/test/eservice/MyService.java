/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.eservice;

import com.chinasoft_ohos.commontools.toast.Toast;

import ohos.aafwk.ability.IntentAbility;
import ohos.aafwk.content.Intent;
import timber.log.Timber;

import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EService;
import org.ohosannotations.annotations.Trace;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.test.ebean.EnhancedClass;

/**
 * 服务Service
 *
 * @since 2021-06-09
 */
@EService
public class MyService extends IntentAbility {
    @Bean
    EnhancedClass dependency;

    /**
     * 构造参数
     */
    public MyService() {
        super(MyService.class.getSimpleName());
    }

    @Override
    protected void onProcessIntent(Intent intent) {
        showToast();
        workInBackground();
    }

    @Trace
    @UiThread
    void showToast() {
        Toast.show("Hello World!");
    }

    @Trace
    @Background
    void workInBackground() {
        Timber.d(MyService.class.getSimpleName() + "Doing some background work.");
    }
}
