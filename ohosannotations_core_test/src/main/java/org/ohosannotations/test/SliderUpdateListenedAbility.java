/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Slider;

import org.ohosannotations.annotations.SliderProgressUpdated;
import org.ohosannotations.annotations.SliderTouchStart;
import org.ohosannotations.annotations.SliderTouchEnd;
import org.ohosannotations.annotations.EAbility;

/**
 * SliderUpdateListenedAbility
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_sliders)
public class SliderUpdateListenedAbility extends Ability {
    boolean handled;
    int progress;
    boolean fromUser;
    Slider slider;

    @SliderProgressUpdated(ResourceTable.Id_slider1)
    void m1(Slider slider) {
        handled = true;
    }

    @SliderProgressUpdated(ResourceTable.Id_slider1)
    void m2(Slider slider, int progress) {
        this.progress = progress;
    }

    @SliderProgressUpdated(ResourceTable.Id_slider1)
    void m3(Slider slider, int progress, boolean fromUser) {
        this.fromUser = fromUser;
    }

    @SliderProgressUpdated(ResourceTable.Id_slider1)
    void m4(boolean fromUser, int progress) {
    }

    @SliderProgressUpdated({ResourceTable.Id_slider1, ResourceTable.Id_slider2})
    void m5(Slider slider, boolean fromUser, int progress) {
        this.slider = slider;
    }

    @SliderProgressUpdated({ResourceTable.Id_slider1, ResourceTable.Id_slider2})
    void m6(Boolean fromUser, Integer progress) {
    }

    @SliderProgressUpdated({ResourceTable.Id_slider1, ResourceTable.Id_slider2})
    void m7() {
    }

    @SliderTouchStart(ResourceTable.Id_slider2)
    @SliderProgressUpdated(ResourceTable.Id_slider2)
    @SliderTouchEnd(ResourceTable.Id_slider2)
    void m8(Slider slider) {
    }

    @SliderTouchEnd(ResourceTable.Id_slider1)
    void m9(Slider slider) {
    }

    @SliderTouchEnd(ResourceTable.Id_slider1)
    void m10() {
    }

    @SliderTouchEnd
    void slider1SliderTouchEnded() {
        handled = true;
    }

    @SliderTouchStart(ResourceTable.Id_slider1)
    void m11(Slider slider) {
    }

    @SliderTouchStart(ResourceTable.Id_slider1)
    void m12() {
    }

}
