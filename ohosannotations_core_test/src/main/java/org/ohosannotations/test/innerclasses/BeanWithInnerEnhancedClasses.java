/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.innerclasses;

import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EBean;
import org.ohosannotations.annotations.res.StringRes;
import org.ohosannotations.annotations.sharedpreferences.DefaultInt;
import org.ohosannotations.annotations.sharedpreferences.DefaultRes;
import org.ohosannotations.annotations.sharedpreferences.Pref;
import org.ohosannotations.annotations.sharedpreferences.SharedPref;
import org.ohosannotations.test.ResourceTable;
import org.ohosannotations.test.ebean.SomeImplementation;

/**
 * 与内部增强类bean
 *
 * @author dev
 * @since 2021-06-07
 */
@EBean
public class BeanWithInnerEnhancedClasses {
    @Bean
    InnerEnhancedBean innerEnhancedBean;

    @Bean
    SomeImplementation someImplementation;

    /**
     * 内部加强豆
     *
     * @author dev
     * @since 2021-07-22
     */
    @EBean
    public static class InnerEnhancedBean {
        @StringRes(ResourceTable.String_hello)
        String hello;

    }

    /**
     * 内心的首选项
     *
     * @author dev
     * @since 2021-07-22
     */
    @SharedPref
    public interface InnerPrefs {
        /**
         * int值
         *
         * @return int
         */
        @DefaultInt(12)
        int intValue();

        /**
         * 字符串值
         *
         * @return {@link String}
         */
        @DefaultRes(ResourceTable.String_hello)
        String stringValue();
    }
}
