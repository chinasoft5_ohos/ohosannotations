/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.inheritance;

import org.ohosannotations.annotations.AfterInject;
import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.EBean;

/**
 * Mother
 *
 * @since 2021-07-20
 */
@EBean
public abstract class Mother {
    /**
     * 初始化
     */
    protected boolean motherInitCalled = false;
    /**
     * 视图初始化
     */
    protected boolean motherInitViewsCalled = false;

    @AfterInject
    void initMother() {
        motherInitCalled = true;
    }

    @AfterViews
    void initViewsMother() {
        motherInitViewsCalled = true;
    }
}
