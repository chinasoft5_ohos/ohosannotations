/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.prefs;


import org.ohosannotations.annotations.sharedpreferences.DefaultRes;
import org.ohosannotations.annotations.sharedpreferences.SharedPref;
import org.ohosannotations.annotations.sharedpreferences.SharedPref.Scope;
import org.ohosannotations.test.ResourceTable;

/**
 * SomeResPrefs
 *
 * @since 2021-07-20
 */
@SharedPref(Scope.UNIQUE)
public interface SomeResPrefs {
    /**
     * prefDefaultString
     *
     * @return String 字符串
     */
    @DefaultRes
    String prefDefaultString();

    /**
     * nameResId
     *
     * @return String 字符串
     */
    @DefaultRes(ResourceTable.String_prefDefaultString)
    String nameResId();

    /**
     * nameResName
     *
     * @return String 字符串
     */
    @DefaultRes(resName = "prefDefaultString")
    String nameResName();

    /**
     * prefDefaultInt
     *
     * @return int 整数
     */
    @DefaultRes
    int prefDefaultInt();

    /**
     * ageResId
     *
     * @return int整数
     */
    @DefaultRes(ResourceTable.Float_prefDefaultInt)
    int ageResId();

    /**
     * prefDefaultLong
     *
     * @return long类型
     */
    @DefaultRes
    long prefDefaultLong();

    /**
     * ageLongResId
     *
     * @return long类型
     */
    @DefaultRes(ResourceTable.Float_prefDefaultLong)
    long ageLongResId();

    /**
     * prefDefaultFloat
     *
     * @return float浮点
     */
    @DefaultRes
    float prefDefaultFloat();

    /**
     * ageFloatResId
     *
     * @return float 浮点
     */
    @DefaultRes(ResourceTable.Float_prefDefaultFloat)
    float ageFloatResId();

    /**
     * prefsDefaultBool
     *
     * @return boolean 布尔
     */
    @DefaultRes
    boolean prefsDefaultBool();

    /**
     * isAwesomeResId
     *
     * @return boolean 布尔
     */
    @DefaultRes(ResourceTable.Boolean_prefsDefaultBool)
    boolean isAwesomeResId();

}
