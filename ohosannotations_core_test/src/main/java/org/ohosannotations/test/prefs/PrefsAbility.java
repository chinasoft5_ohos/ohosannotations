/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the ohosannotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.prefs;

import ohos.aafwk.ability.Ability;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.sharedpreferences.Pref;
import org.ohosannotations.test.ResourceTable;

/**
 * PrefsAbility
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_ability_main)
public class PrefsAbility extends Ability {
    @Pref
    SomePrefs_ somePrefs;
    @Pref
    SomeResPrefs_ someResPrefs;
    @Pref
    AbilityDefaultPrefs_ abilityDefaultPrefs;

    @Pref
    AbilityPrefs_ abilityPrefs;

    @Pref
    ApplicationDefaultPrefs_ applicationDefaultPrefs;

    @Pref
    DefaultPrefs_ defaultPrefs;

    @Pref
    PublicPrefs_ publicPrefs;

    @Pref
    UniquePrefs_ uniquePrefs;

    @Pref
    InnerPrefs_.InnerSharedPrefs_ innerPrefs;

    SomePrefs_ methodInjectedPref;
    SomePrefs_ firstMultiInjectedPref;
    AbilityPrefs_ secondMultiInjectedPref;

    @Pref
    void methodInjectedPref(SomePrefs_ somePrefs) {
        methodInjectedPref = somePrefs;
    }

    void methodInjectedPref(@Pref SomePrefs_ somePrefs, @Pref AbilityPrefs_ abilityPrefs) {
        firstMultiInjectedPref = somePrefs;
        secondMultiInjectedPref = abilityPrefs;
    }
}
