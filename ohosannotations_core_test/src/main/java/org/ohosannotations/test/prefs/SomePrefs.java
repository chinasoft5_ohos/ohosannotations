/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.prefs;

import org.ohosannotations.annotations.sharedpreferences.DefaultBoolean;
import org.ohosannotations.annotations.sharedpreferences.DefaultFloat;
import org.ohosannotations.annotations.sharedpreferences.DefaultInt;
import org.ohosannotations.annotations.sharedpreferences.DefaultLong;
import org.ohosannotations.annotations.sharedpreferences.DefaultRes;
import org.ohosannotations.annotations.sharedpreferences.DefaultString;
import org.ohosannotations.annotations.sharedpreferences.DefaultStringSet;
import org.ohosannotations.annotations.sharedpreferences.SharedPref;
import org.ohosannotations.test.ResourceTable;

import java.util.Set;

/**
 * 一些首选项
 *
 * @author dev
 * @since 2021-07-22
 */
@SharedPref(SharedPref.Scope.UNIQUE)
public interface SomePrefs {
    /**
     * 名字
     *
     * @return {@link String}
     */
    @DefaultString("John")
    String name();

    /**
     * 年龄
     *
     * @return int
     */
    @DefaultInt(42)
    int age();

    /**
     * 年龄长
     *
     * @return long
     */
    @DefaultLong(400000L)
    long ageLong();

    /**
     * 年龄浮动
     *
     * @return float
     */
    @DefaultFloat(42f)
    float ageFloat();

    /**
     * isAwesome
     *
     * @return boolean
     */
    @DefaultBoolean(true)
    boolean isAwesome();

    /**
     * 字符串res键
     *
     * @return int
     */
    @DefaultInt(value = 42, keyRes = ResourceTable.String_prefStringKey)
    int stringResKeyPref();

    /**
     * 最后一次更新
     *
     * @return long
     */
    long lastUpdated();

    /**
     * 类型
     *
     * @return {@link Set}
     */
    Set<String> types();

    /**
     * 设置,默认
     *
     * @return {@link Set}
     */
    @DefaultStringSet({"a", "b", "c"})
    Set<String> setWithDefault();

    /**
     * emtpy字符串
     *
     * @return {@link Set}
     */
    @DefaultStringSet("")
    Set<String> emtpyString();

    /**
     * emtpy字符串设置
     *
     * @return {@link Set}
     */
    @DefaultStringSet({})
    Set<String> emtpyStringSet();

    /**
     * 行星的字符串集
     *
     * @return {@link Set}
     */
    @DefaultRes(ResourceTable.Strarray_planets_array)
    Set<String> planetsStringSet();
}
