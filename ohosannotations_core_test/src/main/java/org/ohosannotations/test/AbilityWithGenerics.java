/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;

import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.UiThread;

import java.util.List;

/**
 * AbilityWithGenerics
 *
 * @since 2021-07-20
 */
@EAbility
public class AbilityWithGenerics extends Ability {
    /**
     * emptyUiMethod
     *
     * @param param 参数1
     * @param param2 参数2
     * @param <T> 子类泛型
     * @param <S> 子类泛型
     */
    @UiThread
    <T, S extends Number & List<String>> void emptyUiMethod(T param, S param2) {
    }

    /**
     * emptyUiMethod
     *
     * @param param 参数1
     * @param param2 参数2
     * @param <T> 子类泛型
     * @param <S> 子类泛型
     */
    @UiThread
    <T, S extends Number> void emptyUiMethod(List<? extends T> param, List<? super S> param2) {
    }

    /**
     * emptyUiMethod
     *
     * @param param 参数
     * @param <T> 子类泛型
     * @param <S> 子类泛型
     */
    @UiThread
    <T, S extends Number> void emptyUiMethod(T param) {
    }

    /**
     * emptyBackgroundMethod
     *
     * @param param 参数
     * @param <T> 子类泛型
     * @param <S> 子类泛型
     */
    @Background
    <T, S extends Number> void emptyBackgroundMethod(T param) {
    }

    /**
     * emptyBackgroundMethod
     *
     * @param param 参数1
     * @param param2 参数2
     * @param <T> 子类泛型
     */
    @Background
    <T extends Number> void emptyBackgroundMethod(T param, List<T> param2) {
    }
}
