/**
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.eview;

import ohos.agp.components.Component;
import ohos.app.Context;

import org.ohosannotations.annotations.EView;

/**
 * 使用实例状态查看
 *
 * @since 2021-07-20
 */
@EView
public class ViewWithInstanceState extends Component {
    /**
     * 构造参数
     *
     * @param context 上下文
     */
    public ViewWithInstanceState(Context context) {
        super(context);
    }
}
