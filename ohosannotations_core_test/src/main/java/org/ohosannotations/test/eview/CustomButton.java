/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test.eview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.app.Context;

import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.Background;
import org.ohosannotations.annotations.EView;
import org.ohosannotations.annotations.Trace;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.annotations.res.StringRes;
import org.ohosannotations.test.ResourceTable;

/**
 * 自定义按钮
 *
 * @since 2021-06-04
 */
@EView
public class CustomButton extends Button {
    @StringRes(ResourceTable.String_app_name)
    String res;

    /**
     * 构造参数
     *
     * @param context 上下文
     */
    public CustomButton(Context context) {
        super(context);
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param constructorParameter 构造函数参数
     */
    public CustomButton(Context context, int constructorParameter) {
        super(context);
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param attrs attrs
     */
    public CustomButton(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    @Trace
    @AfterViews
    void afterViews() {
    }

    @Background
    void someBackgroundTask() {
    }

    @UiThread
    void someUiThreadTask() {
    }
}
