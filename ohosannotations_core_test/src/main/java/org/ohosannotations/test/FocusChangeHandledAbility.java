/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;


import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.FocusChange;

/**
 * FocusChangeHandledAbility
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_clickable_widgets)
public class FocusChangeHandledAbility extends EventsHandledAbstractAbility {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");
    Component view;

    boolean hasFocus;

    /**
     * conventionButton
     *
     * @param evt 视图
     * @param hasFocus 是否有焦点
     */
    @FocusChange
    public void conventionButton(Component evt, boolean hasFocus) {
    }

    /**
     * buttonWithButtonArgument
     *
     * @param evt 按钮
     * @param hasFocus 是否有焦点
     */
    @FocusChange
    public void buttonWithButtonArgument(Button evt, boolean hasFocus) {
        view = evt;
        HiLog.info(LABEL, "" + evt.toString());
    }

    /**
     * 蛇情况下按钮
     *
     * @param hasFocus 有焦点
     */
    @FocusChange
    public void snakeCaseButton(boolean hasFocus) {
        snakeCaseButtonEventHandled = true;
    }

    /**
     * extendedConventionButton
     *
     * @param evt 视图
     */
    @FocusChange
    public void extendedConventionButton(Component evt) {
        view = evt;
    }

    /**
     * overridenConventionButton
     */
    @FocusChange(ResourceTable.Id_configurationOverConventionButton)
    public void overridenConventionButton() {
    }

    /**
     * buttonWithViewArgument
     */
    @FocusChange
    public void buttonWithViewArgument() {
    }

    /**
     * multipleButtonWithViewArgument
     *
     * @param component 视图
     * @param isHasFocus 是否有焦点
     */
    @FocusChange({ResourceTable.Id_button1, ResourceTable.Id_button2})
    public void multipleButtonWithViewArgument(Component component, boolean isHasFocus) {
        this.hasFocus = isHasFocus;
        HiLog.info(LABEL, "" + isHasFocus);
    }
}
