/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.content.Intent;

import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.NonConfigurationInstance;
import org.ohosannotations.test.ebean.EmptyDependency;
import org.ohosannotations.test.ebean.SomeImplementation;
import org.ohosannotations.test.ebean.SomeInterface;

import java.util.logging.Logger;

/**
 * NonConfigurationAbility
 *
 * @since 2021-06-04
 */
@EAbility
public class NonConfigurationAbility extends EventsHandledAbstractAbility {
    @Bean
    @NonConfigurationInstance
    EmptyDependency maintainedDependency;

    @Bean
    EmptyDependency recreatedDependency;

    @Bean(SomeImplementation.class)
    @NonConfigurationInstance
    SomeInterface maintainedAbstracted;

    @NonConfigurationInstance
    Object someObject;

    @Override
    protected void onStop() {
        super.onStop();
        maintainedDependency = null;
        recreatedDependency = null;
        maintainedAbstracted = null;
        someObject = null;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Logger.getGlobal().warning(maintainedDependency.toString());
        Logger.getGlobal().warning(recreatedDependency.toString());
        Logger.getGlobal().warning(maintainedAbstracted.toString());
        Logger.getGlobal().warning(someObject.toString());
    }
}
