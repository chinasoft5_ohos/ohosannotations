/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.test;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Text;

import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.TextUpdated;

/**
 * 文本监听Ability
 *
 * @since 2021-07-20
 */
@EAbility(ResourceTable.Layout_ability_main)
public class TextWatchedAbility extends Ability {
    @TextUpdated(ResourceTable.Id_helloTextView)
    void m1(String ss) {
    }

    @TextUpdated(ResourceTable.Id_helloTextView)
    void m2(String ss, int start) {
    }

    @TextUpdated(ResourceTable.Id_helloTextView)
    void m3(String ss, int start, int before, int count) {
    }

    @TextUpdated(ResourceTable.Id_helloTextView)
    void m4(Text text, String ss, int start, int before, int count) {
    }

    @TextUpdated(ResourceTable.Id_helloTextView)
    void m5(String ss, int before, int start, int count) {
    }

    @TextUpdated(ResourceTable.Id_helloTextView)
    void helloTextViewTextUpdated() {
    }
}
