/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  暂不支持此注解@FractionArg
 * <p>
 * The annotation value is the key used for argument. If not set, the field or
 * method name will be used as the key.
 * </p>
 * <p>
 * When {@link FractionArg} is used, the intent builder will hold dedicated
 * methods for each annotated fields.
 * </p>
 * <p>
 * Your code related to injected extra should go in an {@link AfterInject}
 * annotated method.
 * </p>
 * <blockquote>
 *
 * @see EFraction
 * @see FractionById
 * @see FractionByTag
 */
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface FractionArg {
    /**
     * The key of the injected Fraction argument.
     *
     * @return the key of the argument
     */
    String value() default "";
}
