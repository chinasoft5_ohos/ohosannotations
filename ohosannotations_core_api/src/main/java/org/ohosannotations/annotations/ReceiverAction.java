/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.annotations;

import ohos.event.commonevent.CommonEventData;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Should be used on a method that must respond to a specific action in an
 * {@link EReceiver} annotated class.
 * </p>
 * <p>
 * The class MAY contain several {@link ReceiverAction} annotated methods.
 * </p>
 * <p>
 * The method annotated with {@link ReceiverAction} may have as parameters :
 * </p>
 * <ul>
 * <li>A {@link ohos.app.Context} which will be the context given in
 * {@code void onReceive(Context context, Intent intent)}</li>
 * <li>A {@link ohos.aafwk.content.Intent} which will be the intent given in
 * {@code void onReceive(Context context, Intent intent)}</li>
 * <li>Any native, {@link ohos.utils.Parcel} or {@link java.io.Serializable}
 * parameters annotated with {@link Extra} which will be the
 * extra put in the intent. The key of this extra is the value of the annotation
 * {@link Extra} if set or the name of the parameter.</li>
 * </ul>
 *
 * <blockquote>
 * <p>
 * Example :
 *
 * <pre>
 * &#064;EReceiver
 * public class MyReceiver extends BroadcastReceiver {
 *
 * 	&#064;ReceiverAction
 * 	void mySimpleAction(Intent intent) {
 * 		// ...
 *    }
 *
 * 	&#064;ReceiverAction
 * 	void myAction(@ReceiverAction.Extra String valueString, Context context) {
 * 		// ...
 *    }
 *
 * 	&#064;ReceiverAction
 * 	void anotherAction(@ReceiverAction.Extra(&quot;specialExtraName&quot;) String valueString, @ReceiverAction.Extra long valueLong) {
 * 		// ...
 *    }
 *
 * 	&#064;Override
 * 	public void onReceive(Context context, Intent intent) {
 * 		// empty, will be overridden in generated subclass
 *    }
 * }
 * </pre>
 *
 * </blockquote>
 *
 * <p>
 * Note: Since
 * {@link ohos.event.commonevent.CommonEventSubscriber#onReceiveEvent(CommonEventData)}
 * BroadcastReceiver#onReceive} is abstract, you have to add an empty
 * implementation. For convenience, we provide the
 * {@link ohos.event.commonevent.CommonEventSubscriber
 * AbstractBroadcastReceiver} class, which implements that method, so you do not
 * have to do in your actual class if you derive it.
 * </p>
 *
 * @see EReceiver
 * @see ohos.event.commonevent.CommonEventSubscriber
 * AbstractBroadcastReceiver
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface ReceiverAction {
    /**
     * Define a set of actions this method should handle.
     *
     * @return the actions
     */
    String[] actions();

    /**
     * Define a set of data schemes to filter the Intent. If this field isn't all
     * schemes are allowed
     *
     * @return the data schemes to filter
     */
    String[] dataSchemes() default {};

    /**
     * <p>
     * Should be used on any native, {@link ohos.utils.Parcel} or
     * {@link java.io.Serializable} parameter of a method annotated with
     * {@link ReceiverAction} to inject the extra put in the intent parameter of
     * {@code void onReceive(Context context, Intent intent)}. The key of this extra
     * is the value of the annotation {@link Extra} if it is set or
     * the name of the parameter. If <a href="http://parceler.org">Parceler</a> is
     * on the classpath, extras annotated with &#064;Parcel, or collections
     * supported by Parceler will be automatically marshaled using a
     * {@link ohos.utils.Parcel Parcelable} through the Parcels utility class.
     * </p>
     */
    @Retention(RetentionPolicy.CLASS)
    @Target(ElementType.PARAMETER)
    public @interface Extra {
        /**
         * Define the extra's name. If this parameter isn't set the annotated parameter
         * name will be used.
         *
         * @return the extra's name
         */
        String value() default "";
    }
}
