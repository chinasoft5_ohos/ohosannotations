/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * This annotation is intended to be used on methods to receive events defined
 * by
 * when a list item has been clicked by the user.
 * </p>
 * <p>
 * The method MAY have one parameter :
 * </p>
 * <ul>
 * <li>An <code>int</code> parameter to know the position of the clicked item.
 * Or, a parameter of the type of the Provider linked to the listContainer.</li>
 * </ul>
 *
 * @see ItemLongClick
 * @see ItemSelect
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface ItemClick {
    /**
     * The R.id.* fields which refer to the AdapterViews.
     *
     * @return the ids of the AdapterViews
     */
    int[] value() default ResId.DEFAULT_VALUE;

    /**
     * The resource names as strings which refer to the AdapterViews.
     *
     * @return the resource names of the AdapterViews
     */
    String[] resName() default "";
}
