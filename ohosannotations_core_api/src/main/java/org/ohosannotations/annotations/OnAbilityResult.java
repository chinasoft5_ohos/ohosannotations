/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * This annotation is intended to be used on methods to receive results from a
 * previously started Ability using
 * {@link ohos.aafwk.ability#onAbilityResult(int requestCode, int resultCode, Intent resultData)
 * Ability#startAbilityForResult(Intent, int)} or the generated
 * <code>IntentBuilder.startAbilityForResult()</code> method of the Ability.
 * </p>
 * <p>
 * The annotation value must be an integer constant that represents the
 * <b>requestCode</b> associated with the given result.
 * </p>
 * <p>
 * The method may have multiple parameter :
 * </p>
 * <ul>
 * <li>A {@link ohos.aafwk.content Intent} that contains data</li>
 * <li>An <code>int</code> or an {@link Integer Integer} to get the
 * resultCode</li>
 * <li>Any native, {@link ohos.utils Sequenceable} or
 * {@link java.io.Serializable Serializable} parameter annotated with
 * {@link Extra
 * OnAbilityResult.Extra} to get an object put in the extras of the
 * intent.</li>
 * </ul>
 *
 * <blockquote>
 * <p>
 * Some usage examples of &#064;OnAbilityResult annotation:
 *
 * <pre>
 * &#064;OnAbilityResult(<b>REQUEST_CODE</b>)
 * void onResult(int resultCode, Intent data) {
 * }
 *
 * &#064;OnAbilityResult(<b>REQUEST_CODE</b>)
 * void onResult(int resultCode) {
 * }
 *
 * &#064;OnAbilityResult(<b>ANOTHER_REQUEST_CODE</b>)
 * void onResult(Intent data) {
 * }
 *
 * &#064;OnAbilityResult(<b>ANOTHER_REQUEST_CODE</b>)
 * void onResult(&#064;OnAbilityResult.Extra anExtra) {
 * }
 * </pre>
 *
 * </blockquote>
 *
 * @see EAbility
 * @see ohos.aafwk.ability.AbilitySlice#startAbilityForResult(ohos.aafwk.content.Intent, int)
 * @see ohos.aafwk.ability.AbilitySlice#OnAbilityResult(int, int, ohos.aafwk.content.Intent)
 */

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface OnAbilityResult {
    /**
     * The <b>requestCode</b> associated with the given result.
     *
     * @return the requestCode
     */
    int value();

    /**
     * <p>
     * Use on any native, {@link ohos.utils.Sequenceable} or
     * {@link java.io.Serializable} parameter of an {@link OnAbilityResult}
     * annotated method to bind it with the value from the Intent. If
     * <a href="http://parceler.org">Parceler</a> is on the classpath, extras
     * annotated with &#064;Parcel, or collections supported by Parceler will be
     * automatically marshaled using a {@link ohos.utils.Sequenceable Sequenceable}
     * through the Parcels utility class.
     * </p>
     * <p>
     * The annotation value is the key used for the result data. If not set, the
     * field name will be used as the key.
     * </p>
     *
     * <blockquote>
     * <p>
     * Some usage examples of &#064;Result annotation:
     *
     * <pre>
     * &#064;OnAbilityResult(REQUEST_CODE)
     * void onResult(int resultCode, Intent data, <b>@OnAbilityResult.Extra String value</b>) {
     * }
     *
     * &#064;OnAbilityResult(REQUEST_CODE)
     * void onResult(int resultCode, <b>@OnAbilityResult.Extra(value = "key") String value</b>) {
     * }
     *
     * &#064;OnAbilityResult(REQUEST_CODE)
     * void onResult(<b>@OnAbilityResult.Extra String strVal</b>, <b>@OnAbilityResult.Extra int intVal</b>) {
     * }
     * </pre>
     *
     * </blockquote>
     *
     * @see ohos.aafwk.ability.AbilitySlice(int, int, ohos.aafwk.content.Intent)
     * @see OnAbilityResult
     */

    @Retention(RetentionPolicy.CLASS)
    @Target(ElementType.PARAMETER)
    public @interface Extra {
        /**
         * They key of the result data.
         *
         * @return the key
         */
        String value() default "";
    }
}
