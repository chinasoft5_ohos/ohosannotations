/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the OhosAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.annotations;

import org.ohosannotations.api.KotlinOpen;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Should be used on custom classes to enable usage of OhosAnnotations.
 * </p>
 * <p>
 * This class MUST have either a default constructor (ie without parameters) or
 * a constructor with only a parameter of type {@link ohos.app.Context}.
 * </p>
 * <p>
 * Your code related to injected beans should go in an {@link AfterInject}
 * annotated method.
 * </p>
 * <p>
 * If the class is abstract, the enhanced bean will not be generated. Otherwise,
 * it will be generated as a final class. You can use OhosAnnotations to
 * create Abstract classes that handle common code.
 * </p>
 * <p>
 * {@link EBean} can only be used on classes and NOT on interfaces. If you want
 * to use an {@link EBean} with interface you must annotate the implementation
 * class and not the interface.
 * </p>
 * <p>
 * Most annotations are supported in {@link EBean} classes, except the ones
 * related to extras. Views related annotations will only work if the bean was
 * injected in an ability with a layout containing the views you're dealing
 * with. If your bean needs a {@link ohos.app.Context} you can inject on
 * by using an {@link RootContext} annotated field.
 * </p>
 * <p>
 * Beans have two possible scopes : default or singleton. Default scope should
 * be preferred but in some case it may be useful to use a singleton scope
 * (mainly if you want to keep some runtime state in your bean).
 * </p>
 * <p>
 * The enhanced bean can also be injected in any enhanced class by using
 * {@link Bean} annotation.
 * </p>
 * <blockquote>
 * <p>
 * Example :
 *
 * <pre>
 * &#064;EBean
 * public class MyBean {
 *
 * 	&#064;RootContext
 * 	Context context;
 * 	&#064;Bean
 * 	MySingletonBean mySingletonBean;
 *
 * 	&#064;AfterInject
 * 	void init() {
 * 		mySingletonBean.doSomeStuff(context);
 *    }
 * }
 *
 * &#064;EBean(scope = Scope.Singleton)
 * public class MySingletonBean {
 *
 * 	public void doSomeStuff(Context context) {
 * 		// ...
 *    }
 * }
 * </pre>
 *
 * </blockquote>
 *
 * @see AfterInject
 * @see RootContext
 * @see Bean
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
@KotlinOpen
public @interface EBean {
    /**
     * The scope of an enhanced bean.
     */
    enum Scope {
        /**
         * A new instance of the bean is created each time it is needed.
         */
        Default, //

        /**
         * A new instance of the bean is created the first time it is needed in an
         * Ability scope, it is then retained and the same instance is always returned
         * from within the same Ability. Different Abilities hold different copies of
         * the bean.
         */
        Ability,

        /**
         * A new instance of the bean is created the first time it is needed in a
         * Fragment scope, it is then retained and the same instance is always returned
         * from within the same Fraction. Different Fractions hold different copies of
         * the bean.
         */
        Fraction,

        /**
         * A new instance of the bean is created the first time it is needed, it is then
         * retained and the same instance is always returned.
         */
        Singleton, //
    }

    /**
     * The scope of the enhanced bean.
     *
     * @return the scope of the bean.
     */
    Scope scope() default Scope.Default;
}
