/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Deprecated
public @interface HttpsClient {
    /**
     * The default value of {@link #trustStorePwd()} and {@link #keyStorePwd()}.
     */
    String DEFAULT_PASSWD = "changeit";

    /**
     * @return the id of the trust store file
     */
    int trustStore() default ResId.DEFAULT_VALUE;

    /**
     * The resource name which refers to the trust store file.
     *
     * @return the resource name of the trust store file.
     */
    String trustStoreResName() default "";

    /**
     * The trust store password.
     *
     * @return the trust store password
     */
    String trustStorePwd() default DEFAULT_PASSWD;

    /**
     * @return the id of the key store file
     */
    int keyStore() default ResId.DEFAULT_VALUE;

    /**
     * @return the resource name of the key store file
     */
    String keyStoreResName() default "";

    /**
     * The key store password.
     *
     * @return the key store password
     */
    String keyStorePwd() default DEFAULT_PASSWD;

    /**
     * Whether to authorizes any TLS/SSL hostname.
     *
     * @return <b>true</b> if authorizes any TLS/SSL hostname, <b>false</b>
     *         otherwise.
     */
    boolean allowAllHostnames() default true;
}
