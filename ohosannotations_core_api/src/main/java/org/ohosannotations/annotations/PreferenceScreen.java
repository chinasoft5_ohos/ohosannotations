/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Should be used on {@link EAbility
 * EAbility} or {@link EFraction EFraction}
 * classes which are subclass of {@link  ohos.data.preferences.Preferences
 * PreferenceAbility} or <code>PreferenceFragment</code> or
 * <code> ohos.data.preferences.Preferences</code> or
 * <code>ohos.data.preferences.Preferences</code>, to inject the preference
 * screen from resource.
 * </p>
 * <p>
 * The annotation value should be one of R.xml.* fields.
 * </p>
 *
 * <blockquote>
 *
 * Example :
 *
 * <pre>
 * &#064;PreferenceScreen(R.xml.settings)
 * &#064;EAbility
 * public class SettingsAbility extends PreferenceAbility {
 *
 * 	&#064;PreferenceByKey(R.string.myPref1)
 * 	Preference myPreference1;
 *
 * 	&#064;PreferenceByKey(R.string.checkBoxPref)
 * 	CheckBoxPreference checkBoxPref;
 *
 * 	&#064;AfterPreferences
 * 	void initPrefs() {
 * 		checkBoxPref.setChecked(false);
 * 	}
 * }
 * </pre>
 *
 * </blockquote>
 *
 * @see PreferenceHeaders
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface PreferenceScreen {
    /**
     * The R.xml.* field which refers to the Preference Screen.
     *
     * @return the identifier of the Preference Screen
     */
    int value() default ResId.DEFAULT_VALUE;

    /**
     * The resource name which refers to the Preference Screen.
     *
     * @return the identifier of the Preference Screen
     */
    String resName() default "";
}
