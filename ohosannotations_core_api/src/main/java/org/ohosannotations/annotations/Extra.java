/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Use on any native,
 * {@link java.io.Serializable Serializable} field in an {@link EAbility}
 * annotated class to bind it with Ohos's arguments. If
 * <a href="http://parceler.org">Parceler</a> is on the classpath, extras
 * annotated with &#064;Parcel, or collections supported by Parceler will be
 * automatically marshaled using a
 * through the Parcels utility class.
 * </p>
 * <p>
 * The annotation value is the key used for extra. If not set, the field or
 * method name will be used as the key.
 * </p>
 * <p>
 * When {@link Extra} is used, the intent builder will hold dedicated methods
 * for each annotated fields.
 * </p>
 * <p>
 * Your code related to injected extra should go in an {@link AfterInject}
 * annotated method.
 * </p>
 * <p>
 * Calling {@link ohos.aafwk.ability.Ability#setIntent(ohos.aafwk.content.Intent)
 * Ability#setIntent(Intent)} will automatically update the annotated extras.
 * </p>
 * <blockquote>
 * <p>
 * Example :
 *
 * <pre>
 * &#064;EAbility
 * public class MyAbility extends Ability {
 *
 * 	&#064;Click
 * 	void buttonClicked() {
 * 		MyExtraAbility_.intent(this) //
 * 				.myMessage(&quot;test&quot;) //
 * 				.startAbility();
 *    }
 * }
 *
 * &#064;EAbility
 * public class MyExtraAbility extends Ability {
 *
 * 	&#064;Extra
 * 	String myMessage;
 *
 * 	&#064;Extra
 * 	void singleInjection(String myMessage) {
 * 		// do stuff
 *    }
 *
 * 	void multiInjection(&#064;Extra String myMessage, &#064;Extra String myMessage2) {
 * 		// do stuff
 *    }
 *
 * 	&#064;AfterInject
 * 	void init() {
 * 		Log.d(&quot;AA&quot;, &quot;extra myMessage = &quot; + myMessage);
 *    }
 * }
 * </pre>
 *
 * </blockquote>
 *
 * @see AfterInject
 * @see EAbility
 */
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface Extra {
    /**
     * The key of the injected extra.
     *
     * @return the key of the extra
     */
    String value() default "";
}
