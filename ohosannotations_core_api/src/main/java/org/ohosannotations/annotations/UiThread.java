/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface UiThread {
    /**
     * The delay of the execution in milliseconds.
     *
     * @return the delay of the execution
     */
    long delay() default 0;

    /**
     * If propagation is {@link Propagation#REUSE}, the method will check first if
     * it is inside the UI thread already. If so, it will directly call the method
     * instead of using the handler. The default value is
     * {@link Propagation#ENQUEUE}, which will always call the handler.
     * <p>
     * When using a non-zero {@link #delay() delay} the propagation parameter is
     * ignored.
     *
     * @return {@link Propagation#ENQUEUE} to always call the handler,
     * {@link Propagation#REUSE}, to check whether it is already on the UI
     * thread
     */
    Propagation propagation() default Propagation.ENQUEUE;

    /**
     * Indicates the propagation behavior of the UiThread annotated method.
     */
    enum Propagation {
        /**
         * The method will always call the Handler.
         */
        ENQUEUE,
        /**
         * The method will check first if it is inside the UI thread already.
         */
        REUSE
    }

    /**
     * Identifier for cancellation.
     * <p>
     * To cancel all tasks having a specified id:
     *
     * <pre>
     * UiThreadExecutor.cancelAll(&quot;my_background_id&quot;);
     * </pre>
     *
     * @return the id for cancellation
     */
    int id() default -1;
}
