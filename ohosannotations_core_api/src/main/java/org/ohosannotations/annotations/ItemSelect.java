/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 项目选择
 * <p>
 * This annotation is intended to be used on methods to receive events defined
 * by
 * when a list item has been selected by the user.
 * </p>
 * <p>
 * The method MAY have one or two parameters :
 * </p>
 *
 * @author dev
 * @since 2021-07-26
 * @see ItemClick
 * @see ItemLongClick
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface ItemSelect {
    /**
     * 字段引用BaseItemProvider
     *
     * @return int
     */
    int[] value() default ResId.DEFAULT_VALUE;

    /**
     * 资源名作为引用BaseItemProvider的字符串。
     *
     * @return String
     */
    String[] resName() default "";
}
