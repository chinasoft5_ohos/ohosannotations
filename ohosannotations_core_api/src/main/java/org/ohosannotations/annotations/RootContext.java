/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Use it on {@link ohos.app.Context} fields in an {@link EBean}
 * annotated classes to inject context of the parent class.
 * </p>
 * <p>
 * This field may not be injected at runtime if the context used to create the
 * bean is not of the appropriate type. For example, if you create a new
 * instance of the bean using a Service context, and you use {@link RootContext}
 * on a field that extends Ability, this field will be null at runtime.
 * </p>
 * <blockquote>
 *
 * Example :
 *
 * <pre>
 * &#064;EBean
 * public class MyClass {
 *
 * 	&#064;RootContext
 * 	Context context;
 *
 * 	// Only injected if the root context is an ability
 * 	&#064;RootContext
 * 	Ability ability;
 *
 * 	// Only injected if the root context is a service
 * 	&#064;RootContext
 * 	Service service;
 *
 * 	// Only injected if the root context is an instance of MyAbility
 * 	&#064;RootContext
 * 	MyAbility myAbility;
 *
 * 	&#064;RootContext
 * 	void singleInjection(Context context) {
 * 		// do stuff
 * 	}
 *
 * 	void multiInjection(&#064;RootContext Context context, &#064;RootContext Ability ability) {
 * 		// do stuff
 * 	}
 * }
 * </pre>
 *
 * </blockquote>
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
public @interface RootContext {
}
