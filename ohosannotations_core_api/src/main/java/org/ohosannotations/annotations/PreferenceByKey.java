/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Use it on a {@link ohos.data.preferences.Preferences Preference} or
 * {@link ohos.data.preferences.Preferences Preference} subtype or
 * <code>ohos.data.preferences.Preferences</code> or
 * <code>ohos.data.preferences.Preferences</code> subtype fields or
 * methods with applicable parameters in a
 * {@link EAbility EAbility} or
 * {@link EFraction EFraction} annotated
 * class, which is a subclass of {@link ohos.data.preferences.Preferences
 * PreferenceAbility} or <code>PreferenceFragment(Compat)</code>, respectively.
 * </p>
 * <p>
 * The annotation value should be an array of R.string.* fields.
 * </p>
 * <p>
 * Your code related to injected preferences should go in an
 * {@link AfterPreferences AfterPreferences}
 * annotated method.
 * </p>
 * <blockquote>
 *
 * Example :
 *
 * <pre>
 * &#064;EAbility
 * public class SettingsAbility extends PreferenceAbility {
 *
 * 	&#064;PreferenceByKey(R.string.myPref1)
 * 	Preference myPreference1;
 *
 * 	&#064;PreferenceByKey(R.string.checkBoxPref)
 * 	CheckBoxPreference checkBoxPref;
 *
 * 	&#064;PreferenceByKey
 * 	void singleInjection(Preference myPreference1) {
 * 		// do stuff
 * 	}
 *
 * 	void multiInjection(&#064;PreferenceByKey Preference myPreference1,
 * 	&#064;PreferenceByKey(R.string.checkBoxPref) CheckBoxPreference checkBoxPref) {
 * 		// do stuff
 * 	}
 *
 * 	&#064;AfterPreferences
 * 	void initPrefs() {
 * 		checkBoxPref.setChecked(false);
 * 	}
 * }
 * </pre>
 *
 * </blockquote>
 *
 * @see AfterPreferences AfterPreferences
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
public @interface PreferenceByKey {
    /**
     * The R.string.* field which refers to the injected Preference.
     *
     * @return the key of the Preference
     */
    int value() default org.ohosannotations.annotations.ResId.DEFAULT_VALUE;

    /**
     * The resource name which refers to the injected Preference.
     *
     * @return the key of the Preference
     */
    String resName() default "";
}
