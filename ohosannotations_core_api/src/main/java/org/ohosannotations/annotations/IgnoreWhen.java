/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * When used standalone in an {@link EFraction} or in conjunction with the
 * {@link UiThread} or {@link Background} annotations, the annotated method will
 * be wrapped in an 'if attached' block such that no code will be executed if
 * the {@link EFraction} is no longer bound to its parent ability or
 * <code>DETACHED</code>the {@link EFraction} views are destroyed
 * </p>
 * <p>
 * Should be used on method that must meet the following criteria
 * </p>
 * <p>
 * 1) Can only be used in conjunction with classes annotated with
 * {@link EFraction}
 * </p>
 * <p>
 * 2) The annotated method MUST return void and MAY contain parameters.
 * </p>
 *
 * @see EFraction
 * @see UiThread
 * @see Background
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface IgnoreWhen {
    /**
     * The lifecycle state after the method should not be executed.
     *
     * @return the state that skips method execution
     */
    State value();

    /**
     * The lifecycle state after the method should not be executed.
     */
    enum State {
        /**
         * Skip execution if the {@link EFraction} is no longer bound to its parent
         *
         */
        DETACHED,

        /**
         * Skip execution if the {@link EFraction} views are destroyed.
         */
        VIEW_DESTROYED
    }
}
