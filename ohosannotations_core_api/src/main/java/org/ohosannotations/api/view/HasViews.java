/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.view;

import ohos.agp.components.Component;

/**
 * 视图
 *
 * @since 2021-07-19
 */
public interface HasViews {
    /**
     * 查找视图ID
     *
     * @param id 视图id
     * @param <T> 泛型
     * @return T 泛型
     */
    <T extends Component> T internalFindViewById(int id);
}
