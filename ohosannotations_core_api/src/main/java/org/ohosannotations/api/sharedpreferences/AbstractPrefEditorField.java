/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.sharedpreferences;

/**
 * 抽象类编辑器字段
 *
 * @param <T> 泛型
 * @since 2021-07-19
 */
public abstract class AbstractPrefEditorField<T extends EditorHelper<T>> {
    /**
     * 编辑助手
     */
    protected final T editorHelper;
    /**
     * 键
     */
    protected final String key;

    /**
     * 构造参数
     *
     * @param editorHelper 编辑助手
     * @param key 键
     */
    public AbstractPrefEditorField(T editorHelper, String key) {
        this.editorHelper = editorHelper;
        this.key = key;
    }

    /**
     * 移除键
     *
     * @return T 泛型
     */
    public final T remove() {
        editorHelper.getEditor().delete(key);
        return editorHelper;
    }
}
