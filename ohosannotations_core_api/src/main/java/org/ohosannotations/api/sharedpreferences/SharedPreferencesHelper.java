/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.sharedpreferences;

import ohos.app.Context;
import ohos.data.preferences.Preferences;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.Set;

/**
 * SharedPreferencesHelper
 *
 * @since 2021-07-20
 */
public abstract class SharedPreferencesHelper {
    private final Preferences sharedPreferences;

    /**
     * 构造参数
     *
     * @param sharedPreferences 预设参数
     */
    public SharedPreferencesHelper(Preferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    /**
     * 获得共同的喜好
     *
     * @return {@link Preferences}
     */
    public final Preferences getSharedPreferences() {
        return sharedPreferences;
    }

    /**
     * 清除
     */
    public final void clear() {
        sharedPreferences.clear();
        SharedPreferencesCompat.apply(sharedPreferences);
    }

    /**
     * intField
     *
     * @param key 键
     * @param defaultValue 默认值
     * @return IntPrefField 整数类型字段
     */
    protected IntPrefField intField(String key, int defaultValue) {
        return new IntPrefField(sharedPreferences, key, defaultValue);
    }

    /**
     * stringField
     *
     * @param key 键
     * @param defaultValue 默认值
     * @return StringPrefField 字符串字段
     */
    protected StringPrefField stringField(String key, String defaultValue) {
        return new StringPrefField(sharedPreferences, key, defaultValue);
    }

    /**
     * stringSetField
     *
     * @param key 键
     * @param defaultValue 默认值
     * @return StringSetPrefField 字符串字段
     */
    protected StringSetPrefField stringSetField(String key, Set<String> defaultValue) {
        return new StringSetPrefField(sharedPreferences, key, defaultValue);
    }

    /**
     * booleanField
     *
     * @param key 键
     * @param defaultValue 默认值
     * @return BooleanPrefField 布尔字段
     */
    protected BooleanPrefField booleanField(String key, boolean defaultValue) {
        return new BooleanPrefField(sharedPreferences, key, defaultValue);
    }

    /**
     * floatField
     *
     * @param key 键
     * @param defaultValue 默认值
     * @return FloatPrefField 浮点字段
     */
    protected FloatPrefField floatField(String key, float defaultValue) {
        return new FloatPrefField(sharedPreferences, key, defaultValue);
    }

    /**
     * longField
     *
     * @param key 键
     * @param defaultValue 默认值
     * @return LongPrefField long字段
     */
    protected LongPrefField longField(String key, long defaultValue) {
        return new LongPrefField(sharedPreferences, key, defaultValue);
    }

    /**
     * getFloat
     *
     * @param context 上下文
     * @param RId 资源Id
     * @return float 浮点类型
     */
    protected float getFloat(Context context, int RId) {
        float value = 0;
        try {
            value = context.getResourceManager().getElement(RId).getFloat();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * getBoolean
     *
     * @param context context
     * @param RId RId
     * @return value
     */
    protected boolean getBoolean(Context context, int RId) {
        boolean value = false;
        try {
            value = context.getResourceManager().getElement(RId).getBoolean();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * getInteger
     *
     * @param context context
     * @param RId RId
     * @return value
     */
    protected int getInteger(Context context, int RId) {
        int value = 0;
        try {
            value = context.getResourceManager().getElement(RId).getInteger();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
}
