/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.sharedpreferences;

import ohos.data.preferences.Preferences;

import java.util.Set;

/**
 * 字符串设置前缀字段
 *
 * @since 2021-07-20
 */
public final class StringSetPrefField extends AbstractPrefField<Set<String>> {
    /**
     * 构造参数
     *
     * @param sharedPreferences 预设参数
     * @param key 键
     * @param defaultValue 默认值
     */
    StringSetPrefField(Preferences sharedPreferences, String key, Set<String> defaultValue) {
        super(sharedPreferences, key, defaultValue);
    }

    @Override
    public Set<String> getOr(Set<String> defaultValue) {
        return SharedPreferencesCompat.getStringSet(edit(), key, defaultValue);
    }

    @Override
    protected void putInternal(Set<String> value) {
        SharedPreferencesCompat.putStringSet(edit(), key, value);
        apply(edit());
    }
}
