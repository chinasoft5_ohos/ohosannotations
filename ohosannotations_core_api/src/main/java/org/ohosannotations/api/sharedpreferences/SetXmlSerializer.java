/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.sharedpreferences;

import java.io.StringWriter;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * 设置XMl序列化
 *
 * @since 2021-07-20
 */
public final class SetXmlSerializer {
    private static final String NAMESPACE = "";
    private static final String STRING_TAG = "AA_string";
    private static final String SET_TAG = "AA_set";

    private SetXmlSerializer() {
    }

    /**
     * 序列化
     *
     * @param set 字符串集合
     * @return String 字符串
     */
    public static String serialize(Set<String> set) {
        StringWriter writer = new StringWriter();
        return writer.toString();
    }

    /**
     * 反序列
     *
     * @param data 字符串
     * @return Set 集合
     */
    public static Set<String> deserialize(String data) {
        Set<String> stringSet = new TreeSet<>();
        return stringSet;
    }
}
