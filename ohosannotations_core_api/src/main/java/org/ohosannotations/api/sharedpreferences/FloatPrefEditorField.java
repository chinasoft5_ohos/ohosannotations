/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.sharedpreferences;

/**
 * 浮点类型编辑器字段
 *
 * @param <T> 泛型
 * @since 2021-07-19
 */
public final class FloatPrefEditorField<T extends EditorHelper<T>> extends AbstractPrefEditorField<T> {
    /**
     * 构造参数
     *
     * @param editorHelper 编辑助手
     * @param key 键
     */
    FloatPrefEditorField(T editorHelper, String key) {
        super(editorHelper, key);
    }

    /**
     * 存放
     *
     * @param value 存放的浮点值
     * @return T 泛型
     */
    public T put(float value) {
        editorHelper.getEditor().putFloat(key, value);
        return editorHelper;
    }
}
