/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.sharedpreferences;

import ohos.data.preferences.Preferences;

/**
 * 整数字段
 *
 * @since 2021-07-19
 */
public final class IntPrefField extends AbstractPrefField<Integer> {
    /**
     * 构造参数
     *
     * @param sharedPreferences 预设参数
     * @param key 键
     * @param defaultValue 默认值
     */
    IntPrefField(Preferences sharedPreferences, String key, Integer defaultValue) {
        super(sharedPreferences, key, defaultValue);
    }

    @Override
    public Integer getOr(Integer defaultValue) {
        try {
            return edit().getInt(key, defaultValue);
        } catch (ClassCastException e) {
            try {
                String value = edit().getString(key, "" + defaultValue);
                return Integer.parseInt(value);
            } catch (Exception e2) {
                throw e;
            }
        }
    }

    @Override
    protected void putInternal(Integer value) {
        edit().putInt(key, value);
        apply(edit());
    }
}
