/**
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.bean;

/**
 * BeanHolder
 *
 * @author dev
 * @since 2021-06-02
 */
public interface BeanHolder {
    /**
     * getBean
     *
     * @param key 关键
     * @param <T> 子类泛型
     * @return {@link T}
     */
    <T> T getBean(Class<T> key);

    /**
     * putBean
     *
     * @param key 关键
     * @param <T> 子类泛型
     * @param value 价值
     */
    <T> void putBean(Class<T> key, T value);
}
