/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.builder;

import ohos.aafwk.ability.Ability;
import ohos.app.Context;

/**
 * Ability 启动器
 *
 * @since 2021-06-03
 */
public class PostAbilityStarter {
    private Context context;

    /**
     * 构造参数
     *
     * @param context 上下文
     */
    public PostAbilityStarter(Context context) {
        this.context = context;
    }

    /**
     * 动画
     *
     * @param enterAnim 输入动画
     * @param exitAnim 出口动画
     */
    public void withAnimation(int enterAnim, int exitAnim) {
        if (context instanceof Ability) {
            ((Ability) context).setTransitionAnimation(enterAnim, exitAnim);
        }
    }
}
