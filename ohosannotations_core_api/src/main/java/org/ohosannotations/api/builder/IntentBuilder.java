/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.builder;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 意图创建
 *
 * @param <I>
 * @since 2021-06-18
 */
public abstract class IntentBuilder<I extends IntentBuilder<I>> extends Builder {
    /**
     * 上下文
     */
    protected final Context context;
    /**
     * 意图
     */
    protected final Intent intent;

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param clazz clazz
     */
    public IntentBuilder(Context context, Class<?> clazz) {
        this.context = context;
        this.intent = initIntent(context, clazz);
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param intent 意图
     */
    public IntentBuilder(Context context, Intent intent) {
        this.context = context;
        this.intent = intent;
    }

    /**
     * init的意图
     *
     * @param con 反对
     * @param clazz clazz
     * @return {@link Intent}
     */
    private Intent initIntent(Context con, Class<?> clazz) {
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
            .withDeviceId("")
            .withBundleName(con.getBundleName()) // 包名
            .withAbilityName(clazz) // 跳转的ability
            .build();
        intent1.setOperation(operation);
        return intent1;
    }

    /**
     * 获得上下文
     *
     * @return {@link Context}
     */
    public Context getContext() {
        return context;
    }

    /**
     * 获取意图
     *
     * @return Intent
     */
    public Intent get() {
        return intent;
    }

    /**
     * 设置flags
     *
     * @param flags 旗帜
     * @return I
     */
    public I flags(int flags) {
        if (intent.getOperation() != null) {
            ((Intent.OperationBuilder) intent.getOperation()).withFlags(flags);
        } else {
            intent.setFlags(flags);
        }
        return (I) this;
    }

    /**
     * 设置意图动作
     *
     * @param action 行动
     * @return I
     */
    public I action(String action) {
        if (intent.getOperation() != null) {
            ((Intent.OperationBuilder) intent.getOperation()).withAction(action);
        } else {
            intent.setAction(action);
        }
        return (I) this;
    }

    /**
     * 设置类型
     *
     * @param type 类型
     * @return I
     */
    public I type(String type) {
        intent.setType(type);
        return (I) this;
    }

    /**
     * 添加种类
     *
     * @param category 类别
     * @return I
     */
    public I category(String category) {
        intent.addEntity(category);
        return (I) this;
    }

    /**
     * 设置数据
     *
     * @param data 数据
     * @return I
     */
    public I data(Uri data) {
        if (intent.getOperation() != null) {
            ((Intent.OperationBuilder) intent.getOperation()).withUri(data);
        } else {
            intent.setUri(data);
        }
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param isValue 值
     * @return I
     */
    public I extra(String name, boolean isValue) {
        intent.setParam(name, isValue);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, byte value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, char value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, short value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, int value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, long value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, float value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, double value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, String value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, CharSequence value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, Sequenceable value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, Sequenceable[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, Serializable value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, boolean[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, byte[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, short[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, char[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, int[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, long[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, float[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, double[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I extra(String name, String[] value) {
        intent.setParam(name, value);
        return (I) this;
    }

    /**
     * 设置Bundle参数
     *
     * @param name 名字
     * @return I
     */
    public I extra(String name) {
        intent.setBundle(name);
        return (I) this;
    }

    /**
     * 替换参数
     *
     * @param src src
     * @return I
     */
    public I extras(Intent src) {
        intent.replaceParams(src);
        return (I) this;
    }

    /**
     * 设置数组参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I parcelableArrayListExtra(String name, ArrayList<? extends Sequenceable> value) {
        intent.setSequenceableArrayListParam(name, value);
        return (I) this;
    }

    /**
     * 设置整数类型数据参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I integerArrayListExtra(String name, ArrayList<Integer> value) {
        intent.setIntegerArrayListParam(name, value);
        return (I) this;
    }

    /**
     * 设置字符串数组参数
     *
     * @param name 名字
     * @param value 值
     * @return I
     */
    public I stringArrayListExtra(String name, ArrayList<String> value) {
        intent.setStringArrayListParam(name, value);
        return (I) this;
    }
}
