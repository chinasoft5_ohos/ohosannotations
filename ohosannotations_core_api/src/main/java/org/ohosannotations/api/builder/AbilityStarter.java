/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.builder;

/**
 * Ability 启动器
 *
 * @since 2021-06-03
 */
public interface AbilityStarter {
    /**
     * 启动
     *
     * @return PostAbilityStarter
     */
    PostAbilityStarter start();

    /**
     * 启动Ability结果回调
     *
     * @param requestCode 请求码
     * @return PostAbilityStarter
     */
    PostAbilityStarter startForResult(int requestCode);
}
