/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.builder;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;

/**
 * 服务意图创建
 *
 * @param <I> 子类泛型
 * @since 2021-06-03
 */
public abstract class ServiceIntentBuilder<I extends ServiceIntentBuilder<I>> extends IntentBuilder<I> {
    /**
     * 构造参数
     *
     * @param context 上下文
     * @param clazz clazz
     */
    public ServiceIntentBuilder(Context context, Class<?> clazz) {
        super(context, clazz);
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param intent 意图
     */
    public ServiceIntentBuilder(Context context, Intent intent) {
        super(context, intent);
    }

    /**
     * 开始服务
     */
    public void start() {
        ((Ability) context).startAbility(intent);
    }

    /**
     * 停止服务
     *
     * @return boolean布尔值
     */
    public boolean stop() {
        return context.stopAbility(intent);
    }
}
