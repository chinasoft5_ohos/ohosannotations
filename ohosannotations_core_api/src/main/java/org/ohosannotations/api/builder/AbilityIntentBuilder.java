/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.builder;

import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.utils.PacMap;

/**
 * Ability意图创建
 *
 * @param <I>
 * @since 2021-06-03
 */
public abstract class AbilityIntentBuilder<I extends AbilityIntentBuilder<I>> extends IntentBuilder<I>
    implements AbilityStarter {
    private static final int CONSTANT = Integer.MAX_VALUE;
    /**
     * 包装类
     */
    protected PacMap lastOptions;

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param clazz clazz
     */
    public AbilityIntentBuilder(Context context, Class<?> clazz) {
        super(context, clazz);
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param intent 意图
     */
    public AbilityIntentBuilder(Context context, Intent intent) {
        super(context, intent);
    }

    @Override
    public PostAbilityStarter start() {
        startForResult(CONSTANT);
        return new PostAbilityStarter(context);
    }

    @Override
    public abstract PostAbilityStarter startForResult(int requestCode);

    /**
     * 配置选项
     *
     * @param options 选项
     * @return AbilityStarter
     */
    public AbilityStarter withOptions(PacMap options) {
        lastOptions = options;
        return this;
    }
}
