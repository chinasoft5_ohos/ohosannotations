/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.api.builder;

import ohos.utils.PacMap;
import ohos.utils.PlainArray;
import ohos.utils.Sequenceable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 分数建设者
 *
 * @param <I> 子类
 * @param <F> 子类
 * @since 2021-07-20
 */
public abstract class FractionBuilder<I extends FractionBuilder<I, F>, F> extends Builder {
    /**
     * PacMap
     */
    protected PacMap args;

    /**
     * 分数建设者
     */
    public FractionBuilder() {
        args = new PacMap();
    }

    /**
     * 构建
     *
     * @return {@link F}
     */
    public abstract F build();

    /**
     * 参数
     *
     * @param map 地图
     * @return {@link I}
     */
    public I arg(PacMap map) {
        args.putAll(map);
        return (I) this;
    }

    /**
     * 参数
     *
     * @param key 关键
     * @param value 价值
     * @return {@link I}
     */
    public I arg(String key, boolean value) {
        args.putBooleanValue(key, value);
        return (I) this;
    }

    /**
     * 参数
     *
     * @param key 关键
     * @param value 价值
     * @return {@link I}
     */
    public I arg(String key, byte value) {
        args.putByteValue(key, value);
        return (I) this;
    }

    /**
     * 参数
     *
     * @param key 关键
     * @param value 价值
     * @return {@link I}
     */
    public I arg(String key, char value) {
        args.putChar(key, value);
        return (I) this;
    }

    /**
     * 参数
     *
     * @param key 关键
     * @param value 价值
     * @return {@link I}
     */
    public I arg(String key, short value) {
        args.putShortValue(key, value);
        return (I) this;
    }

    /**
     * 参数
     *
     * @param key 关键
     * @param value 价值
     * @return {@link I}
     */
    public I arg(String key, int value) {
        args.putIntValue(key, value);
        return (I) this;
    }

    /**
     * 参数
     *
     * @param key 关键
     * @param value 价值
     * @return {@link I}
     */
    public I arg(String key, long value) {
        args.putLongValue(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, float value) {
        args.putFloatValue(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, double value) {
        args.putDoubleValue(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, String value) {
        args.putString(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, Sequenceable value) {
        args.putSequenceableObject(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, Sequenceable[] value) {
        args.putSequenceableObjectArray(key, value);
        return (I) this;
    }

    /**
     * parcelable数组列表参数
     * parcelableArrayListArg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I parcelableArrayListArg(String key, ArrayList<Sequenceable> value) {
        args.putSequenceableObjectList(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */

    public I arg(String key, PlainArray<? extends Sequenceable> value) {
        args.putPlainArray(key, value);
        return (I) this;
    }

    /**
     * 整数数组列表参数
     * integerArrayListArg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I integerArrayListArg(String key, ArrayList<Integer> value) {
        args.putIntegerList(key, value);
        return (I) this;
    }

    /**
     * 字符串数组列表参数
     * stringArrayListArg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I stringArrayListArg(String key, ArrayList<String> value) {
        args.putStringList(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, Serializable value) {
        args.putSerializableObject(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, boolean[] value) {
        args.putBooleanValueArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, byte[] value) {
        args.putByteValueArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */

    public I arg(String key, short[] value) {
        args.putShortValueArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, char[] value) {
        args.putCharArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, int[] value) {
        args.putIntValueArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, long[] value) {
        args.putLongValueArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, float[] value) {
        args.putFloatValueArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, double[] value) {
        args.putDoubleValueArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, String[] value) {
        args.putStringArray(key, value);
        return (I) this;
    }

    /**
     * 参数
     * arg
     *
     * @param key key
     * @param value value
     * @return this
     */
    public I arg(String key, PacMap value) {
        args.putPacMap(key, value);
        return (I) this;
    }

    /**
     * arg游戏
     * args
     *
     * @return args
     */
    public PacMap args() {
        return args;
    }
}
