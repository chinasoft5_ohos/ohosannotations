/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.otto;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.handler.AnnotationHandler;
import org.ohosannotations.otto.handler.ProduceHandler;
import org.ohosannotations.otto.handler.SubscribeHandler;
import org.ohosannotations.plugin.OhosAnnotationsPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * @since 2021-06-04
 */
public class OttoPlugin extends OhosAnnotationsPlugin {
    private static final String NAME = "Otto";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public boolean shouldCheckApiAndProcessorVersions() {
        return false;
    }

    @Override
    public List<AnnotationHandler<?>> getHandlers(OhosAnnotationsEnvironment ohosAnnotationEnv) {
        List<AnnotationHandler<?>> annotationHandlers = new ArrayList<>();
        annotationHandlers.add(new SubscribeHandler(ohosAnnotationEnv));
        annotationHandlers.add(new ProduceHandler(ohosAnnotationEnv));
        return annotationHandlers;
    }
}
