/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.otto.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.ValidatorParameterHelper;
import org.ohosannotations.holder.EComponentHolder;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

import com.helger.jcodemodel.JMethod;

/**
 * 文摘奥托处理程序
 *
 * @author dev
 * @since 2021-06-04
 */
public abstract class AbstractOttoHandler extends BaseAnnotationHandler<EComponentHolder> {
    /**
     * 文摘奥托处理程序
     *
     * @param target 目标
     * @param environment 环境
     */
    public AbstractOttoHandler(String target, OhosAnnotationsEnvironment environment) {
        super(target, environment);
    }

    @Override
    public void validate(Element element, ElementValidation valid) {
        if (!annotationHelper.enclosingElementHasEnhancedComponentAnnotation(element)) {
            // do nothing when otto annotations are used in non-enhanced classes
            return;
        }

        ExecutableElement executableElement = (ExecutableElement) element;

        validateReturnType(executableElement, valid);

        validatorHelper.isPublic(element, valid);

        validatorHelper.doesntThrowException(executableElement, valid);

        validatorHelper.isNotFinal(element, valid);

        getParamValidator().validate(executableElement, valid);
    }

    /**
     * 得到参数验证器
     *
     * @return {@link ValidatorParameterHelper.Validator}
     */
    protected abstract ValidatorParameterHelper.Validator getParamValidator();

    /**
     * 验证返回类型
     *
     * @param executableElement 可执行元素
     * @param validation 验证
     */
    protected abstract void validateReturnType(ExecutableElement executableElement, ElementValidation validation);

    @Override
    public void process(Element element, EComponentHolder holder) throws Exception {
        if (!annotationHelper.enclosingElementHasEnhancedComponentAnnotation(element)) {
            // do nothing when otto annotations are used in non-enhanced classes
            return;
        }
        ExecutableElement executableElement = (ExecutableElement) element;

        JMethod method = codeModelHelper.overrideAnnotatedMethod(executableElement, holder);

        addOttoAnnotation(executableElement, method);
    }

    private void addOttoAnnotation(ExecutableElement element, JMethod method) {
        for (AnnotationMirror annotationMirror : element.getAnnotationMirrors()) {
            if (annotationMirror.getAnnotationType().toString().equals(getTarget())) {
                codeModelHelper.copyAnnotation(method, annotationMirror);
                break;
            }
        }
    }
}
