/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.otto.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.helper.ValidatorParameterHelper;
import org.ohosannotations.otto.helper.OttoClasses;

import javax.lang.model.element.ExecutableElement;

/**
 * @since 2021-06-04
 */
public class SubscribeHandler extends AbstractOttoHandler {
    public SubscribeHandler(OhosAnnotationsEnvironment environment) {
        super(OttoClasses.SUBSCRIBE, environment);
    }

    @Override
    protected ValidatorParameterHelper.Validator getParamValidator() {
        return validatorHelper.param.anyType();
    }

    @Override
    protected void validateReturnType(ExecutableElement executableElement, ElementValidation validation) {
        validatorHelper.returnTypeIsVoid(executableElement, validation);
    }
}
