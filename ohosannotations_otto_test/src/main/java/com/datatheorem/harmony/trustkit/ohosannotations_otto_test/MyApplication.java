package com.datatheorem.harmony.trustkit.ohosannotations_otto_test;

import ohos.aafwk.ability.AbilityPackage;

import com.chinasoft_ohos.commontools.toast.Toast;

/**
 * 我的应用程序
 *
 * @author dev
 * @since 2021-07-26
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        Toast.init(getContext()); // 吐司初始化
    }
}
