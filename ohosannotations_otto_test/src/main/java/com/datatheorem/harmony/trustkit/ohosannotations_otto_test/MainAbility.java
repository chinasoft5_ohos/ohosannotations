/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datatheorem.harmony.trustkit.ohosannotations_otto_test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import com.chinasoft_ohos.commontools.toast.Toast;

/**
 * @since 2021-06-19
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_otto);
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
            .withDeviceId("")
            .withBundleName("com.datatheorem.harmony.trustkit.ohosannotations_otto_test")
            .withAbilityName("com.datatheorem.harmony.trustkit.ohosannotations_otto_test.OttoAbility_")
            .build();
        // 把operation设置到intent中
        intent1.setOperation(operation);
        startAbility(intent1);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}