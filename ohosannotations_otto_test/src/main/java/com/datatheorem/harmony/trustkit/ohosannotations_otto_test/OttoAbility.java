/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datatheorem.harmony.trustkit.ohosannotations_otto_test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import com.chinasoft_ohos.commontools.toast.Toast;
import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import org.ohosannotations.annotations.EAbility;

import java.util.logging.Logger;

/**
 * OttoAbility
 *
 * @author dev
 * @since 2021-07-22
 */
@EAbility(ResourceTable.Layout_ability_otto)
public class OttoAbility extends Ability {
    private Event lastEvent;

    /**
     * 在开始
     *
     * @param intent 意图
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        Bus bus = new Bus();
        bus.register(this);
    }

    /**
     * 在事件
     *
     * @param event 事件
     */
    @Subscribe
    public void onEvent(Event event) {
        lastEvent = event;
        Toast.show("onEvent"+lastEvent);
    }

    /**
     * 生产活动
     *
     * @return {@link Event}
     */
    @Produce
    public Event produceEvent() {
        return new Event();
    }

    /**
     * 在停止
     */
    @Override
    public void onStop() {
        super.onStop();
    }
}
