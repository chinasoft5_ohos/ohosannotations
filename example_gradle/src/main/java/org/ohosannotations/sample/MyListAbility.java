/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;

import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.ItemSelect;
import org.ohosannotations.annotations.res.StringArrayRes;

/**
 * 列表Ability
 *
 * @since 2021-06-08
 */
@EAbility(ResourceTable.Layout_my_list_ability_main)
public class MyListAbility extends Ability {
    @ComponentById
    ListContainer listContainer;

    @StringArrayRes
    String[] bestFoods;

    private ListAdapter adapter;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @AfterViews
    void initListContainer() {
        adapter = new ListAdapter(this, bestFoods);
        listContainer.setItemProvider(adapter);
    }

    @ItemSelect(ResourceTable.Id_listContainer)
    void listItemSelected(boolean isSomethingSelected, String food) {
    }
}
