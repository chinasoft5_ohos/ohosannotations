/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * APP入口Ability 暂时不能使用注解，ohos问题，已提交工单 2021/6/10 15:20
 *
 * @since 2021-06-10
 */
public class LaunchAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_launch);
        Intent intent1 = new Intent();
        intent1.setOperation(new Intent.OperationBuilder()
            .withBundleName(BuildConfig.BUNDLE_NAME)
            .withAbilityName(BuildConfig.BUNDLE_NAME + ".MyAbility_").build());
        startAbility(intent1);
        terminateAbility();
    }
}
