package org.ohosannotations.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.bundle.AbilityInfo;
import ohos.global.configuration.Configuration;
import ohos.utils.PacMap;

import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.EAbility;

import timber.log.Timber;

/**
 * MainAbility
 *
 * @since 2021-07-19
 */
@EAbility(ResourceTable.Layout_my_ability)
public class MainAbility extends Ability {
    int state;
    @ComponentById(ResourceTable.Id_myText)
    Text text;

    @AfterViews
    void setText() {
        state = 5;
        text.setText("inject Text succeed state:" + state);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        Timber.e("onSaveAbilityState");
        outState.putIntValue("state", state);
        super.onSaveAbilityState(outState);
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        Timber.e("onRestoreAbilityState");
        state = inState.getIntValue("state");
        super.onRestoreAbilityState(inState);
    }

    @Override
    public Object onStoreDataWhenConfigChange() {
        Timber.e("onStoreDataWhenConfigChange");
        return super.onStoreDataWhenConfigChange();
    }

    @Override
    public Object getLastStoredDataWhenConfigChanged() {
        Timber.e("getLastStoredDataWhenConfigChanged");
        return super.getLastStoredDataWhenConfigChanged();
    }

    @Override
    public void onConfigurationUpdated(Configuration configuration) {
        Timber.e("onConfigurationUpdated");
        super.onConfigurationUpdated(configuration);
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        Timber.e("onOrientationChanged");
        super.onOrientationChanged(displayOrientation);
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void setIntent(Intent intent) {
        super.setIntent(intent);
    }
}
