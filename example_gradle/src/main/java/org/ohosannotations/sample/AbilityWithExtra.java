package org.ohosannotations.sample;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Text;

import org.ohosannotations.annotations.AfterViews;
import org.ohosannotations.annotations.ComponentById;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.Extra;

import java.util.Date;

/**
 * Ability注解
 *
 * @since 2021-07-19
 */
@EAbility(ResourceTable.Layout_ability_with_extra)
public class AbilityWithExtra extends Ability {
    /**
     * 字符串类型标签
     */
    public static final String MY_STRING_EXTRA = "myStringExtra";
    /**
     * 日期类型标签
     */
    public static final String MY_DATE_EXTRA = "myDateExtra";
    /**
     * 整数类型标签
     */
    public static final String MY_INT_EXTRA = "myIntExtra";

    @ComponentById
    Text extraTextView;

    @Extra(MY_STRING_EXTRA)
    String myMessage;

    @Extra(MY_DATE_EXTRA)
    Date myDate;

    @Extra("unboundExtra")
    String unboundExtra = "unboundExtraDefaultValue";

    /**
     * The logs will output a classcast exception, but the program flow won't be interrupted
     */
    @Extra(MY_INT_EXTRA)
    String classCastExceptionExtra = "classCastExceptionExtraDefaultValue";

    /**
     * 初始化视图
     */
    @AfterViews
    protected void initView() {
        extraTextView.setText(myMessage + " " + myDate
            + System.lineSeparator() + unboundExtra + " " + classCastExceptionExtra);
    }
}