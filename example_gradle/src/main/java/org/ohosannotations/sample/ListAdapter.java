/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.sample;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.muddzdev.styleabletoast.styleabletoast.StyleableToast;

/**
 * 列表适配器
 *
 * @since 2021-06-08
 */
public class ListAdapter extends BaseItemProvider {
    private static final int COLOR = 0xf2f2f2;
    private Context context;
    private String[] bestFoods;

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param bestFoods 最好的食物
     */
    public ListAdapter(Context context, String[] bestFoods) {
        this.context = context;
        this.bestFoods = bestFoods.clone();
    }

    @Override
    public int getCount() {
        return bestFoods == null ? 0 : bestFoods.length;
    }

    @Override
    public String getItem(int position) {
        return bestFoods[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder holder;
        Component com = component;
        if (com == null) {
            com = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_simple_list_item_1, null, false);
            holder = new ViewHolder(com);
            com.setTag(holder);
        } else {
            holder = (ViewHolder) com.getTag();
        }
        holder.text.setText(bestFoods[position]);
        holder.text.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new StyleableToast.Builder(context)
                    .backgroundColor(new Color(COLOR))
                    .textColor(Color.BLACK)
                    .text("click: " + bestFoods[position])
                    .show();
            }
        });
        holder.text.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                new StyleableToast.Builder(context)
                    .backgroundColor(new Color(COLOR))
                    .textColor(Color.BLACK)
                    .text("long click: " + bestFoods[position])
                    .show();
            }
        });
        return com;
    }

    /**
     * viewholder
     *
     * @since 2021-06-18
     */
    static class ViewHolder {
        private Text text;

        /**
         * 构造参数
         *
         * @param component 组件
         */
        ViewHolder(Component component) {
            text = (Text) component.findComponentById(ResourceTable.Id_text1);
        }
    }
}
