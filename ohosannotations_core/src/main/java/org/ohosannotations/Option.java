/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations;

/**
 * 选项
 *
 * @since 2021-07-19
 */
public class Option {
    private final String name;
    private final String defaultValue;

    /**
     * 构造参数
     *
     * @param name 名字
     * @param defaultValue 默认值
     */
    public Option(String name, String defaultValue) {
        this.name = name;
        this.defaultValue = defaultValue;
    }

    /**
     * 获取名字
     *
     * @return String名字
     */
    public String getName() {
        return name;
    }

    /**
     * 获取默认值
     *
     * @return String 默认值
     */
    public String getDefaultValue() {
        return defaultValue;
    }
}
