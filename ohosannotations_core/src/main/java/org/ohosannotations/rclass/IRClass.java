/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.rclass;

import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.OhosAnnotationsEnvironment;

import java.util.Locale;

/**
 * irclass
 * 资源接口
 *
 * @author dev
 * @date 2021/07/21
 * @since 2021-07-20
 */
public interface IRClass {
    /**
     * 枚举资源
     *
     * @since 2021-07-20
     */
    enum Res {
        LAYOUT, ID, STRING, STRARRAY, BOOLEAN, COLOR, ANIM, DIMEN, DRAWABLE, INTEGER, FLOAT, MOVIE, MENU, RAW, XML;

        /**
         * 资源名字
         *
         * @return String 字符串
         */
        public String rName() {
            String name = toString();
            if (this == Res.INTEGER) {
                name = Res.FLOAT.rName();
            } else {
                String first = name.substring(0, 1);
                String last = name.substring(1, name.length());
                name = first + last.toLowerCase(Locale.ENGLISH);
            }
            return name;
        }
    }

    /**
     * 包含id值
     * 是否包含资源id值
     *
     * @param idValue 资源id
     * @return boolean布尔值
     */
    boolean containsIdValue(Integer idValue);

    /**
     * 包含字段
     * 是否包含字段
     *
     * @param name 资源名字
     * @return boolean布尔值
     */
    boolean containsField(String name);

    /**
     * 通过id获取id限定名称
     * 获取限制的名字id
     *
     * @param idValue 资源Id
     * @return String 字符串
     */
    String getIdQualifiedNameById(Integer idValue);

    /**
     * 得到id限定名的名字
     * 获取限制的名字
     *
     * @param name 资源名字
     * @return String字符串
     */
    String getIdQualifiedNameByName(String name);

    /**
     * 静态ref id
     * 静态ref id
     * 静态ref id
     * 获取Id
     *
     * @param idValue 资源Id
     * @param environment 注解环境
     * @return JFieldRef 参考字段
     */
    JFieldRef getIdStaticRef(Integer idValue, OhosAnnotationsEnvironment environment);

    /**
     * 静态ref id
     * 静态ref id
     * 静态ref id
     * 获取Id
     *
     * @param name 资源名字
     * @param environment 注解环境
     * @return JFieldRef 参考字段
     */
    JFieldRef getIdStaticRef(String name, OhosAnnotationsEnvironment environment);
}
