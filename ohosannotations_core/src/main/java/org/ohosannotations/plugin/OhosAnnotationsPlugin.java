/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.plugin;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.Option;
import org.ohosannotations.handler.AnnotationHandler;
import org.ohosannotations.internal.exception.VersionNotFoundException;
import org.ohosannotations.logger.Logger;
import org.ohosannotations.logger.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * 嗳哟注释插件
 *
 * @author dev
 * @since 2021-07-22
 */
public abstract class OhosAnnotationsPlugin {
    private static final Logger LOGGER = LoggerFactory.getLogger(OhosAnnotationsPlugin.class);
    private String version;
    private String apiVersion;

    /**
     * 得到的名字
     *
     * @return {@link String}
     */
    public abstract String getName();

    /**
     * 得到处理程序
     *
     * @param annotationEnv 注释env
     * @return {@link List}
     */
    public abstract List<AnnotationHandler<?>> getHandlers(OhosAnnotationsEnvironment annotationEnv);

    /**
     * 字符串
     *
     * @return {@link String}
     */
    @Override
    public String toString() {
        return getName();
    }

    /**
     * 得到支持选项
     *
     * @return {@link List}
     */
    public List<Option> getSupportedOptions() {
        return Collections.emptyList();
    }

    /**
     * 应该检查api和处理器版本
     *
     * @return boolean
     */
    public boolean shouldCheckApiAndProcessorVersions() {
        return true;
    }

    /**
     * 加载的版本
     *
     * @throws FileNotFoundException 文件未发现异常
     * @throws VersionNotFoundException 版本没有发现异常
     */
    public final void loadVersion() throws FileNotFoundException, VersionNotFoundException {
        version = getVersionFromPropertyFile(getName().toLowerCase(Locale.ENGLISH));
        apiVersion = getVersionFromPropertyFile(getName().toLowerCase(Locale.ENGLISH) + "-api");
    }

    /**
     * 获得属性文件的版本
     *
     * @param name 的名字
     * @return {@link String}
     * @throws FileNotFoundException 文件未发现异常
     * @throws VersionNotFoundException 版本没有发现异常
     */
    private String getVersionFromPropertyFile(String name) throws FileNotFoundException, VersionNotFoundException {
        String filename = name + ".properties";
        Properties properties = null;
        try {
            URL url = getClass().getClassLoader().getResource(filename);
            properties = new Properties();
            properties.load(url.openStream());
        } catch (IOException e) {
            LOGGER.error("Property file {} couldn't be parsed", filename);
            return "";
        }

        String version1 = properties.getProperty("version");

        if (version1.isEmpty()) {
            LOGGER.error("{} plugin is missing 'version' property!", getName());
            return "";
        }

        return version1;
    }

    /**
     * 获得api版本
     *
     * @return {@link String}
     */
    public String getApiVersion() {
        return apiVersion;
    }

    /**
     * 获得版本
     *
     * @return {@link String}
     */
    public String getVersion() {
        return version;
    }
}

