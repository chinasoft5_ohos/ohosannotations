/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.plugin;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JCodeModel;
import com.helger.jcodemodel.JDefinedClass;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.holder.GeneratedClassHolder;

import javax.lang.model.element.TypeElement;

/**
 * PluginClassHolder
 *
 * @param <H> 子类
 * @since 2021-07-20
 */
public class PluginClassHolder<H extends GeneratedClassHolder> {
    private H holder;

    /**
     * 构造参数
     *
     * @param holder 持有者
     */
    public PluginClassHolder(H holder) {
        this.holder = holder;
    }

    /**
     * holder
     *
     * @return H 子类泛型
     */
    public H holder() {
        return holder;
    }

    /**
     * getGeneratedClass
     *
     * @return JDefinedClass 定义类
     */
    public JDefinedClass getGeneratedClass() {
        return holder.getGeneratedClass();
    }

    /**
     * getAnnotatedElement
     *
     * @return 类型元素
     */
    public TypeElement getAnnotatedElement() {
        return holder.getAnnotatedElement();
    }

    /**
     * environment
     *
     * @return OhosAnnotationsEnvironment 注解环境
     */
    public OhosAnnotationsEnvironment environment() {
        return holder().getEnvironment();
    }

    /**
     * getJClass
     *
     * @param fullyQualifiedClassName 字节类名
     * @return AbstractJClass 抽象类型
     */
    protected AbstractJClass getJClass(String fullyQualifiedClassName) {
        return environment().getJClass(fullyQualifiedClassName);
    }

    /**
     * getJClass
     *
     * @param clazz 字节类
     * @return AbstractJClass 抽象类型
     */
    protected AbstractJClass getJClass(Class<?> clazz) {
        return environment().getJClass(clazz);
    }

    /**
     * getCodeModel
     *
     * @return JCodeModel 代码模型
     */
    protected JCodeModel getCodeModel() {
        return environment().getCodeModel();
    }
}
