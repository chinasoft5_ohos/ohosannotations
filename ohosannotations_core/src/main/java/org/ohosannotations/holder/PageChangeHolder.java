/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JPrimitiveType;
import com.helger.jcodemodel.JVar;

/**
 * 页面改变Holder
 *
 * @since 2021-07-20
 */
public class PageChangeHolder {
    private EComponentWithViewSupportHolder holder;
    private JVar viewPagerVariable;
    private JDefinedClass listenerClass;
    private JBlock pageScrollStateChangedBody;
    private JVar pageScrollStateChangedStateParam;
    private JBlock pageScrolledBody;
    private JVar pageScrolledPositionParam;
    private JVar pageScrolledPositionOffsetParam;
    private JVar pageScrolledPositionOffsetPixelsParam;
    private JBlock pageSelectedBody;
    private JVar pageSelectedPositionParam;

    /**
     * 构造参数
     *
     * @param holder 视图组件holder
     * @param viewPagerVariable 变量
     * @param onPageChangeListenerClass 页面更改侦听器类
     */
    public PageChangeHolder(EComponentWithViewSupportHolder holder,
        JVar viewPagerVariable, JDefinedClass onPageChangeListenerClass) {
        this.holder = holder;
        this.viewPagerVariable = viewPagerVariable;
        listenerClass = onPageChangeListenerClass;
        createPageScrollStateChanged();
        createPageScroll();
        createPageSelected();
    }

    private void createPageScrollStateChanged() {
        JMethod method = listenerClass.method(JMod.PUBLIC, holder.getCodeModel().VOID, "onPageSlideStateChanged");
        method.annotate(Override.class);
        pageScrollStateChangedBody = method.body();
        JPrimitiveType intClass = holder.getCodeModel().INT;
        pageScrollStateChangedStateParam = method.param(intClass, "state");
    }

    private void createPageScroll() {
        JMethod method = listenerClass.method(JMod.PUBLIC, holder.getCodeModel().VOID, "onPageSliding");
        method.annotate(Override.class);
        pageScrolledBody = method.body();
        JPrimitiveType intClass = holder.getCodeModel().INT;
        pageScrolledPositionParam = method.param(intClass, "position");
        pageScrolledPositionOffsetParam = method.param(holder.getCodeModel().FLOAT, "positionOffset");
        pageScrolledPositionOffsetPixelsParam = method.param(intClass, "positionOffsetPixels");
    }

    private void createPageSelected() {
        JMethod method = listenerClass.method(JMod.PUBLIC, holder.getCodeModel().VOID, "onPageChosen");
        method.annotate(Override.class);
        pageSelectedBody = method.body();
        JPrimitiveType intClass = holder.getCodeModel().INT;
        pageSelectedPositionParam = method.param(intClass, "position");
    }

    /**
     * 获取ViewPager变量
     *
     * @return JVar字段
     */
    public JVar getViewPagerVariable() {
        return viewPagerVariable;
    }

    /**
     * 获取监听类
     *
     * @return JDefinedClass 定义类
     */
    public JDefinedClass getListenerClass() {
        return listenerClass;
    }

    /**
     * 页面滚动状态改变主体
     *
     * @return JBlock J板块
     */
    public JBlock getPageScrollStateChangedBody() {
        return pageScrollStateChangedBody;
    }

    /**
     * 页面滚动改变状态参数
     *
     * @return JVar 字段
     */
    public JVar getPageScrollStateChangedStateParam() {
        return pageScrollStateChangedStateParam;
    }

    /**
     * 页面滚动主体
     *
     * @return JBlock J板块
     */
    public JBlock getPageScrolledBody() {
        return pageScrolledBody;
    }

    /**
     * 获取页面滚动位置参数
     *
     * @return JVar 字段
     */
    public JVar getPageScrolledPositionParam() {
        return pageScrolledPositionParam;
    }

    /**
     * 获取页面滚动位置偏移量参数
     *
     * @return JVar 字段
     */
    public JVar getPageScrolledPositionOffsetParam() {
        return pageScrolledPositionOffsetParam;
    }

    /**
     * 获取页面滚动位置偏移像素参数
     *
     * @return JVar 字段
     */
    public JVar getPageScrolledPositionOffsetPixelsParam() {
        return pageScrolledPositionOffsetPixelsParam;
    }

    /**
     * 获取页面选择主体
     *
     * @return JBlock J板块
     */
    public JBlock getPageSelectedBody() {
        return pageSelectedBody;
    }

    /**
     * 获取页面选择位置参数
     *
     * @return JVar 字段
     */
    public JVar getPageSelectedPositionParam() {
        return pageSelectedPositionParam;
    }
}
