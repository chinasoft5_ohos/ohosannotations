/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import static com.helger.jcodemodel.JExpr._this;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.helper.OhosManifest;
import org.ohosannotations.holder.ReceiverRegistrationDelegate.IntentFilterData;
import org.ohosannotations.internal.core.helper.IntentBuilder;
import org.ohosannotations.internal.core.helper.ServiceIntentBuilder;

import javax.lang.model.element.TypeElement;

/**
 * EServiceHolder
 *
 * @since 2021-06-09
 */
public class EServiceHolder extends EComponentHolder implements HasIntentBuilder, HasReceiverRegistration {
    private ServiceIntentBuilder intentBuilder;
    private JDefinedClass intentBuilderClass;
    private ReceiverRegistrationDelegate<EServiceHolder> receiverRegistrationDelegate;
    private JBlock onStartAfterSuperBlock;
    private JBlock onStopBeforeSuperBlock;

    /**
     * EServiceHolder
     *
     * @param environment 环境
     * @param annotatedElement 带注释的元素
     * @param ohosManifest 清单
     * @throws Exception 异常
     */
    public EServiceHolder(OhosAnnotationsEnvironment environment,
        TypeElement annotatedElement, OhosManifest ohosManifest) throws Exception {
        super(environment, annotatedElement);
        receiverRegistrationDelegate = new ReceiverRegistrationDelegate<>(this);
        intentBuilder = new ServiceIntentBuilder(this, ohosManifest);
        intentBuilder.build();
    }

    @Override
    public IntentBuilder getIntentBuilder() {
        return intentBuilder;
    }

    @Override
    protected void setContextRef() {
        contextRef = _this();
    }

    @Override
    protected void setInit() {
        init = generatedClass.method(PRIVATE, getCodeModel().VOID, "init" + generationSuffix());
        setOnStart();
    }

    private void setOnStart() {
        JMethod onStartMethod = generatedClass.method(PUBLIC, getCodeModel().VOID, "onStart");
        JVar intent = onStartMethod.param(getClasses().INTENT, "intent");
        onStartMethod.annotate(Override.class);
        JBlock onStartBody = onStartMethod.body();
        onStartBody.invoke(getInit());
        onStartBody.invoke(JExpr._super(), onStartMethod).arg(intent);
        onStartAfterSuperBlock = onStartBody.blockVirtual();
    }

    private void setOnStop() {
        JMethod onStop = generatedClass.method(PUBLIC, getCodeModel().VOID, "onStop");
        onStop.annotate(Override.class);
        JBlock onStopBody = onStop.body();
        onStopBeforeSuperBlock = onStopBody.blockSimple();
        onStopBody.invoke(JExpr._super(), onStop);
    }

    @Override
    public void setIntentBuilderClass(JDefinedClass intentBuilderClass) {
        this.intentBuilderClass = intentBuilderClass;
    }

    @Override
    public JDefinedClass getIntentBuilderClass() {
        return intentBuilderClass;
    }

    @Override
    public JFieldVar getIntentFilterField(IntentFilterData intentFilterData) {
        return receiverRegistrationDelegate.getIntentFilterField(intentFilterData);
    }

    @Override
    public JBlock getIntentFilterInitializationBlock(IntentFilterData intentFilterData) {
        return getInitBodyInjectionBlock();
    }

    @Override
    public JBlock getStartLifecycleAfterSuperBlock() {
        return onStartAfterSuperBlock;
    }

    @Override
    public JBlock getEndLifecycleBeforeSuperBlock() {
        if (onStopBeforeSuperBlock == null) {
            setOnStop();
        }

        return onStopBeforeSuperBlock;
    }
}
