/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JDefinedClass;

import org.ohosannotations.OhosAnnotationsEnvironment;

import javax.lang.model.element.TypeElement;

/**
 * 生成的类
 *
 * @since 2021-07-20
 */
public interface GeneratedClassHolder {
    /**
     * 获取生成的类
     *
     * @return JDefinedClass 定义类
     */
    JDefinedClass getGeneratedClass();

    /**
     * 注解元素
     *
     * @return 元素类型
     */
    TypeElement getAnnotatedElement();

    /**
     * 获取环境
     *
     * @return 环境
     */
    OhosAnnotationsEnvironment getEnvironment();
}
