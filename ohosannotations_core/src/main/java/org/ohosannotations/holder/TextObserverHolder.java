/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JPrimitiveType;
import com.helger.jcodemodel.JVar;

/**
 * @since 2021-06-04
 */
public class TextObserverHolder {
    private EComponentWithViewSupportHolder holder;
    private JVar textViewVariable;
    private JDefinedClass listenerClass;
    private JBlock textUpdatedBody;
    private JVar textUpdatedTextParam;
    private JVar textUpdatedStartParam;
    private JVar textUpdatedBeforeParam;
    private JVar textUpdatedCountParam;

    /**
     * 文本观察者持有人
     *
     * @param holder 持有人
     * @param viewVariable 查看变量
     * @param onTextChangeListenerClass 在文本更改侦听器类
     */
    public TextObserverHolder(EComponentWithViewSupportHolder holder,
        JVar viewVariable, JDefinedClass onTextChangeListenerClass) {
        this.holder = holder;
        textViewVariable = viewVariable;
        listenerClass = onTextChangeListenerClass;
        createBeforeTextChanged();
    }

    private void createBeforeTextChanged() {
        JPrimitiveType intClass = holder.getCodeModel().INT;
        JMethod beforeTextChangedMethod = listenerClass
            .method(JMod.PUBLIC, holder.getCodeModel().VOID, "onTextUpdated");
        beforeTextChangedMethod.annotate(Override.class);
        textUpdatedBody = beforeTextChangedMethod.body();
        textUpdatedTextParam = beforeTextChangedMethod.param(holder.getClasses().STRING, "s");
        textUpdatedStartParam = beforeTextChangedMethod.param(intClass, "start");
        textUpdatedBeforeParam = beforeTextChangedMethod.param(intClass, "before");
        textUpdatedCountParam = beforeTextChangedMethod.param(intClass, "count");
    }

    public JVar getTextViewVariable() {
        return textViewVariable;
    }

    public JBlock getTextUpdatedBody() {
        return textUpdatedBody;
    }

    public JVar getTextUpdatedTextParam() {
        return textUpdatedTextParam;
    }

    public JVar getTextUpdatedStartParam() {
        return textUpdatedStartParam;
    }

    public JVar getTextUpdatedBeforeParam() {
        return textUpdatedBeforeParam;
    }

    public JVar getTextUpdatedCountParam() {
        return textUpdatedCountParam;
    }
}
