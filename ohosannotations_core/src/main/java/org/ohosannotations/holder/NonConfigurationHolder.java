/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;


import static com.helger.jcodemodel.JMod.PUBLIC;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JClassAlreadyExistsException;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMod;

import static com.helger.jcodemodel.JMod.PUBLIC;

/**
 * 非配置持有人
 *
 * @author dev
 * @since 2021-06-03
 */
public class NonConfigurationHolder {
    private JDefinedClass generatedClass;
    private JFieldVar superNonConfigurationInstanceField;

    /**
     * 非配置持有人
     *
     * @param eAbilityHolder e能力持有者
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    public NonConfigurationHolder(EAbilityHolder eAbilityHolder) throws JClassAlreadyExistsException {
        setGeneratedClass(eAbilityHolder);
    }

    private void setGeneratedClass(EAbilityHolder eAbilityHolder) throws JClassAlreadyExistsException {
        generatedClass = eAbilityHolder.generatedClass
            ._class(JMod.PRIVATE | JMod.STATIC, "NonConfigurationInstancesHolder");
    }

    /**
     * 把生成的类
     *
     * @return {@link JDefinedClass}
     */
    public JDefinedClass getGeneratedClass() {
        return generatedClass;
    }

    /**
     * 获得超级非配置实例字段
     *
     * @return {@link JFieldVar}
     */
    public JFieldVar getSuperNonConfigurationInstanceField() {
        if (superNonConfigurationInstanceField == null) {
            setSuperNonConfigurationInstanceField();
        }
        return superNonConfigurationInstanceField;
    }

    private void setSuperNonConfigurationInstanceField() {
        superNonConfigurationInstanceField = generatedClass
            .field(PUBLIC, Object.class, "superNonConfigurationInstance");
    }

    /**
     * 创建字段
     *
     * @param fieldName 字段名
     * @param fieldType 字段类型
     * @return {@link JFieldVar}
     */
    public JFieldVar createField(String fieldName, AbstractJClass fieldType) {
        return generatedClass.field(PUBLIC, fieldType, fieldName);
    }
}
