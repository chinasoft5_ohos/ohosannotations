/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JVar;

/**
 * 选项菜单
 *
 * @since 2021-07-20
 */
public interface HasOptionsMenu extends GeneratedClassHolder {
    /**
     * 创建选项菜单方法体
     *
     * @return JBlock J板块
     */
    JBlock getOnCreateOptionsMenuMethodBody();

    /**
     * 创建选项菜单方法填充体
     *
     * @return JBlock J板块
     */
    JBlock getOnCreateOptionsMenuMethodInflateBody();

    /**
     * 创建选项菜单方法填填充Var
     *
     * @return JVar 字段
     */
    JVar getOnCreateOptionsMenuMenuInflaterVar();

    /**
     * 创建选项菜单参数
     *
     * @return JVar 字段
     */
    JVar getOnCreateOptionsMenuMenuParam();

    /**
     * 选项
     *
     * @return JVar字段
     */
    JVar getOnOptionsItemSelectedItem();

    /**
     * 选项条目选中条目ID
     *
     * @return JVar字段
     */
    JVar getOnOptionsItemSelectedItemId();

    /**
     * 选项条目选择中间块
     *
     * @return JBlock J板块
     */
    JBlock getOnOptionsItemSelectedMiddleBlock();
}
