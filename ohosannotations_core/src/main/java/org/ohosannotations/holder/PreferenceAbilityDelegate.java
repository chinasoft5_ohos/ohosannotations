/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

import static com.helger.jcodemodel.JMod.PUBLIC;

/**
 * 预设参数Ability委托
 *
 * @since 2021-07-20
 */
public class PreferenceAbilityDelegate extends PreferencesDelegate implements HasPreferenceHeaders {
    private JBlock onBuildHeadersBlock;

    private JVar onBuildHeadersTargetParam;

    /**
     * 构造参数
     *
     * @param holder 持有者
     */
    public PreferenceAbilityDelegate(EComponentWithViewSupportHolder holder) {
        super(holder);
    }

    @Override
    public JBlock getOnBuildHeadersBlock() {
        if (onBuildHeadersBlock == null) {
            setOnBuildHeadersBlock();
        }
        return onBuildHeadersBlock;
    }

    @Override
    public JVar getOnBuildHeadersTargetParam() {
        if (onBuildHeadersTargetParam == null) {
            setOnBuildHeadersBlock();
        }
        return onBuildHeadersTargetParam;
    }

    private void setOnBuildHeadersBlock() {
        JMethod method = getGeneratedClass().method(PUBLIC, codeModel().VOID, "onBuildHeaders");
        method.annotate(Override.class);
        onBuildHeadersBlock = method.body();
        onBuildHeadersTargetParam = method.param(getClasses()
            .LIST.narrow(getClasses().PREFERENCE_ABILITY_HEADER), "target");
    }
}
