/**
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;

/**
 * 具有简单的生命周期方法
 *
 * @since 2021-07-19
 */
public interface HasSimpleLifecycleMethods extends GeneratedClassHolder {
    /**
     * 启动生命周期后的超级块
     *
     * @return JBlock J板块
     */
    JBlock getStartLifecycleAfterSuperBlock();

    /**
     * 结束生命周期后的超级块
     *
     * @return J板块
     */
    JBlock getEndLifecycleBeforeSuperBlock();
}
