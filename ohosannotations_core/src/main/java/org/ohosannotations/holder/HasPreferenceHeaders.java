/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JVar;

/**
 * 预设参数头部
 *
 * @since 2021-07-19
 */
public interface HasPreferenceHeaders extends HasPreferences {
    /**
     * 获得构建头部块
     *
     * @return JBlock J板块
     */
    JBlock getOnBuildHeadersBlock();

    /**
     * 获取构建头目标参数
     *
     * @return JVar 字段
     */
    JVar getOnBuildHeadersTargetParam();
}
