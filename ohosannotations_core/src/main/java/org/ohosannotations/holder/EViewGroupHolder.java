/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JExpr;

import org.ohosannotations.OhosAnnotationsEnvironment;

import javax.lang.model.element.TypeElement;

import static com.helger.jcodemodel.JMod.PUBLIC;

/**
 * EViewGroupHolder
 *
 * @since 2021-07-20
 */
public class EViewGroupHolder extends EViewHolder {
    private JBlock setContentViewBlock;

    /**
     * 构造参数
     *
     * @param environment 注解环境
     * @param annotatedElement 类型元素
     * @throws Exception 异常
     */
    public EViewGroupHolder(OhosAnnotationsEnvironment environment, TypeElement annotatedElement) throws Exception {
        super(environment, annotatedElement);
    }

    /**
     * setOnFinishInflate
     */
    protected void setOnFinishInflate() {
        onFinishInflate = generatedClass.method(PUBLIC, getCodeModel().VOID, "onFinishInflate");
        onFinishInflate.javadoc().append(ALREADY_INFLATED_COMMENT);

        JBlock ifNotInflated = onFinishInflate.body()._if(getAlreadyInflated().not())._then();
        ifNotInflated.assign(getAlreadyInflated(), JExpr.TRUE);

        setContentViewBlock = ifNotInflated.blockSimple();

        getInit();
        viewNotifierHelper.invokeViewChanged(ifNotInflated);
    }

    /**
     * getSetContentViewBlock
     *
     * @return JBlock J板块
     */
    public JBlock getSetContentViewBlock() {
        if (setContentViewBlock == null) {
            setOnFinishInflate();
        }
        return setContentViewBlock;
    }
}
