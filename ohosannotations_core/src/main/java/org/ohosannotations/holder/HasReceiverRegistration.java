/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldVar;

import org.ohosannotations.holder.ReceiverRegistrationDelegate.IntentFilterData;

/**
 * 接受注册
 *
 * @since 2021-07-20
 */
public interface HasReceiverRegistration extends HasSimpleLifecycleMethods {
    /**
     * 获取上下文裁判
     *
     * @return IJExpression 表达式
     */
    IJExpression getContextRef();

    /**
     * 获取意图过滤字段
     *
     * @param intentFilterData 意图过滤字段
     * @return JFieldVar 字段
     */
    JFieldVar getIntentFilterField(IntentFilterData intentFilterData);

    /**
     * 获取意图过滤初始化块
     *
     * @param intentFilterData 意图过滤数据
     * @return JBlock J板块
     */
    JBlock getIntentFilterInitializationBlock(IntentFilterData intentFilterData);
}
