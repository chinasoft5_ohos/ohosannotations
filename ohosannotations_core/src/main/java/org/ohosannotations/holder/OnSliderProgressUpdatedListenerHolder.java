/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

/**
 * 滑动器进度更新监听器Holder
 *
 * @since 2021-07-20
 */
public class OnSliderProgressUpdatedListenerHolder {
    private EComponentWithViewSupportHolder holder;
    private JDefinedClass listenerClass;
    private JBlock onProgressUpdatedBody;
    private JVar onProgressUpdatedSliderParam;
    private JVar onProgressUpdatedProgressParam;
    private JVar onProgressUpdatedFromUserParam;
    private JBlock onTouchStartBody;
    private JVar onTouchStarSliderParam;
    private JBlock onTouchEndBody;
    private JVar onTouchEndSliderParam;

    /**
     * 构造参数
     *
     * @param holder 视图组件持有者
     * @param onSliderProgressUpdatedListenerClass 自定义类
     */
    public OnSliderProgressUpdatedListenerHolder(EComponentWithViewSupportHolder holder,
        JDefinedClass onSliderProgressUpdatedListenerClass) {
        this.holder = holder;
        listenerClass = onSliderProgressUpdatedListenerClass;
        createOnProgressUpdated();
        createOnTouchStart();
        createOnTouchEnd();
    }

    private void createOnProgressUpdated() {
        JMethod onProgressUpdatedMethod = listenerClass
            .method(JMod.PUBLIC, holder.getCodeModel().VOID, "onProgressUpdated");
        onProgressUpdatedMethod.annotate(Override.class);
        onProgressUpdatedBody = onProgressUpdatedMethod.body();
        onProgressUpdatedSliderParam = onProgressUpdatedMethod.param(holder.getClasses().SLIDER, "slider");
        onProgressUpdatedProgressParam = onProgressUpdatedMethod.param(holder.getCodeModel().INT, "progress");
        onProgressUpdatedFromUserParam = onProgressUpdatedMethod.param(holder.getCodeModel().BOOLEAN, "fromUser");
    }

    private void createOnTouchStart() {
        JMethod onTouchStartMethod = listenerClass.method(JMod.PUBLIC, holder.getCodeModel().VOID, "onTouchStart");
        onTouchStartMethod.annotate(Override.class);
        onTouchStartBody = onTouchStartMethod.body();
        onTouchStarSliderParam = onTouchStartMethod.param(holder.getClasses().SLIDER, "slider");
    }

    private void createOnTouchEnd() {
        JMethod onTouchEndMethod = listenerClass.method(JMod.PUBLIC, holder.getCodeModel().VOID, "onTouchEnd");
        onTouchEndMethod.annotate(Override.class);
        onTouchEndBody = onTouchEndMethod.body();
        onTouchEndSliderParam = onTouchEndMethod.param(holder.getClasses().SLIDER, "slider");
    }

    /**
     * 获取滑动器更新主体
     *
     * @return JBlock J板块
     */
    public JBlock getOnProgressUpdatedBody() {
        return onProgressUpdatedBody;
    }

    /**
     * 获取滑动器更新参数
     *
     * @return JVar字段
     */
    public JVar getOnProgressUpdatedSliderParam() {
        return onProgressUpdatedSliderParam;
    }

    /**
     * 获取滑动器更新进度参数
     *
     * @return JVar 字段
     */
    public JVar getOnProgressUpdatedProgressParam() {
        return onProgressUpdatedProgressParam;
    }

    /**
     * 从用户参数获得进展更新
     *
     * @return JVar 字段
     */
    public JVar getOnProgressUpdatedFromUserParam() {
        return onProgressUpdatedFromUserParam;
    }

    /**
     * 触摸主体开始
     *
     * @return JBlock J 板块
     */
    public JBlock getOnTouchStartBody() {
        return onTouchStartBody;
    }

    /**
     * 触摸星形滑块参数
     *
     * @return JVar 字段
     */
    public JVar getOnTouchStarSliderParam() {
        return onTouchStarSliderParam;
    }

    /**
     * 触控端体
     *
     * @return JBlock J板块
     */
    public JBlock getOnTouchEndBody() {
        return onTouchEndBody;
    }

    /**
     * 在触摸结束滑块参数
     *
     * @return JVar 字段
     */
    public JVar getOnTouchEndSliderParam() {
        return onTouchEndSliderParam;
    }
}
