/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import static com.helger.jcodemodel.JExpr._new;
import static com.helger.jcodemodel.JMod.FINAL;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.ohosannotations.annotations.Receiver.RegisterAt;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldVar;

/**
 * 接收机登记委托
 *
 * @param <T> 子类泛型
 * @author dev
 * @since 2021-07-22
 */
public class ReceiverRegistrationDelegate<T extends EComponentHolder & HasReceiverRegistration>
    extends GeneratedClassHolderDelegate<T> {

    private Map<IntentFilterData, JFieldVar> intentFilterFields = new HashMap<>();

    /**
     * 接收机登记委托
     *
     * @param holder 持有人
     */
    public ReceiverRegistrationDelegate(T holder) {
        super(holder);
    }

    /**
     * 得到目的过滤字段
     *
     * @param intentFilterData 目的筛选数据
     * @return {@link JFieldVar}
     */
    public JFieldVar getIntentFilterField(IntentFilterData intentFilterData) {
        JFieldVar intentFilterField = intentFilterFields.get(intentFilterData);
        if (intentFilterField == null) {
            intentFilterField = createIntentFilterField(intentFilterData);
            intentFilterFields.put(intentFilterData, intentFilterField);
        }
        return intentFilterField;
    }

    /**
     * 创建目的过滤字段
     *
     * @param intentFilterData 目的筛选数据
     * @return {@link JFieldVar}
     */
    private JFieldVar createIntentFilterField(IntentFilterData intentFilterData) {
        String intentFilterName = "intentFilter" + (intentFilterFields.size() + 1) + generationSuffix();
        IJExpression newIntentFilterExpr = _new(getClasses().INTENT_FILTER);
        JFieldVar intentFilterField = getGeneratedClass().field(PRIVATE | FINAL, getClasses().INTENT_FILTER, intentFilterName, newIntentFilterExpr);

        JBlock intentFilterTarget = holder.getIntentFilterInitializationBlock(intentFilterData);
        for (String action : intentFilterData.getActionSet()) {
            intentFilterTarget.invoke(intentFilterField, "addAction").arg(action);
        }
        for (String dataScheme : intentFilterData.getDataSchemeSet()) {
            intentFilterTarget.invoke(intentFilterField, "addDataScheme").arg(dataScheme);
        }

        return intentFilterField;
    }

    /**
     * IntentFilterData
     *
     * @since 2021-07-22
     */
    public static class IntentFilterData {
        private final RegisterAt registerAt;
        private final Set<String> actionSet;
        private final Set<String> dataSchemeSet;

        /**
         * 目的筛选数据
         *
         * @param actions 行动
         * @param dataSchemes 数据计划
         * @param registerAt 注册在
         */
        public IntentFilterData(String[] actions, String[] dataSchemes, RegisterAt registerAt) {
            this.registerAt = registerAt;
            actionSet = new HashSet<>(Arrays.asList(actions));
            dataSchemeSet = new HashSet<>(Arrays.asList(dataSchemes));
        }

        /**
         * 获得注册
         *
         * @return {@link RegisterAt}
         */
        public RegisterAt getRegisterAt() {
            return registerAt;
        }

        /**
         * 获取操作设置
         *
         * @return {@link Set}
         */
        public Set<String> getActionSet() {
            return actionSet;
        }

        /**
         * 获取数据方案设置
         *
         * @return {@link Set}
         */
        public Set<String> getDataSchemeSet() {
            return dataSchemeSet;
        }


        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (actionSet == null ? 0 : actionSet.hashCode());
            result = prime * result + (registerAt == null ? 0 : registerAt.hashCode());
            result = prime * result + (dataSchemeSet == null ? 0 : dataSchemeSet.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            IntentFilterData other = (IntentFilterData) obj;
            if (actionSet == null) {
                if (other.actionSet != null) {
                    return false;
                }
            } else if (!actionSet.equals(other.actionSet)) {
                return false;
            } else {
            }
            if (registerAt == null) {
                if (other.registerAt != null) {
                    return false;
                }
            } else if (!registerAt.equals(other.registerAt)) {
                return false;
            } else {
            }
            if (dataSchemeSet == null) {
                if (other.dataSchemeSet != null) {
                    return false;
                }
            } else if (!dataSchemeSet.equals(other.dataSchemeSet)) {
                return false;
            } else {
            }
            return true;
        }
    }
}
