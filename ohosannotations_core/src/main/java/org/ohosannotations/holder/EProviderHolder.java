/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.OhosAnnotationsEnvironment;

import javax.lang.model.element.TypeElement;

import static com.helger.jcodemodel.JExpr._super;
import static com.helger.jcodemodel.JExpr.invoke;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

/**
 * eprovider持有人
 *
 * @author dev
 * @since 2021-07-22
 */
public class EProviderHolder extends EComponentHolder {
    /**
     * eprovider持有人
     *
     * @param environment 环境
     * @param annotatedElement 带注释的元素
     * @throws Exception 异常
     */
    public EProviderHolder(OhosAnnotationsEnvironment environment, TypeElement annotatedElement) throws Exception {
        super(environment, annotatedElement);
    }

    /**
     * 设置上下文裁判
     */
    @Override
    protected void setContextRef() {
        contextRef = invoke("getContext");
    }

    /**
     * 设置初始化
     */
    @Override
    protected void setInit() {
        init = generatedClass.method(PRIVATE, getCodeModel().VOID, "init" + generationSuffix());
        init.param(getClasses().INTENT, "intent");
        createOnCreate();
    }

    /**
     * 创建在创建
     */
    private void createOnCreate() {
        JMethod onCreate = generatedClass.method(PUBLIC, getCodeModel().VOID, "onStart");
        onCreate.annotate(Override.class);
        JVar intent = onCreate.param(getClasses().INTENT, "intent");
        JBlock onCreateBody = onCreate.body();
        onCreateBody.invoke(_super(), onCreate).arg(intent);
        onCreateBody.invoke(getInit()).arg(intent);
    }
}
