/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;

/**
 * 生命周期方法
 *
 * @since 2021-07-19
 */
public interface HasAbilityLifecycleMethods extends GeneratedClassHolder {
    /**
     * 在父类板块之后创建
     *
     * @return JBlock J板块
     */
    JBlock getOnCreateAfterSuperBlock();

    /**
     * 在父类板块之前摧毁
     *
     * @return JBlock J板块
     */
    JBlock getOnDestroyBeforeSuperBlock();

    /**
     * 在父类板块之后开始
     *
     * @return JBlock J板块
     */
    JBlock getOnStartAfterSuperBlock();

    /**
     * 在父类板块之前停止
     *
     * @return JBlock J板块
     */
    JBlock getOnStopBeforeSuperBlock();

    /**
     * 在父类板块之后恢复
     *
     * @return JBlock J板块
     */
    JBlock getOnResumeAfterSuperBlock();

    /**
     * 在父类板块之前暂停
     *
     * @return JBlock J板块
     */
    JBlock getOnPauseBeforeSuperBlock();
}
