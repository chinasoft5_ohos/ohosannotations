/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

/**
 * 实例的状态
 *
 * @since 2021-07-20
 */
public interface HasInstanceState extends GeneratedClassHolder {
    /**
     * 获取保存方法体
     *
     * @return JBlock J板块
     */
    JBlock getSaveStateMethodBody();

    /**
     * 获取保存Bundle参数
     *
     * @return JVar 字段
     */
    JVar getSaveStateBundleParam();

    /**
     * 获取恢复状态的方法
     *
     * @return JMethod J方法
     */
    JMethod getRestoreStateMethod();

    /**
     * 获取恢复状态方法体
     *
     * @return JBlock J板块
     */
    JBlock getRestoreStateMethodBody();

    /**
     * 获取恢复状态包参数
     *
     * @return JVar字段
     */
    JVar getRestoreStateBundleParam();
}
