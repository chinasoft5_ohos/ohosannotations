/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

/**
 * 参数设置
 *
 * @since 2021-07-20
 */
public interface HasPreferences extends GeneratedClassHolder {
    /**
     * 获取首选项屏幕初始化块
     *
     * @return JBlock J板块
     */
    JBlock getPreferenceScreenInitializationBlock();

    /**
     * 从资源注入块中获取添加首选项
     *
     * @return JBlock J板块
     */
    JBlock getAddPreferencesFromResourceInjectionBlock();

    /**
     * 在注入块后从资源中获取添加参数
     *
     * @return JBlock J板块
     */
    JBlock getAddPreferencesFromResourceAfterInjectionBlock();

    /**
     * 获取参数设置持有者
     *
     * @param idRef id
     * @param preferenceClass 预设参数类
     * @return FoundPreferenceHolder 持有者
     */
    FoundPreferenceHolder getFoundPreferenceHolder(JFieldRef idRef, AbstractJClass preferenceClass);

    /**
     * 获取参数设置持有者
     *
     * @param idRef id
     * @param preferenceClass 预设参数类
     * @param fieldRef 字段
     * @return FoundPreferenceHolder 持有者
     */
    FoundPreferenceHolder getFoundPreferenceHolder(JFieldRef idRef,
        AbstractJClass preferenceClass, IJAssignmentTarget fieldRef);

    /**
     * 使用支持V7预设参数
     *
     * @return boolean 布尔值
     */
    boolean usingSupportV7Preference();

    /**
     * 使用Ohos预设参数
     *
     * @return boolean布尔值
     */
    boolean usingOhosPreference();

    /**
     * 获取基类优先级
     *
     * @return AbstractJClass 抽象J类
     */
    AbstractJClass getBasePreferenceClass();
}
