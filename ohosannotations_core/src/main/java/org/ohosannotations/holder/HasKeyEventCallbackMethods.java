/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JSwitch;
import com.helger.jcodemodel.JVar;

/**
 * 关键事件回调方法
 *
 * @since 2021-07-19
 */
public interface HasKeyEventCallbackMethods extends GeneratedClassHolder {
    /**
     * 获取按下键开关主体
     *
     * @return JSwitch J开关
     */
    JSwitch getOnKeyDownSwitchBody();

    /**
     * 获取按下键事件参数
     *
     * @return JVar 字段
     */
    JVar getOnKeyDownKeyEventParam();

    /**
     * 获取长按事件开关主体
     *
     * @return JSwitch J开关
     */
    JSwitch getOnKeyLongPressSwitchBody();

    /**
     * 获取长按事件参数
     *
     * @return JVar字段
     */
    JVar getOnKeyLongPressKeyEventParam();

    /**
     * 获取抬起事件开关主题
     *
     * @return JSwitch J开关
     */
    JSwitch getOnKeyUpSwitchBody();

    /**
     * 获取抬起事件参数
     *
     * @return JVar 字段
     */
    JVar getOnKeyUpKeyEventParam();
}
