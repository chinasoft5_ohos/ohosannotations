/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;

/**
 * 视图持有者
 *
 * @since 2021-07-20
 */
public class FoundViewHolder extends FoundHolder {
    /**
     * 构造参数
     *
     * @param holder 生成的类
     * @param viewClass 抽象的J类
     * @param view 表达式
     * @param block J板块
     */
    public FoundViewHolder(GeneratedClassHolder holder, AbstractJClass viewClass, IJExpression view, JBlock block) {
        super(holder, viewClass, view, block);
    }

    @Override
    protected AbstractJClass getBaseType() {
        return getClasses().COMPONENT;
    }
}
