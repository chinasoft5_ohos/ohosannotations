/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.OhosAnnotationsEnvironment;

import javax.lang.model.element.TypeElement;

import static com.helger.jcodemodel.JExpr._super;
import static com.helger.jcodemodel.JExpr._this;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static com.helger.jcodemodel.JMod.STATIC;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

/**
 * eapplication持有人
 *
 * @author dev
 * @since 2021-07-22
 */
public class EApplicationHolder extends EComponentHolder {
    /**
     * 让应用程序实例
     */
    public static final String GET_APPLICATION_INSTANCE = "getInstance";
    private JFieldVar staticInstanceField;

    /**
     * eapplication持有人
     *
     * @param environment 环境
     * @param annotatedElement 带注释的元素
     * @throws Exception 异常
     */
    public EApplicationHolder(OhosAnnotationsEnvironment environment, TypeElement annotatedElement) throws Exception {
        super(environment, annotatedElement);
        createSingleton();
        createOnCreate();
    }

    /**
     * 创建单例
     */
    private void createSingleton() {
        AbstractJClass annotatedComponent = generatedClass._extends();

        staticInstanceField = generatedClass.field(PRIVATE | STATIC, annotatedComponent,
            "INSTANCE" + generationSuffix());

        // Static singleton getter and setter
        JMethod getInstance = generatedClass.method(PUBLIC | STATIC, annotatedComponent, GET_APPLICATION_INSTANCE);
        getInstance.body()._return(staticInstanceField);

        JMethod setInstance = generatedClass.method(PUBLIC | STATIC, getCodeModel().VOID, "setForTesting");
        setInstance.javadoc().append("Visible for testing purposes");
        JVar applicationParam = setInstance.param(annotatedComponent, "application");
        setInstance.body().assign(staticInstanceField, applicationParam);
    }

    /**
     * 创建在创建
     */
    private void createOnCreate() {
        JMethod onCreate = generatedClass.method(PUBLIC, getCodeModel().VOID, "onInitialize");
        onCreate.annotate(Override.class);
        JBlock onCreateBody = onCreate.body();
        onCreateBody.assign(staticInstanceField, _this());
        onCreateBody.invoke(getInit());
        onCreateBody.invoke(_super(), onCreate);
    }

    /**
     * 设置上下文裁判
     */
    @Override
    protected void setContextRef() {
        contextRef = JExpr._this();
    }

    /**
     * 设置初始化
     */
    @Override
    protected void setInit() {
        init = generatedClass.method(PRIVATE, getCodeModel().VOID, "init" + generationSuffix());
    }
}
