/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JClassAlreadyExistsException;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.EFraction;
import org.ohosannotations.annotations.Receiver.RegisterAt;
import org.ohosannotations.holder.ReceiverRegistrationDelegate.IntentFilterData;

import javax.lang.model.element.TypeElement;

import static com.helger.jcodemodel.JExpr.FALSE;
import static com.helger.jcodemodel.JExpr.TRUE;
import static com.helger.jcodemodel.JExpr._new;
import static com.helger.jcodemodel.JExpr._null;
import static com.helger.jcodemodel.JExpr._super;
import static com.helger.jcodemodel.JExpr._this;
import static com.helger.jcodemodel.JExpr.cond;
import static com.helger.jcodemodel.JExpr.invoke;
import static com.helger.jcodemodel.JExpr.ref;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static com.helger.jcodemodel.JMod.PROTECTED;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static com.helger.jcodemodel.JMod.STATIC;
import static com.helger.jcodemodel.JMod.VOLATILE;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

/**
 * EFractionHolder
 *
 * @author dev
 * @since 2021-07-21
 */
public class EFractionHolder extends EComponentWithViewSupportHolder
    implements HasInstanceState, HasOptionsMenu, HasOnAbilityResult,
    HasAbilityLifecycleMethods, HasReceiverRegistration, HasPreferences {
    private JFieldVar contentComponent;
    private JFieldVar componentDestroyedField;
    private JBlock setOnComponentAttachedBlock;
    private JVar scatter;
    private JVar container;
    private JDefinedClass fractionBuilderClass;
    private AbstractJClass narrowBuilderClass;
    private JFieldRef fractionArgumentsBuilderField;
    private JMethod injectArgsMethod;
    private JBlock injectArgsBlock;
    private JVar injectBundleArgs;
    private InstanceStateDelegate instanceStateDelegate;
    private OnAbilityResultDelegate onAbilityResultDelegate;
    private ReceiverRegistrationDelegate<EFractionHolder> receiverRegistrationDelegate;
    private PreferencesDelegate preferencesDelegate;
    private JBlock onCreateOptionsMenuMethodBody;
    private JBlock onCreateOptionsMenuMethodInflateBody;
    private JVar onCreateOptionsMenuMenuInflaterVar;
    private JVar onCreateOptionsMenuMenuParam;
    private JVar onOptionsItemSelectedItem;
    private JVar onOptionsItemSelectedItemId;
    private JBlock onOptionsItemSelectedMiddleBlock;
    private JBlock onStartAfterSuperBlock;
    private JBlock onStopBeforeSuperBlock;
    private JBlock onActiveAfterSuperBlock;
    private JBlock onInactiveBeforeSuperBlock;
    private JBlock onBackgroundBeforeSuperBlock;
    private JBlock onForegroundBeforeSuperBlock;
    private JBlock onComponentDetachBeforeSuperBlock;
    private JBlock onComponentDetachAfterSuperBlock;

    /**
     * EFractionHolder
     *
     * @param environment 环境
     * @param annotatedElement 带注释的元素
     * @throws Exception 异常
     */
    public EFractionHolder(OhosAnnotationsEnvironment environment, TypeElement annotatedElement) throws Exception {
        super(environment, annotatedElement);
        instanceStateDelegate = new InstanceStateDelegate(this);
        onAbilityResultDelegate = new OnAbilityResultDelegate(this);
        receiverRegistrationDelegate = new ReceiverRegistrationDelegate<>(this);
        preferencesDelegate = new PreferencesDelegate(this);
        setContentView();
        setOnComponentAttached();
        setFragmentBuilder();
        implementBeanHolder();
    }

    /**
     * 在附加组件
     */
    private void setOnComponentAttached() {
        JMethod onComponentAttached = generatedClass.method(PUBLIC, getClasses().COMPONENT, "onComponentAttached");
        onComponentAttached.annotate(Override.class);
        scatter = onComponentAttached.param(getClasses().LAYOUT_SCATTER, "scatter");
        container = onComponentAttached.param(getClasses().COMPONENT_CONTAINER, "container");

        JVar intent = onComponentAttached.param(getClasses().INTENT, "intent");

        boolean forceInjection = getAnnotatedElement().getAnnotation(EFraction.class).forceLayoutInjection();

        JBlock onCreateBody = onComponentAttached.body();

        if (!forceInjection) {
            onCreateBody.assign(contentComponent, _super().invoke(onComponentAttached)
                .arg(scatter).arg(container).arg(intent));
        }

        JVar previousNotifier = viewNotifierHelper.replacePreviousNotifier(onCreateBody);
        onCreateBody.invoke(getInit());
        setOnComponentAttachedBlock = onCreateBody.blockSimple();
        viewNotifierHelper.resetPreviousNotifier(onCreateBody, previousNotifier);
        viewNotifierHelper.invokeViewChanged(onCreateBody);

        onCreateBody._return(contentComponent);
    }

    /**
     * 在组件分离
     */
    private void setOnComponentDetach() {
        JMethod onComponentDetach = generatedClass.method(PROTECTED, getCodeModel().VOID, "onComponentDetach");
        onComponentDetach.annotate(Override.class);
        JBlock body = onComponentDetach.body();
        body.invoke(_super(), onComponentDetach);
        body.assign(contentComponent, _null());
        onComponentDetachAfterSuperBlock = body.blockSimple();
    }

    /**
     * 在组件分离后超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnComponentDetachAfterSuperBlock() {
        if (onComponentDetachAfterSuperBlock == null) {
            setContentViewRelatedMethods();
        }
        return onComponentDetachAfterSuperBlock;
    }

    /**
     * 通过id获取找到视图表达式
     *
     * @param idParam id参数
     * @return {@link IJExpression}
     */
    @Override
    public IJExpression getFindViewByIdExpression(JVar idParam) {
        JFieldVar contentView = getContentComponent();
        JInvocation invocation = contentView.invoke("findComponentById").arg(idParam);
        return cond(contentView.eq(_null()), _null(), invocation);
    }

    /**
     * 设置片段构建器
     *
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    private void setFragmentBuilder() throws JClassAlreadyExistsException {
        fractionBuilderClass = generatedClass._class(PUBLIC | STATIC, "FractionBuilder" + generationSuffix());

        narrowBuilderClass = narrow(fractionBuilderClass);

        codeModelHelper.generify(fractionBuilderClass, annotatedElement);
        AbstractJClass superClass = getJClass(org.ohosannotations.api.builder.FractionBuilder.class);
        superClass = superClass.narrow(narrowBuilderClass, getAnnotatedClass());
        fractionBuilderClass._extends(superClass);
        fractionArgumentsBuilderField = ref("args");
        setFragmentBuilderBuild();
        setFragmentBuilderCreate();
    }

    /**
     * 设置片段builder构建
     */
    private void setFragmentBuilderBuild() {
        JMethod method = fractionBuilderClass.method(PUBLIC, generatedClass._extends(), "build");
        method.annotate(Override.class);
        JBlock body = method.body();

        AbstractJClass result = narrow(generatedClass);
        JVar fraction = body.decl(result, "fraction_", _new(result));
        body._return(fraction);
    }

    /**
     * 设置片段builder创建
     */
    private void setFragmentBuilderCreate() {
        JMethod method = generatedClass.method(STATIC | PUBLIC, narrowBuilderClass, "builder");
        codeModelHelper.generify(method, annotatedElement);
        method.body()._return(_new(narrowBuilderClass));
    }

    /**
     * 创建选项菜单
     */
    private void setOnCreateOptionsMenu() {
        JMethod method = generatedClass.method(PUBLIC, getCodeModel().VOID, "onCreateOptionsMenu");
        method.annotate(Override.class);
        JBlock methodBody = method.body();
        onCreateOptionsMenuMenuParam = method.param(getClasses().MENU, "menu");
        onCreateOptionsMenuMenuInflaterVar = method.param(getClasses().MENU_INFLATER, "inflater");
        onCreateOptionsMenuMethodInflateBody = methodBody.blockSimple();
        onCreateOptionsMenuMethodBody = methodBody.blockSimple();
        methodBody.invoke(_super(), method).arg(onCreateOptionsMenuMenuParam).arg(onCreateOptionsMenuMenuInflaterVar);

        getInitBody().invoke("setHasOptionsMenu").arg(JExpr.TRUE);
    }

    /**
     * 项目选择上设置选项
     */
    private void setOnOptionsItemSelected() {
        JMethod method = generatedClass.method(JMod.PUBLIC, getCodeModel().BOOLEAN, "onOptionsItemSelected");
        method.annotate(Override.class);
        JBlock methodBody = method.body();
        onOptionsItemSelectedItem = method.param(getClasses().MENU_ITEM, "item");
        onOptionsItemSelectedItemId = methodBody.decl(getCodeModel().INT, "itemId_",
            onOptionsItemSelectedItem.invoke("getItemId"));
        onOptionsItemSelectedMiddleBlock = methodBody.blockSimple();

        methodBody._return(invoke(_super(), method).arg(onOptionsItemSelectedItem));
    }

    /**
     * 设置上下文裁判
     */
    @Override
    protected void setContextRef() {
        contextRef = JExpr.invoke("getFractionAbility").invoke("getContext");
    }

    /**
     * 设置根片段裁判
     */
    @Override
    protected void setRootFragmentRef() {
        rootFractionRef = _this();
    }

    /**
     * 设置初始化
     */
    @Override
    protected void setInit() {
        init = generatedClass.method(PRIVATE, getCodeModel().VOID, "init" + generationSuffix());
    }

    /**
     * 让内容组件
     *
     * @return {@link JFieldVar}
     */
    public JFieldVar getContentComponent() {
        if (contentComponent == null) {
            setContentViewRelatedMethods();
        }
        return contentComponent;
    }

    /**
     * 组相关内容视图的方法
     */
    private void setContentViewRelatedMethods() {
        setContentView();
        setOnComponentDetach();
    }

    /**
     * 设置内容视图
     */
    private void setContentView() {
        if (contentComponent == null) {
            contentComponent = generatedClass.field(PRIVATE, getClasses().COMPONENT,
                "contentView" + generationSuffix());
        }
    }

    /**
     * 获得组件销毁场
     *
     * @return {@link JFieldVar}
     */
    public JFieldVar getComponentDestroyedField() {
        if (componentDestroyedField == null) {
            setViewDestroyedField();
        }
        return componentDestroyedField;
    }

    /**
     * 设置视图摧毁字段
     */
    private void setViewDestroyedField() {
        componentDestroyedField = generatedClass.field(PRIVATE | VOLATILE, getCodeModel().BOOLEAN,
            "viewDestroyed" + generationSuffix(), TRUE);
        getOnCreateAfterSuperBlock().assign(componentDestroyedField, FALSE);
        getOnComponentDetachAfterSuperBlock().assign(componentDestroyedField, TRUE);
    }

    /**
     * 明确注射视图
     *
     * @param fieldRef 现场裁判
     */
    public void clearInjectedView(IJAssignmentTarget fieldRef) {
        JBlock block = getOnComponentDetachAfterSuperBlock();
        block.assign(fieldRef, _null());
    }

    /**
     * 在开始
     */
    private void setOnStart() {
        JMethod onStart = generatedClass.method(PUBLIC, getCodeModel().VOID, "onStart");
        onStart.param(getClasses().INTENT, "intent");
        onStart.annotate(Override.class);
        JBlock onStartBody = onStart.body();
        onStartBody.invoke(_super(), onStart);
        onStartAfterSuperBlock = onStartBody.blockSimple();
    }

    /**
     * 在活跃的
     */
    private void setOnActive() {
        JMethod onActive = generatedClass.method(PROTECTED, getCodeModel().VOID, "onActive");
        onActive.annotate(Override.class);
        JBlock onActiveBody = onActive.body();
        onActiveBody.invoke(_super(), onActive);
        onActiveAfterSuperBlock = onActiveBody.blockSimple();
    }

    /**
     * 在不活跃的
     */
    private void setOnInactive() {
        JMethod onInactive = generatedClass.method(PROTECTED, getCodeModel().VOID, "onInactive");
        onInactive.annotate(Override.class);
        JBlock onInactiveBody = onInactive.body();
        onInactiveBeforeSuperBlock = onInactiveBody.blockSimple();
        onInactiveBody.invoke(_super(), onInactive);
    }

    /**
     * 设置在背景
     */
    private void setOnBackground() {
        JMethod onBackground = generatedClass.method(PROTECTED, getCodeModel().VOID, "onBackground");
        onBackground.annotate(Override.class);
        JBlock onBackgroundBody = onBackground.body();
        onBackgroundBeforeSuperBlock = onBackgroundBody.blockSimple();
        onBackgroundBody.invoke(_super(), onBackground);
    }

    /**
     * 设置在前台
     */
    private void setOnForeground() {
        JMethod onForeground = generatedClass.method(PROTECTED, getCodeModel().VOID, "onForeground");
        onForeground.annotate(Override.class);
        JBlock onForegroundBody = onForeground.body();
        onForegroundBeforeSuperBlock = onForegroundBody.blockSimple();
        onForegroundBody.invoke(_super(), onForeground);
    }

    /**
     * 在分离
     */
    private void setOnDetach() {
        JMethod onComponentDetach = generatedClass.method(PROTECTED, getCodeModel().VOID, "onComponentDetach");
        onComponentDetach.annotate(Override.class);
        JBlock onComponentDetachBody = onComponentDetach.body();
        onComponentDetachBeforeSuperBlock = onComponentDetachBody.blockSimple();
        onComponentDetachBody.invoke(_super(), onComponentDetach);
    }

    /**
     * 在停止
     */
    private void setOnStop() {
        JMethod onStop = generatedClass.method(PUBLIC, getCodeModel().VOID, "onStop");
        onStop.annotate(Override.class);
        JBlock onStopBody = onStop.body();
        onStopBeforeSuperBlock = onStopBody.blockSimple();
        onStopBody.invoke(_super(), onStop);
    }

    /**
     * 得到散射
     *
     * @return {@link JVar}
     */
    public JVar getScatter() {
        if (scatter == null) {
            setOnComponentAttached();
        }
        return scatter;
    }

    /**
     * 得到集装箱
     *
     * @return {@link JVar}
     */
    public JVar getContainer() {
        if (container == null) {
            setOnComponentAttached();
        }
        return container;
    }

    /**
     * 得到构建器类
     *
     * @return {@link JDefinedClass}
     */
    public JDefinedClass getBuilderClass() {
        return fractionBuilderClass;
    }

    /**
     * 得到施工参数字段
     *
     * @return {@link JFieldRef}
     */
    public JFieldRef getBuilderArgsField() {
        return fractionArgumentsBuilderField;
    }

    /**
     * 得到注入参数的方法
     *
     * @return {@link JMethod}
     */
    public JMethod getInjectArgsMethod() {
        if (injectArgsMethod == null) {
            setInjectArgs();
        }
        return injectArgsMethod;
    }

    /**
     * 得到注入参数块
     *
     * @return {@link JBlock}
     */
    public JBlock getInjectArgsBlock() {
        if (injectArgsBlock == null) {
            setInjectArgs();
        }
        return injectArgsBlock;
    }

    /**
     * 得到注射包参数
     *
     * @return {@link JVar}
     */
    public JVar getInjectBundleArgs() {
        if (injectBundleArgs == null) {
            setInjectArgs();
        }
        return injectBundleArgs;
    }

    /**
     * 设置注入参数
     */
    private void setInjectArgs() {
        injectArgsMethod = generatedClass.method(PRIVATE, getCodeModel().VOID,
            "injectFragmentArguments" + generationSuffix());
        JBlock injectExtrasBody = injectArgsMethod.body();
        injectBundleArgs = injectExtrasBody.decl(getClasses().PACMAP, "args_", invoke("getArguments"));
        injectArgsBlock = injectExtrasBody._if(injectBundleArgs.ne(_null()))._then();

        getInitBodyInjectionBlock().invoke(injectArgsMethod);
    }

    /**
     * 保存状态方法主体
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getSaveStateMethodBody() {
        return instanceStateDelegate.getSaveStateMethodBody();
    }

    /**
     * 保存状态包参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getSaveStateBundleParam() {
        return instanceStateDelegate.getSaveStateBundleParam();
    }

    /**
     * 恢复状态的方法
     *
     * @return {@link JMethod}
     */
    @Override
    public JMethod getRestoreStateMethod() {
        return instanceStateDelegate.getRestoreStateMethod();
    }

    /**
     * 让身体恢复状态方法
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getRestoreStateMethodBody() {
        return instanceStateDelegate.getRestoreStateMethodBody();
    }

    /**
     * 恢复状态包参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getRestoreStateBundleParam() {
        return instanceStateDelegate.getRestoreStateBundleParam();
    }

    /**
     * 创建选项菜单方法主体
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnCreateOptionsMenuMethodBody() {
        if (onCreateOptionsMenuMethodBody == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMethodBody;
    }

    /**
     * 创建选项菜单方法膨胀的身体
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnCreateOptionsMenuMethodInflateBody() {
        if (onCreateOptionsMenuMethodInflateBody == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMethodInflateBody;
    }

    /**
     * 创建选项菜单菜单增压泵var
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnCreateOptionsMenuMenuInflaterVar() {
        if (onCreateOptionsMenuMenuInflaterVar == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMenuInflaterVar;
    }

    /**
     * 创建选项菜单菜单参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnCreateOptionsMenuMenuParam() {
        if (onCreateOptionsMenuMenuParam == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMenuParam;
    }

    /**
     * 选择项选择项
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnOptionsItemSelectedItem() {
        if (onOptionsItemSelectedItem == null) {
            setOnOptionsItemSelected();
        }
        return onOptionsItemSelectedItem;
    }

    /**
     * 选择项选中项id
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnOptionsItemSelectedItemId() {
        if (onOptionsItemSelectedItemId == null) {
            setOnOptionsItemSelected();
        }
        return onOptionsItemSelectedItemId;
    }

    /**
     * 选择项选中的中间区域
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnOptionsItemSelectedMiddleBlock() {
        if (onOptionsItemSelectedMiddleBlock == null) {
            setOnOptionsItemSelected();
        }
        return onOptionsItemSelectedMiddleBlock;
    }

    /**
     * 相处能力结果块
     *
     * @param requestCode 请求的代码
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnAbilityResultCaseBlock(int requestCode) {
        return onAbilityResultDelegate.getCaseBlock(requestCode);
    }

    /**
     * 结果数据参数的能力
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnAbilityResultDataParam() {
        return onAbilityResultDelegate.getDataParam();
    }

    /**
     * 结果结果代码的能力参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnAbilityResultResultCodeParam() {
        return onAbilityResultDelegate.getResultCodeParam();
    }

    /**
     * 能力的结果的方法
     *
     * @return {@link JMethod}
     */
    @Override
    public JMethod getOnAbilityResultMethod() {
        return onAbilityResultDelegate.getMethod();
    }

    /**
     * 得到目的过滤字段
     *
     * @param intentFilterData 目的筛选数据
     * @return {@link JFieldVar}
     */
    @Override
    public JFieldVar getIntentFilterField(IntentFilterData intentFilterData) {
        return receiverRegistrationDelegate.getIntentFilterField(intentFilterData);
    }

    /**
     * 超级块后开始生命周期
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getStartLifecycleAfterSuperBlock() {
        return getOnCreateAfterSuperBlock();
    }

    /**
     * 超级块之前把生命周期结束
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getEndLifecycleBeforeSuperBlock() {
        return getOnDestroyBeforeSuperBlock();
    }

    /**
     * 在创建后超级块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnCreateAfterSuperBlock() {
        if (setOnComponentAttachedBlock == null) {
            setOnComponentAttached();
        }
        return setOnComponentAttachedBlock;
    }

    /**
     * 在破坏之前超级块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnDestroyBeforeSuperBlock() {
        return null;
    }

    /**
     * 超级块后开始
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnStartAfterSuperBlock() {
        if (onStartAfterSuperBlock == null) {
            setOnStart();
        }
        return onStartAfterSuperBlock;
    }

    /**
     * 超级块之前停止
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnStopBeforeSuperBlock() {
        if (onStopBeforeSuperBlock == null) {
            setOnStop();
        }
        return onStopBeforeSuperBlock;
    }

    /**
     * 超级块后的简历
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnResumeAfterSuperBlock() {
        return null;
    }

    /**
     * 暂停超级块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnPauseBeforeSuperBlock() {
        return null;
    }

    /**
     * 活跃在超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnActiveAfterSuperBlock() {
        if (onActiveAfterSuperBlock == null) {
            setOnActive();
        }
        return onActiveAfterSuperBlock;
    }

    /**
     * 不活跃的超级块之前
     *
     * @return {@link JBlock}
     */
    public JBlock getOnInactiveBeforeSuperBlock() {
        if (onInactiveBeforeSuperBlock == null) {
            setOnInactive();
        }
        return onInactiveBeforeSuperBlock;
    }

    /**
     * 背景在超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnBackgroundBeforeSuperBlock() {
        if (onBackgroundBeforeSuperBlock == null) {
            setOnBackground();
        }
        return onBackgroundBeforeSuperBlock;
    }

    /**
     * 前台在超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnForegroundBeforeSuperBlock() {
        if (onForegroundBeforeSuperBlock == null) {
            setOnForeground();
        }
        return onForegroundBeforeSuperBlock;
    }

    /**
     * 在超级块组件分离
     *
     * @return {@link JBlock}
     */
    public JBlock getOnComponentDetachBeforeSuperBlock() {
        if (onComponentDetachBeforeSuperBlock == null) {
            setOnDetach();
        }
        return onComponentDetachBeforeSuperBlock;
    }

    /**
     * 得到意图过滤器初始化块
     *
     * @param intentFilterData 目的筛选数据
     * @return {@link JBlock}
     */
    @Override
    public JBlock getIntentFilterInitializationBlock(IntentFilterData intentFilterData) {
        if (RegisterAt.OnAttachOnDetach.equals(intentFilterData.getRegisterAt())) {
            return getOnCreateAfterSuperBlock();
        }
        return getInitBodyInjectionBlock();
    }

    /**
     * 首选项屏幕初始化块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getPreferenceScreenInitializationBlock() {
        return getOnCreateAfterSuperBlock();
    }

    /**
     * 从资源注入块得到添加首选项
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getAddPreferencesFromResourceInjectionBlock() {
        return preferencesDelegate.getAddPreferencesFromResourceInjectionBlock();
    }

    /**
     * 从资源注入块后得到添加首选项
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getAddPreferencesFromResourceAfterInjectionBlock() {
        return preferencesDelegate.getAddPreferencesFromResourceAfterInjectionBlock();
    }

    /**
     * 会发现偏好持有人
     *
     * @param idRef id裁判
     * @param preferenceClass 偏好类
     * @return {@link FoundPreferenceHolder}
     */
    @Override
    public FoundPreferenceHolder getFoundPreferenceHolder(JFieldRef idRef, AbstractJClass preferenceClass) {
        return preferencesDelegate.getFoundPreferenceHolder(idRef, preferenceClass);
    }

    /**
     * 会发现偏好持有人
     *
     * @param idRef id裁判
     * @param preferenceClass 偏好类
     * @param fieldRef 现场裁判
     * @return {@link FoundPreferenceHolder}
     */
    @Override
    public FoundPreferenceHolder getFoundPreferenceHolder(JFieldRef idRef, AbstractJClass preferenceClass,
        IJAssignmentTarget fieldRef) {
        return preferencesDelegate.getFoundPreferenceHolder(idRef, preferenceClass, fieldRef);
    }

    /**
     * 使用支持v7的偏好
     *
     * @return boolean
     */
    @Override
    public boolean usingSupportV7Preference() {
        return preferencesDelegate.usingSupportV7Preference();
    }

    /**
     * usingOhosPreference
     *
     * @return boolean
     */
    @Override
    public boolean usingOhosPreference() {
        return preferencesDelegate.usingOhosPreference();
    }

    /**
     * 得到基本偏好类
     *
     * @return {@link AbstractJClass}
     */
    @Override
    public AbstractJClass getBasePreferenceClass() {
        return preferencesDelegate.getBasePreferenceClass();
    }
}
