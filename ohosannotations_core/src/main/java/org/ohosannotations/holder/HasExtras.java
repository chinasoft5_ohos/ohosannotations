/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

/**
 * 附加功能
 *
 * @since 2021-07-20
 */
public interface HasExtras extends GeneratedClassHolder {
    /**
     * 获取注入额外方法
     *
     * @return J方法
     */
    JMethod getInjectExtrasMethod();

    /**
     * 获取注入额外板块
     *
     * @return J板块
     */
    JBlock getInjectExtrasBlock();

    /**
     * 获取额外注入
     *
     * @return JVar字段
     */
    JVar getInjectExtras();
}
