/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JSwitch;
import com.helger.jcodemodel.JVar;

import static com.helger.jcodemodel.JExpr._super;
import static com.helger.jcodemodel.JMod.PUBLIC;

/**
 * KeyEventCallbackMethodsDelegate
 *
 * @param <T> 泛型
 * @since 2021-07-20
 */
public class KeyEventCallbackMethodsDelegate<T extends EComponentWithViewSupportHolder & HasKeyEventCallbackMethods>
    extends GeneratedClassHolderDelegate<T> {
    private JSwitch onKeyDownSwitchBody;
    private JVar onKeyDownKeyEventParam;
    private JSwitch onKeyPressAndHoldSwitchBody;
    private JVar onKeyPressAndHoldKeyEventParam;
    private JSwitch onKeyUpSwitchBody;
    private JVar onKeyUpKeyEventParam;

    /**
     * 构造参数
     *
     * @param holder 持有者
     */
    public KeyEventCallbackMethodsDelegate(T holder) {
        super(holder);
    }

    private void createOnKeyDownMethod() {
        JMethod method = getGeneratedClass().method(PUBLIC, codeModel().BOOLEAN, "onKeyDown");
        method.annotate(Override.class);
        JVar keyCode = method.param(codeModel().INT, "keyCode");
        onKeyDownKeyEventParam = method.param(getClasses().KEY_EVENT, "keyEvent");
        JBlock methodBody = method.body();
        onKeyDownSwitchBody = methodBody._switch(keyCode);
        methodBody._return(_super().invoke(method).arg(keyCode).arg(onKeyDownKeyEventParam));
    }

    private void createOnKeyLongPressMethod() {
        JMethod method = getGeneratedClass().method(PUBLIC, codeModel().BOOLEAN, "onKeyPressAndHold");
        method.annotate(Override.class);
        JVar keyCode = method.param(codeModel().INT, "keyCode");
        onKeyPressAndHoldKeyEventParam = method.param(getClasses().KEY_EVENT, "keyEvent");
        JBlock methodBody = method.body();
        onKeyPressAndHoldSwitchBody = methodBody._switch(keyCode);
        methodBody._return(_super().invoke(method).arg(keyCode).arg(onKeyPressAndHoldKeyEventParam));
    }

    private void createOnKeyUpMethod() {
        JMethod method = getGeneratedClass().method(PUBLIC, codeModel().BOOLEAN, "onKeyUp");
        method.annotate(Override.class);
        JVar keyCode = method.param(codeModel().INT, "keyCode");
        onKeyUpKeyEventParam = method.param(getClasses().KEY_EVENT, "keyEvent");
        JBlock methodBody = method.body();
        onKeyUpSwitchBody = methodBody._switch(keyCode);
        methodBody._return(_super().invoke(method).arg(keyCode).arg(onKeyUpKeyEventParam));
    }

    /**
     * getOnKeyDownSwitchBody
     *
     * @return JSwitch J开关
     */
    public JSwitch getOnKeyDownSwitchBody() {
        if (onKeyDownSwitchBody == null) {
            createOnKeyDownMethod();
        }
        return onKeyDownSwitchBody;
    }

    /**
     * getOnKeyDownKeyEventParam
     *
     * @return JVar 字段
     */
    public JVar getOnKeyDownKeyEventParam() {
        if (onKeyDownKeyEventParam == null) {
            createOnKeyDownMethod();
        }
        return onKeyDownKeyEventParam;
    }

    /**
     * getOnKeyPressAndHoldSwitchBody
     *
     * @return JSwitch J开关
     */
    public JSwitch getOnKeyPressAndHoldSwitchBody() {
        if (onKeyPressAndHoldSwitchBody == null) {
            createOnKeyLongPressMethod();
        }
        return onKeyPressAndHoldSwitchBody;
    }

    /**
     * getOnKeyPressAndHoldKeyEventParam
     *
     * @return JVar 字段
     */
    public JVar getOnKeyPressAndHoldKeyEventParam() {
        if (onKeyPressAndHoldKeyEventParam == null) {
            createOnKeyLongPressMethod();
        }
        return onKeyPressAndHoldKeyEventParam;
    }

    /**
     * getOnKeyUpSwitchBody
     *
     * @return JSwitch J开关
     */
    public JSwitch getOnKeyUpSwitchBody() {
        if (onKeyUpSwitchBody == null) {
            createOnKeyUpMethod();
        }
        return onKeyUpSwitchBody;
    }

    /**
     * getOnKeyUpKeyEventParam
     *
     * @return JVar 字段
     */
    public JVar getOnKeyUpKeyEventParam() {
        if (onKeyUpKeyEventParam == null) {
            createOnKeyUpMethod();
        }
        return onKeyUpKeyEventParam;
    }
}
