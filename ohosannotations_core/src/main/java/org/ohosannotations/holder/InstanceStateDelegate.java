/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

import static com.helger.jcodemodel.JExpr._null;
import static com.helger.jcodemodel.JExpr.ref;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

/**
 * 国家委托实例
 * The type Instance state delegate.
 *
 * @author Administrator
 * @since 2021-07-22
 */
public class InstanceStateDelegate extends GeneratedClassHolderDelegate<EComponentHolder> implements HasInstanceState {
    private JBlock saveStateMethodBody;
    private JVar saveStateBundleParam;
    private JMethod restoreStateMethod;
    private JBlock restoreStateMethodBody;
    private JVar restoreStateBundleParam;

    /**
     * 国家委托实例
     * Instantiates a new Instance state delegate.
     *
     * @param holder the holder
     */
    public InstanceStateDelegate(EComponentHolder holder) {
        super(holder);
    }

    /**
     * 保存状态方法主体
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getSaveStateMethodBody() {
        if (saveStateMethodBody == null) {
            setSaveStateMethod();
        }
        return saveStateMethodBody;
    }

    /**
     * 保存状态包参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getSaveStateBundleParam() {
        if (saveStateBundleParam == null) {
            setSaveStateMethod();
        }
        return saveStateBundleParam;
    }

    /**
     * 设置保存状态的方法
     */
    private void setSaveStateMethod() {
        JMethod method = getGeneratedClass().method(PUBLIC, codeModel().VOID, "onSaveInstanceState");
        method.annotate(Override.class);
        saveStateBundleParam = method.param(getClasses().PACMAP, "bundle" + generationSuffix());

        saveStateMethodBody = method.body();

        saveStateMethodBody.invoke(JExpr._super(), "onSaveInstanceState").arg(saveStateBundleParam);
    }

    /**
     * 恢复状态的方法
     *
     * @return {@link JMethod}
     */
    @Override
    public JMethod getRestoreStateMethod() {
        if (restoreStateMethod == null) {
            setRestoreStateMethod();
        }
        return restoreStateMethod;
    }

    /**
     * 让身体恢复状态方法
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getRestoreStateMethodBody() {
        if (restoreStateMethodBody == null) {
            setRestoreStateMethod();
        }
        return restoreStateMethodBody;
    }

    /**
     * 恢复状态包参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getRestoreStateBundleParam() {
        if (restoreStateBundleParam == null) {
            setRestoreStateMethod();
        }
        return restoreStateBundleParam;
    }

    /**
     * 设置恢复状态的方法
     */
    private void setRestoreStateMethod() {
        restoreStateMethod = getGeneratedClass()
            .method(PRIVATE, codeModel().VOID, "restoreSavedInstanceState" + generationSuffix());
        restoreStateBundleParam = restoreStateMethod.param(getClasses().PACMAP, "savedInstanceState");
        holder.getInitBodyInjectionBlock().invoke(restoreStateMethod).arg(restoreStateBundleParam);

        restoreStateMethodBody = restoreStateMethod.body();
        restoreStateMethodBody //
            ._if(ref("savedInstanceState").eq(_null())) //
            ._then()._return();
    }
}
