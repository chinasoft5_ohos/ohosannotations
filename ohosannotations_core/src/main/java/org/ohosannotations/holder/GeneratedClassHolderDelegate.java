/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JCodeModel;
import com.helger.jcodemodel.JDefinedClass;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.internal.process.ProcessHolder;

import javax.lang.model.element.TypeElement;

/**
 * 生成的类持有人委托
 *
 * @author dev
 * @param <T> 子类泛型
 * @since 2021-06-03
 */
public abstract class GeneratedClassHolderDelegate<T extends GeneratedClassHolder> implements GeneratedClassHolder {
    /**
     * 持有人
     */
    protected T holder;

    /**
     * 生成的类持有人委托
     *
     * @param holder 持有人
     */
    public GeneratedClassHolderDelegate(T holder) {
        this.holder = holder;
    }

    /**
     * 把生成的类
     *
     * @return {@link JDefinedClass}
     */
    @Override
    public final JDefinedClass getGeneratedClass() {
        return holder.getGeneratedClass();
    }

    /**
     * 得到带注释的元素
     *
     * @return {@link TypeElement}
     */
    @Override
    public final TypeElement getAnnotatedElement() {
        return holder.getAnnotatedElement();
    }

    /**
     * 让环境
     *
     * @return {@link OhosAnnotationsEnvironment}
     */
    @Override
    public OhosAnnotationsEnvironment getEnvironment() {
        return holder.getEnvironment();
    }

    /**
     * 得到类
     *
     * @return {@link ProcessHolder.Classes}
     */
    protected final ProcessHolder.Classes getClasses() {
        return getEnvironment().getClasses();
    }

    /**
     * 代码模型
     *
     * @return {@link JCodeModel}
     */
    protected final JCodeModel codeModel() {
        return getEnvironment().getCodeModel();
    }

    /**
     * ref类
     *
     * @param fullyQualifiedClassName 完全限定类名
     * @return {@link AbstractJClass}
     */
    protected final AbstractJClass refClass(String fullyQualifiedClassName) {
        return getEnvironment().getJClass(fullyQualifiedClassName);
    }

    /**
     * ref类
     *
     * @param clazz clazz
     * @return {@link AbstractJClass}
     */
    protected final AbstractJClass refClass(Class<?> clazz) {
        return getEnvironment().getJClass(clazz);
    }
}
