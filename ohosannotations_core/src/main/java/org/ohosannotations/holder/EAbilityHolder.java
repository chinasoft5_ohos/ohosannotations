/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import static com.helger.jcodemodel.JExpr._new;
import static com.helger.jcodemodel.JExpr._null;
import static com.helger.jcodemodel.JExpr._super;
import static com.helger.jcodemodel.JExpr._this;
import static com.helger.jcodemodel.JExpr.cast;
import static com.helger.jcodemodel.JExpr.invoke;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.TypeElement;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.helper.OhosManifest;
import org.ohosannotations.helper.AnnotationHelper;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.holder.ReceiverRegistrationDelegate.IntentFilterData;
import org.ohosannotations.internal.core.helper.AbilityIntentBuilder;
import org.ohosannotations.internal.core.helper.IntentBuilder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.AbstractJType;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JClassAlreadyExistsException;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

/**
 * EAbilityHolder
 *
 * @author dev
 * @since 2021-07-21
 */
public class EAbilityHolder extends EComponentWithViewSupportHolder
    implements HasIntentBuilder, HasExtras, HasInstanceState, HasOptionsMenu,
    HasOnAbilityResult, HasAbilityLifecycleMethods, HasReceiverRegistration, HasPreferenceHeaders {
    private AbilityIntentBuilder intentBuilder;
    private JMethod onCreate;
    private JMethod setIntent;
    private JMethod setContentViewLayout;
    private JVar initSavedInstanceParam;
    private JDefinedClass intentBuilderClass;
    private InstanceStateDelegate instanceStateDelegate;
    private OnAbilityResultDelegate onAbilityResultDelegate;
    private ReceiverRegistrationDelegate<EAbilityHolder> receiverRegistrationDelegate;
    private PreferenceAbilityDelegate preferencesHolder;
    private JMethod injectExtrasMethod;
    private JBlock injectExtrasBlock;
    private JVar injectExtras;
    private JBlock onCreateOptionsMenuMethodBody;
    private JBlock onCreateOptionsMenuMethodInflateBody;
    private JVar onCreateOptionsMenuMenuInflaterVar;
    private JVar onCreateOptionsMenuMenuParam;
    private JVar onOptionsItemSelectedItem;
    private JVar onOptionsItemSelectedItemId;
    private JBlock onOptionsItemSelectedMiddleBlock;
    private NonConfigurationHolder nonConfigurationHolder;
    private JBlock initIfNonConfigurationNotNullBlock;
    private JVar initNonConfigurationInstance;
    private JMethod getLastNonConfigurationInstance;
    private JBlock onRetainNonConfigurationInstanceBindBlock;
    private JVar onRetainNonConfigurationInstance;
    private JBlock onStartAfterSuperBlock;
    private JBlock onRestartAfterSuperBlock;
    private JBlock onResumeAfterSuperBlock;
    private JBlock onPauseBeforeSuperBlock;
    private JBlock onPauseAfterSuperBlock;
    private JBlock onStopBeforeSuperBlock;
    private JMethod onDestroyMethod;
    private JBlock onDestroyBeforeSuperBlock;
    private JBlock onDestroyAfterSuperBlock;
    private JMethod onNewIntentMethod;
    private JBlock onNewIntentAfterSuperBlock;
    private JBlock onConfigurationChangedBeforeSuperBlock;
    private JBlock onConfigurationChangedAfterSuperBlock;
    private JVar onConfigurationChangedNewConfigParam;
    private JMethod onContentChanged;
    private JBlock onContentChangedAfterSuperBlock;

    /**
     * EAbilityHolder
     *
     * @param environment 环境
     * @param annotatedElement 带注释的元素
     * @param ohosConfig 配置
     * @throws Exception 异常
     */
    public EAbilityHolder(OhosAnnotationsEnvironment environment,
        TypeElement annotatedElement, OhosManifest ohosConfig) throws Exception {
        super(environment, annotatedElement);
        instanceStateDelegate = new InstanceStateDelegate(this);
        onAbilityResultDelegate = new OnAbilityResultDelegate(this);
        receiverRegistrationDelegate = new ReceiverRegistrationDelegate<>(this);
        preferencesHolder = new PreferenceAbilityDelegate(this);
        setUIContent();
        intentBuilder = new AbilityIntentBuilder(this, ohosConfig);
        intentBuilder.build();
        implementBeanHolder();
    }

    /**
     * 设置上下文
     */
    @Override
    protected void setContextRef() {
        contextRef = _this();
    }

    /**
     * 设置初始化
     */
    @Override
    protected void setInit() {
        init = generatedClass.method(PRIVATE, getCodeModel().VOID, "init" + generationSuffix());
        AbstractJClass intentClass = getClasses().INTENT;
        initSavedInstanceParam = init.param(intentClass, "intent");
        getOnStart();
    }

    /**
     * 开始
     *
     * @return {@link JMethod}
     */
    public JMethod getOnStart() {
        if (onCreate == null) {
            setOnStart();
        }
        return onCreate;
    }

    /**
     * 得到设置意图
     *
     * @return {@link JMethod}
     */
    public JMethod getSetIntent() {
        if (setIntent == null) {
            setSetIntent();
        }
        return setIntent;
    }

    /**
     * 开始
     */
    private void setOnStart() {
        onCreate = generatedClass.method(PUBLIC, getCodeModel().VOID, "onStart");
        onCreate.annotate(Override.class);
        AbstractJClass intentClass = getClasses().INTENT;
        JVar onCreateSavedInstanceState = onCreate.param(intentClass, "intent");
        JBlock onCreateBody = onCreate.body();
        JVar previousNotifier = viewNotifierHelper.replacePreviousNotifier(onCreateBody);
        onCreateBody.invoke(_super(), onCreate).arg(onCreateSavedInstanceState);
        onCreateBody.invoke(getInit()).arg(onCreateSavedInstanceState);
        viewNotifierHelper.resetPreviousNotifier(onCreateBody, previousNotifier);
    }

    // CHECKSTYLE:OFF

    /**
     * 重启后超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnRestartAfterSuperBlock() { // ohos 没有OnRestart
        return onRestartAfterSuperBlock;
    }

    /**
     * 重启
     */
    private void setOnRestart() {
        JMethod method = generatedClass.method(JMod.PUBLIC, getCodeModel().VOID, "onRestart");
        method.annotate(Override.class);
        JBlock body = method.body();
        body.invoke(_super(), method);
        onRestartAfterSuperBlock = body.blockSimple();
    }

    /**
     * 活跃的
     */
    private void setOnActive() {
        JMethod method = generatedClass.method(JMod.PUBLIC, getCodeModel().VOID, "onActive");
        method.annotate(Override.class);
        JBlock body = method.body();
        body.invoke(_super(), method);
        onResumeAfterSuperBlock = body.blockSimple();
    }

    /**
     * 不活跃的
     */
    private void setOnInactive() {
        JMethod method = generatedClass.method(JMod.PUBLIC, getCodeModel().VOID, "onInactive");
        method.annotate(Override.class);
        JBlock body = method.body();
        onPauseBeforeSuperBlock = body.blockSimple();
        body.invoke(_super(), method);
        onPauseAfterSuperBlock = body.blockSimple();
    }

    /**
     * 设置新意图
     */
    private void setOnNewIntent() {
        onNewIntentMethod = generatedClass.method(JMod.PUBLIC, getCodeModel().VOID, "onNewIntent");
        onNewIntentMethod.annotate(Override.class);
        JVar intent = onNewIntentMethod.param(getClasses().INTENT, "intent");
        JBlock body = onNewIntentMethod.body();
        body.invoke(_super(), onNewIntentMethod).arg(intent);
        onNewIntentAfterSuperBlock = body.blockSimple();
    }

    /**
     * 设置设置意图
     */
    private void setSetIntent() {
        setIntent = generatedClass.method(PUBLIC, getCodeModel().VOID, "setIntent");
        setIntent.annotate(Override.class);
        JVar methodParam = setIntent.param(getClasses().INTENT, "newIntent");
        JBlock setIntentBody = setIntent.body();
        setIntentBody.invoke(_super(), setIntent).arg(methodParam);
    }

    /**
     * 停止
     *
     * @return {@link JMethod}
     */
    public JMethod getOnStop() {
        if (onDestroyMethod == null) {
            setOnStop();
        }
        return onDestroyMethod;
    }

    /**
     * 停止
     */
    private void setOnStop() {
        onDestroyMethod = generatedClass.method(JMod.PUBLIC, getCodeModel().VOID, "onStop");
        onDestroyMethod.annotate(Override.class);
        JBlock body = onDestroyMethod.body();
        onDestroyBeforeSuperBlock = body.blockSimple();
        body.invoke(_super(), onDestroyMethod);
        onDestroyAfterSuperBlock = body.blockSimple();
    }

    /**
     * 在配置改变之前超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnConfigurationChangedBeforeSuperBlock() {
        if (onConfigurationChangedBeforeSuperBlock == null) {
            setOnConfigurationChanged();
        }
        return onConfigurationChangedBeforeSuperBlock;
    }

    /**
     * 超级块配置更改
     *
     * @return {@link JBlock}
     */
    public JBlock getOnConfigurationChangedAfterSuperBlock() {
        if (onConfigurationChangedAfterSuperBlock == null) {
            setOnConfigurationChanged();
        }
        return onConfigurationChangedAfterSuperBlock;
    }

    /**
     * 在配置改变新的配置参数
     *
     * @return {@link JVar}
     */
    public JVar getOnConfigurationChangedNewConfigParam() {
        if (onConfigurationChangedNewConfigParam == null) {
            setOnConfigurationChanged();
        }
        return onConfigurationChangedNewConfigParam;
    }

    /**
     * 设置在配置改变
     */
    private void setOnConfigurationChanged() {
        JMethod method = generatedClass.method(JMod.PUBLIC, getCodeModel().VOID, "onConfigurationUpdated");
        method.annotate(Override.class);
        AbstractJClass configurationClass = getClasses().CONFIGURATION;
        onConfigurationChangedNewConfigParam = method.param(configurationClass, "configuration");
        JBlock body = method.body();
        onConfigurationChangedBeforeSuperBlock = body.blockSimple();
        body.invoke(_super(), method).arg(onConfigurationChangedNewConfigParam);
        onConfigurationChangedAfterSuperBlock = body.blockSimple();
    }

    /**
     * 上内容改变了
     *
     * @return {@link JMethod}
     */
    public JMethod getOnContentChanged() {
        if (onContentChanged == null) {
            setOnContentChanged();
        }
        return onContentChanged;
    }

    /**
     * 超级块内容更改
     *
     * @return {@link JBlock}
     */
    public JBlock getOnContentChangedAfterSuperBlock() {
        if (onContentChangedAfterSuperBlock == null) {
            setOnContentChanged();
        }
        return onContentChangedAfterSuperBlock;
    }

    /**
     * 设置内容改变了
     */
    private void setOnContentChanged() { // ohos 不支持
        onContentChanged = generatedClass.method(JMod.PUBLIC, getCodeModel().VOID, "onContentChanged");
        onContentChanged.annotate(Override.class);
        JBlock body = onContentChanged.body();
        body.invoke(_super(), onContentChanged);
        onContentChangedAfterSuperBlock = body.blockSimple();
    }

    /**
     * 创建选项菜单
     */
    private void setOnCreateOptionsMenu() { // ohos 不支持
        JMethod method = generatedClass
            .method(PUBLIC, getCodeModel().BOOLEAN, "onCreateOptionsMenu");
        method.annotate(Override.class);
        JBlock methodBody = method.body();
        onCreateOptionsMenuMenuParam = method.param(getClasses().MENU, "menu");
        onCreateOptionsMenuMenuInflaterVar = methodBody
            .decl(getClasses().MENU_INFLATER, "menuInflater", invoke("getMenuInflater"));
        onCreateOptionsMenuMethodInflateBody = methodBody.blockSimple();
        onCreateOptionsMenuMethodBody = methodBody.blockSimple();
        methodBody._return(_super().invoke(method).arg(onCreateOptionsMenuMenuParam));
    }

    /**
     * 项目选择上设置选项
     */
    private void setOnOptionsItemSelected() { // ohos 不支持
        JMethod method = generatedClass
            .method(JMod.PUBLIC, getCodeModel().BOOLEAN, "onOptionsItemSelected");
        method.annotate(Override.class);
        JBlock methodBody = method.body();
        onOptionsItemSelectedItem = method.param(getClasses().MENU_ITEM, "item");
        onOptionsItemSelectedItemId = methodBody
            .decl(getCodeModel().INT, "itemId_", onOptionsItemSelectedItem.invoke("getItemId"));
        onOptionsItemSelectedMiddleBlock = methodBody.blockSimple();

        methodBody._return(invoke(_super(), method).arg(onOptionsItemSelectedItem));
    }

    /**
     * 通过标签找到本地片段
     */
    @Override
    protected void setFindNativeFragmentByTag() {
        JMethod method = generatedClass
            .method(PRIVATE, getClasses().FRACTION, "findNativeFragmentByTag");
        JVar tagParam = method.param(getClasses().STRING, "tag");
        JBlock body = method.body();
        body._return(invoke("getFractionManager")
            .invoke("getFractionByTag").arg(tagParam));
        findNativeFragmentByTag = method;
    }

    /**
     * 设置内容视图布局
     *
     * @return {@link JMethod}
     */
    public JMethod getSetContentViewLayout() {
        if (setContentViewLayout == null) {
            setUIContent();
        }
        return setContentViewLayout;
    }

    /**
     * 设置uicontent
     */
    private void setUIContent() {
        getOnStart();
        setContentViewLayout = setUIContentMethod(new AbstractJType[]{getClasses()
            .COMPONENT_CONTAINER}, new String[]{"componentContainer"});
    }

    /**
     * 设置uicontent方法
     *
     * @param paramTypes 参数类型
     * @param paramNames 参数名称
     * @return {@link JMethod}
     */
    private JMethod setUIContentMethod(AbstractJType[] paramTypes, String[] paramNames) {
        JMethod method = generatedClass
            .method(JMod.PUBLIC, getCodeModel().VOID, "setUIContent");
        method.annotate(Override.class);

        List<JVar> params = new ArrayList<>();
        for (int i = 0; i < paramTypes.length; i++) {
            JVar param = method.param(paramTypes[i], paramNames[i]);
            params.add(param);
        }
        JBlock body = method.body();
        JInvocation superCall = body.invoke(JExpr._super(), method);
        for (JVar arg : params) {
            superCall.arg(arg);
        }
        viewNotifierHelper.invokeViewChanged(body);
        return method;
    }

    /**
     * 通过id获取找到视图表达式
     *
     * @param idParam id参数
     * @return {@link IJExpression}
     */
    @Override
    public IJExpression getFindViewByIdExpression(JVar idParam) {
        return JExpr._this().invoke("findComponentById").arg(idParam);
    }

    /**
     * 得到init参数保存实例
     *
     * @return {@link JVar}
     */
    public JVar getInitSavedInstanceParam() {
        return initSavedInstanceParam;
    }

    /**
     * 得到目的构建器
     *
     * @return {@link IntentBuilder}
     */
    @Override
    public IntentBuilder getIntentBuilder() {
        return intentBuilder;
    }

    /**
     * 设置意图构建器类
     *
     * @param intentBuilderClass 目的构建器类
     */
    @Override
    public void setIntentBuilderClass(JDefinedClass intentBuilderClass) {
        this.intentBuilderClass = intentBuilderClass;
    }

    /**
     * 得到目的构建器类
     *
     * @return {@link JDefinedClass}
     */
    @Override
    public JDefinedClass getIntentBuilderClass() {
        return intentBuilderClass;
    }

    /**
     * 得到注入额外的方法
     *
     * @return {@link JMethod}
     */
    @Override
    public JMethod getInjectExtrasMethod() {
        if (injectExtrasMethod == null) {
            setInjectExtras();
        }
        return injectExtrasMethod;
    }

    /**
     * 得到注入额外的块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInjectExtrasBlock() {
        if (injectExtrasBlock == null) {
            setInjectExtras();
        }
        return injectExtrasBlock;
    }

    /**
     * 得到注入额外的
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getInjectExtras() {
        if (injectExtras == null) {
            setInjectExtras();
        }
        return injectExtras;
    }

    /**
     * 设置注入额外的
     */
    private void setInjectExtras() {
        injectExtrasMethod = generatedClass
            .method(PRIVATE, getCodeModel().VOID, "injectExtras" + generationSuffix());
        JBlock injectExtrasBody = injectExtrasMethod.body();
        injectExtras = injectExtrasBody.decl(getClasses()
            .INTENT_PARAMS, "params_", invoke("getIntent").invoke("getParams"));
        injectExtrasBlock = injectExtrasBody._if(injectExtras.ne(_null()))._then();

        getSetIntent().body().invoke(injectExtrasMethod);
        getInitBodyInjectionBlock().invoke(injectExtrasMethod);
    }

    /**
     * 超级块后得到新的目的
     *
     * @return {@link JBlock}
     */
    public JBlock getOnNewIntentAfterSuperBlock() {
        if (onNewIntentAfterSuperBlock == null) {
            setOnNewIntent();
        }
        return onNewIntentAfterSuperBlock;
    }

    /**
     * 保存状态方法主体
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getSaveStateMethodBody() {
        return instanceStateDelegate.getSaveStateMethodBody();
    }

    /**
     * 保存状态包参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getSaveStateBundleParam() {
        return instanceStateDelegate.getSaveStateBundleParam();
    }

    /**
     * 恢复状态的方法
     *
     * @return {@link JMethod}
     */
    @Override
    public JMethod getRestoreStateMethod() {
        return instanceStateDelegate.getRestoreStateMethod();
    }

    /**
     * 让身体恢复状态方法
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getRestoreStateMethodBody() {
        return instanceStateDelegate.getRestoreStateMethodBody();
    }

    /**
     * 恢复状态包参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getRestoreStateBundleParam() {
        return instanceStateDelegate.getRestoreStateBundleParam();
    }

    /**
     * 创建选项菜单方法主体
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnCreateOptionsMenuMethodBody() {
        if (onCreateOptionsMenuMethodBody == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMethodBody;
    }

    /**
     * 创建选项菜单方法膨胀的身体
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnCreateOptionsMenuMethodInflateBody() {
        if (onCreateOptionsMenuMethodInflateBody == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMethodInflateBody;
    }

    /**
     * 创建选项菜单菜单增压泵var
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnCreateOptionsMenuMenuInflaterVar() {
        if (onCreateOptionsMenuMenuInflaterVar == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMenuInflaterVar;
    }

    /**
     * 创建选项菜单菜单参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnCreateOptionsMenuMenuParam() {
        if (onCreateOptionsMenuMenuParam == null) {
            setOnCreateOptionsMenu();
        }
        return onCreateOptionsMenuMenuParam;
    }

    /**
     * 选择项选择项
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnOptionsItemSelectedItem() {
        if (onOptionsItemSelectedItem == null) {
            setOnOptionsItemSelected();
        }
        return onOptionsItemSelectedItem;
    }

    /**
     * 选择项选中项id
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnOptionsItemSelectedItemId() {
        if (onOptionsItemSelectedItemId == null) {
            setOnOptionsItemSelected();
        }
        return onOptionsItemSelectedItemId;
    }

    /**
     * 选择项选中的中间区域
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnOptionsItemSelectedMiddleBlock() {
        if (onOptionsItemSelectedMiddleBlock == null) {
            setOnOptionsItemSelected();
        }
        return onOptionsItemSelectedMiddleBlock;
    }

    /**
     * 获取非配置持有人
     *
     * @return {@link NonConfigurationHolder}
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    public NonConfigurationHolder getNonConfigurationHolder() throws JClassAlreadyExistsException {
        if (nonConfigurationHolder == null) {
            setNonConfigurationHolder();
        }
        return nonConfigurationHolder;
    }

    /**
     * 设置非配置持有人
     *
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    private void setNonConfigurationHolder() throws JClassAlreadyExistsException {
        nonConfigurationHolder = new NonConfigurationHolder(this);
    }

    /**
     * 如果非空非配置块得到初始化
     *
     * @return {@link JBlock}
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    public JBlock getInitIfNonConfigurationNotNullBlock() throws JClassAlreadyExistsException {
        if (initIfNonConfigurationNotNullBlock == null) {
            setInitNonConfigurationInstance();
        }
        return initIfNonConfigurationNotNullBlock;
    }

    /**
     * 获取初始化非配置实例
     *
     * @return {@link JVar}
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    public JVar getInitNonConfigurationInstance() throws JClassAlreadyExistsException {
        if (initNonConfigurationInstance == null) {
            setInitNonConfigurationInstance();
        }
        return initNonConfigurationInstance;
    }

    /**
     * 设置初始化非配置实例
     *
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    private void setInitNonConfigurationInstance() throws JClassAlreadyExistsException {
        JBlock initBody = getInitBodyInjectionBlock();
        JDefinedClass ncHolderClass = getNonConfigurationHolder().getGeneratedClass();
        initNonConfigurationInstance = initBody.decl(ncHolderClass, "nonConfigurationInstance",
            cast(ncHolderClass, _super().invoke(getGetLastNonConfigurationInstance())));
        initIfNonConfigurationNotNullBlock = initBody._if(initNonConfigurationInstance.ne(_null()))._then();
    }

    /**
     * 获得最后一个非配置实例
     *
     * @return {@link JMethod}
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    public JMethod getGetLastNonConfigurationInstance() throws JClassAlreadyExistsException {
        if (getLastNonConfigurationInstance == null) {
            setGetLastNonConfigurationInstance();
        }
        return getLastNonConfigurationInstance;
    }

    /**
     * 获得最后一个非配置实例
     *
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    private void setGetLastNonConfigurationInstance() throws JClassAlreadyExistsException {
        String getLastNonConfigurationInstanceName = "getLastStoredDataWhenConfigChanged";

        NonConfigurationHolder ncHolder = getNonConfigurationHolder();
        JDefinedClass ncHolderClass = ncHolder.getGeneratedClass();
        JFieldVar superNonConfigurationInstanceField = ncHolder.getSuperNonConfigurationInstanceField();

        getLastNonConfigurationInstance = generatedClass
            .method(PUBLIC, Object.class, getLastNonConfigurationInstanceName);
        getLastNonConfigurationInstance.annotate(Override.class);
        JBlock body = getLastNonConfigurationInstance.body();
        JVar nonConfigurationInstance = body.decl(ncHolderClass, "nonConfigurationInstance",
            cast(ncHolderClass, _super().invoke(getLastNonConfigurationInstance)));
        body._if(nonConfigurationInstance.eq(_null()))._then()._return(_null());
        body._return(nonConfigurationInstance.ref(superNonConfigurationInstanceField));
    }

    /**
     * 保留非配置实例绑定块
     *
     * @return {@link JBlock}
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    public JBlock getOnRetainNonConfigurationInstanceBindBlock() throws JClassAlreadyExistsException {
        if (onRetainNonConfigurationInstanceBindBlock == null) {
            setOnRetainNonConfigurationInstance();
        }
        return onRetainNonConfigurationInstanceBindBlock;
    }

    /**
     * 保留非配置实例
     *
     * @return {@link JVar}
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    public JVar getOnRetainNonConfigurationInstance() throws JClassAlreadyExistsException {
        if (onRetainNonConfigurationInstance == null) {
            setOnRetainNonConfigurationInstance();
        }
        return onRetainNonConfigurationInstance;
    }

    /**
     * 保留非配置实例
     *
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    private void setOnRetainNonConfigurationInstance() throws JClassAlreadyExistsException {
        String onRetainNonConfigurationInstanceName = "onStoreDataWhenConfigChange";

        NonConfigurationHolder ncHolder = getNonConfigurationHolder();
        JDefinedClass ncHolderClass = ncHolder.getGeneratedClass();

        JMethod onRetainNonConfigurationInstanceMethod = generatedClass
            .method(PUBLIC, ncHolderClass, onRetainNonConfigurationInstanceName);
        onRetainNonConfigurationInstanceMethod.annotate(Override.class);
        JBlock methodBody = onRetainNonConfigurationInstanceMethod.body();
        onRetainNonConfigurationInstance = methodBody
            .decl(ncHolderClass, "nonConfigurationInstanceState_", _new(ncHolderClass));
        IJExpression superCall = _super().invoke(onRetainNonConfigurationInstanceMethod);
        methodBody.assign(onRetainNonConfigurationInstance.ref(ncHolder
            .getSuperNonConfigurationInstanceField()), superCall);
        onRetainNonConfigurationInstanceBindBlock = methodBody.blockSimple();
        methodBody._return(onRetainNonConfigurationInstance);
    }

    /**
     * 得到片段的能力
     *
     * @param annotationHelper 注释的助手
     * @return {@link TypeElement}
     */
    private TypeElement getFragmentAbility(AnnotationHelper annotationHelper) {
        TypeElement supportFragmentAbility = annotationHelper
            .typeElementFromQualifiedName(CanonicalNameConstants.FRACTION_ABILITY);
        if (supportFragmentAbility == null) {
            return annotationHelper.typeElementFromQualifiedName(CanonicalNameConstants.OHOSX_FRAGMENT_ABILITY);
        }
        return supportFragmentAbility;
    }

    /**
     * 相处能力结果块
     *
     * @param requestCode 请求的代码
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnAbilityResultCaseBlock(int requestCode) {
        return onAbilityResultDelegate.getCaseBlock(requestCode);
    }

    /**
     * 结果数据参数的能力
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnAbilityResultDataParam() {
        return onAbilityResultDelegate.getDataParam();
    }

    /**
     * 结果结果代码的能力参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnAbilityResultResultCodeParam() {
        return onAbilityResultDelegate.getResultCodeParam();
    }

    /**
     * 超级块后相处能力的结果
     *
     * @return {@link JBlock}
     */
    public JBlock getOnAbilityResultAfterSuperBlock() {
        return onAbilityResultDelegate.getAfterSuperBlock();
    }

    /**
     * 相处能力请求代码参数结果
     *
     * @return {@link JVar}
     */
    public JVar getOnAbilityResultRequestCodeParam() {
        return onAbilityResultDelegate.getRequestCodeParam();
    }

    /**
     * 能力的结果的方法
     *
     * @return {@link JMethod}
     */
    @Override
    public JMethod getOnAbilityResultMethod() {
        return onAbilityResultDelegate.getMethod();
    }

    /**
     * 破坏后的超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnDestroyAfterSuperBlock() {
        if (onDestroyAfterSuperBlock == null) {
            setOnStop();
        }
        return onDestroyAfterSuperBlock;
    }

    /**
     * 超级块后的简历
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnResumeAfterSuperBlock() {
        if (onResumeAfterSuperBlock == null) {
            setOnActive();
        }
        return onResumeAfterSuperBlock;
    }

    /**
     * 在创建后超级块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnCreateAfterSuperBlock() {
        return getInitBody();
    }

    /**
     * 在破坏之前超级块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnDestroyBeforeSuperBlock() {
        if (onDestroyBeforeSuperBlock == null) {
            setOnStop();
        }
        return onDestroyBeforeSuperBlock;
    }

    /**
     * 超级块后开始
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnStartAfterSuperBlock() {
        return onStartAfterSuperBlock;
    }

    /**
     * 超级块之前停止
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnStopBeforeSuperBlock() {
        return onStopBeforeSuperBlock;
    }

    /**
     * 暂停超级块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnPauseBeforeSuperBlock() {
        if (onPauseBeforeSuperBlock == null) {
            setOnInactive();
        }
        return onPauseBeforeSuperBlock;
    }

    /**
     * 暂停后超级块
     *
     * @return {@link JBlock}
     */
    public JBlock getOnPauseAfterSuperBlock() {
        if (onPauseAfterSuperBlock == null) {
            setOnInactive();
        }
        return onPauseAfterSuperBlock;
    }

    /**
     * 超级块后开始生命周期
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getStartLifecycleAfterSuperBlock() {
        return getOnCreateAfterSuperBlock();
    }

    /**
     * 超级块之前把生命周期结束
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getEndLifecycleBeforeSuperBlock() {
        return getOnDestroyAfterSuperBlock();
    }

    /**
     * 得到目的过滤字段
     *
     * @param intentFilterData 目的筛选数据
     * @return {@link JFieldVar}
     */
    @Override
    public JFieldVar getIntentFilterField(IntentFilterData intentFilterData) {
        return receiverRegistrationDelegate.getIntentFilterField(intentFilterData);
    }

    /**
     * 得到意图过滤器初始化块
     *
     * @param intentFilterData 目的筛选数据
     * @return {@link JBlock}
     */
    @Override
    public JBlock getIntentFilterInitializationBlock(IntentFilterData intentFilterData) {
        return getInitBodyInjectionBlock();
    }

    /**
     * 首选项屏幕初始化块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getPreferenceScreenInitializationBlock() {
        return getOnStart().body();
    }

    /**
     * 从资源注入块得到添加首选项
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getAddPreferencesFromResourceInjectionBlock() {
        return preferencesHolder.getAddPreferencesFromResourceInjectionBlock();
    }

    /**
     * 从资源注入块后得到添加首选项
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getAddPreferencesFromResourceAfterInjectionBlock() {
        return preferencesHolder.getAddPreferencesFromResourceAfterInjectionBlock();
    }

    /**
     * 会发现偏好持有人
     *
     * @param idRef id裁判
     * @param preferenceClass 偏好类
     * @return {@link FoundPreferenceHolder}
     */
    @Override
    public FoundPreferenceHolder getFoundPreferenceHolder
    (JFieldRef idRef, AbstractJClass preferenceClass) {
        return preferencesHolder.getFoundPreferenceHolder(idRef, preferenceClass);
    }

    /**
     * 会发现偏好持有人
     *
     * @param idRef id裁判
     * @param preferenceClass 偏好类
     * @param fieldRef 现场裁判
     * @return {@link FoundPreferenceHolder}
     */
    @Override
    public FoundPreferenceHolder getFoundPreferenceHolder(JFieldRef idRef,
        AbstractJClass preferenceClass, IJAssignmentTarget fieldRef) {
        return preferencesHolder.getFoundPreferenceHolder(idRef, preferenceClass, fieldRef);
    }

    /**
     * 使用支持v7的偏好
     *
     * @return boolean
     */
    @Override
    public boolean usingSupportV7Preference() {
        return preferencesHolder.usingSupportV7Preference();
    }

    /**
     * 使用嗳哟偏好
     *
     * @return boolean
     */
    @Override
    public boolean usingOhosPreference() {
        return preferencesHolder.usingOhosPreference();
    }

    /**
     * 得到基本偏好类
     *
     * @return {@link AbstractJClass}
     */
    @Override
    public AbstractJClass getBasePreferenceClass() {
        return preferencesHolder.getBasePreferenceClass();
    }

    /**
     * 建立头信息块
     *
     * @return {@link JBlock}
     */
    @Override
    public JBlock getOnBuildHeadersBlock() {
        return preferencesHolder.getOnBuildHeadersBlock();
    }

    /**
     * 构建头目标参数
     *
     * @return {@link JVar}
     */
    @Override
    public JVar getOnBuildHeadersTargetParam() {
        return preferencesHolder.getOnBuildHeadersTargetParam();
    }
}
