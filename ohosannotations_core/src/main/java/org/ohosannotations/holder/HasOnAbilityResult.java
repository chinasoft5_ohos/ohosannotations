/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

/**
 * Ability结果
 *
 * @since 2021-07-20
 */
public interface HasOnAbilityResult extends GeneratedClassHolder {
    /**
     * 获得Ability结果
     *
     * @param requestCode 请求码
     * @return JBlock J板块
     */
    JBlock getOnAbilityResultCaseBlock(int requestCode);

    /**
     * 获取Ability结果数据参数
     *
     * @return JVar艾端
     */
    JVar getOnAbilityResultDataParam();

    /**
     * 获取Ability结果代码参数
     *
     * @return JVar 字段
     */
    JVar getOnAbilityResultResultCodeParam();

    /**
     * 获取Ability结果方法
     *
     * @return JMethod j方法
     */
    JMethod getOnAbilityResultMethod();
}
