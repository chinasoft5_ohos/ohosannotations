/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.holder;

import com.helger.jcodemodel.JDefinedClass;

import org.ohosannotations.internal.core.helper.IntentBuilder;

/**
 * 意图创建者
 *
 * @since 2021-07-19
 */
public interface HasIntentBuilder extends GeneratedClassHolder {
    /**
     * 得到目的构建器
     *
     * @return IntentBuilder 意图创建者
     */
    IntentBuilder getIntentBuilder();

    /**
     * 设置意图创建者
     *
     * @param intentBuilderClass 意图构造器
     */
    void setIntentBuilderClass(JDefinedClass intentBuilderClass);

    /**
     * 获取意图构造器
     *
     * @return JDefinedClass 定义类
     */
    JDefinedClass getIntentBuilderClass();
}
