/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.SliderTouchEnd;
import org.ohosannotations.holder.OnSliderProgressUpdatedListenerHolder;

/**
 * SliderTouchEndHandler
 *
 * @since 2021-06-16
 */
public class SliderTouchEndHandler extends AbstractSliderTouchHandler {
    /**
     * SliderTouchEndHandler
     *
     * @param environment 环境
     */
    public SliderTouchEndHandler(OhosAnnotationsEnvironment environment) {
        super(SliderTouchEnd.class, environment);
    }

    @Override
    protected JBlock getMethodBodyToCall(OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder) {
        return onSliderProgressUpdatedListenerHolder.getOnTouchEndBody();
    }

    @Override
    protected JVar getMethodParamToPass(OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder) {
        return onSliderProgressUpdatedListenerHolder.getOnTouchEndSliderParam();
    }
}
