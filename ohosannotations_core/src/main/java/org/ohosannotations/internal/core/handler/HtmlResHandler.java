/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.holder.EComponentHolder;
import org.ohosannotations.internal.core.model.OhosRes;

import javax.lang.model.element.Element;

/**
 * Html资源Handler
 *
 * @since 2021-06-18
 */
public class HtmlResHandler extends AbstractResHandler {
    /**
     * 构造参数
     *
     * @param environment 环境
     */
    public HtmlResHandler(OhosAnnotationsEnvironment environment) {
        super(OhosRes.HTML, environment);
    }

    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        valid.addError("暂时不支持此注解@HtmlRes");
        super.validateEnclosingElement(element, valid);
    }

    @Override
    protected IJExpression getInstanceInvocation(EComponentHolder holder, JFieldRef idRef,
        IJAssignmentTarget fieldRef, JBlock targetBlock) {
        return getClasses().HTML.staticInvoke("fromHtml").arg(holder.getResourcesRef()
            .invoke(OhosRes.HTML.getResourceMethodName()).arg(idRef));
    }
}
