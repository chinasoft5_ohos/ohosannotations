/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JMethod;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.SupposeUiThread;
import org.ohosannotations.api.BackgroundExecutor;
import org.ohosannotations.holder.EComponentHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

/**
 * 假设ui线程处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class SupposeUiThreadHandler extends SupposeThreadHandler {
    private static final String METHOD_CHECK_UI_THREAD = "checkUiThread";

    /**
     * 假设ui线程处理程序
     *
     * @param environment 环境
     */
    public SupposeUiThreadHandler(OhosAnnotationsEnvironment environment) {
        super(SupposeUiThread.class, environment);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     * @throws Exception 异常
     */
    @Override
    public void process(Element element, EComponentHolder holder) throws Exception {
        ExecutableElement executableElement = (ExecutableElement) element;

        JMethod delegatingMethod = codeModelHelper.overrideAnnotatedMethod(executableElement, holder);
        JBlock body = delegatingMethod.body();

        AbstractJClass bgExecutor = getJClass(BackgroundExecutor.class);

        body.pos(0);
        body.staticInvoke(bgExecutor, METHOD_CHECK_UI_THREAD);
        body.pos(body.getContents().size());
    }
}
