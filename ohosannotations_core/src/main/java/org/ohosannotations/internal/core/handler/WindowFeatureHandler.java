/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JExpr;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.WindowFeature;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.holder.EAbilityHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * 窗口功能处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class WindowFeatureHandler extends BaseAnnotationHandler<EAbilityHolder> {
    private static final String WINDOW_FEATURE = "supportRequestWindowFeature";

    /**
     * WindowFeatureHandler
     *
     * @param environment 环境
     */
    public WindowFeatureHandler(OhosAnnotationsEnvironment environment) {
        super(WindowFeature.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validation.addError("暂时不支持此注解@WindowFeature");
        validatorHelper.hasEAbility(element, validation);
    }

    @Override
    public void process(Element element, EAbilityHolder holder) throws Exception {
        WindowFeature annotation = element.getAnnotation(WindowFeature.class);
        int[] features = annotation.value();

        TypeElement appCompatAbility =
            annotationHelper.typeElementFromQualifiedName(CanonicalNameConstants.APPCOMPAT_ABILITY);
        TypeElement ohosxAppCompatAbility =
            annotationHelper.typeElementFromQualifiedName(CanonicalNameConstants.OHOSX_APPCOMPAT_ABILITY);
        TypeElement actionBarAbility =
            annotationHelper.typeElementFromQualifiedName(CanonicalNameConstants.ACTIONBAR_ABILITY);
        TypeElement type = (TypeElement) element;

        String methodName;
        if (appCompatAbility != null && annotationHelper.isSubtype(type, appCompatAbility)) {
            methodName = WINDOW_FEATURE;
        } else if (ohosxAppCompatAbility != null && annotationHelper.isSubtype(type, ohosxAppCompatAbility)) {
            methodName = WINDOW_FEATURE;
        } else if (actionBarAbility != null && annotationHelper.isSubtype(type, actionBarAbility)) {
            methodName = WINDOW_FEATURE;
        } else {
            methodName = "requestWindowFeature";
        }
        for (int feature : features) {
            holder.getInitBodyInjectionBlock().invoke(methodName).arg(JExpr.lit(feature));
        }
    }
}
