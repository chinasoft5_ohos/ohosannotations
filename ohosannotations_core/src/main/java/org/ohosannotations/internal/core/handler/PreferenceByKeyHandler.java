/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.type.TypeMirror;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.annotations.PreferenceByKey;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.FoundPreferenceHolder;
import org.ohosannotations.holder.HasPreferences;
import org.ohosannotations.rclass.IRClass;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

/**
 * 偏好的关键处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class PreferenceByKeyHandler extends BaseAnnotationHandler<HasPreferences>
    implements MethodInjectionHandler<HasPreferences> {
    private final InjectHelper<HasPreferences> injectHelper;

    /**
     * 偏好的关键处理程序
     *
     * @param environment 环境
     */
    public PreferenceByKeyHandler(OhosAnnotationsEnvironment environment) {
        super(PreferenceByKey.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    protected void validate(Element element, ElementValidation valid) {
        injectHelper.validate(PreferenceByKey.class, element, valid);
        if (!valid.isValid()) {
            return;
        }

        if (element.getKind() == ElementKind.PARAMETER) {
            validatorHelper
                .enclosingElementExtendsPreferenceAbilityOrPreferenceFraction(element.getEnclosingElement(), valid);
        } else {
            validatorHelper.enclosingElementExtendsPreferenceAbilityOrPreferenceFraction(element, valid);
        }

        Element param = injectHelper.getParam(element);
        validatorHelper.isDeclaredType(param, valid);

        validatorHelper.extendsPreference(param, valid);

        validatorHelper.isNotPrivate(element, valid);

        validatorHelper.resIdsExist(element, IRClass.Res
            .STRING, IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, valid);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     * @throws Exception 异常
     */
    @Override
    public void process(Element element, HasPreferences holder) throws Exception {
        injectHelper.process(element, holder);
    }

    /**
     * 得到调用块
     *
     * @param holder 持有人
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInvocationBlock(HasPreferences holder) {
        return holder.getAddPreferencesFromResourceInjectionBlock();
    }

    /**
     * 赋值
     *
     * @param targetBlock 目标块
     * @param fieldRef 现场裁判
     * @param holder 持有人
     * @param element 元素
     * @param param 参数
     */
    @Override
    public void assignValue(JBlock targetBlock,
                            IJAssignmentTarget fieldRef, HasPreferences holder, Element element, Element param) {
        TypeMirror prefFieldTypeMirror = param.asType();
        String typeQualifiedName = prefFieldTypeMirror.toString();

        JFieldRef idRef = annotationHelper.extractOneAnnotationFieldRef(element, IRClass.Res.STRING, true);
        AbstractJClass preferenceClass = getJClass(typeQualifiedName);

        IJAssignmentTarget preferenceHolderTarget = null;
        if (element.getKind() == ElementKind.FIELD) {
            preferenceHolderTarget = fieldRef;
        }
        FoundPreferenceHolder preferenceHolder = holder
            .getFoundPreferenceHolder(idRef, preferenceClass, preferenceHolderTarget);
        if (!preferenceHolder.getRef().equals(preferenceHolderTarget)) {
            targetBlock.add(fieldRef.assign(preferenceHolder.getOrCastRef(preferenceClass)));
        }
    }

    /**
     * 验证封装元素
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEAbilityOrEFraction(element, valid);
    }
}

