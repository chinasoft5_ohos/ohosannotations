/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;


import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.holder.GeneratedClassHolder;
import org.ohosannotations.internal.core.helper.CoreValidatorHelper;

/**
 * 核心基础注释处理程序
 *
 * @param <T> 泛型
 * @since 2021-07-20
 */
public abstract class CoreBaseAnnotationHandler<T extends GeneratedClassHolder> extends BaseAnnotationHandler<T> {
    /**
     * 核心验证器辅助
     */
    protected final CoreValidatorHelper coreValidatorHelper;

    /**
     * 构造参数
     *
     * @param targetClass 字节类
     * @param environment 注解环境
     */
    public CoreBaseAnnotationHandler(Class<?> targetClass, OhosAnnotationsEnvironment environment) {
        this(targetClass.getCanonicalName(), environment);
    }

    /**
     * 构造参数
     *
     * @param target 目标
     * @param environment 环境
     */
    public CoreBaseAnnotationHandler(String target, OhosAnnotationsEnvironment environment) {
        super(target, environment);
        coreValidatorHelper = new CoreValidatorHelper(annotationHelper);
    }
}
