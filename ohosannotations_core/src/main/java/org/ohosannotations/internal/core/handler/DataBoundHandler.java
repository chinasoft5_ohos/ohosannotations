/**
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.DataBound;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;

import javax.lang.model.element.Element;

/**
 * 数据绑定处理程序
 *
 * @since 2021-07-20
 */
public class DataBoundHandler extends CoreBaseAnnotationHandler<EComponentWithViewSupportHolder> {
    /**
     * 构造参数
     *
     * @param environment 环境
     */
    public DataBoundHandler(OhosAnnotationsEnvironment environment) {
        super(DataBound.class, environment);
    }

    @Override
    protected void validate(Element element, ElementValidation validation) {
        validation.addError("暂时不支持此注解@DataBound");
        coreValidatorHelper.hasDataBindingOnClasspath(validation);
        coreValidatorHelper.hasEAbilityOrEFractionOrEViewGroup(element, element, validation);
    }

    @Override
    public void process(Element element, EComponentWithViewSupportHolder holder) throws Exception {
        // nothing to do
    }
}
