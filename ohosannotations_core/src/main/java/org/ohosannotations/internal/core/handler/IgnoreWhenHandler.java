/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JConditional;
import com.helger.jcodemodel.JMethod;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.IgnoreWhen;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.holder.EFractionHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

import static com.helger.jcodemodel.JExpr._null;
import static com.helger.jcodemodel.JExpr.invoke;

/**
 * 忽略当处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class IgnoreWhenHandler extends BaseAnnotationHandler<EFractionHolder> {
    /**
     * 忽略当处理程序
     *
     * @param environment 环境
     */
    public IgnoreWhenHandler(OhosAnnotationsEnvironment environment) {
        super(IgnoreWhen.class, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validate(Element element, ElementValidation valid) {
        validatorHelper.isNotPrivate(element, valid);

        validatorHelper.isNotFinal(element, valid);

        validatorHelper.returnTypeIsVoid((ExecutableElement) element, valid);

        validatorHelper.enclosingElementHasEFraction(element, valid);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     * @throws Exception 异常
     */
    @Override
    public void process(Element element, EFractionHolder holder) throws Exception {
        ExecutableElement executableElement = (ExecutableElement) element;
        JMethod delegatingMethod = codeModelHelper.overrideAnnotatedMethod(executableElement, holder);
        JBlock previousMethodBody = codeModelHelper.removeBody(delegatingMethod);

        IgnoreWhen ignoreWhen = element.getAnnotation(IgnoreWhen.class);
        JBlock methodBody = delegatingMethod.body();
        JConditional conditional = null;
        switch (ignoreWhen.value()) {
            case VIEW_DESTROYED:
                conditional = methodBody._if(holder.getComponentDestroyedField().not());
                break;
            case DETACHED:
                conditional = methodBody._if(invoke(holder.getGeneratedClass()
                    .staticRef("this"), "getFractionAbility").ne(_null()));
                break;
            default:
                conditional = methodBody._if(holder.getComponentDestroyedField().not());
                break;
        }
        conditional._then().add(previousMethodBody);
    }
}
