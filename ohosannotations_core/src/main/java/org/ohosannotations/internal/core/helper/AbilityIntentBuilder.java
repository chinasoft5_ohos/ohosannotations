/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.helper;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JClassAlreadyExistsException;
import com.helger.jcodemodel.JConditional;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.api.builder.PostAbilityStarter;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.Constant;
import org.ohosannotations.helper.OhosManifest;
import org.ohosannotations.holder.HasIntentBuilder;

import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;

import static com.helger.jcodemodel.JExpr._new;
import static com.helger.jcodemodel.JExpr.ref;
import static com.helger.jcodemodel.JMod.PRIVATE;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static com.helger.jcodemodel.JMod.STATIC;
import static org.ohosannotations.helper.ModelConstants.generationSuffix;

/**
 * Ability Intent Builder
 *
 * @since 2021-06-03
 */
public class AbilityIntentBuilder extends IntentBuilder {
    private static final int MIN_SDK_WITH_FRACTION_SUPPORT = 11;

    private static final int MIN_SDK_WITH_ABILITY_OPTIONS = 16;
    private static final int CONSTANT = 2;

    private JFieldVar fractionField;
    private JFieldVar fractionSupportField;
    private JFieldRef optionsField;

    /**
     * 构造参数
     *
     * @param holder 持有人
     * @param ohosManifest 清单
     */
    public AbilityIntentBuilder(HasIntentBuilder holder, OhosManifest ohosManifest) {
        super(holder, ohosManifest);
    }

    /**
     * 创建build
     *
     * @throws JClassAlreadyExistsException
     */
    @Override
    public void build() throws JClassAlreadyExistsException {
        super.build();

        optionsField = ref("lastOptions");

        createAdditionalConstructor(); // See issue #541
        createAdditionalIntentMethods();
        overrideStartForResultMethod();
    }

    private void createAdditionalIntentMethods() {
        if (hasFractionInClasspath()) {
            // intent() with ohos.aafwk.ability.fraction.Fraction
            JMethod method = holder.getGeneratedClass().method(STATIC | PUBLIC, holder.getIntentBuilderClass(),
                Constant.INTENT_STRING);
            JVar fractionParam = method.param(getClasses().FRACTION, Constant.FRACTION);
            method.body()._return(_new(holder.getIntentBuilderClass()).arg(fractionParam));
        }
        if (hasOhosxFractionInClasspath()) {
            // intent() with ohos.aafwk.ability.fraction.Fraction param
            JMethod method = holder.getGeneratedClass().method(STATIC | PUBLIC, holder.getIntentBuilderClass(),
                Constant.INTENT_STRING);
            JVar fractionParam = method.param(getClasses().OHOSX_FRACTION, "supportFraction");
            method.body()._return(_new(holder.getIntentBuilderClass()).arg(fractionParam));
        } else if (hasFractionSupportInClasspath()) {
            // intent() with ohos.aafwk.ability.fraction.Fraction param
            JMethod method = holder.getGeneratedClass().method(STATIC | PUBLIC, holder.getIntentBuilderClass(),
                Constant.INTENT_STRING);
            JVar fractionParam = method.param(getClasses().SUPPORT_V4_FRACTION, "supportFraction");
            method.body()._return(_new(holder.getIntentBuilderClass()).arg(fractionParam));
        } else {
        }
    }

    @Override
    protected AbstractJClass getSuperClass() {
        AbstractJClass superClass = environment.getJClass(org.ohosannotations.api.builder.AbilityIntentBuilder.class);
        return superClass.narrow(builderClass);
    }

    private void createAdditionalConstructor() {
        if (hasFractionInClasspath()) {
            fractionField = addFractionConstructor(getClasses().FRACTION, Constant.FRACTION + generationSuffix());
        }
        if (hasOhosxFractionInClasspath()) {
            fractionSupportField = addFractionConstructor(getClasses().OHOSX_FRACTION,
                "fractionSupport" + generationSuffix());
        } else if (hasFractionSupportInClasspath()) {
            fractionSupportField = addFractionConstructor(getClasses().SUPPORT_V4_FRACTION,
                "fractionSupport" + generationSuffix());
        }
    }

    private JFieldVar addFractionConstructor(AbstractJClass fractionClass, String fieldName) {
        JFieldVar field = holder.getIntentBuilderClass().field(PRIVATE, fractionClass, fieldName);
        IJExpression generatedClass = holder.getGeneratedClass().dotclass();

        JMethod constructor = holder.getIntentBuilderClass().constructor(JMod.PUBLIC);
        JVar constructorFractionParam = constructor.param(fractionClass, Constant.FRACTION);
        JBlock constructorBody = constructor.body();
        constructorBody.invoke("super").arg(constructorFractionParam.invoke("getFractionAbility")).arg(generatedClass);
        constructorBody.assign(field, constructorFractionParam);

        return field;
    }

    private void overrideStartForResultMethod() {
        AbstractJClass postAbilityStarterClass = environment.getJClass(PostAbilityStarter.class);

        JMethod method = holder.getIntentBuilderClass().method(PUBLIC, postAbilityStarterClass, "startForResult");
        method.annotate(Override.class);
        JVar requestCode = method.param(environment.getCodeModel().INT, "requestCode");
        JBlock body = method.body();

        JConditional condition = null;
        if (fractionSupportField != null) {
            condition = body._if(fractionSupportField.ne(JExpr._null()));
            condition._then() //
                .invoke(fractionSupportField, Constant.START_ABILITY).arg(intentField).arg(requestCode);
        }
        if (fractionField != null) {
            if (condition == null) {
                condition = body._if(fractionField.ne(JExpr._null()));
            } else {
                condition = condition._elseif(fractionField.ne(JExpr._null()));
            }

            JBlock fractionStartInvocationBlock;

            if (hasAbilityOptionsInFraction() && shouldGuardAbilityOptions()) {
                fractionStartInvocationBlock = createCallWithIfGuard(requestCode, condition._then(),
                    fractionField);
            } else {
                fractionStartInvocationBlock = condition._then();
            }
            JInvocation invocation = fractionStartInvocationBlock //
                .invoke(fractionField, Constant.START_ABILITY).arg(intentField).arg(requestCode);
            if (hasAbilityOptionsInFraction()) {
                invocation.arg(optionsField);
            }
        }

        JConditional abilityCondition = getjConditional(method, requestCode, condition);

        if (hasAbilityOptionsInFraction()) {
            JBlock startInvocationBlock;
            if (shouldGuardAbilityOptions()) {
                startInvocationBlock = createCallWithIfGuard(null, abilityCondition._else(), contextField);
            } else {
                startInvocationBlock = abilityCondition._else();
            }
            startInvocationBlock.invoke(contextField, Constant.START_ABILITY).arg(intentField).arg(requestCode);
        } else {
            abilityCondition._else().invoke(contextField, Constant.START_ABILITY).arg(intentField).arg(requestCode);
        }

        body._return(_new(postAbilityStarterClass).arg(contextField));
    }

    private JConditional getjConditional(JMethod method, JVar requestCode, JConditional condition) {
        JBlock abilityStartInvocationBlock = null;

        if (condition != null) {
            abilityStartInvocationBlock = condition._else();
        } else {
            abilityStartInvocationBlock = method.body();
        }

        JConditional abilityCondition = abilityStartInvocationBlock
            ._if(contextField._instanceof(getClasses().ABILITY));
        JBlock thenBlock = abilityCondition._then();
        JVar abilityVar = thenBlock.decl(getClasses().ABILITY, "ability",
            JExpr.cast(getClasses().ABILITY, contextField));

        AbstractJClass abilityCompat = getAbilityCompat();
        if (abilityCompat != null) {
            thenBlock.staticInvoke(abilityCompat, Constant.START_ABILITY_FOR_RESULT) //
                .arg(abilityVar).arg(intentField).arg(requestCode).arg(optionsField);
        } else if (hasAbilityOptionsInFraction()) {
            JBlock startForResultInvocationBlock;
            if (shouldGuardAbilityOptions()) {
                startForResultInvocationBlock = createCallWithIfGuard(requestCode, thenBlock, abilityVar);
            } else {
                startForResultInvocationBlock = thenBlock;
            }

            startForResultInvocationBlock.invoke(abilityVar, Constant.START_ABILITY_FOR_RESULT) //
                .arg(intentField).arg(requestCode).arg(optionsField);
        } else {
            thenBlock.invoke(abilityVar, Constant.START_ABILITY_FOR_RESULT).arg(intentField).arg(requestCode);
        }
        return abilityCondition;
    }

    private JBlock createCallWithIfGuard(JVar requestCode, JBlock thenBlock, IJExpression invocationTarget) {
        JConditional guardIf = thenBlock._if(
            getClasses().BUILD_VERSION.staticRef("getApiVersion")
                .gte(getClasses().BUILD_VERSION_CODES.staticRef("JELLY_BEAN")));
        JBlock startInvocationBlock = guardIf._then();
        String methodName = requestCode != null ? Constant.START_ABILITY_FOR_RESULT : Constant.START_ABILITY;

        JInvocation invocation = guardIf._else().invoke(invocationTarget, methodName).arg(intentField);
        if (requestCode != null) {
            invocation.arg(requestCode);
        }
        return startInvocationBlock;
    }

    /**
     * Fraction所在的class路径
     *
     * @return boolean
     */
    protected boolean hasFractionInClasspath() {
        boolean isFractionExistsInSdk = ohosManifest.getMinSdkVersion() >= MIN_SDK_WITH_FRACTION_SUPPORT;
        return isFractionExistsInSdk && elementUtils.getTypeElement(CanonicalNameConstants.FRACTION) != null;
    }

    /**
     * Fraction父类所在的class路径
     *
     * @return boolean
     */
    protected boolean hasFractionSupportInClasspath() {
        return elementUtils.getTypeElement(CanonicalNameConstants.SUPPORT_V4_FRACTION) != null;
    }

    /**
     * Ohos Fraction所在的class路径
     *
     * @return boolean
     */
    protected boolean hasOhosxFractionInClasspath() {
        return elementUtils.getTypeElement(CanonicalNameConstants.OHOSX_FRACTION) != null;
    }

    /**
     * 在Ability中是否有选项
     *
     * @return boolean
     */
    protected boolean hasAbilityOptionsInFraction() {
        if (!hasFractionInClasspath()) {
            return false;
        }

        TypeElement fraction = elementUtils.getTypeElement(CanonicalNameConstants.FRACTION);

        return hasAbilityOptions(fraction, 1);
    }

    private AbstractJClass getAbilityCompat() {
        TypeElement ohosAbilityCompat = elementUtils
            .getTypeElement(CanonicalNameConstants.OHOSX_ABILITY_COMPAT);
        if (hasAbilityOptions(ohosAbilityCompat, CONSTANT)) {
            return getClasses().OHOSX_ABILITY_COMPAT;
        }

        TypeElement abilityCompat = elementUtils.getTypeElement(CanonicalNameConstants.ABILITY_COMPAT);
        if (hasAbilityOptions(abilityCompat, CONSTANT)) {
            return getClasses().ABILITY_COMPAT;
        }

        return null; // 返回空
    }

    private boolean hasAbilityOptions(TypeElement type, int optionsParamPosition) {
        if (type == null) {
            return false;
        }

        for (ExecutableElement element : ElementFilter.methodsIn(elementUtils.getAllMembers(type))) {
            if (element.getSimpleName().contentEquals(Constant.START_ABILITY)) {
                List<? extends VariableElement> parameters = element.getParameters();
                if (parameters.size() == optionsParamPosition + 1) {
                    VariableElement parameter = parameters.get(optionsParamPosition);
                    if (parameter.asType().toString().equals(CanonicalNameConstants.PACMAP)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 应该保护能力选项
     *
     * @return boolean
     */
    protected boolean shouldGuardAbilityOptions() {
        return ohosManifest.getMinSdkVersion() < MIN_SDK_WITH_ABILITY_OPTIONS;
    }
}
