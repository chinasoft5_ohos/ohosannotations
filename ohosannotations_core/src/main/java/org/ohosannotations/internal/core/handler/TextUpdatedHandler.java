/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.TextUpdated;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.holder.TextObserverHolder;
import org.ohosannotations.rclass.IRClass;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

/**
 * 文本处理程序更新
 *
 * @author dev
 * @since 2021-06-04
 */
public class TextUpdatedHandler extends CoreBaseAnnotationHandler<EComponentWithViewSupportHolder> {
    private static final int DEFAULT = -1;

    /**
     * 文本处理程序更新
     *
     * @param environment 环境
     */
    public TextUpdatedHandler(OhosAnnotationsEnvironment environment) {
        super(TextUpdated.class, environment);
    }

    @Override
    protected void validate(Element element, ElementValidation validation) {
        validatorHelper.enclosingElementHasEnhancedViewSupportAnnotation(element, validation);

        validatorHelper.resIdsExist(element, IRClass.Res.ID,
            IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, validation);

        validatorHelper.isNotPrivate(element, validation);

        validatorHelper.doesntThrowException(element, validation);

        validatorHelper.returnTypeIsVoid((ExecutableElement) element, validation);

        coreValidatorHelper.hasOnTextUpdatedMethodParameters((ExecutableElement) element, validation);
    }

    @Override
    public void process(Element element, EComponentWithViewSupportHolder holder) throws Exception {
        String methodName = element.getSimpleName().toString();

        ExecutableElement executableElement = (ExecutableElement) element;
        List<? extends VariableElement> parameters = executableElement.getParameters();

        int startParameterPosition = DEFAULT;
        int beforeParameterPosition = DEFAULT;
        int countParameterPosition = DEFAULT;
        int textParameterPosition = DEFAULT;
        int viewParameterPosition = DEFAULT;
        TypeMirror viewParameterType = null;

        for (int ii = 0; ii < parameters.size(); ii++) {
            VariableElement parameter = parameters.get(ii);
            String parameterName = parameter.toString();
            TypeMirror parameterType = parameter.asType();

            if (CanonicalNameConstants.STRING.equals(parameterType.toString())) {
                textParameterPosition = ii;
            } else if (parameterType.getKind() == TypeKind.INT
                || CanonicalNameConstants.INTEGER.equals(parameterType.toString())) {
                if ("start".equals(parameterName)) {
                    startParameterPosition = ii;
                } else if ("before".equals(parameterName)) {
                    beforeParameterPosition = ii;
                } else if ("count".equals(parameterName)) {
                    countParameterPosition = ii;
                } else {
                }
            } else {
                TypeMirror textViewType = annotationHelper
                    .typeElementFromQualifiedName(CanonicalNameConstants.TEXT_VIEW).asType();
                if (annotationHelper.isSubtype(parameterType, textViewType)) {
                    viewParameterPosition = ii;
                    viewParameterType = parameterType;
                }
            }
        }

        setElementData(element, holder, methodName, parameters,
            startParameterPosition, beforeParameterPosition, countParameterPosition,
            textParameterPosition, viewParameterPosition, viewParameterType);
    }

    private void setElementData(Element element, EComponentWithViewSupportHolder holder,
        String methodName, List<? extends VariableElement> parameters, int startParameterPosition,
        int beforeParameterPosition, int countParameterPosition, int textParameterPosition,
        int viewParameterPosition, TypeMirror viewParameterType) {
        List<JFieldRef> idsRefs = annotationHelper.extractAnnotationFieldRefs(element, IRClass.Res.ID, true);

        for (JFieldRef idRef : idsRefs) {
            TextObserverHolder textObserverHolder = holder.getTextWatcherHolder(idRef, viewParameterType);
            JBlock methodBody = textObserverHolder.getTextUpdatedBody();

            IJExpression abilityRef = holder.getGeneratedClass().staticRef("this");
            JInvocation textChangeCall = methodBody.invoke(abilityRef, methodName);

            for (int i = 0; i < parameters.size(); i++) {
                if (i == startParameterPosition) {
                    JVar startParameter = textObserverHolder.getTextUpdatedStartParam();
                    textChangeCall.arg(startParameter);
                } else if (i == beforeParameterPosition) {
                    JVar beforeParameter = textObserverHolder.getTextUpdatedBeforeParam();
                    textChangeCall.arg(beforeParameter);
                } else if (i == countParameterPosition) {
                    JVar countParameter = textObserverHolder.getTextUpdatedCountParam();
                    textChangeCall.arg(countParameter);
                } else if (i == textParameterPosition) {
                    JVar textParameter = textObserverHolder.getTextUpdatedTextParam();
                    textChangeCall.arg(textParameter);
                } else if (i == viewParameterPosition) {
                    JVar viewParameter = textObserverHolder.getTextViewVariable();
                    textChangeCall.arg(viewParameter);
                }
            }
        }
    }
}
