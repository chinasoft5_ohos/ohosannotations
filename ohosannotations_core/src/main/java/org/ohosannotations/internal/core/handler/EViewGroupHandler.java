/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.DataBound;
import org.ohosannotations.annotations.EViewGroup;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EViewGroupHolder;
import org.ohosannotations.rclass.IRClass;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * 触摸屏组处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class EViewGroupHandler extends CoreBaseGeneratingAnnotationHandler<EViewGroupHolder> {
    /**
     * 触摸屏组处理程序
     *
     * @param environment 环境
     */
    public EViewGroupHandler(OhosAnnotationsEnvironment environment) {
        super(EViewGroup.class, environment);
    }

    /**
     * 创建生成类持有人
     *
     * @param environment 环境
     * @param annotatedElement 带注释的元素
     * @return {@link EViewGroupHolder}
     * @throws Exception 异常
     */
    @Override
    public EViewGroupHolder createGeneratedClassHolder(OhosAnnotationsEnvironment environment,
        TypeElement annotatedElement) throws Exception {
        return new EViewGroupHolder(environment, annotatedElement);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        super.validate(element, validation);

        validatorHelper.extendsViewGroup(element, validation);

        validatorHelper.resIdsExist(element, IRClass.Res.LAYOUT,
            IdValidatorHelper.FallbackStrategy.ALLOW_NO_RES_ID, validation);

        coreValidatorHelper.checkDataBoundAnnotation(element, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public void process(Element element, EViewGroupHolder holder) {
        JFieldRef contentViewId = annotationHelper.extractOneAnnotationFieldRef(element, IRClass.Res.LAYOUT, false);
        if (contentViewId == null) {
            return;
        }

        JBlock block = holder.getSetContentViewBlock();
        if (element.getAnnotation(DataBound.class) != null) {
            block.assign(holder.getDataBindingField(),
                holder.getDataBindingInflationExpression(contentViewId, JExpr._this(), true));
        } else {
            JVar component = block.decl(getClasses().COMPONENT_CONTAINER, "component",
                JExpr.cast(getClasses().COMPONENT_CONTAINER, getClasses().LAYOUT_SCATTER.staticInvoke("getInstance")
                    .arg(holder.getContextRef())
                    .invoke("parse")
                    .arg(contentViewId)
                    .arg(JExpr._this()).arg(JExpr.FALSE)));
            block.invoke("addComponent").arg(component);
        }
    }
}
