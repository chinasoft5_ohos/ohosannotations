/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.Extra;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.CaseHelper;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EAbilityHolder;
import org.ohosannotations.internal.core.helper.IntentBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;

import static com.helger.jcodemodel.JExpr.lit;
import static com.helger.jcodemodel.JMod.FINAL;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static com.helger.jcodemodel.JMod.STATIC;

/**
 * 额外的处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class ExtraHandler extends BaseAnnotationHandler<EAbilityHolder>
    implements MethodInjectionHandler<EAbilityHolder>,
    MethodInjectionHandler.AfterAllParametersInjectedHandler<EAbilityHolder> {
    private final InjectHelper<EAbilityHolder> injectHelper;

    /**
     * 额外的处理程序
     *
     * @param environment 环境
     */
    public ExtraHandler(OhosAnnotationsEnvironment environment) {
        super(Extra.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        injectHelper.validate(Extra.class, element, validation);
        if (!validation.isValid()) {
            return;
        }

        validatorHelper.isNotPrivate(element, validation);

        Element param = injectHelper.getParam(element);
        validatorHelper.canBePutInABundle(param, validation);
    }

    @Override
    public void process(Element element, EAbilityHolder holder) {
        injectHelper.process(element, holder);
    }

    @Override
    public JBlock getInvocationBlock(EAbilityHolder holder) {
        return holder.getInjectExtrasBlock();
    }

    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
                            EAbilityHolder holder, Element element, Element param) {
        String fieldName = element.getSimpleName().toString();
        String extraKey = extractExtraKey(element, fieldName);

        JFieldVar extraKeyStaticField = getOrCreateStaticExtraField(holder, extraKey, fieldName);
        if (element.getKind() != ElementKind.PARAMETER) {
            holder.getIntentBuilder().getPutExtraMethod(element,
                new IntentBuilder.IntentExtra(param.asType(), fieldName, extraKeyStaticField));
        }

        AbstractJClass elementClass = codeModelHelper.typeMirrorToJClass(param.asType());

        JVar extras = holder.getInjectExtras();

        JBlock ifContainsKey = targetBlock._if(JExpr.invoke(extras, "hasParam").arg(extraKeyStaticField))._then();
        ifContainsKey.assign(fieldRef,
            JExpr.cast(elementClass, JExpr.invoke(extras, "getParam").arg(extraKeyStaticField)));
    }

    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEAbility(element, valid);
    }

    @Override
    public void afterAllParametersInjected(EAbilityHolder holder, ExecutableElement method,
        List<InjectHelper.ParamHelper> parameterList) {
        List<IntentBuilder.IntentExtra> params = new ArrayList<>();
        for (InjectHelper.ParamHelper paramHelper : parameterList) {
            Element param = paramHelper.getParameterElement();
            String fieldName = param.getSimpleName().toString();
            String extraKey = extractExtraKey(param, fieldName);
            JFieldVar extraKeyStaticField = getOrCreateStaticExtraField(holder, extraKey, fieldName);
            params.add(new IntentBuilder.IntentExtra(param.asType(), fieldName, extraKeyStaticField));
        }
        holder.getIntentBuilder().getPutExtraMethod(method, params);
    }

    private String extractExtraKey(Element element, String fieldName) {
        Extra annotation = element.getAnnotation(Extra.class);
        String extraKey = annotation.value();
        if (extraKey.isEmpty()) {
            extraKey = fieldName;
        }
        return extraKey;
    }

    private JFieldVar getOrCreateStaticExtraField(EAbilityHolder holder, String extraKey, String fieldName) {
        String staticFieldName = CaseHelper.camelCaseToUpperSnakeCase(null, fieldName, "Extra");
        JFieldVar staticExtraField = holder.getGeneratedClass().fields().get(staticFieldName);
        if (staticExtraField == null) {
            staticExtraField = holder.getGeneratedClass().field(PUBLIC | STATIC | FINAL,
                getClasses().STRING, staticFieldName, lit(extraKey));
        }
        return staticExtraField;
    }
}
