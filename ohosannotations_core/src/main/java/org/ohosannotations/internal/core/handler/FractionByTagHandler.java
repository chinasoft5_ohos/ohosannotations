/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JMethod;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.FractionByTag;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;

import javax.lang.model.element.Element;

import static com.helger.jcodemodel.JExpr.lit;

/**
 * 暂不支持此注解@FractionByTag
 * 分数由标记处理程序
 *
 * @author dev
 * @since 2021-07-23
 */
public class FractionByTagHandler extends AbstractFractionByHandler {
    /**
     * 分数由标记处理程序
     *
     * @param environment 环境
     */
    public FractionByTagHandler(OhosAnnotationsEnvironment environment) {
        super(FractionByTag.class, environment, "findFragmentByTag");
    }

    @Override
    protected JMethod getFindFractionMethod(boolean isNativeFraction, EComponentWithViewSupportHolder holder) {
        return holder.getFindNativeFragmentByTag();
    }

    @Override
    protected IJExpression getFractionId(Element element, String fieldName) {
        FractionByTag annotation = element.getAnnotation(FractionByTag.class);
        String tagValue = annotation.value();
        if ("".equals(tagValue)) {
            tagValue = fieldName;
        }
        return lit(tagValue);
    }
}
