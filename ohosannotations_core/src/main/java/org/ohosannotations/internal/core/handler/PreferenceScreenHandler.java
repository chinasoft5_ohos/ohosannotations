/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import javax.lang.model.element.Element;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.annotations.PreferenceScreen;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.HasPreferences;
import org.ohosannotations.rclass.IRClass;

import com.helger.jcodemodel.JFieldRef;

/**
 * PreferenceScreenHandler
 *
 * @since 2021-07-20
 */
public class PreferenceScreenHandler extends BaseAnnotationHandler<HasPreferences> {
    /**
     * 构造参数
     *
     * @param environment 注解环境
     */
    public PreferenceScreenHandler(OhosAnnotationsEnvironment environment) {
        super(PreferenceScreen.class, environment);
    }

    @Override
    protected void validate(Element element, ElementValidation valid) {
        validatorHelper.extendsPreferenceAbilityOrPreferenceFraction(element, valid);
        validatorHelper.hasEAbilityOrEFraction(element, valid);
        validatorHelper.resIdsExist(element, IRClass.Res.XML, IdValidatorHelper.FallbackStrategy.NEED_RES_ID, valid);
    }

    @Override
    public void process(Element element, HasPreferences holder) throws Exception {
        JFieldRef preferenceId = annotationHelper.extractAnnotationFieldRefs(element, IRClass.Res.XML, false).get(0);

        holder.getPreferenceScreenInitializationBlock().invoke("addPreferencesFromResource").arg(preferenceId);
    }
}

