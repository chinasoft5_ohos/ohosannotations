/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JInvocation;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.Fullscreen;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.holder.EAbilityHolder;

import javax.lang.model.element.Element;

import static com.helger.jcodemodel.JExpr.invoke;

/**
 * 全屏幕处理
 *
 * @author dev
 * @since 2021-07-22
 */
public class FullscreenHandler extends BaseAnnotationHandler<EAbilityHolder> {
    /**
     * 全屏幕处理
     *
     * @param environment 环境
     */
    public FullscreenHandler(OhosAnnotationsEnvironment environment) {
        super(Fullscreen.class, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        validatorHelper.hasEAbility(element, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public void process(Element element, EAbilityHolder holder) {
        JFieldRef fullScreen = getClasses().WINDOW_MANAGER_LAYOUT_CONFIG.staticRef("MARK_FULL_SCREEN");
        JInvocation setFlagsInvocation = invoke(invoke("getWindow"), "addWindowFlags").arg(fullScreen);
        holder.getInitBodyInjectionBlock().add(setFlagsInvocation);
    }
}
