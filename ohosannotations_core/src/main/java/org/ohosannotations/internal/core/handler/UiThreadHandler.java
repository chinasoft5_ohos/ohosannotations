/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JClassAlreadyExistsException;
import com.helger.jcodemodel.JConditional;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JOp;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.UiThread;
import org.ohosannotations.api.UiThreadExecutor;
import org.ohosannotations.holder.EComponentHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

import static com.helger.jcodemodel.JExpr._new;
import static com.helger.jcodemodel.JExpr.lit;

/**
 * ui线程处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class UiThreadHandler extends AbstractRunnableHandler {
    private static final String METHOD_CUR_THREAD = "currentThread";
    private static final String METHOD_MAIN_LOOPER = "getMainLooper";
    private static final String METHOD_GET_THREAD = "getThread";
    private static final String METHOD_RUN_TASK = "runTask";

    /**
     * ui线程处理程序
     *
     * @param environment 环境
     */
    public UiThreadHandler(OhosAnnotationsEnvironment environment) {
        super(UiThread.class, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validate(Element element, ElementValidation valid) {
        super.validate(element, valid);

        coreValidatorHelper.usesEnqueueIfHasId(element, valid);

        UiThread annotation = element.getAnnotation(UiThread.class);
        long delay = annotation.delay();
        UiThread.Propagation propagation = annotation.propagation();
        if (delay != 0 && propagation == UiThread.Propagation.REUSE) {
            valid.addWarning("propagation=REUSE is ignored when using a delay");
        }
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     * @throws Exception 异常
     */
    @Override
    public void process(Element element, EComponentHolder holder) throws Exception {
        ExecutableElement executableElement = (ExecutableElement) element;
        JMethod delegatingMethod = codeModelHelper.overrideAnnotatedMethod(executableElement, holder);
        JBlock previousBody = codeModelHelper.removeBody(delegatingMethod);
        JDefinedClass anonymousRunnableClass = codeModelHelper.createDelegatingAnonymousRunnableClass(previousBody);

        UiThread annotation = element.getAnnotation(UiThread.class);
        long delay = annotation.delay();
        JInvocation jInvocation = getJClass(UiThreadExecutor.class).staticInvoke(METHOD_RUN_TASK);
        if (annotation.id() != -1) {
            jInvocation.arg(annotation.id());
        }
        jInvocation.arg(_new(anonymousRunnableClass))
            .arg(lit(delay));
        delegatingMethod.body().add(jInvocation);
    }

    /**
     * 添加uithread检查
     * Add the pre-check to see if we are already in the UI thread.
     *
     * @param delegatingMethod delegatingMethod
     * @param previousBody previousBody
     * @param holder holder
     * @throws JClassAlreadyExistsException jclass已经存在异常
     */
    private void addUIThreadCheck(JMethod delegatingMethod, JBlock previousBody,
        EComponentHolder holder) throws JClassAlreadyExistsException {
        // Get the Thread and Looper class.
        AbstractJClass tClass = getClasses().THREAD;
        AbstractJClass lClass = getClasses().LOOPER;

        // invoke the methods.
        IJExpression lhs = tClass.staticInvoke(METHOD_CUR_THREAD);
        IJExpression rhs = lClass.staticInvoke(METHOD_MAIN_LOOPER).invoke(METHOD_GET_THREAD);

        // create the conditional and the block.
        JConditional con = delegatingMethod.body()._if(JOp.eq(lhs, rhs));
        JBlock thenBlock = con._then().add(previousBody);
        thenBlock._return();
    }
}
