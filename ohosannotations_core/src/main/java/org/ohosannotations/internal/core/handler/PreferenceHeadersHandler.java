/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import javax.lang.model.element.Element;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.annotations.PreferenceHeaders;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.HasPreferenceHeaders;
import org.ohosannotations.rclass.IRClass;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JVar;

/**
 * 偏好头处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class PreferenceHeadersHandler extends BaseAnnotationHandler<HasPreferenceHeaders> {
    /**
     * 偏好头处理程序
     *
     * @param environment 环境
     */
    public PreferenceHeadersHandler(OhosAnnotationsEnvironment environment) {
        super(PreferenceHeaders.class, environment);
    }

    @Override
    protected void validate(Element element, ElementValidation valid) {
        validatorHelper.isPreferenceFractionClassPresent(element, valid);
        validatorHelper.extendsPreferenceAbility(element, valid);
        validatorHelper.hasEAbility(element, valid);
        validatorHelper.resIdsExist(element, IRClass.Res.XML, IdValidatorHelper.FallbackStrategy.NEED_RES_ID, valid);
    }

    @Override
    public void process(Element element, HasPreferenceHeaders holder) throws Exception {
        JFieldRef headerId = annotationHelper.extractAnnotationFieldRefs(element, IRClass.Res.XML, false).get(0);

        JBlock block = holder.getOnBuildHeadersBlock();
        JVar targetParam = holder.getOnBuildHeadersTargetParam();
        block.invoke("loadHeadersFromResource").arg(headerId).arg(targetParam);
        block.invoke(JExpr._super(), "onBuildHeaders").arg(targetParam);
    }
}
