/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.EReceiver;
import org.ohosannotations.handler.BaseGeneratingAnnotationHandler;
import org.ohosannotations.holder.EReceiverHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * EReceiverHandler
 *
 * @since 2021-07-20
 */
public class EReceiverHandler extends BaseGeneratingAnnotationHandler<EReceiverHolder> {
    /**
     * 构造参数
     *
     * @param environment 注解环境
     */
    public EReceiverHandler(OhosAnnotationsEnvironment environment) {
        super(EReceiver.class, environment);
    }

    @Override
    public EReceiverHolder createGeneratedClassHolder(OhosAnnotationsEnvironment environment,
        TypeElement annotatedElement) throws Exception {
        return new EReceiverHolder(environment, annotatedElement);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        super.validate(element, validation);
        validatorHelper.extendsReceiver(element, validation);
        validatorHelper.componentRegistered(element, getEnvironment().getOhosManifest(), false, validation);
    }

    @Override
    public void process(Element element, EReceiverHolder holder) {
        /* Do nothing */
    }
}
