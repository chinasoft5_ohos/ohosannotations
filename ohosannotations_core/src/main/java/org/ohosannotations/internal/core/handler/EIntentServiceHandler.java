/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.EIntentService;
import org.ohosannotations.annotations.ServiceAction;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.GeneratingAnnotationHandler;
import org.ohosannotations.holder.EIntentServiceHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * EIntentServiceHandler
 *
 * @since 2021-06-09
 */
public class EIntentServiceHandler extends BaseAnnotationHandler<EIntentServiceHolder>
    implements GeneratingAnnotationHandler<EIntentServiceHolder> {
    /**
     * EIntentServiceHandler
     *
     * @param environment 环境
     */
    public EIntentServiceHandler(OhosAnnotationsEnvironment environment) {
        super(EIntentService.class, environment);
    }

    @Override
    public EIntentServiceHolder createGeneratedClassHolder(OhosAnnotationsEnvironment environment,
        TypeElement annotatedElement) throws Exception {
        return new EIntentServiceHolder(environment, annotatedElement, getEnvironment().getOhosManifest());
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validatorHelper.extendsIntentService(element, validation);

        validatorHelper.hasNotMultipleAnnotatedMethodWithSameName(element, validation, ServiceAction.class);

        validatorHelper.isNotFinal(element, validation);

        validatorHelper.componentRegistered(element, getEnvironment().getOhosManifest(), validation);

        validatorHelper.isAbstractOrHasEmptyConstructor(element, validation);
    }

    @Override
    public void process(Element element, EIntentServiceHolder holder) {
        /* Do nothing */
    }
}
