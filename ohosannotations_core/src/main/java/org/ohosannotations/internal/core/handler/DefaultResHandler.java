/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.helper.Constant;
import org.ohosannotations.holder.EComponentHolder;
import org.ohosannotations.internal.core.model.OhosRes;

import static com.helger.jcodemodel.JExpr.invoke;

/**
 * 默认资源Handler
 *
 * @since 2021-06-08
 */
public class DefaultResHandler extends AbstractResHandler {
    /**
     * 构造参数
     *
     * @param ohosRes res
     * @param environment 环境
     */
    public DefaultResHandler(OhosRes ohosRes, OhosAnnotationsEnvironment environment) {
        super(ohosRes, environment);
    }

    @Override
    protected IJExpression getInstanceInvocation(EComponentHolder holder, JFieldRef idRef,
        IJAssignmentTarget fieldRef, JBlock targetBlock) {
        if (ohosRes.getResourceMethodName().equals(OhosRes.STRING.getResourceMethodName())) {
            return holder.getContextRef().invoke(ohosRes.getResourceMethodName()).arg(idRef);
        } else if (ohosRes.getResourceMethodName().equals(OhosRes.STRING_ARRAY.getResourceMethodName())) {
            return holder.getContextRef().invoke(ohosRes.getResourceMethodName()).arg(idRef);
        } else if (ohosRes.getResourceMethodName().equals(OhosRes.INT_ARRAY.getResourceMethodName())) {
            return holder.getContextRef().invoke(ohosRes.getResourceMethodName()).arg(idRef);
        } else if (ohosRes.getResourceMethodName().equals(OhosRes.INTEGER.getResourceMethodName())) {
            return invoke(holder.getResourcesRef(),
                        Constant.GET_ELEMENT).arg(idRef).invoke(ohosRes.getResourceMethodName());
        } else if (ohosRes.getResourceMethodName().equals(OhosRes.FLOAT.getResourceMethodName())) {
            return invoke(holder.getResourcesRef(),
                        Constant.GET_ELEMENT).arg(idRef).invoke(ohosRes.getResourceMethodName());
        } else if (ohosRes.getResourceMethodName().equals(OhosRes.BOOLEAN.getResourceMethodName())) {
            return invoke(holder.getResourcesRef(),
                        Constant.GET_ELEMENT).arg(idRef).invoke(ohosRes.getResourceMethodName());
        } else {
            return invoke(holder.getResourcesRef(), ohosRes.getResourceMethodName()).arg(idRef);
        }
    }
}
