/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import static com.helger.jcodemodel.JExpr._new;

import java.util.List;

import javax.lang.model.element.Element;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.holder.FoundPreferenceHolder;
import org.ohosannotations.holder.HasPreferences;
import org.ohosannotations.rclass.IRClass.Res;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JFieldRef;

/**
 * 抽象的偏好侦听器处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public abstract class AbstractPreferenceListenerHandler extends AbstractListenerHandler<HasPreferences> {
    /**
     * 抽象的偏好侦听器处理程序
     *
     * @param targetClass 目标类
     * @param environment 环境
     */
    public AbstractPreferenceListenerHandler(Class<?> targetClass, OhosAnnotationsEnvironment environment) {
        super(targetClass, environment);
    }

    /**
     * 抽象的偏好侦听器处理程序
     *
     * @param target 目标
     * @param environment 环境
     */
    public AbstractPreferenceListenerHandler(String target, OhosAnnotationsEnvironment environment) {
        super(target, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validate(Element element, ElementValidation valid) {
        super.validate(element, valid);
        validatorHelper.enclosingElementHasEAbilityOrEFraction(element, valid);
    }

    /**
     * 让听众目标类
     *
     * @param holder 持有人
     * @return {@link AbstractJClass}
     */
    @Override
    protected final AbstractJClass getListenerTargetClass(HasPreferences holder) {
        return holder.getBasePreferenceClass();
    }

    /**
     * 获得资源类型
     *
     * @return {@link Res}
     */
    @Override
    protected final Res getResourceType() {
        return Res.STRING;
    }

    /**
     * 指定监听器
     *
     * @param holder 持有人
     * @param idsRefs ids参
     * @param listenerAnonymousClass 侦听器匿名类
     */
    @Override
    protected final void assignListeners(HasPreferences holder,
        List<JFieldRef> idsRefs, JDefinedClass listenerAnonymousClass) {
        for (JFieldRef idRef : idsRefs) {
            FoundPreferenceHolder foundPreferenceHolder
                = holder.getFoundPreferenceHolder(idRef, getListenerTargetClass(holder));
            foundPreferenceHolder.getIfNotNullBlock()
                .invoke(foundPreferenceHolder.getRef(), getSetterName()).arg(_new(listenerAnonymousClass));
        }
    }
}

