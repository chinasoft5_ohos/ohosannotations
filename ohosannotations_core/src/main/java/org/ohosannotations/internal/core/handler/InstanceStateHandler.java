/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.InstanceState;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.BundleHelper;
import org.ohosannotations.holder.HasInstanceState;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

import static com.helger.jcodemodel.JExpr.ref;

/**
 * 实例状态处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class InstanceStateHandler extends BaseAnnotationHandler<HasInstanceState> {
    /**
     * 实例状态处理程序
     *
     * @param environment 环境
     */
    public InstanceStateHandler(OhosAnnotationsEnvironment environment) {
        super(InstanceState.class, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        validatorHelper.enclosingElementHasEAbilityOrEFractionOrEViewOrEViewGroup(element, validation);

        validatorHelper.isNotPrivate(element, validation);

        validatorHelper.canBePutInABundle(element, validation);

        validatorHelper.isNotFinal(element, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public void process(Element element, HasInstanceState holder) {
        AbstractJClass elementClass = codeModelHelper.typeMirrorToJClass(element.asType());
        String fieldName = element.getSimpleName().toString();

        JBlock saveStateBody = holder.getSaveStateMethodBody();
        JVar saveStateBundleParam = holder.getSaveStateBundleParam();
        JMethod restoreStateMethod = holder.getRestoreStateMethod();
        JBlock restoreStateBody = holder.getRestoreStateMethodBody();
        JVar restoreStateBundleParam = holder.getRestoreStateBundleParam();

        TypeMirror type = codeModelHelper.getActualType(element, holder);

        BundleHelper bundleHelper = new BundleHelper(getEnvironment(), type);

        JFieldRef ref = ref(fieldName);
        saveStateBody.add(bundleHelper.getExpressionToSaveFromField(saveStateBundleParam, JExpr.lit(fieldName), ref));

        IJExpression restoreMethodCall = bundleHelper
            .getExpressionToRestoreFromBundle(elementClass, restoreStateBundleParam, JExpr.lit(fieldName), restoreStateMethod);
        restoreStateBody.assign(ref, restoreMethodCall);
    }
}
