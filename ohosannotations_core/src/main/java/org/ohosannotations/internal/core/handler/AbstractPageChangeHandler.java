/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.rclass.IRClass;

/**
 * 摘要页面改变处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public abstract class AbstractPageChangeHandler extends BaseAnnotationHandler<EComponentWithViewSupportHolder> {
    /**
     * 摘要页面改变处理程序
     *
     * @param targetClass 目标类
     * @param environment 环境
     */
    public AbstractPageChangeHandler(Class<?> targetClass, OhosAnnotationsEnvironment environment) {
        super(targetClass, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    protected void validate(Element element, ElementValidation validation) {
        validatorHelper.enclosingElementHasEnhancedViewSupportAnnotation(element, validation);

        validatorHelper.isViewPagerClassPresent(validation);

        validatorHelper.resIdsExist(element, IRClass.Res.ID,
            IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, validation);

        validatorHelper.isNotPrivate(element, validation);

        validatorHelper.doesntThrowException(element, validation);

        validatorHelper.returnTypeIsVoid((ExecutableElement) element, validation);

        validatorHelper.uniqueResourceId(element, IRClass.Res.ID, validation);
    }

    /**
     * 添加页更改侦听器方法吗
     *
     * @return boolean
     */
    protected boolean hasAddOnPageChangeListenerMethod() {
        TypeElement viewPager = getProcessingEnvironment().getElementUtils()
            .getTypeElement(CanonicalNameConstants.VIEW_PAGER);
        TypeElement ohosxViewPager = getProcessingEnvironment().getElementUtils()
            .getTypeElement(CanonicalNameConstants.OHOSX_VIEW_PAGER);
        return hasTargetMethod(viewPager, "addPageChangedListener")
            || hasTargetMethod(ohosxViewPager, "addPageChangedListener");
    }

    /**
     * 是视图寻呼机参数
     *
     * @param parameterType 参数类型
     * @return boolean
     */
    protected boolean isViewPagerParameter(TypeMirror parameterType) {
        TypeElement viewPagerTypeElement = annotationHelper
            .typeElementFromQualifiedName(CanonicalNameConstants.VIEW_PAGER);
        TypeElement ohosxViewPagerTypeElement = annotationHelper
            .typeElementFromQualifiedName(CanonicalNameConstants.OHOSX_VIEW_PAGER);
        TypeMirror viewPagerType = viewPagerTypeElement == null ? null : viewPagerTypeElement.asType();
        TypeMirror ohosxViewPagerType = ohosxViewPagerTypeElement == null ? null : ohosxViewPagerTypeElement.asType();
        return viewPagerType != null && annotationHelper.isSubtype(parameterType, viewPagerType)
            || ohosxViewPagerType != null && annotationHelper.isSubtype(parameterType, ohosxViewPagerType);
    }
}

