/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import static com.helger.jcodemodel.JExpr.cast;
import static com.helger.jcodemodel.JExpr.lit;
import static org.ohosannotations.helper.LogHelper.logTagForClassHolder;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.annotations.RootContext;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EBeanHolder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JConditional;
import com.helger.jcodemodel.JInvocation;

/**
 * 根上下文处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class RootContextHandler extends BaseAnnotationHandler<EBeanHolder>
    implements MethodInjectionHandler<EBeanHolder> {
    private final InjectHelper<EBeanHolder> injectHelper;

    /**
     * 根上下文处理程序
     *
     * @param environment 环境
     */
    public RootContextHandler(OhosAnnotationsEnvironment environment) {
        super(RootContext.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        injectHelper.validate(RootContext.class, element, validation);
        if (!validation.isValid()) {
            return;
        }

        Element param = injectHelper.getParam(element);
        validatorHelper.extendsContext(param, validation);

        validatorHelper.isNotPrivate(element, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public void process(Element element, EBeanHolder holder) {
        injectHelper.process(element, holder);
    }

    /**
     * 得到调用块
     *
     * @param holder 持有人
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInvocationBlock(EBeanHolder holder) {
        return holder.getInitBodyInjectionBlock();
    }

    /**
     * 赋值
     *
     * @param targetBlock 目标块
     * @param fieldRef 现场裁判
     * @param holder 持有人
     * @param element 元素
     * @param param 参数
     */
    @Override
    public void assignValue(JBlock targetBlock,
                            IJAssignmentTarget fieldRef, EBeanHolder holder, Element element, Element param) {
        TypeMirror elementType = param.asType();
        String typeQualifiedName = elementType.toString();

        IJExpression contextRef = holder.getContextRef();

        if (CanonicalNameConstants.CONTEXT.equals(typeQualifiedName)) {
            targetBlock.add(fieldRef.assign(contextRef));
        } else {
            AbstractJClass extendingContextClass = getEnvironment().getJClass(typeQualifiedName);

            JConditional cond = targetBlock._if(contextRef._instanceof(extendingContextClass));
            cond._then().add(fieldRef.assign(cast(extendingContextClass, contextRef)));

            JInvocation warningInvoke = getClasses().LOG.staticInvoke("w");
            warningInvoke.arg(logTagForClassHolder(holder));
            warningInvoke.arg(
                lit("Due to Context class ").plus(contextRef.invoke("getClass")
                    .invoke("getSimpleName")).plus(lit(", the @RootContext "
                    + extendingContextClass.name() + " won't be populated")));
            cond._else().add(warningInvoke);
        }
    }

    /**
     * 验证封装元素
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEBeanAnnotation(element, valid);
    }
}

