/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JConditional;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JTryBlock;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.RunningLock;
import org.ohosannotations.annotations.RunningLock.RunningLockType;
import org.ohosannotations.holder.EComponentHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

/**
 * RunningLocKHandler
 *
 * @since 2021-06-16
 */
public class RunningLockHandler extends CoreBaseAnnotationHandler<EComponentHolder> {
    /**
     * RunningLocKHandler
     *
     * @param environment 环境
     */
    public RunningLockHandler(OhosAnnotationsEnvironment environment) {
        super(RunningLock.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEnhancedComponentAnnotation(element, valid);

        ExecutableElement executableElement = (ExecutableElement) element;
        coreValidatorHelper.doesNotHaveTraceAnnotationAndReturnValue(executableElement, valid);
        coreValidatorHelper.doesNotUseFlagsWithPartialWakeLock(element, valid);
        validatorHelper.hasWakeLockPermission(getEnvironment().getOhosManifest(), valid);
        validatorHelper.isNotPrivate(element, valid);
        validatorHelper.isNotFinal(element, valid);
    }

    @Override
    public void process(Element element, EComponentHolder holder) {
        ExecutableElement executableElement = (ExecutableElement) element;
        RunningLock annotation = executableElement.getAnnotation(RunningLock.class);

        String tag = extractTag(executableElement);
        RunningLockType level = annotation.lockType();
        long time = annotation.timeOutMs();

        JMethod method = codeModelHelper.overrideAnnotatedMethod(executableElement, holder);
        JBlock previousMethodBody = codeModelHelper.removeBody(method);

        JBlock methodBody = method.body();

        IJExpression levelAndFlags = getClasses().POWER_RUNNING_LOCK_TYPE.staticRef(level.name());
        JInvocation newWakeLock = holder.getPowerManagerRef()
            .invoke("createRunningLock").arg(JExpr.lit(tag)).arg(levelAndFlags);

        JVar wakeLock = methodBody.decl(getClasses().RUNNING_LOCK, "wakeLock", JExpr._null());

        JTryBlock tryBlock = methodBody._try();
        tryBlock.body().assign(wakeLock, newWakeLock);
        tryBlock.body().add(wakeLock.invoke("lock").arg(time));
        tryBlock.body().add(previousMethodBody);

        JBlock finallyBlock = tryBlock._finally();
        JConditional ifStatement = finallyBlock._if(wakeLock.ne(JExpr._null()));
        ifStatement._then().add(wakeLock.invoke("unLock"));
    }

    private String extractTag(Element element) {
        RunningLock annotation = element.getAnnotation(RunningLock.class);
        String tag = annotation.tag();
        if (RunningLock.DEFAULT_TAG.equals(tag)) {
            tag = element.getEnclosingElement().getSimpleName().toString() + "." + element.getSimpleName().toString();
        }
        return tag;
    }
}
