/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.holder.OnSliderProgressUpdatedListenerHolder;
import org.ohosannotations.rclass.IRClass;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

/**
 * 抽象的滑块接触处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public abstract class AbstractSliderTouchHandler extends CoreBaseAnnotationHandler<EComponentWithViewSupportHolder> {
    /**
     * 抽象的滑块接触处理程序
     *
     * @param targetClass 目标类
     * @param environment 环境
     */
    public AbstractSliderTouchHandler(Class<?> targetClass, OhosAnnotationsEnvironment environment) {
        super(targetClass, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        validatorHelper.enclosingElementHasEnhancedViewSupportAnnotation(element, validation);

        validatorHelper.resIdsExist(element, IRClass.Res.ID,
            IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, validation);

        validatorHelper.isNotPrivate(element, validation);

        validatorHelper.doesntThrowException(element, validation);

        validatorHelper.returnTypeIsVoid((ExecutableElement) element, validation);

        coreValidatorHelper.hasSliderTouchMethodParameters((ExecutableElement) element, validation);

        validatorHelper.param.type(CanonicalNameConstants.SLIDER)
            .optional().validate((ExecutableElement) element, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     * @throws Exception 异常
     */
    @Override
    public void process(Element element, EComponentWithViewSupportHolder holder) throws Exception {
        String methodName = element.getSimpleName().toString();

        List<JFieldRef> idsRefs = annotationHelper
            .extractAnnotationFieldRefs(element, IRClass.Res.ID, true);

        for (JFieldRef idRef : idsRefs) {
            OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder
                = holder.getOnSliderProgressUpdatedListenerHolder(idRef);
            JBlock methodBody = getMethodBodyToCall(onSliderProgressUpdatedListenerHolder);

            IJExpression abilityRef = holder.getGeneratedClass().staticRef("this");
            JInvocation textChangeCall = methodBody.invoke(abilityRef, methodName);

            ExecutableElement executableElement = (ExecutableElement) element;
            List<? extends VariableElement> parameters = executableElement.getParameters();

            if (parameters.size() == 1) {
                JVar progressParameter = getMethodParamToPass(onSliderProgressUpdatedListenerHolder);
                textChangeCall.arg(progressParameter);
            }
        }
    }

    /**
     * get方法调用
     * getMethodBodyToCall
     *
     * @param onSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder
     * @return null
     */
    protected abstract JBlock getMethodBodyToCall
    (OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder);

    /**
     * get方法参数传递
     * getMethodParamToPass
     *
     * @param onSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder
     * @return null
     */
    protected abstract JVar getMethodParamToPass
    (OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder);
}
