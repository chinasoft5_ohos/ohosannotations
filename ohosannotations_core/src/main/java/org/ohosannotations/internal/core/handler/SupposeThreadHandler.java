/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import javax.lang.model.element.Element;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.Option;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.holder.EComponentHolder;

/**
 * SupposeThreadHandler
 *
 * @since 2021-06-16
 */
public abstract class SupposeThreadHandler extends BaseAnnotationHandler<EComponentHolder> {
    /**
     * 选择线程控制
     */
    public static final Option OPTION_THREAD_CONTROL = new Option("threadControl", "true");

    /**
     * 假设线程处理程序
     *
     * @param targetClass 目标类
     * @param environment 环境
     */
    public SupposeThreadHandler(Class<?> targetClass, OhosAnnotationsEnvironment environment) {
        super(targetClass, environment);
    }

    @Override
    public boolean isEnabled() {
        return getEnvironment().getOptionBooleanValue(OPTION_THREAD_CONTROL);
    }

    @Override
    protected void validate(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEnhancedComponentAnnotation(element, valid);
        validatorHelper.isNotPrivate(element, valid);
        validatorHelper.isNotFinal(element, valid);
    }
}
