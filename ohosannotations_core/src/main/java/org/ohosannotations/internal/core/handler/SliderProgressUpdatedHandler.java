/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.SliderProgressUpdated;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.holder.OnSliderProgressUpdatedListenerHolder;
import org.ohosannotations.rclass.IRClass;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

/**
 * SliderProgressUpdatedHandler
 *
 * @since 2021-06-16
 */
public class SliderProgressUpdatedHandler extends CoreBaseAnnotationHandler<EComponentWithViewSupportHolder> {
    private static final int DEF_INT = -1;

    /**
     * SliderProgressUpdatedHandler
     *
     * @param environment 环境
     */
    public SliderProgressUpdatedHandler(OhosAnnotationsEnvironment environment) {
        super(SliderProgressUpdated.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validatorHelper.enclosingElementHasEnhancedViewSupportAnnotation(element, validation);

        validatorHelper.resIdsExist(element, IRClass.Res.ID,
            IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, validation);

        validatorHelper.isNotPrivate(element, validation);

        validatorHelper.doesntThrowException(element, validation);

        validatorHelper.returnTypeIsVoid((ExecutableElement) element, validation);

        coreValidatorHelper.hasSliderProgressUpdatedMethodParameters((ExecutableElement) element, validation);
    }

    @Override
    public void process(Element element, EComponentWithViewSupportHolder holder) throws Exception {
        String methodName = element.getSimpleName().toString();

        ExecutableElement executableElement = (ExecutableElement) element;
        List<? extends VariableElement> parameters = executableElement.getParameters();

        int sliderViewParameterPosition = DEF_INT;
        int progressParameterPosition = DEF_INT;
        int fromUserParameterPosition = DEF_INT;

        for (int index = 0; index < parameters.size(); index++) {
            VariableElement parameter = parameters.get(index);
            TypeMirror parameterType = parameter.asType();

            if (CanonicalNameConstants.SLIDER.equals(parameterType.toString())) {
                sliderViewParameterPosition = index;
            } else if (parameterType.getKind() == TypeKind.INT
                || CanonicalNameConstants.INTEGER.equals(parameterType.toString())) {
                progressParameterPosition = index;
            } else if (parameterType.getKind() == TypeKind.BOOLEAN
                || CanonicalNameConstants.BOOLEAN.equals(parameterType.toString())) {
                fromUserParameterPosition = index;
            } else {
            }
        }

        List<JFieldRef> idsRefs = annotationHelper.extractAnnotationFieldRefs(element, IRClass.Res.ID, true);

        for (JFieldRef idRef : idsRefs) {
            OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder =
                holder.getOnSliderProgressUpdatedListenerHolder(idRef);
            JBlock methodBody = onSliderProgressUpdatedListenerHolder.getOnProgressUpdatedBody();

            IJExpression abilityRef = holder.getGeneratedClass().staticRef("this");
            JInvocation textChangeCall = methodBody.invoke(abilityRef, methodName);

            for (int index = 0; index < parameters.size(); index++) {
                if (index == sliderViewParameterPosition) {
                    JVar sliderViewParameter = onSliderProgressUpdatedListenerHolder.getOnProgressUpdatedSliderParam();
                    textChangeCall.arg(sliderViewParameter);
                } else if (index == progressParameterPosition) {
                    JVar progressParameter = onSliderProgressUpdatedListenerHolder.getOnProgressUpdatedProgressParam();
                    textChangeCall.arg(progressParameter);
                } else if (index == fromUserParameterPosition) {
                    JVar fromUserParameter = onSliderProgressUpdatedListenerHolder.getOnProgressUpdatedFromUserParam();
                    textChangeCall.arg(fromUserParameter);
                } else {
                }
            }
        }
    }
}
