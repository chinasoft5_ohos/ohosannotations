/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JConditional;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.FractionArg;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.BundleHelper;
import org.ohosannotations.helper.CaseHelper;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EFractionHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;

import static com.helger.jcodemodel.JExpr._this;
import static com.helger.jcodemodel.JExpr.lit;
import static com.helger.jcodemodel.JMod.FINAL;
import static com.helger.jcodemodel.JMod.PUBLIC;
import static com.helger.jcodemodel.JMod.STATIC;

/**
 * 部分参数处理程序
 * 暂不支持此注解@FractionArg
 *
 * @author dev
 * @since 2021-07-21
 */
public class FractionArgHandler extends BaseAnnotationHandler<EFractionHolder>
    implements MethodInjectionHandler<EFractionHolder>,
    MethodInjectionHandler.AfterAllParametersInjectedHandler<EFractionHolder> {
    private final InjectHelper<EFractionHolder> injectHelper;

    /**
     * 部分参数处理程序
     *
     * @param environment 环境
     */
    public FractionArgHandler(OhosAnnotationsEnvironment environment) {
        super(FractionArg.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        validation.addError("暂不支持此注解@FractionArg");
        injectHelper.validate(FractionArg.class, element, validation);
        if (!validation.isValid()) {
            return;
        }

        validatorHelper.isNotPrivate(element, validation);

        Element param = injectHelper.getParam(element);
        validatorHelper.canBePutInABundle(param, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public void process(Element element, EFractionHolder holder) {
        injectHelper.process(element, holder);
    }

    /**
     * 得到调用块
     *
     * @param holder 持有人
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInvocationBlock(EFractionHolder holder) {
        return holder.getInjectArgsBlock();
    }

    /**
     * 赋值
     *
     * @param targetBlock 目标块
     * @param fieldRef 现场裁判
     * @param holder 持有人
     * @param element 元素
     * @param param 参数
     */
    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
                            EFractionHolder holder, Element element, Element param) {
        String fieldName = element.getSimpleName().toString();
        String argKey = extractArgKey(element, fieldName);

        if (element.getKind() != ElementKind.PARAMETER) {
            createBuilderInjectionMethod(holder, element, new ArgHelper(param, argKey));
        }

        TypeMirror actualType = codeModelHelper.getActualTypeOfEnclosingElementOfInjectedElement(holder, param);
        AbstractJClass elementClass = codeModelHelper.typeMirrorToJClass(actualType);
        BundleHelper bundleHelper = new BundleHelper(getEnvironment(), actualType);

        JVar bundle = holder.getInjectBundleArgs();
        JMethod injectExtrasMethod = holder.getInjectArgsMethod();
        JFieldVar extraKeyStaticField = getOrCreateStaticArgField(holder, argKey, fieldName);

        IJExpression restoreMethodCall = bundleHelper
            .getExpressionToRestoreFromBundle(elementClass, bundle, extraKeyStaticField, injectExtrasMethod);

        JConditional conditional = targetBlock._if(JExpr.invoke(bundle, "containsKey").arg(extraKeyStaticField));
        conditional._then().add(fieldRef.assign(restoreMethodCall));
    }

    /**
     * 验证封装元素
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEFraction(element, valid);
    }

    /**
     * 毕竟参数注入
     *
     * @param holder 持有人
     * @param method 方法
     * @param parameterList 参数列表
     */
    @Override
    public void afterAllParametersInjected(EFractionHolder holder,
        ExecutableElement method, List<InjectHelper.ParamHelper> parameterList) {
        List<ArgHelper> argHelpers = new ArrayList<>();
        for (InjectHelper.ParamHelper paramHelper : parameterList) {
            Element param = paramHelper.getParameterElement();
            String fieldName = param.getSimpleName().toString();
            String argKey = extractArgKey(param, fieldName);
            argHelpers.add(new ArgHelper(param, argKey));
        }
        createBuilderInjectMethod(holder, method, argHelpers);
    }

    /**
     * 提取参数的关键
     *
     * @param element 元素
     * @param fieldName 字段名
     * @return {@link String}
     */
    private String extractArgKey(Element element, String fieldName) {
        FractionArg annotation = element.getAnnotation(FractionArg.class);
        String argKey = annotation.value();
        if (argKey.isEmpty()) {
            argKey = fieldName;
        }
        return argKey;
    }

    /**
     * 获取或创建静态参数
     *
     * @param holder 持有人
     * @param argKey 关键参数
     * @param fieldName 字段名
     * @return {@link JFieldVar}
     */
    private JFieldVar getOrCreateStaticArgField(EFractionHolder holder, String argKey, String fieldName) {
        String staticFieldName = CaseHelper.camelCaseToUpperSnakeCase(null, fieldName, "Arg");
        JFieldVar staticExtraField = holder.getGeneratedClass().fields().get(staticFieldName);
        if (staticExtraField == null) {
            staticExtraField = holder.getGeneratedClass()
                .field(PUBLIC | STATIC | FINAL, getClasses().STRING, staticFieldName, lit(argKey));
        }
        return staticExtraField;
    }

    /**
     * 创建构建器注入方法
     *
     * @param holder 持有人
     * @param element 元素
     * @param argHelper arg助手
     */
    private void createBuilderInjectionMethod(EFractionHolder holder, Element element, ArgHelper argHelper) {
        createBuilderInjectMethod(holder, element, Collections.singletonList(argHelper));
    }

    /**
     * 创建构建器注入方法
     *
     * @param holder 持有人
     * @param element 元素
     * @param argHelpers arg助手
     */
    public void createBuilderInjectMethod(EFractionHolder holder,
        Element element, List<ArgHelper> argHelpers) {
        JDefinedClass builderClass = holder.getBuilderClass();
        JFieldRef builderArgsField = holder.getBuilderArgsField();

        JMethod builderMethod = builderClass.method(PUBLIC, holder
            .narrow(builderClass), element.getSimpleName().toString());

        String docComment = getProcessingEnvironment().getElementUtils().getDocComment(element);
        codeModelHelper.addTrimmedDocComment(builderMethod, docComment);

        for (ArgHelper argHelper : argHelpers) {
            String fieldName = argHelper.param.getSimpleName().toString();

            TypeMirror actualType = codeModelHelper
                .getActualTypeOfEnclosingElementOfInjectedElement(holder, argHelper.param);
            BundleHelper bundleHelper = new BundleHelper(getEnvironment(), actualType);

            JFieldVar argKeyStaticField = getOrCreateStaticArgField(holder, argHelper.argKey, fieldName);

            AbstractJClass paramClass = codeModelHelper.typeMirrorToJClass(actualType);
            JVar arg = builderMethod.param(paramClass, fieldName);
            builderMethod.body().add(bundleHelper
                .getExpressionToSaveFromField(builderArgsField, argKeyStaticField, arg));

            builderMethod.javadoc().addParam(fieldName).append("value for this Fraction argument");
        }

        builderMethod.javadoc().addReturn().append("the FractionBuilder to chain calls");
        builderMethod.body()._return(_this());
    }

    private static class ArgHelper {
        private final Element param;
        private final String argKey;

        ArgHelper(Element param, String argKey) {
            this.param = param;
            this.argKey = argKey;
        }
    }
}
