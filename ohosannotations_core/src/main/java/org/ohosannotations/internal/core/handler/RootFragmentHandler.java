/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JConditional;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JInvocation;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.RootFragment;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EBeanHolder;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

import static com.helger.jcodemodel.JExpr._null;
import static com.helger.jcodemodel.JExpr.cast;
import static com.helger.jcodemodel.JExpr.lit;
import static org.ohosannotations.helper.LogHelper.logTagForClassHolder;

/**
 * 根碎片处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class RootFragmentHandler extends BaseAnnotationHandler<EBeanHolder>
    implements MethodInjectionHandler<EBeanHolder> {
    private JFieldVar tag;
    private final InjectHelper<EBeanHolder> injectHelper;

    /**
     * 根碎片处理程序
     *
     * @param environment 环境
     */
    public RootFragmentHandler(OhosAnnotationsEnvironment environment) {
        super(RootFragment.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        injectHelper.validate(RootFragment.class, element, validation);
        if (!validation.isValid()) {
            return;
        }

        Element param = injectHelper.getParam(element);
        validatorHelper.extendsFraction(param, validation);

        validatorHelper.isNotPrivate(element, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public void process(Element element, EBeanHolder holder) {
        injectHelper.process(element, holder);
    }

    /**
     * 得到调用块
     *
     * @param holder 持有人
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInvocationBlock(EBeanHolder holder) {
        return holder.getInitBodyInjectionBlock();
    }

    /**
     * 赋值
     *
     * @param targetBlock 目标块
     * @param fieldRef 现场裁判
     * @param holder 持有人
     * @param element 元素
     * @param param 参数
     */
    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
                            EBeanHolder holder, Element element, Element param) {
        TypeMirror elementType = param.asType();
        String typeQualifiedName = elementType.toString();

        IJExpression rootFragmentRef = holder.getRootFragmentRef();

        AbstractJClass extendingContextClass = getEnvironment().getJClass(typeQualifiedName);

        JConditional cond = targetBlock._if(rootFragmentRef._instanceof(extendingContextClass));
        cond._then().add(fieldRef.assign(cast(extendingContextClass, rootFragmentRef)));

        tag = holder.getTag(logTagForClassHolder(holder));

        JInvocation warningInvoke = getClasses().HI_LOG.staticInvoke("warn");
        warningInvoke.arg(tag);
        warningInvoke.arg(lit("Due to class ")
            .plus(rootFragmentRef.invoke("getClass").invoke("getSimpleName"))
            .plus(lit(", the @RootFragment " + extendingContextClass.name() + " won't be populated")));

        JInvocation warningInvokeIfNull = getClasses().HI_LOG.staticInvoke("warn");
        warningInvokeIfNull.arg(tag);
        warningInvokeIfNull.arg(lit("Due to not having a rootFragment reference the @RootFragment "
            + extendingContextClass.name() + " won't be populated"));

        JConditional ifNotNull = cond._elseif(rootFragmentRef.ne(_null()));
        ifNotNull._then().add(warningInvoke);
        ifNotNull._else().add(warningInvokeIfNull);
    }

    /**
     * 验证封装元素
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEBeanAnnotation(element, valid);
    }
}

