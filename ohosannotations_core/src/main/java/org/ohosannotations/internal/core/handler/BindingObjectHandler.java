/**
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDirectClass;
import com.helger.jcodemodel.JExpr;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.BindingObject;
import org.ohosannotations.annotations.EFraction;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.holder.EFractionHolder;
import org.ohosannotations.internal.rclass.ProjectRClassFinder;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;

import static java.util.Arrays.asList;

/**
 * 绑定对象处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class BindingObjectHandler extends CoreBaseAnnotationHandler<EComponentWithViewSupportHolder>
    implements MethodInjectionHandler<EComponentWithViewSupportHolder> {
    private final InjectHelper<EComponentWithViewSupportHolder> injectHelper;

    /**
     * 绑定对象处理程序
     *
     * @param environment 环境
     */
    public BindingObjectHandler(OhosAnnotationsEnvironment environment) {
        super(BindingObject.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    protected void validate(Element element, ElementValidation validation) {
        validation.addError("暂时不支持此注解@BindingObject");
        if (element.getKind() == ElementKind.METHOD || element.getKind() == ElementKind.PARAMETER) {
            ExecutableElement methodElement = (ExecutableElement) (element.getKind() == ElementKind.METHOD
                ? element : element.getEnclosingElement());

            validatorHelper.param.extendsAnyOfTypes(CanonicalNameConstants.VIEW_DATA_BINDING,
                CanonicalNameConstants.OHOSX_VIEW_DATA_BINDING).validate(methodElement, validation);

            if (!validation.isValid()) {
                return;
            }
        }

        injectHelper.validate(BindingObject.class, element, validation);

        if (validation.isValid()) {
            validatorHelper.isNotPrivate(element, validation);
            validatorHelper.extendsOneOfTypes(injectHelper.getParam(element),
                asList(CanonicalNameConstants.VIEW_DATA_BINDING, CanonicalNameConstants.OHOSX_VIEW_DATA_BINDING),
                validation);
        }
    }

    /**
     * 验证封装元素
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        coreValidatorHelper.checkDataBoundAnnotation(element.getEnclosingElement(), valid);
        coreValidatorHelper.hasEAbilityOrEFractionOrEViewGroup(element.getEnclosingElement(), element, valid);
        coreValidatorHelper.enclosingElementHasDataBoundAnnotation(element, valid);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     * @throws Exception 异常
     */
    @Override
    public void process(Element element, EComponentWithViewSupportHolder holder) throws Exception {
        injectHelper.process(element, holder);

        if (element.getKind() == ElementKind.FIELD && holder instanceof EFraction) {
            ((EFractionHolder) holder).clearInjectedView(JExpr._this().ref(element.getSimpleName().toString()));
        }
    }

    /**
     * 得到调用块
     *
     * @param holder 持有人
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInvocationBlock(EComponentWithViewSupportHolder holder) {
        return holder.getOnViewChangedBodyAfterInjectionBlock();
    }

    /**
     * 赋值
     *
     * @param targetBlock 目标块
     * @param fieldRef 现场裁判
     * @param holder 持有人
     * @param element 元素
     * @param param 参数
     */
    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
                            EComponentWithViewSupportHolder holder, Element element, Element param) {
        String bindingClassQualifiedName;

        if (!param.asType().toString().contains(".")) {
            // the class is generated in this round, so only the simple name is available
            String resourcePackageName = getEnvironment()
                .getOptionValue(ProjectRClassFinder.OPTION_RESOURCE_PACKAGE_NAME);

            if (resourcePackageName == null) {
                resourcePackageName = getEnvironment().getOhosManifest().getApplicationPackage();
            }

            bindingClassQualifiedName = resourcePackageName + ".databinding." + param.asType().toString();
        } else {
            bindingClassQualifiedName = param.asType().toString();
        }

        JDirectClass bindingClass = getCodeModel().directClass(bindingClassQualifiedName);
        targetBlock.add(fieldRef.assign(JExpr.cast(bindingClass, holder.getDataBindingField())));
    }
}
