/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.model;

import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.helper.CanonicalNameConstants;

import java.util.HashMap;
import java.util.Map;

import javax.lang.model.type.TypeMirror;

/**
 * SystemServices
 *
 * @since 2021-06-02
 */
public class OhosSystemServices {
    private OhosAnnotationsEnvironment environment;
    private Map<String, String> registeredServices = new HashMap<>();

    /**
     * OhosSystemServices
     *
     * @param environment 环境
     */
    public OhosSystemServices(OhosAnnotationsEnvironment environment) {
        this.environment = environment;
        String keyRadioInfoManager = "ohos.telephony.RadioInfoManager"; // 电话服务
        registeredServices.put(keyRadioInfoManager, keyRadioInfoManager);
        String keySimInfoManager = "ohos.telephony.SimInfoManager"; // SIM卡
        registeredServices.put(keySimInfoManager, keySimInfoManager);
        String keyNetManager = "ohos.net.NetManager"; // 网络管理
        registeredServices.put(keyNetManager, keyNetManager);
        registeredServices.put(CanonicalNameConstants.BLUETOOTH_HOST, CanonicalNameConstants.BLUETOOTH_HOST); // 蓝牙管理
        registeredServices.put(CanonicalNameConstants.USB_CORE, CanonicalNameConstants.USB_CORE); // USB
        registeredServices.put(CanonicalNameConstants.ABILITY_MANAGER,
            CanonicalNameConstants.ABILITY_MANAGER); // IAbilityManager
        registeredServices.put(CanonicalNameConstants.NOTIFICATION_HELPER,
            CanonicalNameConstants.NOTIFICATION_HELPER); // NotificationHelper
        registeredServices.put(CanonicalNameConstants.WINDOW_MANAGER,
            CanonicalNameConstants.WINDOW_MANAGER); // WindowManager
        registeredServices.put(CanonicalNameConstants.WIFI_MANAGER,
            CanonicalNameConstants.WIFI_MANAGER); // WifiDevice
    }

    /**
     * contains
     *
     * @param serviceType 服务类型
     * @return boolean
     */
    public boolean contains(TypeMirror serviceType) {
        return registeredServices.containsKey(serviceType.toString());
    }

    /**
     * getServiceConstant
     *
     * @param serviceType 服务类型
     * @return String
     */
    public String getServiceConstant(TypeMirror serviceType) {
        return registeredServices.get(serviceType.toString());
    }

    /**
     * getServiceConstantRef
     *
     * @param serviceType 服务类型
     * @return JFieldRef
     */
    public JFieldRef getServiceConstantRef(TypeMirror serviceType) {
        return extractIdStaticRef(getServiceConstant(serviceType));
    }

    private JFieldRef extractIdStaticRef(String staticFieldQualifiedName) {
        if (staticFieldQualifiedName != null) {
            int fieldSuffix = staticFieldQualifiedName.lastIndexOf('.');
            String fieldName = staticFieldQualifiedName.substring(fieldSuffix + 1);
            return environment.getJClass(staticFieldQualifiedName).staticRef(fieldName);
        } else {
            return null; // 返回空
        }
    }
}
