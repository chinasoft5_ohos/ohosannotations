/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.JBlock;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.InjectMenu;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.HasOptionsMenu;

import javax.lang.model.element.Element;

/**
 * 注入菜单处理程序
 * 不可用，Menu在OHOS无对应
 *
 * @author dev
 * @since 2021-07-23
 */
public class InjectMenuHandler extends BaseAnnotationHandler<HasOptionsMenu>
    implements MethodInjectionHandler<HasOptionsMenu> {
    private final InjectHelper<HasOptionsMenu> injectHelper;

    /**
     * 注入菜单处理程序
     *
     * @param environment 环境
     */
    public InjectMenuHandler(OhosAnnotationsEnvironment environment) {
        super(InjectMenu.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    @Override
    public void validate(Element element, ElementValidation valid) {
        valid.addError("暂时不支持此注解@InjectMenu");
        injectHelper.validate(InjectMenu.class, element, valid);
        if (!valid.isValid()) {
            return;
        }

        Element param = injectHelper.getParam(element);
        validatorHelper.isDeclaredType(param, valid);

        validatorHelper.extendsMenu(param, valid);

        validatorHelper.isNotPrivate(element, valid);
    }

    @Override
    public void process(Element element, HasOptionsMenu holder) {
        injectHelper.process(element, holder);
    }

    @Override
    public JBlock getInvocationBlock(HasOptionsMenu holder) {
        return holder.getOnCreateOptionsMenuMethodBody();
    }

    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
        HasOptionsMenu holder, Element element, Element param) {
        targetBlock.add(fieldRef.assign(holder.getOnCreateOptionsMenuMenuParam()));
    }

    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEAbilityOrEFraction(element, valid);
    }
}
