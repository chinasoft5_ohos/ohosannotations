/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.FromHtml;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.rclass.IRClass;

import javax.lang.model.element.Element;

import static com.helger.jcodemodel.JExpr._null;
import static com.helger.jcodemodel.JExpr.ref;

/**
 * 从html处理程序
 * OHOS暂不支持@FromHtml
 *
 * @author dev
 * @since 2021-07-23
 */
public class FromHtmlHandler extends BaseAnnotationHandler<EComponentWithViewSupportHolder> {
    /**
     * 从html处理程序
     *
     * @param environment 环境
     */
    public FromHtmlHandler(OhosAnnotationsEnvironment environment) {
        super(FromHtml.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validation.addError("暂不支持@FromHtm");
        validatorHelper.enclosingElementHasEnhancedViewSupportAnnotation(element, validation);

        validatorHelper.hasViewByIdAnnotation(element, validation);

        validatorHelper.extendsTextView(element, validation);

        validatorHelper.resIdsExist(element, IRClass.Res.STRING,
            IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, validation);
    }

    @Override
    public void process(Element element, EComponentWithViewSupportHolder holder) {
        String fieldName = element.getSimpleName().toString();

        JFieldRef idRef = annotationHelper.extractOneAnnotationFieldRef(element, IRClass.Res.STRING, true);

        JBlock methodBody = holder.getOnViewChangedBodyAfterInjectionBlock();
        methodBody //
            ._if(ref(fieldName).ne(_null())) //
            ._then() //
            .invoke(ref(fieldName), "setText").arg(getClasses().HTML
            .staticInvoke("fromHtml").arg(holder.getContextRef().invoke("getString").arg(idRef)));
    }
}
