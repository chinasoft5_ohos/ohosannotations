/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.CustomTitle;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EAbilityHolder;
import org.ohosannotations.rclass.IRClass;

import javax.lang.model.element.Element;

/**
 * 自定义标题处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class CustomTitleHandler extends BaseAnnotationHandler<EAbilityHolder> {
    /**
     * 自定义标题处理程序
     *
     * @param environment 环境
     */
    public CustomTitleHandler(OhosAnnotationsEnvironment environment) {
        super(CustomTitle.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validation.addError("暂时不支持此注解@CustomTitle");
        validatorHelper.hasEAbility(element, validation);

        validatorHelper.resIdsExist(element, IRClass.Res.LAYOUT,
            IdValidatorHelper.FallbackStrategy.NEED_RES_ID, validation);
    }

    @Override
    public void process(Element element, EAbilityHolder holder) {
        JBlock onViewChangedBody = holder.getOnViewChangedBodyBeforeInjectionBlock();

        JFieldRef contentViewId = annotationHelper.extractAnnotationFieldRefs(element, getTarget(),
            getEnvironment().getRClass(), IRClass.Res.LAYOUT, false).get(0);

        JFieldRef customTitleFeature = getClasses().WINDOW.staticRef("FEATURE_CUSTOM_TITLE");
        holder.getInitBodyInjectionBlock().invoke("requestWindowFeature").arg(customTitleFeature);
        onViewChangedBody.add(holder.getContextRef().invoke("getWindow")
            .invoke("setFeatureInt").arg(customTitleFeature).arg(contentViewId));
    }
}
