/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.annotations.PreferenceClick;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.holder.HasPreferences;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

/**
 * 偏好单击处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class PreferenceClickHandler extends AbstractPreferenceListenerHandler {
    /**
     * 偏好单击处理程序
     *
     * @param environment 环境
     */
    public PreferenceClickHandler(OhosAnnotationsEnvironment environment) {
        super(PreferenceClick.class, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validate(Element element, ElementValidation valid) {
        super.validate(element, valid);
        validatorHelper.enclosingElementExtendsPreferenceAbilityOrPreferenceFraction(element, valid);

        ExecutableElement executableElement = (ExecutableElement) element;

        validatorHelper.returnTypeIsVoidOrBoolean(executableElement, valid);

        validatorHelper.param //
            .extendsAnyOfTypes(CanonicalNameConstants.PREFERENCE, CanonicalNameConstants
                .SUPPORT_V7_PREFERENCE, CanonicalNameConstants.OHOSX_PREFERENCE).optional() //
            .validate(executableElement, valid);
    }

    /**
     * 使电话
     *
     * @param listenerMethodBody 侦听器方法主体
     * @param call 调用
     * @param returnType 返回类型
     */
    @Override
    protected void makeCall(JBlock listenerMethodBody, JInvocation call, TypeMirror returnType) {
        boolean returnMethodResult = returnType.getKind() != TypeKind.VOID;
        if (returnMethodResult) {
            listenerMethodBody._return(call);
        } else {
            listenerMethodBody.add(call);
            listenerMethodBody._return(JExpr.TRUE);
        }
    }

    /**
     * 工艺参数
     *
     * @param holder 持有人
     * @param listenerMethod 侦听器方法
     * @param call 调用
     * @param userParameters 用户参数
     */
    @Override
    protected void processParameters(HasPreferences holder,
        JMethod listenerMethod, JInvocation call, List<? extends VariableElement> userParameters) {
        String preferenceClassName = holder.getBasePreferenceClass().fullName();

        JVar preferenceParam = listenerMethod.param(getEnvironment().getJClass(preferenceClassName), "preference");

        if (userParameters.size() == 1) {
            call.arg(castArgumentIfNecessary(holder, preferenceClassName, preferenceParam, userParameters.get(0)));
        }
    }

    /**
     * 创建侦听器方法
     *
     * @param listenerAnonymousClass 侦听器匿名类
     * @return {@link JMethod}
     */
    @Override
    protected JMethod createListenerMethod(JDefinedClass listenerAnonymousClass) {
        return listenerAnonymousClass.method(JMod.PUBLIC, getCodeModel().BOOLEAN, "onPreferenceClick");
    }

    /**
     * setter名字
     *
     * @return {@link String}
     */
    @Override
    protected String getSetterName() {
        return "setOnPreferenceClickListener";
    }

    /**
     * 得到侦听器类
     *
     * @param holder 持有人
     * @return {@link AbstractJClass}
     */
    @Override
    protected AbstractJClass getListenerClass(HasPreferences holder) {
        return holder.usingOhosPreference() ? getClasses().OHOSX_PREFERENCE_CLICK_LISTENER
            : holder.usingSupportV7Preference() ? getClasses()
            .SUPPORT_V7_PREFERENCE_CLICK_LISTENER : getClasses().PREFERENCE_CLICK_LISTENER;
    }
}

