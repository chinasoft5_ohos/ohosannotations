/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.annotations.LongClick;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;

/**
 * 长单击处理程序
 *
 * @author dev
 * @since 2021-06-03
 */
public class LongClickHandler extends AbstractViewListenerHandler {
    /**
     * 长单击处理程序
     *
     * @param environment 环境
     */
    public LongClickHandler(OhosAnnotationsEnvironment environment) {
        super(LongClick.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        super.validate(element, validation);

        ExecutableElement executableElement = (ExecutableElement) element;

        validatorHelper.returnTypeIsVoidOrBoolean(executableElement, validation);

        validatorHelper.param.extendsType(CanonicalNameConstants.COMPONENT)
            .optional().validate(executableElement, validation);
    }

    @Override
    protected void makeCall(JBlock listenerMethodBody, JInvocation call, TypeMirror returnType) {
        boolean returnMethodResult = returnType.getKind() != TypeKind.VOID;
        if (returnMethodResult) {
            listenerMethodBody._return(call);
        } else {
            listenerMethodBody.add(call);
        }
    }

    @Override
    protected void processParameters(EComponentWithViewSupportHolder holder,
        JMethod listenerMethod, JInvocation call, List<? extends VariableElement> parameters) {
        boolean hasViewParameter = parameters.size() == 1;
        JVar viewParam = listenerMethod.param(getClasses().COMPONENT, "component");
        if (hasViewParameter) {
            call.arg(castArgumentIfNecessary(holder, CanonicalNameConstants.COMPONENT, viewParam, parameters.get(0)));
        }
    }

    @Override
    protected JMethod createListenerMethod(JDefinedClass listenerAnonymousClass) {
        return listenerAnonymousClass.method(JMod.PUBLIC, getCodeModel().VOID, "onLongClicked");
    }

    @Override
    protected String getSetterName() {
        return "setLongClickedListener";
    }


    @Override
    protected AbstractJClass getListenerClass(EComponentWithViewSupportHolder holder) {
        return getClasses().COMPONENT_LONG_CLICKED_LISTENER;
    }
}
