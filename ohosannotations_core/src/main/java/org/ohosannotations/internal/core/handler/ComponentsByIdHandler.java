/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.core.handler;

import static com.helger.jcodemodel.JExpr._new;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.annotations.ComponentsById;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.holder.FoundViewHolder;
import org.ohosannotations.rclass.IRClass;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

/**
 * 组件通过id处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class ComponentsByIdHandler extends BaseAnnotationHandler<EComponentWithViewSupportHolder>
    implements MethodInjectionHandler<EComponentWithViewSupportHolder> {
    private final InjectHelper<EComponentWithViewSupportHolder> injectHelper;

    /**
     * 组件通过id处理程序
     *
     * @param environment 环境
     */
    public ComponentsByIdHandler(OhosAnnotationsEnvironment environment) {
        super(ComponentsById.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        injectHelper.validate(ComponentsById.class, element, validation);
        if (!validation.isValid()) {
            return;
        }

        Element param = injectHelper.getParam(element);
        validatorHelper.isDeclaredType(param, validation);

        validatorHelper.extendsListOfView(param, validation);

        validatorHelper.resIdsExist(element, IRClass
            .Res.ID, IdValidatorHelper.FallbackStrategy.NEED_RES_ID, validation);

        validatorHelper.isNotPrivate(element, validation);
    }

    @Override
    public void process(Element element, EComponentWithViewSupportHolder holder) {
        injectHelper.process(element, holder);
    }

    @Override
    public JBlock getInvocationBlock(EComponentWithViewSupportHolder holder) {
        return holder.getOnViewChangedBodyInjectionBlock();
    }

    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
        EComponentWithViewSupportHolder holder, Element element, Element param) {
        TypeMirror viewType = extractViewClass(param);
        AbstractJClass viewClass = codeModelHelper.typeMirrorToJClass(viewType);

        String listName = getListName(element, param);
        IJExpression arrayList = instantiateArrayList(viewType, holder, "list_" + listName);

        List<JFieldRef> idsRefs = annotationHelper.extractAnnotationFieldRefs(element, IRClass.Res.ID, true);
        for (JFieldRef idRef : idsRefs) {
            addViewToListIfNotNull(arrayList, viewClass, idRef, holder);
        }

        targetBlock.add(fieldRef.assign(arrayList));
    }

    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEnhancedViewSupportAnnotation(element, valid);
    }

    private String getListName(Element element, Element param) {
        String listName = param.getSimpleName().toString();
        if (element.getKind() == ElementKind.PARAMETER) {
            listName = element.getEnclosingElement().getSimpleName().toString() + "_" + listName;
        } else if (element.getKind() == ElementKind.METHOD) {
            listName = element.getSimpleName().toString() + "_" + listName;
        } else {
        }
        return listName;
    }

    private IJExpression instantiateArrayList(TypeMirror viewType,
        EComponentWithViewSupportHolder holder, String name) {
        TypeElement arrayListTypeElement = annotationHelper
            .typeElementFromQualifiedName(CanonicalNameConstants.ARRAYLIST);
        DeclaredType arrayListType = getProcessingEnvironment()
            .getTypeUtils().getDeclaredType(arrayListTypeElement, viewType);
        AbstractJClass arrayListClass = codeModelHelper
            .typeMirrorToJClass(arrayListType);

        return holder.getOnViewChangedBodyBeforeInjectionBlock()
            .decl(arrayListClass, name, _new(arrayListClass));
    }

    private TypeMirror extractViewClass(Element element) {
        DeclaredType elementType = (DeclaredType) element.asType();
        List<? extends TypeMirror> elementTypeArguments = elementType.getTypeArguments();

        TypeMirror viewType = annotationHelper
            .typeElementFromQualifiedName(CanonicalNameConstants.COMPONENT).asType();
        if (!elementTypeArguments.isEmpty()) {
            viewType = elementTypeArguments.get(0);
        }
        return viewType;
    }

    private void addViewToListIfNotNull(IJExpression elementRef,
        AbstractJClass viewClass, JFieldRef idRef, EComponentWithViewSupportHolder holder) {
        FoundViewHolder foundViewHolder = holder.getFoundViewHolder(idRef, viewClass);
        foundViewHolder.getIfNotNullBlock()
            .invoke(elementRef, "add").arg(foundViewHolder.getOrCastRef(viewClass));
    }
}
