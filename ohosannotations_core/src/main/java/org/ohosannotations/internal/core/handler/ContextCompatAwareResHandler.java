/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.holder.EComponentHolder;
import org.ohosannotations.internal.core.model.OhosRes;

import static com.helger.jcodemodel.JExpr.invoke;

/**
 * 获取ResourceHandler
 *
 * @since 2021-06-08
 */
public class ContextCompatAwareResHandler extends AbstractResHandler {
    /**
     * 上下文兼容意识到res处理程序
     *
     * @param ohosRes res
     * @param environment 环境
     * @param minSdkWithMethod 最低sdk和方法
     * @param minSdkPlatformName 最低sdk平台名称
     */
    ContextCompatAwareResHandler(OhosRes ohosRes, OhosAnnotationsEnvironment environment,
                                    int minSdkWithMethod, String minSdkPlatformName) {
        super(ohosRes, environment);
    }

    @Override
    protected IJExpression getInstanceInvocation(EComponentHolder holder, JFieldRef idRef,
        IJAssignmentTarget fieldRef, JBlock targetBlock) {
        return invoke(holder.getResourcesRef(), ohosRes.getResourceMethodName()).arg(idRef);
    }
}
