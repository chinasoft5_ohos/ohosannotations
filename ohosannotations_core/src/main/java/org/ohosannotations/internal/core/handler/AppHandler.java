/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.JBlock;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.App;
import org.ohosannotations.annotations.EApplication;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EApplicationHolder;
import org.ohosannotations.holder.EComponentHolder;

import javax.lang.model.element.Element;

import static org.ohosannotations.helper.ModelConstants.classSuffix;

/**
 * 应用处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class AppHandler extends BaseAnnotationHandler<EComponentHolder>
    implements MethodInjectionHandler<EComponentHolder> {
    private final InjectHelper<EComponentHolder> injectHelper;

    /**
     * 应用处理程序
     *
     * @param environment 环境
     */
    public AppHandler(OhosAnnotationsEnvironment environment) {
        super(App.class, environment);
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        validation.addError("暂时不支持此注解@App");
        injectHelper.validate(App.class, element, validation);
        if (!validation.isValid()) {
            return;
        }

        validatorHelper.isNotPrivate(element, validation);

        Element param = injectHelper.getParam(element);
        validatorHelper.typeHasValidAnnotation(EApplication.class, param, validation);
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public void process(Element element, EComponentHolder holder) {
        injectHelper.process(element, holder);
    }

    /**
     * 得到调用块
     *
     * @param holder 持有人
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInvocationBlock(EComponentHolder holder) {
        return holder.getInitBodyInjectionBlock();
    }

    /**
     * 赋值
     *
     * @param targetBlock 目标块
     * @param fieldRef 现场裁判
     * @param holder 持有人
     * @param element 元素
     * @param param 参数
     */
    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
                            EComponentHolder holder, Element element, Element param) {
        String applicationQualifiedName = param.asType().toString();
        AbstractJClass applicationClass = getJClass(applicationQualifiedName + classSuffix());

        targetBlock.add(fieldRef.assign(applicationClass.staticInvoke(EApplicationHolder.GET_APPLICATION_INSTANCE)));
    }

    /**
     * 验证封装元素
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEnhancedComponentAnnotation(element, valid);
    }
}
