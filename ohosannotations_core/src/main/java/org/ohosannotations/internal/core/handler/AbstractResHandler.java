/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JCatchBlock;
import com.helger.jcodemodel.JFieldRef;
import com.helger.jcodemodel.JTryBlock;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.handler.MethodInjectionHandler;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.EComponentHolder;
import org.ohosannotations.internal.core.model.OhosRes;
import org.ohosannotations.rclass.IRClass;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

import static org.ohosannotations.helper.LogHelper.logTagForClassHolder;

/**
 * 抽象资源Handler
 *
 * @author dev
 * @since 2021-06-18
 */
public abstract class AbstractResHandler extends BaseAnnotationHandler<EComponentHolder>
    implements MethodInjectionHandler<EComponentHolder> {
    /**
     * res
     */
    protected OhosRes ohosRes;
    /**
     * 注入助手
     */
    private final InjectHelper<EComponentHolder> injectHelper;

    /**
     * 构造参数
     *
     * @param ohosRes res
     * @param environment 环境
     */
    public AbstractResHandler(OhosRes ohosRes, OhosAnnotationsEnvironment environment) {
        super(ohosRes.getAnnotationClass(), environment);
        this.ohosRes = ohosRes;
        injectHelper = new InjectHelper<>(validatorHelper, this);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public final void validate(Element element, ElementValidation validation) {
        injectHelper.validate(ohosRes.getAnnotationClass(), element, validation);
        if (!validation.isValid()) {
            return;
        }

        validatorHelper.allowedType(element, ohosRes.getAllowedTypes(), validation);

        validatorHelper.resIdsExist(element, ohosRes.getResInnerClass(),
            IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, validation);

        Element enclosingElement = element.getEnclosingElement();
        if (element instanceof VariableElement && enclosingElement instanceof ExecutableElement) {
            validatorHelper.isNotPrivate(enclosingElement, validation);
        } else {
            validatorHelper.isNotPrivate(element, validation);
        }
    }

    /**
     * 过程
     *
     * @param element 元素
     * @param holder 持有人
     */
    @Override
    public final void process(Element element, EComponentHolder holder) {
        injectHelper.process(element, holder);
    }

    /**
     * 得到调用块
     *
     * @param holder 持有人
     * @return {@link JBlock}
     */
    @Override
    public JBlock getInvocationBlock(EComponentHolder holder) {
        return holder.getInitBodyInjectionBlock();
    }

    /**
     * 赋值
     *
     * @param targetBlock 目标块
     * @param fieldRef 现场裁判
     * @param holder 持有人
     * @param element 元素
     * @param param 参数
     */
    @Override
    public void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef,
                            EComponentHolder holder, Element element, Element param) {
        IRClass.Res resInnerClass = ohosRes.getResInnerClass();

        JFieldRef idRef = annotationHelper.extractOneAnnotationFieldRef(element, resInnerClass, true);
        IJExpression resourceInstance = getInstanceInvocation(holder, idRef, fieldRef, targetBlock);

        switch (resInnerClass) {
            case BOOLEAN:
            case FLOAT:
            case INTEGER:
            case LAYOUT:
                JTryBlock tryBlock = targetBlock._try();
                JCatchBlock catchBlock = tryBlock._catch(getClasses().EXCEPTION);
                String fieldName = param.getSimpleName().toString();
                catchBlock.body() //
                    .staticInvoke(getClasses().LOG, "e") //
                    .arg(logTagForClassHolder(holder) + "  Could not create Recourse " + fieldName); //
                if (resourceInstance != null) {
                    tryBlock.body().add(fieldRef.assign(resourceInstance));
                }
                break;
            default:
                if (resourceInstance != null) {
                    targetBlock.add(fieldRef.assign(resourceInstance));
                }
                break;
        }
    }

    /**
     * 验证封装元素
     *
     * @param element 元素
     * @param valid 有效的
     */
    @Override
    public void validateEnclosingElement(Element element, ElementValidation valid) {
        validatorHelper.enclosingElementHasEnhancedComponentAnnotation(element, valid);
    }

    /**
     * 获取注解调用
     *
     * @param holder 持有人
     * @param idRef id裁判
     * @param fieldRef 现场裁判
     * @param targetBlock 目标块
     * @return IJExpression
     */
    protected abstract IJExpression getInstanceInvocation(EComponentHolder holder, JFieldRef idRef,
        IJAssignmentTarget fieldRef, JBlock targetBlock);
}
