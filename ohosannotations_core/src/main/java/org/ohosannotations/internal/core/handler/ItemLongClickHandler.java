/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.ItemLongClick;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

import static com.helger.jcodemodel.JExpr.cast;
import static com.helger.jcodemodel.JExpr.invoke;

/**
 * 项长期单击处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class ItemLongClickHandler extends AbstractViewListenerHandler {
    /**
     * 项长期单击处理程序
     *
     * @param environment 环境
     */
    public ItemLongClickHandler(OhosAnnotationsEnvironment environment) {
        super(ItemLongClick.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        super.validate(element, validation);

        ExecutableElement executableElement = (ExecutableElement) element;

        validatorHelper.returnTypeIsVoidOrBoolean(executableElement, validation);

        validatorHelper.param.anyType().optional().validate(executableElement, validation);
    }

    @Override
    protected void makeCall(JBlock listenerMethodBody, JInvocation call, TypeMirror returnType) {
        boolean returnMethodResult = returnType.getKind() != TypeKind.VOID;
        if (returnMethodResult) {
            listenerMethodBody._return(call);
        } else {
            listenerMethodBody.add(call);
            listenerMethodBody._return(JExpr.TRUE);
        }
    }

    @Override
    protected void processParameters(EComponentWithViewSupportHolder holder,
        JMethod listenerMethod, JInvocation call, List<? extends VariableElement> parameters) {
        boolean hasItemParameter = parameters.size() == 1;
        JVar onItemLongClickedParentParam = listenerMethod.param(getClasses().LIST_CONTAINER, "parent");
        listenerMethod.param(getClasses().COMPONENT, "component");
        JVar onItemClickPositionParam = listenerMethod.param(getCodeModel().INT, "position");
        listenerMethod.param(getCodeModel().LONG, "id");

        if (hasItemParameter) {
            VariableElement parameter = parameters.get(0);

            TypeMirror parameterType = parameter.asType();
            if (parameterType.getKind() == TypeKind.INT) {
                call.arg(onItemClickPositionParam);
            } else {
                AbstractJClass parameterClass = codeModelHelper.typeMirrorToJClass(parameterType);
                call.arg(cast(parameterClass, invoke(onItemLongClickedParentParam, "getItemProvider")
                    .invoke("getItem").arg(onItemClickPositionParam)));

                if (parameterClass.isParameterized()) {
                    codeModelHelper.addSuppressWarnings(listenerMethod, "unchecked");
                }
            }
        }
    }

    @Override
    protected JMethod createListenerMethod(JDefinedClass listenerAnonymousClass) {
        return listenerAnonymousClass.method(JMod.PUBLIC, getCodeModel().BOOLEAN, "onItemLongClicked");
    }

    @Override
    protected String getSetterName() {
        return "setItemLongClickedListener";
    }

    @Override
    protected AbstractJClass getListenerClass(EComponentWithViewSupportHolder holder) {
        return getClasses().ITEM_LONG_CLICKED_LISTENER;
    }

    @Override
    protected AbstractJClass getListenerTargetClass(EComponentWithViewSupportHolder holder) {
        return getClasses().LIST_CONTAINER;
    }
}
