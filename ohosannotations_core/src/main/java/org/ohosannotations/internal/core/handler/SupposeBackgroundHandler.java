/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.SupposeBackground;
import org.ohosannotations.api.BackgroundExecutor;
import org.ohosannotations.holder.EComponentHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

import static com.helger.jcodemodel.JExpr.lit;

/**
 * SupposeBackgroundHandler
 *
 * @since 2021-06-16
 */
public class SupposeBackgroundHandler extends SupposeThreadHandler {
    private static final String METHOD_CHECK_BG_THREAD = "checkBgThread";

    /**
     * SupposeBackgroundHandler
     *
     * @param environment 环境
     */
    public SupposeBackgroundHandler(OhosAnnotationsEnvironment environment) {
        super(SupposeBackground.class, environment);
    }

    @Override
    public void process(Element element, EComponentHolder holder) throws Exception {
        ExecutableElement executableElement = (ExecutableElement) element;

        JMethod delegatingMethod = codeModelHelper.overrideAnnotatedMethod(executableElement, holder);

        AbstractJClass bgExecutor = getJClass(BackgroundExecutor.class);

        SupposeBackground annotation = element.getAnnotation(SupposeBackground.class);
        String[] serial = annotation.serial();
        JInvocation invocation = bgExecutor.staticInvoke(METHOD_CHECK_BG_THREAD);
        for (String ser : serial) {
            invocation.arg(lit(ser));
        }

        JBlock body = delegatingMethod.body();
        body.pos(0);
        body.add(invocation);
        body.pos(body.getContents().size());
    }
}
