/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.AfterInject;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.holder.EComponentHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

/**
 * AfterInjectHandler
 *
 * @since 2021-07-20
 */
public class AfterInjectHandler extends BaseAnnotationHandler<EComponentHolder> {
    /**
     * 构造参数
     *
     * @param environment 注解环境
     */
    public AfterInjectHandler(OhosAnnotationsEnvironment environment) {
        super(AfterInject.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validatorHelper.enclosingElementHasEnhancedComponentAnnotation(element, validation);

        ExecutableElement executableElement = (ExecutableElement) element;

        validatorHelper.returnTypeIsVoid(executableElement, validation);

        validatorHelper.isNotPrivate(element, validation);

        validatorHelper.doesntThrowException(executableElement, validation);

        validatorHelper.param.noParam().validate(executableElement, validation);
    }

    @Override
    public void process(Element element, EComponentHolder holder) {
        String methodName = element.getSimpleName().toString();
        holder.getInitBodyAfterInjectionBlock().invoke(methodName);
    }
}
