/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JSwitch;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.KeyLongPress;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.holder.HasKeyEventCallbackMethods;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

/**
 * 键长按处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class KeyLongPressHandler extends AbstractKeyEventHandler {
    /**
     * 键长按处理程序
     *
     * @param environment 环境
     */
    public KeyLongPressHandler(OhosAnnotationsEnvironment environment) {
        super(KeyLongPress.class, environment);
    }

    @Override
    public String[] getParamTypes() {
        return new String[]{CanonicalNameConstants.KEY_EVENT};
    }

    @Override
    public JSwitch getSwitchBody(HasKeyEventCallbackMethods holder) {
        return holder.getOnKeyLongPressSwitchBody();
    }

    @Override
    public void passParametersToMethodCall(Element element, HasKeyEventCallbackMethods holder, JInvocation methodCall) {
        ExecutableElement executableElement = (ExecutableElement) element;
        List<? extends VariableElement> parameters = executableElement.getParameters();
        if (parameters.size() == 1) {
            methodCall.arg(holder.getOnKeyLongPressKeyEventParam());
        }
    }
}
