/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.SliderTouchStart;
import org.ohosannotations.holder.OnSliderProgressUpdatedListenerHolder;

/**
 * SliderTouchStartHandler
 *
 * @since 2021-06-16
 */
public class SliderTouchStartHandler extends AbstractSliderTouchHandler {
    /**
     * SliderTouchStartHandler
     *
     * @param environment 环境
     */
    public SliderTouchStartHandler(OhosAnnotationsEnvironment environment) {
        super(SliderTouchStart.class, environment);
    }

    @Override
    protected JBlock getMethodBodyToCall(OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder) {
        return onSliderProgressUpdatedListenerHolder.getOnTouchStartBody();
    }

    @Override
    protected JVar getMethodParamToPass(OnSliderProgressUpdatedListenerHolder onSliderProgressUpdatedListenerHolder) {
        return onSliderProgressUpdatedListenerHolder.getOnTouchStarSliderParam();
    }
}
