/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.helper;

import com.helger.jcodemodel.AbstractJClass;

import org.ohosannotations.helper.OhosManifest;
import org.ohosannotations.holder.HasIntentBuilder;

/**
 * 服务意图Builder
 *
 * @since 2021-06-18
 */
public class ServiceIntentBuilder extends IntentBuilder {
    /**
     * 构造参数
     *
     * @param holder 持有者
     * @param ohosManifest 清单
     */
    public ServiceIntentBuilder(HasIntentBuilder holder, OhosManifest ohosManifest) {
        super(holder, ohosManifest);
    }

    @Override
    protected AbstractJClass getSuperClass() {
        AbstractJClass superClass = getjClass(org.ohosannotations.api.builder.ServiceIntentBuilder.class);
        return superClass.narrow(builderClass);
    }
}
