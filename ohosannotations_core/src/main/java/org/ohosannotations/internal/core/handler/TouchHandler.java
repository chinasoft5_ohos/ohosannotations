/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.Touch;
import org.ohosannotations.helper.CanonicalNameConstants;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

/**
 * 接触处理程序
 *
 * @author dev
 * @since 2021-07-22
 */
public class TouchHandler extends AbstractViewListenerHandler {
    /**
     * 接触处理程序
     *
     * @param environment 环境
     */
    public TouchHandler(OhosAnnotationsEnvironment environment) {
        super(Touch.class, environment);
    }

    /**
     * 验证
     *
     * @param element 元素
     * @param validation 验证
     */
    @Override
    public void validate(Element element, ElementValidation validation) {
        super.validate(element, validation);

        ExecutableElement executableElement = (ExecutableElement) element;

        validatorHelper.returnTypeIsVoidOrBoolean(executableElement, validation);

        validatorHelper.param.anyOrder() //
            .type(CanonicalNameConstants.TOUCH_EVENT).optional() //
            .extendsType(CanonicalNameConstants.COMPONENT).optional() //
            .validate(executableElement, validation);
    }

    /**
     * 使电话
     *
     * @param listenerMethodBody 侦听器方法主体
     * @param call 调用
     * @param returnType 返回类型
     */
    @Override
    protected void makeCall(JBlock listenerMethodBody, JInvocation call, TypeMirror returnType) {
        boolean returnMethodResult = returnType.getKind() != TypeKind.VOID;

        if (returnMethodResult) {
            listenerMethodBody._return(call);
        } else {
            listenerMethodBody.add(call);
            listenerMethodBody._return(JExpr.TRUE);
        }
    }

    /**
     * 工艺参数
     *
     * @param holder 持有人
     * @param listenerMethod 侦听器方法
     * @param call 调用
     * @param parameters 参数
     */
    @Override
    protected void processParameters(EComponentWithViewSupportHolder holder,
        JMethod listenerMethod, JInvocation call, List<? extends VariableElement> parameters) {
        JVar viewParam = listenerMethod.param(getClasses().COMPONENT, "component");
        JVar eventParam = listenerMethod.param(getClasses().MOTION_EVENT, "touchEvent");

        for (VariableElement parameter : parameters) {
            String parameterType = parameter.asType().toString();

            if (parameterType.equals(CanonicalNameConstants.TOUCH_EVENT)) {
                call.arg(eventParam);
            } else if (isTypeOrSubclass(CanonicalNameConstants.COMPONENT, parameter)) {
                call.arg(castArgumentIfNecessary(holder, CanonicalNameConstants.COMPONENT, viewParam, parameter));
            }else {
            }
        }
    }

    /**
     * 创建侦听器方法
     *
     * @param listenerAnonymousClass 侦听器匿名类
     * @return {@link JMethod}
     */
    @Override
    protected JMethod createListenerMethod(JDefinedClass listenerAnonymousClass) {
        return listenerAnonymousClass.method(JMod.PUBLIC, getCodeModel().BOOLEAN, "onTouchEvent");
    }

    /**
     * setter名字
     *
     * @return {@link String}
     */
    @Override
    protected String getSetterName() {
        return "setTouchEventListener";
    }

    /**
     * 得到侦听器类
     *
     * @param holder 持有人
     * @return {@link AbstractJClass}
     */
    @Override
    protected AbstractJClass getListenerClass(EComponentWithViewSupportHolder holder) {
        return getClasses().COMPONENT_ON_TOUCH_LISTENER;
    }
}
