/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.model;

import org.ohosannotations.annotations.res.AnimationRes;
import org.ohosannotations.annotations.res.BooleanRes;
import org.ohosannotations.annotations.res.ColorRes;
import org.ohosannotations.annotations.res.ColorStateListRes;
import org.ohosannotations.annotations.res.FloatRes;
import org.ohosannotations.annotations.res.HtmlRes;
import org.ohosannotations.annotations.res.IntArrayRes;
import org.ohosannotations.annotations.res.IntegerRes;
import org.ohosannotations.annotations.res.LayoutRes;
import org.ohosannotations.annotations.res.PixelMapRes;
import org.ohosannotations.annotations.res.PixelValueRes;
import org.ohosannotations.annotations.res.StringArrayRes;
import org.ohosannotations.annotations.res.StringRes;
import org.ohosannotations.helper.Constant;
import org.ohosannotations.rclass.IRClass;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

/**
 * 嗳哟res
 * 资源枚举
 *
 * @author dev
 * @since 2021-06-03
 */
public enum OhosRes {
    /**
     * 字符串资源
     */
    STRING(IRClass.Res.STRING, StringRes.class, "getString", "java.lang.String"),
    /**
     * 字符串数组资源
     */
    STRING_ARRAY(IRClass.Res.STRARRAY, StringArrayRes.class, "getStringArray", "java.lang.String[]"),
    /**
     * 动画资源
     */
    ANIMATION(IRClass.Res.ANIM, AnimationRes.class, "createAnimatorProperty", "ohos.agp.animation.AnimatorProperty"),
    /**
     * html资源
     */
    HTML(IRClass.Res.STRING, HtmlRes.class, "getString", "java.lang.CharSequence"), // ohos不支持
    /**
     * 布尔资源
     */
    BOOLEAN(IRClass.Res.BOOLEAN, BooleanRes.class, "getBoolean", "java.lang.Boolean", "boolean"),
    /**
     * 颜色状态列表资源
     */
    COLOR_STATE_LIST(IRClass.Res.COLOR, ColorStateListRes.class,
        "getColorStateList", "ohos.content.res.ColorStateList"), // ohos不支持
    /**
     * float资源
     */
    FLOAT(IRClass.Res.FLOAT, FloatRes.class, "getFloat", "java.lang.Float", "float"),
    /**
     * 像素资源
     */
    PIXEL_VALUE_RES(IRClass.Res.DIMEN, PixelValueRes.class, "getPixelValue",
        Constant.INTEGER, Constant.INT),
    /**
     * 图片资源
     */
    PIXEL_MAP(IRClass.Res.DRAWABLE, PixelMapRes.class, "", Constant.INTEGER, Constant.INT),
    /**
     * 整数数组资源
     */
    INT_ARRAY(IRClass.Res.STRARRAY, IntArrayRes.class, "getIntArray", "int[]"),
    /**
     * 整数资源
     */
    INTEGER(IRClass.Res.INTEGER, IntegerRes.class, "getInteger", Constant.INTEGER, Constant.INT),
    /**
     * 布局资源
     */
    LAYOUT(IRClass.Res.LAYOUT, LayoutRes.class, "getSolidXml", "ohos.global.resource.solidxml.SolidXml"),
    /**
     * 颜色资源
     */
    COLOR(IRClass.Res.COLOR, ColorRes.class, "getColor", Constant.INT, Constant.INTEGER);
    private final Class<? extends Annotation> annotationClass;
    private final String resourceMethodName;
    private final List<String> allowedTypes;
    private final IRClass.Res resInnerClass;

    /**
     * 嗳哟res
     *
     * @param resInnerClass res内部类
     * @param annotationClass 注释类
     * @param resourceMethodName 资源方法名称
     * @param allowedTypes 允许的类型
     */
    OhosRes(IRClass.Res resInnerClass, Class<? extends Annotation> annotationClass,
            String resourceMethodName, String... allowedTypes) {
        this.annotationClass = annotationClass;
        this.resourceMethodName = resourceMethodName;
        this.allowedTypes = Arrays.asList(allowedTypes);
        this.resInnerClass = resInnerClass;
    }

    /**
     * 得到res内部类
     * getResInnerClass
     *
     * @return resInnerClass
     */
    public IRClass.Res getResInnerClass() {
        return resInnerClass;
    }

    /**
     * 让注释类
     *
     * @return annotationClass
     */
    public Class<? extends Annotation> getAnnotationClass() {
        return annotationClass;
    }

    /**
     * 得到目标
     *
     * @return annotationClass
     */
    public String getTarget() {
        return annotationClass.getName();
    }

    /**
     * 获得资源方法名称
     *
     * @return {@link String}
     */
    public String getResourceMethodName() {
        return resourceMethodName;
    }

    /**
     * 得到允许的类型
     *
     * @return {@link List<String>}
     */
    public List<String> getAllowedTypes() {
        return allowedTypes;
    }
}
