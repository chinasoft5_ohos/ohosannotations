/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JBlock;
import com.helger.jcodemodel.JClassAlreadyExistsException;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JVar;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.Bean;
import org.ohosannotations.annotations.EBean;
import org.ohosannotations.annotations.NonConfigurationInstance;
import org.ohosannotations.handler.BaseAnnotationHandler;
import org.ohosannotations.holder.EAbilityHolder;
import org.ohosannotations.holder.NonConfigurationHolder;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

import static com.helger.jcodemodel.JExpr._this;
import static com.helger.jcodemodel.JExpr.cast;
import static com.helger.jcodemodel.JExpr.ref;
import static org.ohosannotations.helper.ModelConstants.classSuffix;

/**
 * 非配置实例处理程序
 *
 * @author dev
 * @since 2021-06-03
 */
public class NonConfigurationInstanceHandler extends BaseAnnotationHandler<EAbilityHolder> {
    /**
     * 非配置实例处理程序
     *
     * @param environment 环境
     */
    public NonConfigurationInstanceHandler(OhosAnnotationsEnvironment environment) {
        super(NonConfigurationInstance.class, environment);
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validatorHelper.enclosingElementHasEAbility(element, validation);

        validatorHelper.isNotPrivate(element, validation);

        validatorHelper.isNotFinal(element, validation);

        if (element.getAnnotation(Bean.class) != null) {
            validatorHelper.typeIsValid(EBean.class, element.asType(), validation);
        }
    }

    @Override
    public void process(Element element, EAbilityHolder holder) throws JClassAlreadyExistsException {
        String fieldName = element.getSimpleName().toString();
        AbstractJClass fieldType = codeModelHelper.typeMirrorToJClass(element.asType());

        NonConfigurationHolder ncHolder = holder.getNonConfigurationHolder();
        JFieldVar ncHolderField = ncHolder.createField(fieldName, fieldType);

        injectInInit(element, holder, fieldName, ncHolderField);
        retainInOnRetain(holder, fieldName, ncHolderField);
    }

    private void injectInInit(Element element, EAbilityHolder holder,
        String fieldName, JFieldVar ncHolderField) throws JClassAlreadyExistsException {
        JBlock initIfNonConfigurationNotNullBlock = holder.getInitIfNonConfigurationNotNullBlock();
        JVar initNonConfigurationInstance = holder.getInitNonConfigurationInstance();
        initIfNonConfigurationNotNullBlock.assign(ref(fieldName), initNonConfigurationInstance.ref(ncHolderField));
        rebindContextIfBean(element, initIfNonConfigurationNotNullBlock, ncHolderField);
    }

    private void retainInOnRetain(EAbilityHolder holder,
        String fieldName, JFieldVar ncHolderField) throws JClassAlreadyExistsException {
        JBlock onRetainNonConfigurationInstanceBindBlock
            = holder.getOnRetainNonConfigurationInstanceBindBlock();
        JVar onRetainNonConfigurationInstance = holder.getOnRetainNonConfigurationInstance();
        onRetainNonConfigurationInstanceBindBlock
            .assign(onRetainNonConfigurationInstance.ref(ncHolderField), ref(fieldName));
    }

    private void rebindContextIfBean(Element element, JBlock initIfNonConfigurationNotNullBlock, JFieldVar field) {
        boolean hasBeanAnnotation = element.getAnnotation(Bean.class) != null;
        if (hasBeanAnnotation) {
            TypeMirror elementType = annotationHelper.extractAnnotationClassParameter(element, Bean.class.getName());
            if (elementType == null) {
                elementType = element.asType();
            }
            String typeQualifiedName = elementType.toString();
            AbstractJClass fieldGeneratedBeanClass = getJClass(typeQualifiedName + classSuffix());

            // do not generate rebind call for singleton beans
            Element eBeanTypeElement = annotationHelper.getTypeUtils().asElement(elementType);
            EBean eBean = eBeanTypeElement.getAnnotation(EBean.class);
            if (eBean != null && eBean.scope() != EBean.Scope.Singleton) {
                initIfNonConfigurationNotNullBlock.invoke(cast(fieldGeneratedBeanClass, field), "rebind").arg(_this());
            }
        }
    }
}
