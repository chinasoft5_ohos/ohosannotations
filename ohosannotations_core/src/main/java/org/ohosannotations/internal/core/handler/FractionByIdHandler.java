/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.core.handler;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JMethod;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.annotations.FractionById;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.EComponentWithViewSupportHolder;
import org.ohosannotations.rclass.IRClass;

import javax.lang.model.element.Element;

/**
 * 暂不支持此注解@FractionById
 * 分数通过id处理程序
 *
 * @author dev
 * @since 2021-07-23
 */
public class FractionByIdHandler extends AbstractFractionByHandler {
    /**
     * 分数通过id处理程序
     *
     * @param environment 环境
     */
    public FractionByIdHandler(OhosAnnotationsEnvironment environment) {
        super(FractionById.class, environment, "findFractionById");
    }

    @Override
    public void validate(Element element, ElementValidation validation) {
        validation.addError("暂不支持此注解@FractionById");
        super.validate(element, validation);

        validatorHelper.resIdsExist(element, IRClass.Res.ID,
            IdValidatorHelper.FallbackStrategy.USE_ELEMENT_NAME, validation);
    }

    /**
     * 会发现分数的方法
     *
     * @param isNativeFraction 是本地部分
     * @param holder 持有人
     * @return {@link JMethod}
     */
    @Override
    protected JMethod getFindFractionMethod(boolean isNativeFraction, EComponentWithViewSupportHolder holder) {
        // isNativeFraction ? holder.getFindNativeFragmentById() : holder.getFindSupportFragmentById()
        return holder.getFindNativeFragmentByTag();
    }

    @Override
    protected IJExpression getFractionId(Element element, String fieldName) {
        return annotationHelper.extractOneAnnotationFieldRef(element, IRClass.Res.ID, true);
    }
}
