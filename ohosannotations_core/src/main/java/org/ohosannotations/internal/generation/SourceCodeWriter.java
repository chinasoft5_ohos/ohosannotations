/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.generation;

import com.helger.jcodemodel.AbstractCodeWriter;
import com.helger.jcodemodel.JPackage;

import org.ohosannotations.internal.process.OriginatingElements;
import org.ohosannotations.logger.Logger;
import org.ohosannotations.logger.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.annotation.processing.Filer;
import javax.annotation.processing.FilerException;
import javax.lang.model.element.Element;
import javax.tools.JavaFileObject;

/**
 * 源代码的作家
 *
 * @author dev
 * @since 2021-07-22
 */
public class SourceCodeWriter extends AbstractCodeWriter {
    private static final VoidOutputStream VOID_OUTPUT_STREAM = new VoidOutputStream();
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceCodeWriter.class);
    private final Filer filer;
    private OriginatingElements originatingElements;

    /**
     * 无效的输出流
     *
     * @author dev
     * @since 2021-07-22
     */
    private static class VoidOutputStream extends OutputStream {
        /**
         * 写
         *
         * @param arg0 arg0
         * @throws IOException ioexception
         */
        @Override
        public void write(int arg0) throws IOException {
            // Do nothing
        }
    }

    /**
     * 源代码的作家
     *
     * @param filer 精密过滤器
     * @param originatingElements 原始元素
     * @param charset 字符集
     */
    public SourceCodeWriter(Filer filer, OriginatingElements originatingElements, Charset charset) {
        super(charset, getDefaultNewLine());
        this.filer = filer;
        this.originatingElements = originatingElements;
    }

    /**
     * 开放的二进制
     *
     * @param pkg 包裹
     * @param fileName 文件名称
     * @return {@link OutputStream}
     * @throws IOException ioexception
     */
    @Override
    public OutputStream openBinary(JPackage pkg, String fileName) throws IOException {
        String qualifiedClassName = toQualifiedClassName(pkg, fileName);
        LOGGER.debug("Generating class: {}", qualifiedClassName);

        Element[] classOriginatingElements = originatingElements.getClassOriginatingElements(qualifiedClassName);

        try {
            JavaFileObject sourceFile;

            if (classOriginatingElements.length == 0) {
                LOGGER.info("Generating class with no originating element: {}", qualifiedClassName);
            }

            sourceFile = filer.createSourceFile(qualifiedClassName, classOriginatingElements);

            return sourceFile.openOutputStream();
        } catch (FilerException e) {
            LOGGER.error("Could not generate source file for {} due to error: {}", qualifiedClassName, e.getMessage());
            /*
             * This exception is expected, when some files are created twice. We cannot
             * delete existing files, unless using a dirty hack. Files a created twice when
             * the same file is created from different annotation rounds. Happens when
             * renaming classes, and for Background executor. It also probably means I
             * didn't fully understand how annotation processing works. If anyone can point
             * me out...
             */
            return VOID_OUTPUT_STREAM;
        }
    }

    /**
     * 来限定类名
     *
     * @param pkg 包裹
     * @param fileName 文件名称
     * @return {@link String}
     */
    private String toQualifiedClassName(JPackage pkg, String fileName) {
        int suffixPosition = fileName.lastIndexOf('.');
        String className = fileName.substring(0, suffixPosition);

        String qualifiedClassName = pkg.name() + "." + className;
        return qualifiedClassName;
    }

    /**
     * 关闭
     *
     * @throws IOException ioexception
     */
    @Override
    public void close() throws IOException {
    }
}
