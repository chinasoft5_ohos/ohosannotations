/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.generation;

import com.helger.jcodemodel.AbstractCodeWriter;
import com.helger.jcodemodel.JPackage;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.annotation.processing.Filer;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

/**
 * 资源代码作家
 *
 * @author dev
 * @since 2021-07-22
 */
public class ResourceCodeWriter extends AbstractCodeWriter {
    private final Filer filer;

    /**
     * 资源代码作家
     *
     * @param filer 精密过滤器
     * @param charset 字符集
     */
    public ResourceCodeWriter(Filer filer, Charset charset) {
        super(charset, getDefaultNewLine());
        this.filer = filer;
    }

    /**
     * 开放的二进制
     *
     * @param pkg 包裹
     * @param fileName 文件名称
     * @return {@link OutputStream}
     * @throws IOException ioexception
     */
    @Override
    public OutputStream openBinary(JPackage pkg, String fileName) throws IOException {
        FileObject resource = filer.createResource(StandardLocation.SOURCE_OUTPUT, pkg.name(), fileName);
        return resource.openOutputStream();
    }

    /**
     * 关闭
     *
     * @throws IOException ioexception
     */
    @Override
    public void close() throws IOException {
    }
}
