/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.rclass;

import org.ohosannotations.internal.exception.RClassNotFoundException;
import org.ohosannotations.logger.Logger;
import org.ohosannotations.logger.LoggerFactory;
import org.ohosannotations.rclass.IRClass;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

/**
 * OhosRClassFinder
 *
 * @since 2021-06-16
 */
public class OhosRclassFinder {
    private static final Logger LOGGER = LoggerFactory.getLogger(OhosRclassFinder.class);

    private final ProcessingEnvironment processingEnv;

    /**
     * OhosRClassFinder
     *
     * @param processingEnv 处理env
     */
    public OhosRclassFinder(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    /**
     * RClassNotFoundException
     *
     * @return IRClass
     * @throws RClassNotFoundException
     */
    public IRClass find() throws RClassNotFoundException {
        Elements elementUtils = processingEnv.getElementUtils();
        TypeElement resourceTableType = elementUtils.getTypeElement("ohos.global.systemres.ResourceTable");
        if (resourceTableType == null) {
            LOGGER.error("The ohos.global.systemres.ResourceTable class cannot be found");
            throw new RClassNotFoundException("The ohos.global.systemres.ResourceTable class cannot be found");
        }

        LOGGER.info("Found ResourceTable class: {}", resourceTableType.toString());
        return new Rclass(resourceTableType);
    }
}
