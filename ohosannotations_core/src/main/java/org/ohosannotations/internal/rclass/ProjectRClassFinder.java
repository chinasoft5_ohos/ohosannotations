/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.rclass;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.Option;
import org.ohosannotations.helper.OhosManifest;
import org.ohosannotations.internal.exception.RClassNotFoundException;
import org.ohosannotations.logger.Logger;
import org.ohosannotations.logger.LoggerFactory;
import org.ohosannotations.rclass.IRClass;

import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

/**
 * ProjectRClassFinder
 *
 * @since 2021-06-16
 */
public class ProjectRClassFinder {
    /**
     * 选择资源包名称
     */
    public static final Option OPTION_RESOURCE_PACKAGE_NAME = new Option("resourcePackageName", null);
    /**
     * 选择使用r2
     */
    public static final Option OPTION_USE_R2 = new Option("useR2", "false");
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectRClassFinder.class);
    private OhosAnnotationsEnvironment environment;

    /**
     * 项目rclass仪
     *
     * @param environment 环境
     */
    public ProjectRClassFinder(OhosAnnotationsEnvironment environment) {
        this.environment = environment;
    }

    /**
     * 找到
     *
     * @param manifest 清单
     * @return {@link IRClass}
     * @throws RClassNotFoundException rclass没有发现异常
     */
    public IRClass find(OhosManifest manifest) throws RClassNotFoundException {
        Elements elementUtils = environment.getProcessingEnvironment().getElementUtils();
        String rClass = getRClassPackageName(manifest) + "." + getRClassSimpleName();
        TypeElement rType = elementUtils.getTypeElement(rClass);

        if (rType == null) {
            LOGGER.error("The generated {} class cannot be found", rClass);
            throw new RClassNotFoundException("The generated " + rClass + " class cannot be found");
        }

        LOGGER.info("Found project ResourceTable class: {}", rType.toString());
        return new Rclass(rType);
    }

    /**
     * 得到rclass包名称
     *
     * @param manifest 清单
     * @return {@link String}
     */
    public String getRClassPackageName(OhosManifest manifest) {
        String resourcePackageName = environment.getOptionValue(OPTION_RESOURCE_PACKAGE_NAME);
        if (resourcePackageName != null) {
            return resourcePackageName;
        } else {
            return manifest.getApplicationPackage();
        }
    }

    /**
     * 得到rclass简单的名称
     *
     * @return {@link String}
     */
    private String getRClassSimpleName() {
        boolean isUseR2 = environment.getOptionBooleanValue(OPTION_USE_R2);

        return isUseR2 ? "ResourceTable2" : "ResourceTable";
    }
}
