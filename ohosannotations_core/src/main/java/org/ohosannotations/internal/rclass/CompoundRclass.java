/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.internal.rclass;

import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.rclass.IRClass;

/**
 * CompoundRClass
 *
 * @since 2021-06-16
 */
public class CompoundRclass implements IRClass {
    private IRClass ohosRclass;
    private IRClass mrClass;

    /**
     * CompoundRclass
     *
     * @param mrClass 类
     * @param ohosRclass rclass
     */
    public CompoundRclass(IRClass mrClass, IRClass ohosRclass) {
        this.mrClass = mrClass;
        this.ohosRclass = ohosRclass;
    }

    @Override
    public boolean containsIdValue(Integer idValue) {
        return mrClass.containsIdValue(idValue) || ohosRclass.containsIdValue(idValue);
    }

    @Override
    public String getIdQualifiedNameById(Integer idValue) {
        String idQualifiedName = mrClass.getIdQualifiedNameById(idValue);
        if (idQualifiedName == null) {
            idQualifiedName = ohosRclass.getIdQualifiedNameById(idValue);
        }
        return idQualifiedName;
    }

    @Override
    public boolean containsField(String name) {
        return mrClass.containsField(name) || ohosRclass.containsField(name);
    }

    @Override
    public String getIdQualifiedNameByName(String name) {
        String idQualifiedName = mrClass.getIdQualifiedNameByName(name);
        if (idQualifiedName == null) {
            idQualifiedName = ohosRclass.getIdQualifiedNameByName(name);
        }
        return idQualifiedName;
    }

    @Override
    public JFieldRef getIdStaticRef(Integer idValue, OhosAnnotationsEnvironment environment) {
        JFieldRef idStaticRef = mrClass.getIdStaticRef(idValue, environment);
        if (idStaticRef == null) {
            idStaticRef = ohosRclass.getIdStaticRef(idValue, environment);
        }
        return idStaticRef;
    }

    @Override
    public JFieldRef getIdStaticRef(String name, OhosAnnotationsEnvironment environment) {
        JFieldRef idStaticRef = mrClass.getIdStaticRef(name, environment);
        if (idStaticRef == null) {
            idStaticRef = ohosRclass.getIdStaticRef(name, environment);
        }
        return idStaticRef;
    }
}
