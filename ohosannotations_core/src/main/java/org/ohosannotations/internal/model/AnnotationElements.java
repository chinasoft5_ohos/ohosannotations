/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.model;

import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * 注解元素接口
 *
 * @since 2021-07-20
 */
public interface AnnotationElements {
    /**
     * 注释元素和根元素
     *
     * @since 2021-07-20
     */
    class AnnotatedAndRootElements {
        /**
         * 元素
         */
        public final Element annotatedElement;
        /**
         * 元素类型
         */
        public final TypeElement rootTypeElement;

        /**
         * 构造参数
         *
         * @param annotatedElement 元素
         * @param rootTypeElement 元素类型
         */
        AnnotatedAndRootElements(Element annotatedElement, TypeElement rootTypeElement) {
            this.annotatedElement = annotatedElement;
            this.rootTypeElement = rootTypeElement;
        }

        @Override
        public String toString() {
            return annotatedElement.toString();
        }
    }

    /**
     * 获取所有元素
     *
     * @return Set 集合
     */
    Set<? extends Element> getAllElements();

    /**
     * 获取根注释元素
     *
     * @param annotationName 注解名字
     * @return Set 集合
     */
    Set<? extends Element> getRootAnnotatedElements(String annotationName);

    /**
     * 获取父类注解元素
     *
     * @param annotationName 注解名字
     * @return Set 集合
     */
    Set<AnnotatedAndRootElements> getAncestorAnnotatedElements(String annotationName);
}
