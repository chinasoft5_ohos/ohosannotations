/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Element;

/**
 * 原始元素
 *
 * @since 2021-07-20
 */
public class OriginatingElements {
    private final Map<String, List<Element>> originatingElementsByClassName = new HashMap<>();

    /**
     * 添加
     *
     * @param qualifiedName 限定名称
     * @param element 元素
     */
    public void add(String qualifiedName, Element element) {
        List<Element> originatingElements = originatingElementsByClassName.get(qualifiedName);
        if (originatingElements == null) {
            originatingElements = new ArrayList<>();
            originatingElementsByClassName.put(qualifiedName, originatingElements);
        }
        originatingElements.add(element);
    }

    /**
     * 获取类的原始元素
     *
     * @param className 类名
     * @return Element[] 元素集合
     */
    public Element[] getClassOriginatingElements(String className) {
        List<Element> originatingElements = originatingElementsByClassName.get(className);
        if (originatingElements == null) {
            return new Element[0];
        } else {
            return originatingElements.toArray(new Element[originatingElements.size()]);
        }
    }
}
