/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.exception;

import org.ohosannotations.plugin.OhosAnnotationsPlugin;

/**
 * 版本找不到异常
 *
 * @since 2021-07-19
 */
public class VersionNotFoundException extends Exception {
    private final OhosAnnotationsPlugin plugin;

    /**
     * 构造参数
     *
     * @param plugin 插件
     */
    public VersionNotFoundException(OhosAnnotationsPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getMessage() {
        return plugin.getName() + " does not declare its version!";
    }
}
