/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.exception;

import javax.lang.model.element.Element;

/**
 * 处理异常
 *
 * @since 2021-07-19
 */
public class ProcessingException extends Exception {
    private static final long serialVersionUID = -1282996599471872615L;

    private final Element element;

    /**
     * 构造参数
     *
     * @param cause 可抛异常
     * @param element 元素
     */
    public ProcessingException(Throwable cause, Element element) {
        super(cause);
        this.element = element;
    }

    /**
     * 构造参数
     *
     * @param message 异常消息
     * @param cause 可抛异常
     * @param element 元素
     */
    public ProcessingException(String message, Throwable cause, Element element) {
        super(message, cause);
        this.element = element;
    }

    /**
     * 获取元素
     *
     * @return Element
     */
    public Element getElement() {
        return element;
    }
}
