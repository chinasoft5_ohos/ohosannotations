/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.helper;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JAnnotationArrayMember;
import com.helger.jcodemodel.JAnnotationUse;
import com.helger.jcodemodel.JEnumConstantRef;
import com.helger.jcodemodel.JExpr;

import org.ohosannotations.helper.APTCodeModelHelper;

import java.util.List;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.SimpleAnnotationValueVisitor6;

/**
 * 注释参数提取器
 *
 * @author dev
 * @since 2021-07-22
 */
public class AnnotationParamExtractor extends SimpleAnnotationValueVisitor6<Void, String> {
    private JAnnotationUse use;
    private APTCodeModelHelper helper;

    /**
     * 构造参数
     *
     * @param use 注解使用
     * @param helper 助手
     */
    public AnnotationParamExtractor(JAnnotationUse use, APTCodeModelHelper helper) {
        this.use = use;
        this.helper = helper;
    }

    @Override
    public Void visitArray(List<? extends AnnotationValue> vals, String str) {
        JAnnotationArrayMember paramArray = use.paramArray(str);

        for (AnnotationValue annotationValue : vals) {
            annotationValue.accept(new AnnotationArrayParamExtractor(helper), paramArray);
        }

        return null; // 返回空
    }

    @Override
    public Void visitBoolean(boolean isVisit, String p1) {
        use.param(p1, isVisit);
        return null; // 返回空
    }

    @Override
    public Void visitByte(byte b1, String p1) {
        use.param(p1, b1);
        return null; // 返回空
    }

    @Override
    public Void visitChar(char c1, String p1) {
        use.param(p1, c1);
        return null; // 返回空
    }

    @Override
    public Void visitDouble(double d1, String p1) {
        use.param(p1, d1);
        return null; // 返回空
    }

    @Override
    public Void visitFloat(float f1, String p1) {
        use.param(p1, f1);
        return null; // 返回空
    }

    @Override
    public Void visitInt(int i1, String p1) {
        use.param(p1, i1);
        return null; // 返回空
    }

    @Override
    public Void visitLong(long l1, String p1) {
        use.param(p1, l1);
        return null; // 返回空
    }

    @Override
    public Void visitShort(short s1, String p1) {
        use.param(p1, s1);
        return null; // 返回空
    }

    @Override
    public Void visitString(String s1, String p1) {
        use.param(p1, s1);
        return null; // 返回空
    }

    @Override
    public Void visitEnumConstant(VariableElement c1, String p1) {
        AbstractJClass annotationClass = helper.typeMirrorToJClass(c1.asType());
        JEnumConstantRef ref = JExpr.enumConstantRef(annotationClass, c1.getSimpleName().toString());
        use.param(p1, ref);
        return null; // 返回空
    }

    @Override
    public Void visitType(TypeMirror t1, String p1) {
        AbstractJClass annotationClass = helper.typeMirrorToJClass(t1);
        use.param(p1, annotationClass);
        return null; // 返回空
    }

    @Override
    public Void visitAnnotation(AnnotationMirror a1, String p1) {
        AbstractJClass mirror = helper.typeMirrorToJClass(a1.getAnnotationType());
        use.annotationParam(p1, mirror);
        return null; // 返回空
    }
}
