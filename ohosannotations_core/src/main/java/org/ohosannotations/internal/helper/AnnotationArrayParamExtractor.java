/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.internal.helper;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JAnnotationArrayMember;
import com.helger.jcodemodel.JEnumConstantRef;
import com.helger.jcodemodel.JExpr;

import org.ohosannotations.helper.APTCodeModelHelper;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.SimpleAnnotationValueVisitor6;

/**
 * 注释数组参数提取器
 *
 * @author dev
 * @since 2021-07-22
 */
public class AnnotationArrayParamExtractor extends SimpleAnnotationValueVisitor6<Void, JAnnotationArrayMember> {
    private APTCodeModelHelper helper;

    /**
     * 注释数组参数提取器
     *
     * @param helper 助手
     */
    public AnnotationArrayParamExtractor(APTCodeModelHelper helper) {
        this.helper = helper;
    }

    @Override
    public Void visitBoolean(boolean isVisit, JAnnotationArrayMember pp) {
        pp.param(isVisit);
        return null; // 返回空
    }

    @Override
    public Void visitByte(byte bb, JAnnotationArrayMember pp) {
        pp.param(bb);
        return null; // 返回空
    }

    @Override
    public Void visitChar(char cc, JAnnotationArrayMember pp) {
        pp.param(cc);
        return null; // 返回空
    }

    @Override
    public Void visitDouble(double dd, JAnnotationArrayMember pp) {
        pp.param(dd);
        return null; // 返回空
    }

    @Override
    public Void visitFloat(float ff, JAnnotationArrayMember pp) {
        pp.param(ff);
        return null; // 返回空
    }

    @Override
    public Void visitInt(int ii, JAnnotationArrayMember pp) {
        pp.param(ii);
        return null; // 返回空
    }

    @Override
    public Void visitLong(long ll, JAnnotationArrayMember pp) {
        pp.param(ll);
        return null; // 返回空
    }

    @Override
    public Void visitShort(short ss, JAnnotationArrayMember pp) {
        pp.param(ss);
        return null; // 返回空
    }

    @Override
    public Void visitString(String ss, JAnnotationArrayMember pp) {
        pp.param(ss);
        return null; // 返回空
    }

    @Override
    public Void visitType(TypeMirror mirror, JAnnotationArrayMember pp) {
        AbstractJClass annotationClass = helper.typeMirrorToJClass(mirror);
        pp.param(annotationClass);
        return null; // 返回空
    }

    @Override
    public Void visitEnumConstant(VariableElement c1, JAnnotationArrayMember p1) {
        AbstractJClass annotationClass = helper.typeMirrorToJClass(c1.asType());
        JEnumConstantRef ref = JExpr.enumConstantRef(annotationClass, c1.getSimpleName().toString());
        p1.param(ref);
        return null; // 返回空
    }

    @Override
    public Void visitAnnotation(AnnotationMirror a1, JAnnotationArrayMember p1) {
        helper.copyAnnotation(p1, a1);
        return null; // 返回空
    }
}
