package org.ohosannotations.internal.helper.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

/**
 * 配置实体
 *
 * @since 2021-07-19
 */
public class ConfigEntity implements Serializable {
    /**
     * app : {"bundleName":"org.ohosannotations.sample"}
     * deviceConfig : {}
     * module : {"package":"org.ohosannotations}
     */

    /**
     * Bean类
     */
    public AppBean app;
    /**
     * 设备配置Bean类
     */
    public DeviceConfigBean deviceConfig;
    /**
     * 模型Bean类
     */
    public ModuleBean module;

    /**
     * AppBean类
     *
     * @since 2021-07-19
     */
    public static class AppBean implements Serializable {
        /**
         * bundleName : org.ohosannotations.sample
         * vendor : ohosannotations
         * version : {"code":1000000,"name":"1.0.0"}
         * apiVersion : {"compatible":4,"target":5,"releaseType":"Release"}
         */

        /**
         * 包名
         */
        public String bundleName;
        /**
         * 供应商
         */
        public String vendor;
        /**
         * 版本号
         */
        public VersionBean version;
        /**
         * api版本
         */
        public ApiVersionBean apiVersion;

        /**
         * 版本Bean类
         *
         * @since 2021-07-19
         */
        public static class VersionBean implements Serializable {
            /**
             * code : 1000000
             * name : 1.0.0
             */

            /**
             * 版本代码
             */
            public int code;
            /**
             * 版本号
             */
            public String name;

        }

        /**
         * API版本Bean类
         *
         * @since 2021-07-19
         */
        public static class ApiVersionBean implements Serializable {
            /**
             * 兼容性
             */
            public int compatible;
            /**
             * 指标
             */
            public int target;
            /**
             * 发布类型
             */
            public String releaseType;
        }
    }

    /**
     * 设备配置
     *
     * @since 2021-07-19
     */
    public static class DeviceConfigBean implements Serializable {
    }

    /**
     * 模型Bean类
     *
     * @since 2021-07-19
     */
    public static class ModuleBean implements Serializable {
        /**
         * package : org.ohosannotations.sample
         * name : .MyApplication
         * deviceType : ["phone"]
         * distro : {"deliveryWithInstall":true,"moduleName":"example_gradle","moduleType":"entry"}
         * abilities
         */
        @JSONField(name = "package")
        public String packageX;
        /**
         * 名字
         */
        public String name;
        /**
         * 发行版
         */
        public DistroBean distro;
        /**
         * 设备类型集合
         */
        public List<String> deviceType;
        /**
         * 请求权限集合
         */
        public List<ReqPermissionsBean> reqPermissions;
        /**
         * Ability集合
         */
        public List<AbilitiesBean> abilities;
        /**
         * 元数据
         */
        public MetaDataBean metaData;

        /**
         * 元数据Bean
         *
         * @since 2021-07-19
         */
        public static class MetaDataBean implements Serializable {
            /**
             * 自定义数据集合
             */
            public List<CustomizeData> customizeData;

            /**
             * 自定义数据类
             *
             * @since 2021-07-19
             */
            public static class CustomizeData implements Serializable {
                /**
                 * 名字
                 */
                public String name;
                /**
                 * 值
                 */
                public String value;
                /**
                 * 额外
                 */
                public String extra;
            }
        }

        /**
         * 请求权限类
         *
         * @since 2021-07-19
         */
        public static class ReqPermissionsBean implements Serializable {
            /**
             * "name": "ohos.permission.CAMERA",
             * "reason": "$string:permreason_camera",
             * "usedScene":
             * {
             * "ability": ["com.mycamera.Ability", "com.mycamera.AbilityBackground"],
             * "when": "always"
             * }
             */
            public String name;
            /**
             * 理由
             */
            public String reason;
            /**
             * 使用场景
             */
            public UsedSceneBean usedScene;

            /**
             * 使用场景类
             *
             * @since 2021-07-19
             */
            public static class UsedSceneBean implements Serializable {
                /**
                 * when 字段
                 */
                public String when;
                /**
                 * 字符串集合
                 */
                public List<String> ability;
            }

        }

        /**
         * 发行版类
         *
         * @since 2021-07-19
         */
        public static class DistroBean implements Serializable {
            /**
             * deliveryWithInstall : true
             * moduleName : example_gradle
             * moduleType : entry
             */

            /**
             * 交货与安装
             */
            public boolean deliveryWithInstall;
            /**
             * 模板名字
             */
            public String moduleName;
            /**
             * 模板类型
             */
            public String moduleType;
        }

        /**
         * Ability Bean类型
         */
        public static class AbilitiesBean implements Serializable {
            /**
             * skills : [{"entities":["entity.system.home"],"actions":["action.system.home"]}]
             * orientation : unspecified
             * name : org.ohosannotations.sample.MainAbility
             * icon : $media:icon
             * description : $string:mainability_description
             * label : $string:app_name
             * type : page
             * launchType : standard
             */
            /**
             * 方向
             */
            public String orientation;
            /**
             * 名字
             */
            public String name;
            /**
             * icon 图标
             */
            public String icon;
            /**
             * 说明
             */
            public String description;
            /**
             * 标签
             */
            public String label;
            /**
             * 类型
             */
            public String type;
            /**
             * 启动模式
             */
            public String launchType;
            /**
             * 技术集合
             */
            public List<SkillsBean> skills;

            /**
             * 技术bean 类
             *
             * @author dev
             * @since 2021-07-23
             */
            public static class SkillsBean implements Serializable {
                /**
                 * 对象集合
                 */
                public List<String> entities;
                /**
                 * 动作集合
                 */
                public List<String> actions;
            }
        }
    }
}
