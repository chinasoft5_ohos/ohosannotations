/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.logger.appender;

import org.ohosannotations.logger.Level;
import org.ohosannotations.logger.Logger;
import org.ohosannotations.logger.LoggerFactory;
import org.ohosannotations.logger.formatter.FormatterFull;

import java.util.LinkedList;
import java.util.List;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;

/**
 * ConsoleAppender
 *
 * @since 2021-07-20
 */
public class ConsoleAppender extends Appender {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleAppender.class);
    private final List<String> errors = new LinkedList<>();

    /**
     * 构造参数
     */
    public ConsoleAppender() {
        super(new FormatterFull());
    }

    @Override
    public void open() {
    }

    @Override
    public void append(Level level, Element element, AnnotationMirror annotationMirror, String message) {
        if (level.isSmaller(Level.ERROR)) {
            LOGGER.error(message);
        } else {
            errors.add(message);
        }
    }

    @Override
    public synchronized void close(boolean isLastRound) {
        if (isLastRound) {
            for (String error : errors) {
                LOGGER.error(error);
            }
        }
    }
}
