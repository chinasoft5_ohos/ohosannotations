/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.logger.appender;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.logger.Level;
import org.ohosannotations.logger.formatter.Formatter;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;

/**
 * Appender
 *
 * @since 2021-07-20
 */
public abstract class Appender {
    /**
     * Formatter
     */
    protected final Formatter formatter;
    /**
     * ProcessingEnvironment
     */
    protected ProcessingEnvironment processingEnv;

    /**
     * 构造参数
     *
     * @param formatter 格式化器
     */
    public Appender(Formatter formatter) {
        this.formatter = formatter;
    }

    /**
     * getFormatter
     *
     * @return Formatter 格式化器
     */
    public Formatter getFormatter() {
        return formatter;
    }

    /**
     * setEnvironment
     *
     * @param environment 注解环境
     */
    public void setEnvironment(OhosAnnotationsEnvironment environment) {
        this.processingEnv = environment.getProcessingEnvironment();
    }

    /**
     * open
     */
    public abstract void open();

    /**
     * append
     *
     * @param level 等级
     * @param element 元素
     * @param annotationMirror 注解镜像
     * @param message 消息
     */
    public abstract void append(Level level, Element element, AnnotationMirror annotationMirror, String message);

    /**
     * close
     *
     * @param lastRound 最后一轮
     */
    public abstract void close(boolean lastRound);
}
