/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.logger;

/**
 * 枚举等级
 *
 * @since 2021-07-19
 */
public enum Level {
    /**
     * 跟踪
     */
    TRACE(1, "TRACE"), //
    /**
     * 调试
     */
    DEBUG(2, "DEBUG"), //
    /**
     * 信息
     */
    INFO(3, "INFO "), //
    /**
     * 警告
     */
    WARN(4, "WARN "), //
    /**
     * 错误
     */
    ERROR(5, "ERROR");
    /**
     * 权重
     */
    public final int weight;
    /**
     * 名字
     */
    public final String name;

    /**
     * 构造参数
     *
     * @param weight 权重
     * @param name 名字
     */
    Level(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    /**
     * 大于或等于
     *
     * @param ll 等级
     * @return boolean是否大于或者等于
     */
    public boolean isGreaterOrEquals(Level ll) {
        return weight >= ll.weight;
    }

    /**
     * 小于
     *
     * @param ll 等级
     * @return boolean是否小于
     */
    public boolean isSmaller(Level ll) {
        return weight < ll.weight;
    }

    /**
     * 解析
     *
     * @param name 解析名字
     * @return Level 等级
     */
    public static Level parse(String name) {
        for (Level level : values()) {
            if (level.name().equalsIgnoreCase(name)) {
                return level;
            }
        }
        throw new IllegalArgumentException("Can't find Level matching " + name);
    }
}
