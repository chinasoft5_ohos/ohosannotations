/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.logger.formatter;

import org.ohosannotations.logger.Level;

/**
 * 简单格式化程序
 *
 * @since 2021-07-19
 */
public class FormatterSimple extends Formatter {
    @Override
    public String buildLog(Level level, String loggerName, String message, Throwable thr, Object... args) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(buildFullMessage(message, args));

        // Stacktrace
        if (thr != null) {
            stringBuilder.append('\n').append(stackTraceToString(thr));
        }

        return stringBuilder.toString();
    }
}
