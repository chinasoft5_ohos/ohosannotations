/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.handler;

import com.helger.jcodemodel.IJAssignmentTarget;
import com.helger.jcodemodel.JBlock;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.helper.InjectHelper;
import org.ohosannotations.holder.GeneratedClassHolder;

import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

/**
 * MethodInjectionHandler
 *
 * @param <T> 子类泛型
 * @since 2021-07-20
 */
public interface MethodInjectionHandler<T extends GeneratedClassHolder> {
    /**
     * getInvocationBlock
     *
     * @param holder 持有者
     * @return JBlock J板块
     */
    JBlock getInvocationBlock(T holder);

    /**
     * assignValue
     *
     * @param targetBlock J板块
     * @param fieldRef 任务目标
     * @param holder 持有者
     * @param element 元素
     * @param param 参数
     */
    void assignValue(JBlock targetBlock, IJAssignmentTarget fieldRef, T holder, Element element, Element param);

    /**
     * validateEnclosingElement
     *
     * @param element 元素
     * @param valid 元素验证
     */
    void validateEnclosingElement(Element element, ElementValidation valid);

    /**
     * 毕竟注入参数处理程序
     * AfterAllParametersInjectedHandler
     *
     * @author dev
     * @param <T> 子类泛型
     * @since 2021-07-23
     */
    interface AfterAllParametersInjectedHandler<T extends GeneratedClassHolder> {
        /**
         * afterAllParametersInjected
         *
         * @param holder 持有者
         * @param method 可执行元素
         * @param parameterList 集合
         */
        void afterAllParametersInjected(T holder, ExecutableElement method,
            List<InjectHelper.ParamHelper> parameterList);
    }
}
