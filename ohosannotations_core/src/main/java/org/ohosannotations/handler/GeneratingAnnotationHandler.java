/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.handler;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.holder.GeneratedClassHolder;

import javax.lang.model.element.TypeElement;

/**
 * 生成注释处理程序
 *
 * @param <T> 泛型
 * @since 2021-07-20
 */
public interface GeneratingAnnotationHandler<T extends GeneratedClassHolder> extends AnnotationHandler<T> {
    /**
     * 创建生成的类Holder
     *
     * @param environment 注解环境
     * @param annotatedElement 元素类型
     * @return T 泛型
     * @throws Exception 异常
     */
    T createGeneratedClassHolder(OhosAnnotationsEnvironment environment, TypeElement annotatedElement) throws Exception;
}
