/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.holder.GeneratedClassHolder;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * 基础生成注释处理程序
 *
 * @param <T> 泛型
 * @since 2021-07-20
 */
public abstract class BaseGeneratingAnnotationHandler<T extends GeneratedClassHolder>
    extends BaseAnnotationHandler<T> implements GeneratingAnnotationHandler<T> {
    /**
     * 构造参数
     *
     * @param targetClass 目标字节类
     * @param environment 环境
     */
    public BaseGeneratingAnnotationHandler(Class<?> targetClass, OhosAnnotationsEnvironment environment) {
        super(targetClass, environment);
    }

    /**
     * 构造参数
     *
     * @param target 目标字符串
     * @param environment 环境
     */
    public BaseGeneratingAnnotationHandler(String target, OhosAnnotationsEnvironment environment) {
        super(target, environment);
    }

    @Override
    protected void validate(Element element, ElementValidation valid) {
        validatorHelper.isNotFinal(element, valid);

        if (isInnerClass(element)) {

            validatorHelper.isNotPrivate(element, valid);

            validatorHelper.isStatic(element, valid);

            validatorHelper.enclosingElementHasOhosAnnotation(element, valid);

            validatorHelper.enclosingElementIsNotAbstractIfNotAbstract(element, valid);
        }
    }

    private boolean isInnerClass(Element element) {
        TypeElement typeElement = (TypeElement) element;
        return typeElement.getNestingKind().isNested();
    }
}
