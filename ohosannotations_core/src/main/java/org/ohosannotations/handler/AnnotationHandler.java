/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.handler;

import org.ohosannotations.ElementValidation;
import org.ohosannotations.holder.GeneratedClassHolder;

import javax.lang.model.element.Element;

/**
 * 注解处理者
 *
 * @param <T> 泛型
 * @since 2021-07-20
 */
public interface AnnotationHandler<T extends GeneratedClassHolder> {
    /**
     * 获取目标
     *
     * @return String 字符串
     */
    String getTarget();

    /**
     * 验证
     *
     * @param element 元素
     * @return 元素验证
     */
    ElementValidation validate(Element element);

    /**
     * 过程
     *
     * @param element 袁旭
     * @param holder 持有者
     * @throws Exception 异常
     */
    void process(Element element, T holder) throws Exception;

    /**
     * 是否有效
     *
     * @return boolean布尔值
     */
    boolean isEnabled();
}
