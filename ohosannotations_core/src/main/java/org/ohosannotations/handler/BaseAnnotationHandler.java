/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.handler;

import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.ElementValidation;
import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.helper.APTCodeModelHelper;
import org.ohosannotations.helper.IdAnnotationHelper;
import org.ohosannotations.helper.IdValidatorHelper;
import org.ohosannotations.holder.GeneratedClassHolder;
import org.ohosannotations.internal.process.ProcessHolder;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JCodeModel;

/**
 * 基地注释处理程序
 *
 * @author dev
 * @param <T> 子类泛型
 * @since 2021-07-22
 */
public abstract class BaseAnnotationHandler<T extends GeneratedClassHolder> implements AnnotationHandler<T> {
    /**
     * 注释的助手
     */
    protected IdAnnotationHelper annotationHelper;
    /**
     * 验证器辅助
     */
    protected IdValidatorHelper validatorHelper;
    /**
     * 代码模型辅助
     */
    protected APTCodeModelHelper codeModelHelper;

    private OhosAnnotationsEnvironment environment;
    private final String target;

    /**
     * 基地注释处理程序
     *
     * @param targetClass 目标类
     * @param environment 环境
     */
    public BaseAnnotationHandler(Class<?> targetClass, OhosAnnotationsEnvironment environment) {
        this(targetClass.getCanonicalName(), environment);
    }

    /**
     * 基地注释处理程序
     *
     * @param target 目标
     * @param environment 环境
     */
    public BaseAnnotationHandler(String target, OhosAnnotationsEnvironment environment) {
        this.target = target;
        this.environment = environment;
        annotationHelper = new IdAnnotationHelper(environment, target);
        validatorHelper = new IdValidatorHelper(annotationHelper);
        codeModelHelper = new APTCodeModelHelper(environment);
    }

    /**
     * 得到目标
     *
     * @return {@link String}
     */
    @Override
    public String getTarget() {
        return target;
    }

    /**
     * 验证
     *
     * @param element 元素
     * @return {@link ElementValidation}
     */
    @Override
    public ElementValidation validate(Element element) {
        ElementValidation validation = new ElementValidation(target, element);
        validate(element, validation);
        return validation;
    }

    /**
     * isEnabled
     *
     * @return true
     */
    public boolean isEnabled() {
        return true;
    }

    /**
     * validate
     *
     * @param element element
     * @param validation 验证
     */
    protected abstract void validate(Element element, ElementValidation validation);

    /**
     * getEnvironment
     *
     * @return environment
     */
    protected OhosAnnotationsEnvironment getEnvironment() {
        return environment;
    }

    /**
     * getProcessingEnvironment
     *
     * @return environment
     */
    protected ProcessingEnvironment getProcessingEnvironment() {
        return environment.getProcessingEnvironment();
    }

    /**
     * getClasses
     *
     * @return environment
     */
    protected ProcessHolder.Classes getClasses() {
        return environment.getClasses();
    }

    /**
     * getCodeModel
     *
     * @return environment
     */
    protected JCodeModel getCodeModel() {
        return environment.getCodeModel();
    }

    /**
     * getJClass
     *
     * @param fullyQualifiedClassName fullyQualifiedClassName
     * @return environment
     */
    protected AbstractJClass getJClass(String fullyQualifiedClassName) {
        return environment.getJClass(fullyQualifiedClassName);
    }

    /**
     * getJClass
     *
     * @param clazz clazz
     * @return environment
     */
    protected AbstractJClass getJClass(Class<?> clazz) {
        return environment.getJClass(clazz);
    }

    /**
     * hasTargetMethod
     *
     * @param type type
     * @param methodName methodName
     * @return false
     */
    protected boolean hasTargetMethod(TypeElement type, String methodName) {
        if (type == null) {
            return false;
        }

        List<? extends Element> allMembers = getProcessingEnvironment().getElementUtils().getAllMembers(type);
        for (ExecutableElement element : ElementFilter.methodsIn(allMembers)) {
            if (element.getSimpleName().contentEquals(methodName)) {
                return true;
            }
        }
        return false;
    }
}
