/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

import org.ohosannotations.handler.AnnotationHandler;
import org.ohosannotations.handler.GeneratingAnnotationHandler;
import org.ohosannotations.helper.OhosManifest;
import org.ohosannotations.holder.GeneratedClassHolder;
import org.ohosannotations.internal.model.AnnotationElements;
import org.ohosannotations.internal.process.ProcessHolder;
import org.ohosannotations.plugin.OhosAnnotationsPlugin;
import org.ohosannotations.rclass.IRClass;

import com.helger.jcodemodel.AbstractJClass;
import com.helger.jcodemodel.JCodeModel;
import com.helger.jcodemodel.JDefinedClass;

/**
 * 嗳哟注释环境
 *
 * @author dev
 * @since 2021-07-22
 */
public interface OhosAnnotationsEnvironment {
    /**
     * 得到处理环境
     *
     * @return {@link ProcessingEnvironment}
     */
    ProcessingEnvironment getProcessingEnvironment();

    /**
     * 得到支持选项
     *
     * @return {@link Set}
     */
    Set<String> getSupportedOptions();

    /**
     * 得到选项值
     *
     * @param option 选项
     * @return {@link String}
     */
    String getOptionValue(Option option);

    /**
     * 得到选项值
     *
     * @param optionKey 选择键
     * @return {@link String}
     */
    String getOptionValue(String optionKey);

    /**
     * 得到选择布尔值
     *
     * @param option 选项
     * @return boolean
     */
    boolean getOptionBooleanValue(Option option);

    /**
     * 得到选择布尔值
     *
     * @param optionKey 选择键
     * @return boolean
     */
    boolean getOptionBooleanValue(String optionKey);

    /**
     * 得到支持注释类型
     *
     * @return {@link Set}
     */
    Set<String> getSupportedAnnotationTypes();

    /**
     * 得到处理程序
     *
     * @return {@link List}
     */
    List<AnnotationHandler<?>> getHandlers();

    /**
     * 得到装饰处理程序
     *
     * @return {@link List}
     */
    List<AnnotationHandler<?>> getDecoratingHandlers();

    /**
     * 获取生成处理程序
     *
     * @return {@link List}
     */
    List<GeneratingAnnotationHandler<?>> getGeneratingHandlers();

    /**
     * 得到rclass
     *
     * @return {@link IRClass}
     */
    IRClass getRClass();

    /**
     * 得到嗳哟清单
     *
     * @return {@link OhosManifest}
     */
    OhosManifest getOhosManifest();

    /**
     * 得到提取的元素
     *
     * @return {@link AnnotationElements}
     */
    AnnotationElements getExtractedElements();

    /**
     * 得到验证的元素
     *
     * @return {@link AnnotationElements}
     */
    AnnotationElements getValidatedElements();

    /**
     * 得到代码模型
     *
     * @return {@link JCodeModel}
     */
    JCodeModel getCodeModel();

    /**
     * 得到jclass
     *
     * @param fullyQualifiedName 全限定名
     * @return {@link AbstractJClass}
     */
    AbstractJClass getJClass(String fullyQualifiedName);

    /**
     * 得到jclass
     *
     * @param clazz clazz
     * @return {@link AbstractJClass}
     */
    AbstractJClass getJClass(Class<?> clazz);

    /**
     * 得到定义的类
     *
     * @param fullyQualifiedName 全限定名
     * @return {@link JDefinedClass}
     */
    JDefinedClass getDefinedClass(String fullyQualifiedName);

    /**
     * 把生成的类持有人
     *
     * @param element 元素
     * @return {@link GeneratedClassHolder}
     */
    GeneratedClassHolder getGeneratedClassHolder(Element element);

    /**
     * 得到类
     *
     * @return {@link ProcessHolder.Classes}
     */
    ProcessHolder.Classes getClasses();

    /**
     * 得到生成注释
     *
     * @return {@link List}
     */
    List<Class<? extends Annotation>> getGeneratingAnnotations();

    /**
     * 是嗳哟注释
     *
     * @param annotationQualifiedName 注释限定名称
     * @return boolean
     */
    boolean isOhosAnnotation(String annotationQualifiedName);

    /**
     * 把插件
     *
     * @return {@link List}
     */
    List<OhosAnnotationsPlugin> getPlugins();
}
