/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the ohosAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.helper;

import java.io.Serializable;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * CanonicalNameConstants
 *
 * @since 2021-07-20
 */
public final class CanonicalNameConstants {
    // Java
    /**
     * Object
     */
    public static final String OBJECT = Object.class.getCanonicalName();
    /**
     * URI
     */
    public static final String URI = URI.class.getCanonicalName();
    /**
     * Map
     */
    public static final String MAP = Map.class.getCanonicalName();
    /**
     * Set
     */
    public static final String SET = Set.class.getCanonicalName();
    /**
     * List
     */
    public static final String LIST = List.class.getCanonicalName();
    /**
     * Collection
     */
    public static final String COLLECTION = Collection.class.getCanonicalName();
    /**
     * Collections
     */
    public static final String COLLECTIONS = Collections.class.getCanonicalName();
    /**
     * String
     */
    public static final String STRING = String.class.getCanonicalName();
    /**
     * StringBuilder
     */
    public static final String STRING_BUILDER = StringBuilder.class.getCanonicalName();
    /**
     * Set
     */
    public static final String STRING_SET = "java.util.Set<java.lang.String>";
    /**
     * CharSequence
     */
    public static final String CHAR_SEQUENCE = CharSequence.class.getCanonicalName();
    /**
     * SQLException
     */
    public static final String SQL_EXCEPTION = SQLException.class.getCanonicalName();
    /**
     * Integer
     */
    public static final String INTEGER = Integer.class.getCanonicalName();
    /**
     * Boolean
     */
    public static final String BOOLEAN = Boolean.class.getCanonicalName();
    /**
     * Float
     */
    public static final String FLOAT = Float.class.getCanonicalName();
    /**
     * Long
     */
    public static final String LONG = Long.class.getCanonicalName();
    /**
     * ArrayList
     */
    public static final String ARRAYLIST = ArrayList.class.getCanonicalName();
    /**
     * Serializable
     */
    public static final String SERIALIZABLE = Serializable.class.getCanonicalName();
    /**
     * Byte
     */
    public static final String BYTE = Byte.class.getCanonicalName();
    /**
     * Short
     */
    public static final String SHORT = Short.class.getCanonicalName();
    /**
     * Character
     */
    public static final String CHAR = Character.class.getCanonicalName();
    /**
     * Double
     */
    public static final String DOUBLE = Double.class.getCanonicalName();

    // Ohos
    /**
     * HiLog
     */
    public static final String HI_LOG = "ohos.hiviewdfx.HiLog";
    /**
     * HiLogLabel
     */
    public static final String HI_LOG_LABEL = "ohos.hiviewdfx.HiLogLabel";
    /**
     * Timber
     */
    public static final String LOG = "timber.log.Timber";
    /**
     * Sequenceable
     */
    public static final String SEQUENCEABLE = "ohos.utils.Sequenceable";
    /**
     * Intent
     */
    public static final String INTENT = "ohos.aafwk.content.Intent";
    /**
     * IntentParams
     */
    public static final String INTENT_PARAMS = "ohos.aafwk.content.IntentParams";
    /**
     * IntentFilter
     */
    public static final String INTENT_FILTER = "ohos.content.IntentFilter";
    /**
     * ComponentName
     */
    public static final String COMPONENT_NAME = "ohos.content.ComponentName";
    /**
     * PacMap
     */
    public static final String PACMAP = "ohos.utils.PacMap";
    /**
     * IBinder
     */
    public static final String IBINDER = "ohos.os.IBinder";
    /**
     * SparseArray
     */
    public static final String SPARSE_ARRAY = "ohos.util.SparseArray";
    /**
     * SparseBooleanArray
     */
    public static final String SPARSE_BOOLEAN_ARRAY = "ohos.util.SparseBooleanArray";
    /**
     * Application
     */
    public static final String APPLICATION = "ohos.app.Application";
    /**
     * Ability
     */
    public static final String ABILITY = "ohos.aafwk.ability.Ability";
    /**
     * Editable
     */
    public static final String EDITABLE = "ohos.text.Editable";
    /**
     * TextObserver
     */
    public static final String TEXT_OBSERVER = "ohos.agp.components.Text.TextObserver";
    /**
     * Slider
     */
    public static final String SLIDER = "ohos.agp.components.Slider";
    /**
     * ValueChangedListener
     */
    public static final String ON_SLIDER_CHANGE_LISTENER = "ohos.agp.components.Slider.ValueChangedListener";
    /**
     * Text
     */
    public static final String TEXT_VIEW = "ohos.agp.components.Text";
    /**
     * EditorActionListener
     */
    public static final String TEXT_EDITOR_ACTION_LISTENER = "ohos.agp.components.Text.EditorActionListener";
    /**
     * AbsButton
     */
    public static final String ABS_BUTTON = "ohos.agp.components.AbsButton";
    /**
     * CheckedStateChangedListener
     */
    public static final String ABS_BUTTON_ON_CHECKED_STATE_CHANGED_LISTENER
        = "ohos.agp.components.AbsButton.CheckedStateChangedListener";
    /**
     * Component
     */
    public static final String COMPONENT = "ohos.agp.components.Component";
    /**
     * ClickedListener
     */
    public static final String COMPONENT_CLICKED_LISTENER = "ohos.agp.components.Component.ClickedListener";
    /**
     * TouchEventListener
     */
    public static final String COMPONENT_ON_TOUCH_LISTENER = " ohos.agp.components.Component.TouchEventListener";
    /**
     * LongClickedListener
     */
    public static final String COMPONENT_LONG_CLICKED_LISTENER = "ohos.agp.components.Component.LongClickedListener";
    /**
     * FocusChangedListener
     */
    public static final String COMPONENT_FOCUS_CHANGED_LISTENER = "ohos.agp.components.Component.FocusChangedListener";
    /**
     * LayoutConfig
     */
    public static final String VIEW_GROUP_LAYOUT_PARAMS = "ohos.agp.components.ComponentContainer.LayoutConfig";
    /**
     * ComponentContainer
     */
    public static final String COMPONENT_CONTAINER = "ohos.agp.components.ComponentContainer";
    /**
     * Context
     */
    public static final String CONTEXT = "ohos.app.Context";
    /**
     * Context
     */
    public static final String KEY_EVENT = "ohos.multimodalinput.event.KeyEvent";
    /**
     * Callback
     */
    public static final String KEY_EVENT_CALLBACK = "ohos.view.KeyEvent.Callback";
    /**
     * LayoutScatter
     */
    public static final String LAYOUT_SCATTER = "ohos.agp.components.LayoutScatter";
    /**
     * Ability
     */
    public static final String FRACTION_ABILITY = "ohos.support.v4.app.Ability";
    /**
     * Fraction
     */
    public static final String FRACTION = "ohos.aafwk.ability.fraction.Fraction";
    /**
     * Fragment
     */
    public static final String SUPPORT_V4_FRACTION = "ohos.support.v4.app.Fragment";
    /**
     * Html
     */
    public static final String HTML = "ohos.text.Html";
    /**
     * LayoutConfig
     */
    public static final String WINDOW_MANAGER_LAYOUT_CONFIG = "ohos.agp.window.service.WindowManager.LayoutConfig";
    /**
     * AdapterView
     */
    public static final String ADAPTER_VIEW = "ohos.widget.AdapterView";
    /**
     * ListContainer
     */
    public static final String LIST_CONTAINER = "ohos.agp.components.ListContainer";
    /**
     * ItemClickedListener
     */
    public static final String ITEM_CLICKED_LISTENER = "ohos.agp.components.ListContainer.ItemClickedListener";
    /**
     * ItemLongClickedListener
     */
    public static final String ITEM_LONG_CLICKED_LISTENER = "ohos.agp.components.ListContainer.ItemLongClickedListener";
    /**
     * ItemSelectedListener
     */
    public static final String ITEM_SELECTED_LISTENER = "ohos.agp.components.ListContainer.ItemSelectedListener";
    /**
     * Window
     */
    public static final String WINDOW = "ohos.view.Window";
    /**
     * MenuItem
     */
    public static final String MENU_ITEM = "ohos.view.MenuItem";
    /**
     * MenuInflater
     */
    public static final String MENU_INFLATER = "ohos.view.MenuInflater";
    /**
     * Menu
     */
    public static final String MENU = "ohos.view.Menu";
    /**
     * Animation
     */
    public static final String ANIMATION = "ohos.view.animation.Animation";
    /**
     * AnimationUtils
     */
    public static final String ANIMATION_UTILS = "ohos.view.animation.AnimationUtils"; // 暂时没有找到对应
    /**
     * ResourceManager
     */
    public static final String RESOURCES = "ohos.global.resource.ResourceManager";
    /**
     * configuration
     */
    public static final String CONFIGURATION = "ohos.global.configuration";
    /**
     * TouchEvent
     */
    public static final String TOUCH_EVENT = "ohos.multimodalinput.event.TouchEvent";
    /**
     * Handler
     */
    public static final String HANDLER = "ohos.os.Handler";
    /**
     * Ability
     */
    public static final String SERVICE = "ohos.aafwk.ability.Ability";
    /**
     * IntentAbility
     */
    public static final String INTENT_SERVICE = "ohos.aafwk.ability.IntentAbility";
    /**
     * CommonEventSubscriber
     */
    public static final String BROADCAST_RECEIVER = "ohos.event.commonevent.CommonEventSubscriber";
    /**
     * CommonEventData
     */
    public static final String COMMON_EVENT_DATA = "ohos.event.commonevent.CommonEventData";
    /**
     * LocalBroadcastManager
     */
    public static final String LOCAL_BROADCAST_MANAGER = "ohos.support.v4.content.LocalBroadcastManager";
    /**
     * Ability
     */
    public static final String CONTENT_PROVIDER = "ohos.aafwk.ability.Ability";
    /**
     * RdbStore
     */
    public static final String SQLITE_DATABASE = "ohos.data.rdb.RdbStore";
    /**
     * KeyStore
     */
    public static final String KEY_STORE = "java.security.KeyStore";
    /**
     * SQLiteOpenHelper
     */
    public static final String SQLITE_OPEN_HELPER = "ohos.database.sqlite.SQLiteOpenHelper";
    /**
     * ViewServer
     */
    public static final String VIEW_SERVER = "org.ohosannotations.api.ViewServer";
    /**
     * Looper
     */
    public static final String LOOPER = "ohos.os.Looper";
    /**
     * PowerManager
     */
    public static final String POWER_MANAGER = "ohos.powermanager.PowerManager";
    /**
     * RunningLockType
     */
    public static final String POWER_RUNNING_LOCK_TYPE = "ohos.powermanager.PowerManager.RunningLockType";
    /**
     * PowerState
     */
    public static final String POWER_STATE = "ohos.powermanager.PowerManager.PowerState";
    /**
     * RunningLock
     */
    public static final String RUNNING_LOCK = "ohos.powermanager.PowerManager.RunningLock";
    /**
     * SystemVersion
     */
    public static final String BUILD_VERSION = "ohos.system.version.SystemVersion";
    /**
     * null
     */
    public static final String BUILD_VERSION_CODES = "";
    /**
     * null
     */
    public static final String PREFERENCE_ABILITY = "";
    /**
     * null
     */
    public static final String PREFERENCE_FRACTION = "";
    /**
     * null
     */
    public static final String SUPPORT_V4_PREFERENCE_FRACTION = "";
    /**
     * null
     */
    public static final String SUPPORT_V7_PREFERENCE_FRACTIONCOMPAT = "";
    /**
     * null
     */
    public static final String SUPPORT_V14_PREFERENCE_FRACTION = "";
    /**
     * PreferenceFragment
     */
    public static final String MACHINARIUS_V4_PREFERENCE_FRACTION
        = "com.github.machinarius.preferencefragment.PreferenceFragment";
    /**
     * null
     */
    public static final String ABILITY_COMPAT = "";
    /**
     * null
     */
    public static final String CONTEXT_COMPAT = "";
    /**
     * DatabaseHelper
     */
    public static final String DATABASEHELPER = "ohos.data.DatabaseHelper";
    /**
     * Preferences
     */
    public static final String PREFERENCE = "ohos.data.preferences.Preferences";
    /**
     * Preferences
     */
    public static final String SUPPORT_V7_PREFERENCE = "ohos.data.preferences.Preferences";
    /**
     * OnPreferenceChangeListener
     */
    public static final String PREFERENCE_CHANGE_LISTENER = "ohos.preference.Preference.OnPreferenceChangeListener";
    /**
     * OnPreferenceChangeListener
     */
    public static final String SUPPORT_V7_PREFERENCE_CHANGE_LISTENER
        = "ohos.support.v7.preference.Preference.OnPreferenceChangeListener";
    /**
     * OnPreferenceClickListener
     */
    public static final String PREFERENCE_CLICK_LISTENER = "ohos.preference.Preference.OnPreferenceClickListener";
    /**
     * OnPreferenceClickListener
     */
    public static final String SUPPORT_V7_PREFERENCE_CLICK_LISTENER
        = "ohos.support.v7.preference.Preference.OnPreferenceClickListener";
    /**
     * Header
     */
    public static final String PREFERENCE_ABILITY_HEADER = "ohos.preference.PreferenceAbility.Header";
    /**
     * AppWidgetManager
     */
    public static final String APP_WIDGET_MANAGER = "ohos.appwidget.AppWidgetManager";
    /**
     * WifiDevice
     */
    public static final String WIFI_MANAGER = "ohos.wifi.WifiDevice";
    /**
     * AudioManager
     */
    public static final String AUDIO_MANAGER = "ohos.media.AudioManager";
    /**
     * BluetoothHost
     */
    public static final String BLUETOOTH_HOST = "ohos.bluetooth.BluetoothHost";
    /**
     * USBCore
     */
    public static final String USB_CORE = "ohos.usb.USBCore";
    /**
     * IAbilityManager
     */
    public static final String ABILITY_MANAGER = "ohos.app.IAbilityManager";
    /**
     * NotificationHelper
     */
    public static final String NOTIFICATION_HELPER = "ohos.event.notification.NotificationHelper";
    /**
     * WindowManager
     */
    public static final String WINDOW_MANAGER = "ohos.agp.window.service.WindowManager";
    /**
     * ActionBarAbility
     */
    public static final String ACTIONBAR_ABILITY = "ohos.support.v7.app.ActionBarAbility";
    /**
     * AppCompatAbility
     */
    public static final String APPCOMPAT_ABILITY = "ohos.support.v7.app.AppCompatAbility";
    /**
     * PageSlider
     */
    public static final String VIEW_PAGER = "ohos.agp.components.PageSlider";
    /**
     * PageChangedListener
     */
    public static final String PAGE_CHANGE_LISTENER = "ohos.agp.components.PageSlider.PageChangedListener";
    /**
     * Ohos X
     */
    public static final String OHOSX_ABILITY_COMPAT = "ohos.core.app.AbilityCompat";
    /**
     * Fraction
     */
    public static final String OHOSX_FRACTION = "ohos.aafwk.ability.fraction.Fraction";
    /**
     * FragmentAbility
     */
    public static final String OHOSX_FRAGMENT_ABILITY = "ohos.fragment.app.FragmentAbility";
    /**
     * LocalBroadcastManager
     */
    public static final String OHOSX_LOCAL_BROADCAST_MANAGER
        = "ohos.localbroadcastmanager.content.LocalBroadcastManager";
    /**
     * ContextCompat
     */
    public static final String OHOSX_CONTEXT_COMPAT = "ohos.core.content.ContextCompat";
    /**
     * Preference
     */
    public static final String OHOSX_PREFERENCE = "ohos.preference.Preference";
    /**
     * PreferenceFragment
     */
    public static final String OHOSX_PREFERENCE_FRAGMENT = "ohos.preference.PreferenceFragment";
    /**
     * PreferenceFragmentCompat
     */
    public static final String OHOSX_PREFERENCE_FRAGMENTCOMPAT = "ohos.preference.PreferenceFragmentCompat";
    /**
     * OnPreferenceClickListener
     */
    public static final String OHOSX_PREFERENCE_CLICK_LISTENER = "ohos.preference.Preference.OnPreferenceClickListener";
    /**
     * OnPreferenceChangeListener
     */
    public static final String OHOSX_PREFERENCE_CHANGE_LISTENER
        = "ohos.preference.Preference.OnPreferenceChangeListener";
    /**
     * AppCompatAbility
     */
    public static final String OHOSX_APPCOMPAT_ABILITY = "ohos.appcompat.app.AppCompatAbility";
    /**
     * ViewPager
     */
    public static final String OHOSX_VIEW_PAGER = "ohos.viewpager.widget.ViewPager";
    /**
     * OnPageChangeListener
     */
    public static final String OHOSX_PAGE_CHANGE_LISTENER = "ohos.viewpager.widget.ViewPager.OnPageChangeListener";
    /**
     * DataBindingUtil
     */
    public static final String OHOSX_DATA_BINDING_UTIL = "ohos.databinding.DataBindingUtil";
    /**
     * ViewDataBinding
     */
    public static final String OHOSX_VIEW_DATA_BINDING = "ohos.databinding.ViewDataBinding";
    /**
     * Ohos permission
     */
    public static final String INTERNET_PERMISSION = "ohos.permission.INTERNET";
    /**
     * RUNNING_LOCK
     */
    public static final String RUNNING_LOCK_PERMISSION = "ohos.permission.RUNNING_LOCK";
    /**
     * ClientConnectionManager
     */
    public static final String CLIENT_CONNECTION_MANAGER = "org.apache.http.conn.ClientConnectionManager";
    /**
     * DefaultHttpClient
     */
    public static final String DEFAULT_HTTP_CLIENT = "org.apache.http.impl.client.DefaultHttpClient";
    /**
     * SSLSocketFactory
     */
    public static final String SSL_SOCKET_FACTORY = "org.apache.http.conn.ssl.SSLSocketFactory";
    /**
     * PlainSocketFactory
     */
    public static final String PLAIN_SOCKET_FACTORY = "org.apache.http.conn.scheme.PlainSocketFactory";
    /**
     * Scheme
     */
    public static final String SCHEME = "org.apache.http.conn.scheme.Scheme";
    /**
     * SchemeRegistry
     */
    public static final String SCHEME_REGISTRY = "org.apache.http.conn.scheme.SchemeRegistry";
    /**
     * SingleClientConnManager
     */
    public static final String SINGLE_CLIENT_CONN_MANAGER = "org.apache.http.impl.conn.SingleClientConnManager";

    /**
     * Parcel
     */
    public static final String PARCEL_ANNOTATION = "org.parceler.Parcel";
    /**
     * Parcels
     */
    public static final String PARCELS_UTILITY_CLASS = "org.parceler.Parcels";
    /**
     * DataBindingUtil
     */
    public static final String DATA_BINDING_UTIL = "ohos.databinding.DataBindingUtil";
    /**
     * ViewDataBinding
     */
    public static final String VIEW_DATA_BINDING = "ohos.databinding.ViewDataBinding";

    /**
     * HttpClient
     */
    private CanonicalNameConstants() {
    }
}
