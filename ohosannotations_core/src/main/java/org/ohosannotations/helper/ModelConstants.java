/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the ohosannotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.helper;

import static java.util.Arrays.asList;

import java.lang.annotation.Annotation;
import java.util.List;

import javax.lang.model.SourceVersion;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.Option;
import org.ohosannotations.annotations.EAbility;
import org.ohosannotations.annotations.EApplication;
import org.ohosannotations.annotations.EBean;
import org.ohosannotations.annotations.EFraction;
import org.ohosannotations.annotations.EIntentService;
import org.ohosannotations.annotations.EProvider;
import org.ohosannotations.annotations.EReceiver;
import org.ohosannotations.annotations.EService;
import org.ohosannotations.annotations.EView;
import org.ohosannotations.annotations.EViewGroup;

/**
 * 模型常数
 *
 * @author dev
 * @since 2021-07-22
 */
public abstract class ModelConstants {
    /**
     * 选择类后缀
     */
    public static final Option OPTION_CLASS_SUFFIX = new Option("classSuffix", "_");

    /**
     * 有效增强视图支持注释
     */
    public static final List<Class<? extends Annotation>> VALID_ENHANCED_VIEW_SUPPORT_ANNOTATIONS
        = asList(EAbility.class, EViewGroup.class, EView.class, EBean.class, EFraction.class);

    /**
     * 有效增强组件的注释
     */
    public static final List<Class<? extends Annotation>> VALID_ENHANCED_COMPONENT_ANNOTATIONS
        = asList(EApplication.class, EAbility.class, EViewGroup.class, EView.class, EBean.class, EService.class,
        EIntentService.class, EReceiver.class, EProvider.class, EFraction.class);

    private static String generationSuffix = "_";
    private static String classSuffix;

    /**
     * 模型常数
     */
    private ModelConstants() {
    }

    /**
     * 初始化
     *
     * @param environment 环境
     */
    public static void init(OhosAnnotationsEnvironment environment) {
        classSuffix = environment.getOptionValue(OPTION_CLASS_SUFFIX).trim();

        if (classSuffix.isEmpty()) {
            throw new IllegalArgumentException("'" + classSuffix + "' may not be an empty string.");
        }

        if (!SourceVersion.isName("ValidName" + classSuffix) || classSuffix.contains(".")) {
            throw new IllegalArgumentException("'" + classSuffix + "' may not be a valid Java identifier.");
        }
    }

    /**
     * 类后缀
     *
     * @return {@link String}
     */
    public static String classSuffix() {
        return classSuffix;
    }

    /**
     * 一代后缀
     * generationSuffix
     *
     * @return generationSuffix
     */
    public static String generationSuffix() {
        return generationSuffix;
    }
}
