/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Case 助手
 *
 * @since 2021-07-20
 */
public final class CaseHelper {
    private static final Pattern PATTERN = Pattern.compile("([A-Z]|[a-z])[a-z0-9]*");
    private static final int CONSTANT_2 = 2;
    private static final String DIVIDER = "_";

    private CaseHelper() {
    }

    /**
     * 驼峰规范
     *
     * @param camelCase 驼峰写法
     * @return String 字符串
     */
    public static String camelCaseToSnakeCase(String camelCase) {
        List<String> tokens = new ArrayList<>();
        Matcher matcher = PATTERN.matcher(camelCase);
        String acronym = "";
        while (matcher.find()) {
            String found = matcher.group();
            if (found.matches("^[A-Z]$")) {
                acronym += found;
            } else {
                if (acronym.length() > 0) {
                    // we have an acronym to add before we continue
                    tokens.add(acronym);
                    acronym = "";
                }
                tokens.add(found.toLowerCase(Locale.ENGLISH));
            }
        }
        if (acronym.length() > 0) {
            tokens.add(acronym);
        }
        if (tokens.size() > 0) {
            StringBuilder sb = new StringBuilder(tokens.remove(0));
            for (String ss : tokens) {
                sb.append(DIVIDER).append(ss);
            }
            return sb.toString();
        } else {
            return camelCase;
        }
    }

    /**
     * 驼峰规范
     *
     * @param camelCase 驼峰写法
     * @return String 字符串
     */
    public static String camelCaseToUpperSnakeCase(String camelCase) {
        return camelCaseToSnakeCase(camelCase).toUpperCase(Locale.ENGLISH);
    }

    /**
     * camelCaseToUpperSnakeCase
     *
     * @param prefix prefix
     * @param camelCase camelCase
     * @param suffix suffix
     * @return camelCaseToUpperSnakeCase
     */
    public static String camelCaseToUpperSnakeCase(String prefix, String camelCase, String suffix) {
        String tempCamelCase = camelCase;
        if (prefix != null && !tempCamelCase.startsWith(prefix)) {
            tempCamelCase = prefix + DIVIDER + tempCamelCase;
        }
        if (suffix != null && !tempCamelCase.toLowerCase(Locale.ENGLISH).endsWith(suffix.toLowerCase(Locale.ENGLISH))) {
            tempCamelCase = tempCamelCase + DIVIDER + suffix;
        }
        return camelCaseToUpperSnakeCase(tempCamelCase);
    }

    /**
     * 小写的姓
     *
     * @param string 姓氏
     * @return String 字符串
     */
    public static String lowerCaseFirst(String string) {
        if (string.length() < CONSTANT_2) {
            return string.toLowerCase(Locale.ENGLISH);
        }
        String first = string.substring(0, 1).toLowerCase(Locale.ENGLISH);
        String end = string.substring(1, string.length());
        return first + end;
    }

    /**
     * upperCaseFirst
     *
     * @param string string
     * @return first
     */
    public static String upperCaseFirst(String string) {
        if (string.length() < CONSTANT_2) {
            return string.toUpperCase(Locale.ENGLISH);
        }
        String first = string.substring(0, 1).toUpperCase(Locale.ENGLISH);
        String end = string.substring(1, string.length());
        return first + end;
    }
}
