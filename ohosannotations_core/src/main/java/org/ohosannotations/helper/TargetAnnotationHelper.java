/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.helper;

import org.ohosannotations.OhosAnnotationsEnvironment;

import java.lang.annotation.Annotation;

import javax.lang.model.element.Element;
import javax.lang.model.type.DeclaredType;

/**
 * TargetAnnotationHelper
 *
 * @since 2021-07-20
 */
public class TargetAnnotationHelper extends AnnotationHelper {
    private String annotationName;

    /**
     * 构造参数
     *
     * @param environment 注解环境
     * @param annotationName 注解名字
     */
    public TargetAnnotationHelper(OhosAnnotationsEnvironment environment, String annotationName) {
        super(environment);
        this.annotationName = annotationName;
    }

    /**
     * extractAnnotationValueParameter
     *
     * @param element 元素
     * @param <T> 泛型
     * @return T 泛型
     */
    public <T> T extractAnnotationValueParameter(Element element) {
        return (T) extractAnnotationParameter(element, "value");
    }

    /**
     * extractAnnotationParameter
     *
     * @param element 元素
     * @param methodName 方法名
     * @param <T> 泛型
     * @return T 泛型
     */
    public <T> T extractAnnotationParameter(Element element, String methodName) {
        return (T) extractAnnotationParameter(element, annotationName, methodName);
    }

    /**
     * extractAnnotationClassParameter
     *
     * @param element 元素
     * @return DeclaredType 声明类型
     */
    public DeclaredType extractAnnotationClassParameter(Element element) {
        return extractAnnotationClassParameter(element, annotationName);
    }

    /**
     * getTarget
     *
     * @return String 字符串
     */
    public String getTarget() {
        return annotationName;
    }

    /**
     * actionName
     *
     * @return String字符串
     */
    public String actionName() {
        return actionName(annotationName);
    }

    /**
     * annotationName
     *
     * @param annotationName 注解名字
     * @return String 字符串
     */
    public static String annotationName(String annotationName) {
        return "@" + annotationName;
    }

    /**
     * annotationName
     *
     * @param annotation 类注解
     * @return String 字符串
     */
    public static String annotationName(Class<? extends Annotation> annotation) {
        return annotationName(annotation.getName());
    }

    /**
     * annotationName
     *
     * @return String 字符串
     */
    public String annotationName() {
        return annotationName(annotationName);
    }
}
