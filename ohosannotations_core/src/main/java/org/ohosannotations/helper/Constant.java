/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.helper;

/**
 * 常量类
 *
 * @since 2021-06-18
 */
public class Constant {
    /**
     * 包装类整数字符串
     */
    public static final String INTEGER = "java.lang.Integer";

    /**
     * 整数字符串
     */
    public static final String INT = "int";

    /**
     * 获取Element字符串
     */
    public static final String GET_ELEMENT = "getElement";

    /**
     * 上下文字符串
     */
    public static final String CONTEXT = "context";

    /**
     * 意图字符串
     */
    public static final String INTENT_STRING = "intent";

    /**
     * 启动AbilityForResult字符串
     */
    public static final String START_ABILITY_FOR_RESULT = "startAbilityForResult";

    /**
     * 启动Ability字符串
     */
    public static final String START_ABILITY = "startAbility";

    /**
     * Fraction字符串
     */
    public static final String FRACTION = "fraction";

    private Constant() {
    }
}
