/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.helper;

import com.helger.jcodemodel.JFieldRef;

import org.ohosannotations.OhosAnnotationsEnvironment;
import org.ohosannotations.rclass.IRClass;
import org.ohosannotations.rclass.IRClass.Res;

import java.util.List;

import javax.lang.model.element.Element;

/**
 * id注释助手
 * IdAnnotationHelper
 *
 * @author dev
 * @date 2021/07/26
 * @since 2021-07-20
 */
public class IdAnnotationHelper extends TargetAnnotationHelper {
    /**
     * id注释助手
     * 构造参数
     *
     * @param environment 注解环境
     * @param annotationName 注解名字
     */
    public IdAnnotationHelper(OhosAnnotationsEnvironment environment, String annotationName) {
        super(environment, annotationName);
    }

    /**
     * 得到rclass
     *
     * @return {@link IRClass}
     */
    private IRClass getRClass() {
        return getEnvironment().getRClass();
    }

    /**
     * 包含id值
     * containsIdValue
     *
     * @param idValue 值
     * @return boolean布尔值
     */
    public boolean containsIdValue(Integer idValue) {
        IRClass rClass = getRClass();
        return rClass.containsIdValue(idValue);
    }

    /**
     * 包含字段
     * containsField
     *
     * @param name 名字
     * @return boolean布尔值
     */
    public boolean containsField(String name) {
        IRClass rClass = getRClass();
        return rClass.containsField(name);
    }

    /**
     * 提取注释资源
     * extractAnnotationResources
     *
     * @param element 元素
     * @param res 资源
     * @param useElementName 使用元素名字
     * @return List 集合
     */
    public List<String> extractAnnotationResources(Element element, Res res, boolean useElementName) {
        return super.extractAnnotationResources(element, getTarget(), getRClass(), res, useElementName);
    }

    /**
     * 参提取注释字段
     * extractAnnotationFieldRefs
     *
     * @param element 元素
     * @param res 资源
     * @param useElementName 使用元素名字
     * @return List 集合
     */
    public List<JFieldRef> extractAnnotationFieldRefs(Element element, Res res, boolean useElementName) {
        return extractAnnotationFieldRefs(element, res,
            useElementName, DEFAULT_FIELD_NAME_VALUE, DEFAULT_FIELD_NAME_RESNAME);
    }

    /**
     * 参提取注释字段
     * extractAnnotationFieldRefs
     *
     * @param element 元素
     * @param res 资源
     * @param useElementName 使用元素名字
     * @param idFieldName id字段名
     * @param resFieldName 资源字段名字
     * @return List 集合
     */
    public List<JFieldRef> extractAnnotationFieldRefs(Element element,
        Res res, boolean useElementName, String idFieldName, String resFieldName) {
        return super.extractAnnotationFieldRefs(element, getTarget(),
            getRClass(), res, useElementName, idFieldName, resFieldName);
    }

    /**
     * 提取一个注释字段ref
     * extractOneAnnotationFieldRef
     *
     * @param element 元素
     * @param res 资源
     * @param useElementName 使用元素名字
     * @return JFieldRef 字段
     */
    public JFieldRef extractOneAnnotationFieldRef(Element element, Res res, boolean useElementName) {
        return extractOneAnnotationFieldRef(element, getTarget(), res, useElementName);
    }

    /**
     * 提取一个注释字段ref
     * extractOneAnnotationFieldRef
     *
     * @param element 元素
     * @param annotationName 注解名字
     * @param res 资源
     * @param useElementName 使用元素名字
     * @return JFieldRef 字段
     */
    public JFieldRef extractOneAnnotationFieldRef(Element element,
        String annotationName, Res res, boolean useElementName) {
        return extractOneAnnotationFieldRef(element, annotationName, res,
            useElementName, DEFAULT_FIELD_NAME_VALUE, DEFAULT_FIELD_NAME_RESNAME);
    }

    /**
     * 提取一个注释字段ref
     * extractOneAnnotationFieldRef
     *
     * @param element 元素
     * @param annotationName 注解名字
     * @param res 资源
     * @param useElementName 使用元素名字
     * @param idFieldName 使用元素名称
     * @param resFieldName res字段名
     * @return extractOneAnnotationFieldRef
     */
    public JFieldRef extractOneAnnotationFieldRef(Element element,
        String annotationName, Res res, boolean useElementName, String idFieldName, String resFieldName) {
        return extractOneAnnotationFieldRef(element, annotationName,
            getRClass(), res, useElementName, idFieldName, resFieldName);
    }

    /**
     * 提取一个注释字段ref
     * extractOneAnnotationFieldRef
     *
     * @param element 元素
     * @param annotationName 注释的名字
     * @param rClass r类
     * @param res res
     * @param useElementName 使用元素名称
     * @param idFieldName id字段名称
     * @param resFieldName res字段名
     * @return null
     */
    public JFieldRef extractOneAnnotationFieldRef(Element element, String annotationName,
        IRClass rClass, Res res, boolean useElementName, String idFieldName, String resFieldName) {
        List<JFieldRef> jFieldRefs = extractAnnotationFieldRefs(element,
            annotationName, rClass, res, useElementName, idFieldName, resFieldName);
        if (jFieldRefs.size() == 1) {
            return jFieldRefs.get(0);
        } else {
            return null;
        }
    }
}
