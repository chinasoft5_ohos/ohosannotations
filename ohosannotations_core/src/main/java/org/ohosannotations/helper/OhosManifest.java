/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Ohos清单
 *
 * @since 2021-07-20
 */
public final class OhosManifest {
    private final String applicationPackage;
    private final List<String> componentQualifiedNames;
    private final Map<String, MetaDataInfo> metaDataQualifiedNames;
    private final List<String> permissionQualifiedNames;
    private final String applicationClassName;
    private final boolean libraryProject;
    private final boolean debugabble;
    private final int minSdkVersion;
    private final int maxSdkVersion;
    private final int targetSdkVersion;

    @Override
    public String toString() {
        return "OhosManifest [applicationPackage=" + applicationPackage
            + ", componentQualifiedNames=" + componentQualifiedNames
            + ", metaDataQualifiedNames=" + metaDataQualifiedNames
            + ", permissionQualifiedNames=" + permissionQualifiedNames
            + ", applicationClassName=" + applicationClassName + ", libraryProject="
            + libraryProject + ", debugabble=" + debugabble
            + ", minSdkVersion=" + minSdkVersion + ", maxSdkVersion=" + maxSdkVersion
            + ", targetSdkVersion=" + targetSdkVersion + "]";
    }

    /**
     * 创建清单
     *
     * @param applicationPackage 程序包
     * @param applicationClassName 程序类名
     * @param componentQualifiedNames 组件限定名字
     * @param metaDataQualifiedNames 元数据限定名字
     * @param permissionQualifiedNames 权限限定名字
     * @param minSdkVersion 最小SDK版本号
     * @param maxSdkVersion 最大SDK版本号
     * @param targetSdkVersion 目前SDK版本号
     * @param debugabble 调试
     * @return OhosManifest 清单
     */
    public static OhosManifest createManifest(String applicationPackage,
        String applicationClassName, List<String> componentQualifiedNames,
        Map<String, MetaDataInfo> metaDataQualifiedNames,
        List<String> permissionQualifiedNames, int minSdkVersion,
        int maxSdkVersion, int targetSdkVersion, boolean debugabble) {
        return new OhosManifest(false, applicationPackage, applicationClassName,
            componentQualifiedNames, metaDataQualifiedNames, permissionQualifiedNames, minSdkVersion, maxSdkVersion,
            targetSdkVersion, debugabble);
    }

    /**
     * 创建库的清单
     *
     * @param applicationPackage 程序包
     * @param minSdkVersion 最小SDK版本
     * @param maxSdkVersion 最大SDK版本
     * @param targetSdkVersion 目前SDK版本
     * @return OhosManifest 清单
     */
    public static OhosManifest createLibraryManifest(String applicationPackage,
        int minSdkVersion, int maxSdkVersion, int targetSdkVersion) {
        return new OhosManifest(true, applicationPackage, "",
            Collections.<String>emptyList(), new HashMap<String, MetaDataInfo>(),
            Collections.<String>emptyList(), minSdkVersion, maxSdkVersion,
            targetSdkVersion, false);
    }

    /**
     * 构造参数
     *
     * @param libraryProject 是否库的项目
     * @param applicationPackage 程序包名
     * @param applicationClassName 程序类名
     * @param componentQualifiedNames 组件限定名字
     * @param metaDataQualifiedNames 元数据限定名字
     * @param permissionQualifiedNames 权限限定名字
     * @param minSdkVersion 最小SDK版本
     * @param maxSdkVersion 最大SDK版本
     * @param targetSdkVersion 目前SDK版本
     * @param debuggable 调试
     */
    private OhosManifest(boolean libraryProject, String applicationPackage, String applicationClassName,
        List<String> componentQualifiedNames, Map<String, MetaDataInfo> metaDataQualifiedNames,
        List<String> permissionQualifiedNames,
        int minSdkVersion, int maxSdkVersion, int targetSdkVersion, boolean debuggable) {
        this.libraryProject = libraryProject;
        this.applicationPackage = applicationPackage;
        this.applicationClassName = applicationClassName;
        this.componentQualifiedNames = componentQualifiedNames;
        this.metaDataQualifiedNames = metaDataQualifiedNames;
        this.permissionQualifiedNames = permissionQualifiedNames;
        this.minSdkVersion = minSdkVersion;
        this.maxSdkVersion = maxSdkVersion;
        this.targetSdkVersion = targetSdkVersion;
        this.debugabble = debuggable;
    }

    /**
     * 获取应用程序包
     *
     * @return String 字符串
     */
    public String getApplicationPackage() {
        return applicationPackage;
    }

    /**
     * 组件限定名字
     *
     * @return List 集合
     */
    public List<String> getComponentQualifiedNames() {
        return Collections.unmodifiableList(componentQualifiedNames);
    }

    /**
     * 获取元数据限定名字
     *
     * @return Map 集合
     */
    public Map<String, MetaDataInfo> getMetaDataQualifiedNames() {
        return Collections.unmodifiableMap(metaDataQualifiedNames);
    }

    /**
     * 获取权限名字
     *
     * @return List 集合
     */
    public List<String> getPermissionQualifiedNames() {
        return Collections.unmodifiableList(permissionQualifiedNames);
    }

    /**
     * 获取程序类名
     *
     * @return String 字符串
     */
    public String getApplicationClassName() {
        return applicationClassName;
    }

    /**
     * 是否库项目
     *
     * @return boolean 布尔值
     */
    public boolean isLibraryProject() {
        return libraryProject;
    }

    /**
     * isDebuggable
     *
     * @return debugabble
     */
    public boolean isDebuggable() {
        return debugabble;
    }

    /**
     * getMinSdkVersion
     *
     * @return minSdkVersion
     */
    public int getMinSdkVersion() {
        return minSdkVersion;
    }

    /**
     * getMaxSdkVersion
     *
     * @return maxSdkVersion
     */
    public int getMaxSdkVersion() {
        return maxSdkVersion;
    }

    /**
     * getTargetSdkVersion
     *
     * @return targetSdkVersion
     */
    public int getTargetSdkVersion() {
        return targetSdkVersion;
    }

    /**
     * 元数据信息
     * MetaDataInfo
     *
     * @author dev
     * @since 2021-07-23
     */
    public static final class MetaDataInfo {
        private final String name;
        private final String value;
        private final String resource;

        /**
         * MetaDataInfo
         *
         * @param name name
         * @param value value
         * @param resource resource
         */
        public MetaDataInfo(String name, String value, String resource) {
            this.name = name;
            this.value = value;
            this.resource = resource;
        }

        /**
         * getName
         *
         * @return name
         */
        public String getName() {
            return name;
        }

        /**
         * getValue
         *
         * @return value
         */
        public String getValue() {
            return value;
        }

        /**
         * getResource
         *
         * @return resource
         */
        public String getResource() {
            return resource;
        }

        /**
         * toString
         *
         * @return name
         */
        @Override
        public String toString() {
            return "{" + "name='" + name + '\'' + ", value='" + value + '\'' + ", resource='" + resource + '\'' + '}';
        }
    }
}
