# ohosannotations

#### 项目介绍
- 项目名称：ohosannotations
- 所属系列：openharmony的第三方组件适配移植
- 功能：一款能快速开发Ohos的注解使用工具。让你专注于真正重要的事情，通过简化代码，可以简化代码的维护。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 4.8.0

#### 效果演示
![screenshot](screenshot/ohosannotations.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```groovy
// 添加maven仓库  
repositories {  
  maven {   
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'  
    }  
}
```
2.在entry模块的build.gradle文件中，
```groovy
// 添加依赖库  
dependencies {
    annotationProcessor "com.gitee.chinasoft_ohos:ohosannotations_core:1.0.0"
    implementation 'com.gitee.chinasoft_ohos:ohosannotations_core_api:1.0.0'
}

```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
你的Ohos代码是否易于编写、阅读和维护?
看看这个:
```java
@EAbility(ResourceTable.Layout_translate) // Sets content view to R.layout.translate
public class TranslateAbility extends Ability {

    @ComponentById // Injects ResourceTable.Id_textInput
    TextField textInput;

    @ComponentById(ResourceTable.Id_myTextView) // Injects ResourceTable.Id_myTextView
    Text result;

    @Click // When ResourceTable.Id_doTranslate button is clicked
    void doTranslate() {
         translateInBackground(textInput.getText().toString());
    }

    @Background // Executed in a background thread
    void translateInBackground(String text) {
         showResult(text);
    }

    @UiThread // Executed in the ui thread
    void showResult(String text) {
         result.setText(text);
    }

    // [...]
}
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
[Apache License, Version 2.0](LICENSE)

