/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.ormlite.helper;

/**
 * OrmLite类
 *
 * @since 2021-07-19
 */
public final class OrmLiteClasses {
    /**
     * 数据库dao类
     */
    public static final String DAO = "com.j256.ormlite.dao.Dao";
    /**
     * Helper管理者
     */
    public static final String OPEN_HELPER_MANAGER = "com.j256.ormlite.android.apptools.OpenHelperManager";
    /**
     * 运行时异常Dao
     */
    public static final String RUNTIME_EXCEPTION_DAO = "com.j256.ormlite.dao.RuntimeExceptionDao";

    private OrmLiteClasses() {
    }
}