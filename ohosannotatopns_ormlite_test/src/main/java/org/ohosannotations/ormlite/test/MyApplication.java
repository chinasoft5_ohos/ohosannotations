package org.ohosannotations.ormlite.test;

import ohos.aafwk.ability.AbilityPackage;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import org.ohosannotations.ormlite.annotations.OrmLiteDao;

/**
 * 简单测试程序入口
 *
 * @since 2021-07-19
 */
public class MyApplication extends AbilityPackage {
    @OrmLiteDao(helper = DatabaseHelper.class)
    UserDao userDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Car, Long> carDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    RuntimeExceptionDao<Car, Long> runtimeExceptionDao;

    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}