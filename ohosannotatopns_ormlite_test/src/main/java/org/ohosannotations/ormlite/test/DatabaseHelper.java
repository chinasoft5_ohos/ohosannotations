/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.ormlite.test;

import ohos.app.Context;
import ohos.data.rdb.RdbStore;

import com.j256.ormlite.hmos.apptools.OrmLiteRdbOpenHelper;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * 数据库辅助
 *
 * @author dev
 * @since 2021-06-17
 */
public class DatabaseHelper extends OrmLiteRdbOpenHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseHelper.class);
    private static final String DATABASE_NAME = "aaormlite.db";
    private static final int DATABASE_VERSION = 2;

    /**
     * 数据库辅助
     *
     * @param context 上下文
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(RdbStore rdbStore, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Car.class);
            TableUtils.createTable(connectionSource, User.class);
        } catch (SQLException error) {
            LOGGER.error(error.getLocalizedMessage());
        }
    }

    @Override
    public void onUpgrade(RdbStore rdbStore, ConnectionSource connectionSource, int ii, int i1) {
    }
}
