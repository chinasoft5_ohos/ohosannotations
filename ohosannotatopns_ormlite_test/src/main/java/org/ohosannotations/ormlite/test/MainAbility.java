package org.ohosannotations.ormlite.test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import org.ohosannotations.ormlite.test.slice.MainAbilitySlice;

/**
 * MainAbility
 *
 * @since 2021-07-19
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
            .withDeviceId("")
            .withBundleName("org.ohosannotations.ormlite.test")
            .withAbilityName("org.ohosannotations.ormlite.test.MyAbility_")
            .build();
        intent1.setOperation(operation); // 把operation设置到intent中
        startAbility(intent1);
    }
}
