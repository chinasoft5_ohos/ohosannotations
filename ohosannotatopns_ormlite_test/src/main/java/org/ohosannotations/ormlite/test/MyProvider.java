/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.ormlite.test;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import org.ohosannotations.ormlite.annotations.OrmLiteDao;

/**
 * @since 2021-06-17
 */
public class MyProvider extends Ability {
    @OrmLiteDao(helper = DatabaseHelper.class)
    UserDao userDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Car, Long> carDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    RuntimeExceptionDao<Car, Long> runtimeExceptionDao;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }


    @Override
    public int delete(Uri uri, DataAbilityPredicates predicates) {
        return super.delete(uri, predicates);
    }

    @Override
    public int update(Uri uri, ValuesBucket value, DataAbilityPredicates predicates) {
        return super.update(uri, value, predicates);
    }

    @Override
    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        return super.query(uri, columns, predicates);
    }

    @Override
    public int insert(Uri uri, ValuesBucket value) {
        return super.insert(uri, value);
    }
}