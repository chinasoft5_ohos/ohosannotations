/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ohosannotations.ormlite.test;

import ohos.event.commonevent.CommonEventData;
import ohos.event.commonevent.CommonEventSubscribeInfo;
import ohos.event.commonevent.CommonEventSubscriber;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import org.ohosannotations.annotations.EReceiver;
import org.ohosannotations.ormlite.annotations.OrmLiteDao;

/**
 * MyReceiver
 *
 * @author dev
 * @since 2021-06-17
 */
@EReceiver
public class MyReceiver extends CommonEventSubscriber {
    @OrmLiteDao(helper = DatabaseHelper.class)
    UserDao userDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Car, Long> carDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    RuntimeExceptionDao<Car, Long> runtimeExceptionDao;

    /**
     * 构造参数
     *
     * @param subscribeInfo 订阅信息
     */
    public MyReceiver(CommonEventSubscribeInfo subscribeInfo) {
        super(subscribeInfo);
    }

    @Override
    public void onReceiveEvent(CommonEventData commonEventData) {
    }
}