/**
 * Copyright (C) 2010-2016 eBusiness Information, Excilys Group
 * Copyright (C) 2016-2020 the AndroidAnnotations project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.ohosannotations.ormlite.test;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * 用户Bean
 *
 * @since 2021-07-20
 */
@DatabaseTable(daoClass = UserDaoImpl.class)
public class User {
    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField
    private String firstName;

    @DatabaseField
    private String lastName;

    /**
     * 构造参数
     */
    public User() {
    }

    /**
     * 构造参数
     *
     * @param id 用户ID
     */
    public User(long id) {
        this.id = id;
    }

    /**
     * 获取用户Id
     *
     * @return long 类型
     */
    public long getId() {
        return id;
    }

    /**
     * 设置用户Id
     *
     * @param id 用户id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * 获取姓
     *
     * @return String 字符串
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * 设置姓
     *
     * @param firstName 字符串姓
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * 获取名字
     *
     * @return String 字符串
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * 设置名字
     *
     * @param lastName 设置名字
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
